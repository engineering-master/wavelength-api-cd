<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <title>Review calls</title>
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
          <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" >
    <script src="{{ asset('js/bootstrap.bundle.min.js') }}" ></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css') }}"/>
    <script type="text/javascript" src="{{ asset('js/datatables.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/jquery.equalizer.css') }}">
    <script src="{{ asset('js/jquery.equalizer.js') }}" ></script>
    <script src="{{ asset('js/jquery.reverseorder.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" >
    <script>
      $(window).on('load', function(){
        $('.loadingWrap .loadingImg.home').css("display","none");
      });
      $(document).ready(function(){
        $('.switchBtn').click(function(){
          var inputWrap = $(this).children('input');
          if (inputWrap.is(':checked')) {
            inputWrap.attr('checked' , false);
            $(this).removeClass('IsChecked');
          }
          else
          {
            inputWrap.attr('checked' , true);
            $(this).addClass('IsChecked');
          }
        });

        /**************Back to Top****************/
        $(".backToTop").click(function () {
          $("html, body").animate({scrollTop: 0}, 1000);
        });
      });
    </script>
  </head>
  <body class="Cover_Bg loadingWrap">
    <div class="backToTop topBack" >
      <i class="fa fa-angle-up" aria-hidden="true"></i>
    </div>

    <!--<div class="loadingImg home">
      <span><img src="{{ asset('img/loading.png') }}"/></span>
    </div> -->
    <div id="header" class="header">
      <!-- begin container-fluid -->
      <div class="container-fluid">
        <div class="row">
          <div class="logoWrap col-4 col-sm-4">
            <a href="/dashboard" class="navbar-logo"> <img src="{{ asset('img/fullsizelogo.png') }}" /> </a>
          </div>
          <div class="logoWrap col-4 col-sm-4">
            <span class="agentAudioInfo"><b>Agent Name:</b> {{ $selectedcall->agent_id }}</span>
          </div>
          <!-- begin header navigation right -->
          <div class="col-4 col-sm-4">
            <div class="userInfoBox dropdown navbar-user">
              <span class="user-image">{{ $user->id }}</span>
              <span class="hidden-xs">{{ $user->name }}</span> 
              <a href="/auth/signout" class="signout"><i class="fa fa-sign-out" aria-hidden="true"></i></a><b class="caret"></b>
            </div>
          </div>
        </div>
        <!-- end header navigation right -->
      </div>
     <!-- end container-fluid -->
    </div>
 <!--Header CLosED -->
    <div class="container result-page">
    <!--  -->
      <a href="javascript:history.go(-1)"><div class="backBtn"><i class="fa fa-angle-left" aria-hidden="true"></i> Back</div></a>

        <div class="row">
        <div class="col-md-4">     
            <div class="container-fluid leftSideBar collapse show" id="leftSideBar">
                    <ul class="nav nav-tabs">
                      <li>
                        <a data-toggle="tab" href="#AgentCall" class="active">
                          <i class="fa fa-wifi" aria-hidden="true" style="transform: rotate(45deg);">
                          </i>Agent Calls</a>
                      </li>
                      @if(strtolower($selectedcall->client) != 'sprint')
                      <li>
                        <a data-toggle="tab" href="#CustomerCall"><i class="fa fa-user" aria-hidden="true"></i> Customer Calls</a>
                      </li>
                      @endif
                    </ul>
                  <div class="tab-content">
                    <div id="AgentCall" class="tab-pane fade in active show">
                      <div class="table-responsive">
                        <table class="table table-striped DisplayTable" id="resulttable">
                          <thead>
                          <tr>
                          <th>Date & Time</th>
                          <th>Number</th>
                          <th>Duration</th>
                          <th>Direction</th>
                          </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>

                      </div>
                    </div>
                    @if(strtolower($selectedcall->client) != 'sprint')
                    <div id="CustomerCall" class="tab-pane loadingWrap">
                    <!--  <div class="loadingImg cu_call"> 
                        <span><img src="{{asset('img/loading.png') }}" /></span>
                      </div> -->
                      <div class="table-responsive">
                        <table class="table table-striped DisplayTable1" id="customerresult">
                          <thead><tr><th>Date & Time</th><th>Number</th><th>Duration</th><th>Direction</th></tr></thead>
                          <tbody>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    @endif
                  </div>
                </div>
      </div>
      <!--Main Wrap-->
      <main role="main" class="col-md-8">

        <div class="row">
          <div class="col-md-12 searcBar">
           <div class="container-fluid expand_Chevron" id="btnCollapse" data-toggle="collapse" data-target="#leftSideBar"><i class="fa fa-angle-double-left"></i></div>
            <div class="equalizerBar">
              <div id="waveform"></div>
            </div>
            <div id="audio_info" class="audioInfoTable">
            </div>
            <div id="player">
              <div class="player__container" id="btn_Fixed">
                <div class="player__body">
                  <div class="body__cover">
                    <div class="progressBarRang">
                      <input type="range" step="any" id="seekbar"/>
                      <ul id="seekbarWrapper">
                        <li id="audioSeekbar" hidden=""></li>
                        <li id="audioBuffered" hidden=""></li>
                      </ul>
                      <div class="AudioClock">
                        <div id="time" >00:00</div>
                        <div id="totalduration">00:00</div>
                      </div>
                    </div>
                  </div>
                  <div class="body__buttons">
                    <ul class="list list--buttons">
                      <li><a class="btn_backward" onclick="wavesurfer.skipBackward()"><i class="fa fa-backward"></i></a></li>
                      <li><a class="btn_playpause" onclick="wavesurfer.playPause()"><i class="fa fa-play"></i></a></li>
                      <li><a class="btn_forward" onclick="wavesurfer.skipForward()"><i class="fa fa-forward"></i></a></li>
                      <li><a class="btn_playAgin"><i class="fa fa-undo"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="container-fluid searcBar col-md-12">
             <!-- <a class="btn btn btn-info showcomments">Show Comments</a> -->
            @if(is_object($callsurveyid)) <!-- show button if call has survey -->             
              <a class="btn btn btn-info callsurvey" href="javascript:void(0);" id="showsurvey">Show Survey</a>
              <input type="hidden" id="surveyresid" name="surveyresid" value="{{$callsurveyid->id}}" />
              <input type="hidden" id="surveyid" name="surveyid" value="{{$callsurveyid->survey_id}}" />
            @endif
              <a class="btn btn btn-info callsurvey" data-toggle="modal" data-target="#SurveySelectionModal">Create a New Survey</a>              
          </div>
        </div>
        <div class="row">
          <div class="container-fluid searcBar col-md-12">
            <!-- Contened or Principal -->
            <div class="container-fluid col-md-12 comsurcontainer" >
            <div class="commentscontainer"> 
              <div class="comments-container">
                <ul id="comments-list" class="comments-list">
                </ul>
              </div>

              <div class="EditCommentsWrap">
                <div class="CommentsWrap"> 
                    <form method="post" action="/dashboard/updatecomment" id="commentform" class="updatecomment">
                      <div class="comment-content">
                        <textarea rows="3" name="comment" id="comment" required placeholder="Write your notes here."></textarea>
                        <input type="hidden" id="commentid" name="commentid"  />
                        <input type="hidden" id="creator" name="creator" value="{{ $user->id }}"  />
                        <input type="hidden" id="call_id" name="call_id" value="{{$selectedcall->unique_id}}" />
                        <input id="_token" name="_token" type="hidden" value="{{ csrf_token() }}" />
                        <a class="btn btn-danger right" onclick="cancelcomment();" href="javascript:void(0);">Cancel</a>
                        <button type="submit" class="btn btn-success green" id="submit"><i class="fa fa-share"></i> Submit</button>
                      </div>
                    </form>
                </div>
              </div>

              <div class="col-md-12">
                <div class="CommentsWrap">
                  <h3 class="title_1">Add a Note to Call:</h3>
                  <form method="post" action="/dashboard/insertcomment" id="commentform" class="insertcomment">
                    <div class="comment-content">
                      <textarea rows="3" name="comment" id="comment" required placeholder="Write your notes here."></textarea>
                      <input type="hidden" id="creator" name="creator" value="{{ $user->id }}"  />
                      <input type="hidden" id="call_id" name="call_id" value="{{$selectedcall->unique_id}}" />
                      <input id="_token" name="_token" type="hidden" value="{{ csrf_token() }}" />
                      <a class="btn btn-danger right" onclick="cancelcomment();" href="javascript:void(0);">Cancel</a>
                      <button type="submit" class="btn btn-success green" id="submit"><i class="fa fa-share"></i> Submit</button>
                    </div>
                  </form>
                </div><!-- Status Upload  -->
              </div><!-- Widget Area -->
            </div>
            <div class="surveycontainer">
            </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  </div>
<script src="{{ asset('js/wavesurfer.min.js') }}"></script>
<script>
    $(document).ready(function() {
  $("#btnCollapse").click(function() {
          $("main").toggleClass("col-lg-12");
        });

        $(document).on('click','.fa-pencil',function(){
            $('.comment-box .comment-head i').show();
            $(this).hide();
            var CurrentComments = $(this).attr('data_edit_id');
            var CurrCommList = $('.comments-list li#'+CurrentComments);
            

            $('.comments-list li').each(function(){

               if( $(this).hasClass('openComment'))
               {
                  $(this).find('.hidetext').fadeIn();
                  $(this).removeClass('openComment');
                  $('#commentid').val('');
               }
               else{
                setTimeout(function(){
                  CurrCommList.addClass('openComment');
                  var CurrentComText=$('.openComment').find('.comment-content p').html();
                  //$('.openComment').find('.comment-content').html('');
                  $('.openComment .comment-content p').hide();
                  $('.EditCommentsWrap').appendTo('.openComment .comment-content');
                  $('.openComment .comment-content textarea').val(CurrentComText);
                  $('.openComment .comment-content #commentid').val(CurrentComments);
                },100);
               }

            });
                       
        });

        $(document).on('submit',"#commentform", function(event) {

          event.preventDefault();
          var formsec;
          var $form = $(this);

          formsec = $form.attr('class');
          var formcontent;
          if(formsec == 'updatecomment')
          {
            posturl = "{{ url('/comments/updatecomment') }}";
            formcontent = $('.openComment .comment-content').html();
          } else{
            posturl = "{{ url('/comments') }}";
          }
          
          formdata = {};

          formdata['creator'] = $form.find('#creator').val();
          formdata['call_id'] = $form.find('#call_id').val();
          formdata['comment'] = $form.find('#comment').val();
          formdata['commentid'] = $form.find('#commentid').val();
         // formdata['_token'] = $form.find('#_token').val();

          $.ajax({
            type:'POST',
            url: posturl,
            headers: {'X-CSRF-TOKEN': $form.find('#_token').val()},
            data:formdata
          }).done(function(data){
            if(data){
              $('.comments-container').after(formcontent);
              $("#commentform #comment").val(' ');
            }
            loadcomments(formdata['call_id']);
          });

        });
     
     // Agent calls Data table - Starts   
      search_param = {};
      search_param['from_date'] = {!! json_encode($selectedcall->segstart) !!};
      search_param['to_date'] = {!! json_encode($selectedcall->segstop) !!};
      search_param['call_id'] = {!! json_encode($selectedcall->unique_id) !!};

      datatable_load(search_param);

      // Agent calls Data table - Ends

      function musicplaywave(url) {
          jQuery('#waveform wave').remove();
          wavesurfer = WaveSurfer.create({
              container: '#waveform',
              waveColor: '#337ab7',
              progressColor: '#fff',
              backend: 'MediaElement',
          });
          //Set song

          //Set peaks
          wavesurfer.backend.peaks = [0.0218, 0.0183, 0.0165, 0.0198, 0.2137, 0.2888, 0.2313, 0.15, 0.2542, 0.2538, 0.2358, 0.1195, 0.1591, 0.2599, 0.2742, 0.1447, 0.2328, 0.1878, 0.1988, 0.1645, 0.1218, 0.2005, 0.2828, 0.2051, 0.1664, 0.1181, 0.1621, 0.2966, 0.189, 0.246, 0.2445, 0.1621, 0.1618, 0.189, 0.2354, 0.1561, 0.1638, 0.2799, 0.0923, 0.1659, 0.1675, 0.1268, 0.0984, 0.0997, 0.1248, 0.1495, 0.1431, 0.1236, 0.1755, 0.1183, 0.1349, 0.1018, 0.1109, 0.1833, 0.1813, 0.1422, 0.0961, 0.1191, 0.0791, 0.0631, 0.0315, 0.0157, 0.0166, 0.0108];

          //Draw peaks
          wavesurfer.drawBuffer();

          //Variable to check if song is loaded
          wavesurfer.loaded = false;

          //Load song when play is pressed
          wavesurfer.on("play", function() {
              if (!wavesurfer.loaded) {
                  wavesurfer.load(wavesurfer.song, wavesurfer.backend.peaks);
              }
          });

          //Start playing after song is loaded
          wavesurfer.on("ready", function() {
              if (!wavesurfer.loaded) {
                  wavesurfer.loaded = true;
                  wavesurfer.play();
              }
              $('#waveform audio').attr('id', 'background_audio');
              $('.btn_playpause').trigger('click');
              $('.btn_playpause').trigger('click');
              $('.btn_playpause').removeClass('play');
              $('.btn_playpause').addClass('pause');
          });

          wavesurfer.load(url);
      }
  
      function current_audio_info(data){

        //console.log(data);
       
        var table_start = '<table class="display" cellspacing="0" width="100%"><thead><tr><th>Call Date</th><th>Number</th><th>Duration</th><th>Direction</th></tr><tbody><td>'+ $(data).prop('outerHTML')+'</td><td></td><td></td><td></td></tbody></table>';
        
        $("#audio_info").html( table_start );
      }
    
        var call_result = decodeURIComponent("{{ $tempurl }}");
       
          $.urlParam = function(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results==null){
               return null;
            }
            else{
               return results[1] || 0;
            }
          }

          if($.urlParam('page')==null){
            var on_load_call = $("#AgentCall tbody tr.active-custom");
            
            var loadHTML = '<tr class="{{ $selectedcall->unique_id }}" id="'+call_result+'"><td class="sorting_1">{{$selectedcall->segstart}}</td><td>{{$selectedcall->calling_pty}}</td><td>{{$selectedcall->duration}}</td><td>{{$selectedcall->direction}}</td></tr>';

            current_audio_info(loadHTML);
            musicplaywave(decodeURIComponent('{{$tempurl}}'));
          }       
          
          

        $("#AgentCall tbody tr").removeClass("active");
        $.fn.dataTable.ext.errMode = 'none';
        $('#table-id').on('error.dt', function(e, settings, techNote, message) {
            console.log('An error occurred: ', message);
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {

            var target = $(e.target).attr("href") // activated tab
            var call_num = $("#AgentCall tbody tr.active .call_num").html();
            var call_num1 = $("#AgentCall tbody tr.active-custom .call_num").html();
            
            if(typeof (call_num)==="undefined"){
              call_num=call_num1;
            }

            if (target == '#CustomerCall') {

              $('.loadingWrap .cu_call').css("display","table");

              search_param = {};
              search_param['from_date'] = {!! json_encode($selectedcall->segstart) !!};
              search_param['to_date'] = {!! json_encode($selectedcall->segstop) !!};
              search_param['number'] = {!! json_encode($selectedcall->calling_pty) !!};
              datatable_load(search_param);

              $(document).on("click","#customerresult tbody tr", function() {
                  var audio_url = this.id;
                  var audio_info = this;
                  //$('.loadingWrap .loadingImg.home').css("display","table"); // loader on
                  $("#AgentCall tbody tr").removeClass("active");
                  $("#AgentCall tbody tr").removeClass("active-custom");
                  $("#CustomerCall tbody tr").removeClass("active");
                  $(this).addClass("active");
                  var call_result = $(this).attr('class');
                  var call_num = call_result.split(" ");
                  var call_id = call_num[1];
                  $("#commentform #call_id").val(call_num[0]);
                  $.ajax({
                    url:audio_url,
                    type:'GET',
                    error: function()
                    {
                      //file not exists
                      $('#audio_info').html("");
                      //$('.loadingWrap .loadingImg').css("display","none"); // loader display none
                      $('#myModal').modal('show'); // show model
                    },
                    success: function()
                    {
                      //$('.loadingWrap .loadingImg').css("display","none"); // loader display none
                      current_audio_info(audio_info);
                      musicplaywave(audio_url);
                      $(".backToTop").click();
                    }
                  });
                  // Load Audio file
                  $('#background_audio').remove();
                  if(surveyresponseid === undefined){
                    $("#showsurvey").hide();
                    $("#surveyresid").val('');
                    $("#surveyid").val('');
                  } else{
                    $("#surveyresid").val(surveyresponseid);
                    $("#surveyid").val(surveyid);
                    $("#showsurvey").show();
                  }

                  loadcomments(call_num[0]);
              });
            }

        });
        function sformat(s) {
          var fm = [
                //Math.floor(s / 60 / 60 / 24), // DAYS
                Math.floor(s / 60 / 60) % 24, // HOURS
                Math.floor(s / 60) % 60, // MINUTES
                s % 60 // SECONDS
          ];
          return $.map(fm, function(v, i) { return ((v < 10) ? '0' : '') + v; }).join(':');
        }

        $(document).on("click","#AgentCall tbody tr", function() {
        
          var audio_url = this.id;
          var audio_info = this;
          var surveyresponseid, surveyid;
          surveyresponseid = $(this).data("surveyresponseid");
          surveyid = $(this).data("surveyid");
            //$('.loadingWrap .loadingImg.home').css("display","table"); // loader on
            $("#AgentCall tbody tr").removeClass("active");
            $("#AgentCall tbody tr").removeClass("active-custom");
            $(this).addClass("active");
            var call_result = $(this).attr('class');
            var call_num = call_result.split(" ");
            
            var call_id = call_num[1];
            $("#commentform #call_id").val(call_num[0]);
            $.ajax({
              url:audio_url,
              type:'GET',
              error: function()
              {
               
                //file not exists
                $('#audio_info').html("");
                //$('.loadingWrap .loadingImg').css("display","none"); // loader display none
                 $('#myModal').modal('show'); // show model
              },
              success: function()
              {
                 //$('.loadingWrap .loadingImg').css("display","none"); // loader display none
                 current_audio_info(audio_info);
                 musicplaywave(audio_url);
                 $(".backToTop").click();
              }
           });
             $('.surveycontainer').hide();
             $('.commentscontainer').show();
            // Load Audio file
            $('#background_audio').remove();
            if(surveyresponseid === undefined){
              $("#showsurvey").hide();
              $("#surveyresid").val('');
              $("#surveyid").val('');
            } else{
              $("#surveyresid").val(surveyresponseid);
              $("#surveyid").val(surveyid);
              $("#showsurvey").show();
            }

            loadcomments(call_num[0]);
        });
        
        // variable to store HTML5 audio element


        function playAudio(e) {
            if (audioElement.paused) {
                e.removeClass('pause');
                e.addClass('play');
            } else {
                e.removeClass('play');
                e.addClass('pause');
            }
        }

        $('.btn_playpause').click(function() {

            audioElement = document.getElementById('background_audio');
            audioElement.volume = 0.6;

            MinDuration();

            playAudio($(this));

        });

        $('.btn_backward').click(function() {
            audioElement.currentTime -= 5;

        });

        $('.btn_forward').click(function() {
            audioElement.currentTime += 5;
        });

        $('.btn_playAgin').click(function() {
            audioElement.currentTime = 0;
            audioElement.play();
        });

        // Turn off the default controls
        //audioElement.controls = false;

        // detect audio playing
        $('audio#background_audio').bind('play', function() {
            $('#totalduration, #time').fadeIn('slow');
        });
        
        loadcomments($("#call_id").val()); // Load comments on page on load for selected call

        $(document).on("click",".showcomments",function(){
         $('.surveycontainer').hide();
         $('.commentscontainer').show();
          if($(".call_id").val() == ''){
            var callid = {!! json_encode($selectedcall->unique_id) !!};
          } else{
             var callid = $(".call_id").val();
          }
          loadcomments(callid);
        });
       
        function loadcomments(call_id){
           //(call_id);
           $.getJSON("/comments/"+call_id, function(data) {
                var items = [];
                $.each(data, function(key, val) {
                    var editicon='';
                    var loggeduser = {!! json_encode($user->id) !!};
                    if(loggeduser == val.user['id']){
                      editicon = "<i class='fa fa-pencil' aria-hidden='true' data_edit_id="+val.id+"></i> ";
                    }

                    items.push("<li id="+val.id+" ><div class='comment-main-level'><i style='font-size:80px;' class='fa fa-user'></i> <div class='comment-box'> <div class='comment-head' > <h6 class='comment-name'>" + val.user['name'] + "</h6> <span>" + val.created_at + "</span>"+editicon+"</div> <div class='comment-content'><p class='hidetext'>" + val.body + "</p></div> </div> </div></li>");
                });

                 jQuery('#comments-list').html(items);
            });
        }

        function cancelcomment()
        {
          jQuery('#comment').val('');
          jQuery('#comment_id').val('');
          //jQuery('#submit').val('Add Comment');
        }

        /* min and secs  */
        function MinDuration() {

            audioElement.addEventListener("timeupdate", function() {
                var time = document.getElementById('time'),
                    currentTime = audioElement.currentTime,
                    minutes = parseInt(currentTime / 60),
                    seconds = parseInt(currentTime - (minutes * 60)),
                    totalduration = document.getElementById('totalduration'),
                    duration = audioElement.duration,
                    min = parseInt(duration / 60),
                    sec = parseInt(duration - (min * 60)),
                    // How far through the track are we as a percentage seekbar 230px
                    seekbar = (currentTime / duration) * 100 + '%';
                // Change the width of the progress bar
                $('#audioSeekbar').css({
                    width: seekbar
                });
                // seekbar input
                var seekbar = document.getElementById('seekbar');

                function setupSeekbar() {
                    seekbar.min = audioElement.startTime;
                    seekbar.max = audioElement.startTime + audioElement.duration;
                }
                audioElement.ondurationchange = setupSeekbar;

                function seekAudio() {
                    audioElement.currentTime = seekbar.value;
                }

                function updateUI() {
                    var lastBuffered = audioElement.buffered.end(audioElement.buffered.length - 1);
                    seekbar.min = audioElement.startTime;
                    seekbar.max = lastBuffered;
                    seekbar.value = audioElement.currentTime;
                }

                seekbar.onchange = seekAudio;
                audioElement.ontimeupdate = updateUI;


                audioElement.addEventListener('durationchange', setupSeekbar);
                audioElement.addEventListener('timeupdate', updateUI);

                // If the number of minutes is less than 10, add a '0'
                if (min < 10) {
                    min = '0' + min;
                }
                if (minutes < 10) {
                    minutes = '0' + minutes;
                }
                // If the number of seconds is less than 10, add a '0'
                if (sec < 10) {
                    sec = '0' + sec;
                }
                if (seconds < 10) {
                    seconds = '0' + seconds;
                }
                // Display the current time
                time.innerHTML = minutes + ':' + seconds;
                // Display the time(full)
                totalduration.innerHTML = min + ':' + sec;

                var timeCalc = minutes + ':' + seconds;
                var totaldurationCalc = min + ':' + sec;

                if (timeCalc == totaldurationCalc) {
                    var btn_playpause = $('.list--buttons a.btn_playpause');
                    btn_playpause.removeClass('pause');
                    btn_playpause.addClass('play');
                }

            }, false);

        }

        $("#selectsurvey").click(function(){

          subdata = {};
          subdata['id'] = $(".surveylist").val();
          subdata['call_id'] = $("#call_id").val();
          if($(".surveylist").val()){
             $.ajax({
              type:'GET',
              url: "{{ url('surveys') }}",
              data: subdata,
              headers: {'X-CSRF-TOKEN': $('#_token').val()}
            }).done(function(data){
              if(data){
                 $('.surveycontainer').show();
                $('.commentscontainer').hide();
                $('.surveycontainer').html(data);
              } 
            });
          } 
        });

        $(document).on('submit',"#surveyform", function(event) {
          event.preventDefault();
         // var call_id = $(this).find("#call_id").val();
          posturl = "{{ url('/surveys/create') }}";
          
          $.ajax({
            type:'POST',
            url: posturl,
            headers: {'X-CSRF-TOKEN': $('#_token').val()},
            data:$(this).serialize()
          }).done(function(data){
            if(data){
             $('#myModal .modal-body ').html('<h4>Survey submitted successfully</h4>');
             $('#myModal').modal('show'); // show model
             $(".surveycontainer").hide();
             $(".commentscontainer").show();
              //loadcomments(call_id);
            }
            
          });

        });

        $(document).on('click',"#showsurvey", function(event) {

          event.preventDefault();
          subdata = {};
                    
          //subdata['id'] = $(".surveylist").val();
          //subdata['call_id'] = {!! json_encode($selectedcall->unique_id) !!};
          subdata['call_from'] = 'showsurvey';
          subdata['surveyresid'] = $("#surveyresid").val();
          subdata['id'] = $("#surveyid").val();                   

          $.ajax({
            type:'GET',
            url: "{{ url('surveys') }}",
            data: subdata,
            headers: {'X-CSRF-TOKEN': $('#_token').val()}
            }).done(function(data){
              if(data){
                $('.commentscontainer').hide();
                $('.surveycontainer').show();
                $('.surveycontainer').html(data);
              } 
            });
          });

    });

  function datatable_load(search_param)
   {   
      $.fn.dataTableExt.sErrMode = 'throw';

      var tableid;
      var posturl;
      if(search_param['number']){ // Customer calls
        tableid = "customerresult";
        posturl = "{{ url('dashboard/customercall') }}";
      }
      else{  // Agent calls
        tableid = "resulttable";
        posturl = "{{ url('dashboard/agentcall') }}";
      }

      var table='';           
      table = $('#'+tableid).DataTable({
        processing: true,
        serverSide: true,
        deferRender: true,
        destroy: true,
        pageLength: 25,
        fixedHeader: true,
        order:false,
        ajax: { 
              url: posturl,
              dataType: "json",
              type: "GET",     
              data: search_param,
              initComplete: function (resp){  
                     
              }                 
            },
            createdRow: function ( row, data, index ) {

              if(index === 0){
                if(search_param['number']){
                  $(row).attr('class',data.calling_pty+" "+data.calling_pty);
                }else{
                  $(row).attr('class','active-custom '+data.unique_id+" "+data.calling_pty);
                }

              } else{
                $(row).attr('class',data.unique_id+" "+data.calling_pty);
              }


            },
            rowCallback: function(row, data) {
               $(row).attr('id',decodeURIComponent(data.recordingfileurl));
               if(data.surveyresponse != null){
                var surveyres;
                surveyres = data.surveyresponse.split("#");
                
                $(row).attr('data-surveyresponseid',surveyres[0]);
                $(row).attr('data-surveyid',surveyres[1]);
              }             
            },  
            columns: [
              {data:'segstart',name:'segstart', defaultContent:"",class:"sorting_1"},
              {data:'calling_pty',name:'calling_pty',defaultContent:"",class:'call_num'},
              {data: 'duration', name: 'duration', defaultContent: ""},
              {data: 'direction', name: 'direction', defaultContent: ""},
            ],
    });

    table.draw();
    return table;      
   }  
   window.onscroll = function() {
            scrollFunction()
        };
   function scrollFunction() {
        if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
            document.getElementById("btn_Fixed").style = "position: fixed; width:275px";
        } else {
            document.getElementById("btn_Fixed").style= "position:initial";
        }
    }
</script>
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-body center">
      <h4>Oops!</h4><p> Audio doesn't exist.</p>
    </div>
    <div class="modal-footer center">
      <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
    </div>
  </div>

</div>
</div>


<!-- Modal -->
<div id="SurveySelectionModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"></button>
        <h4 class="modal-title">Survey Selection</h4>
      </div>
      <div class="modal-body">
        <p>Please Select A Survey</p>
        <select class="surveylist form-control" name="surveylist" id="surveylist">
                    <option value="" selected>Select A Survey</option>                      
                      @foreach ($surveys as $survey)
                        <option value="{{$survey->id}}">{{$survey->title}}</option>
                      @endforeach
              </select>
              <!-- <div id="surveycontainer"></div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" id="selectsurvey">Submit</button>
      </div>
    </div>

  </div>
</div>
   </body>
</html>