                           @foreach ($comments as $cmt)
                           <li>
                              <div class="comment-main-level">
                                 <!-- Avatar -->
                                 <i style="font-size:80px;" class="fa fa-user"></i>
                                 <!-- Contenedor del Comentario -->
                                 <div class="comment-box">
                                    <div class="comment-head">
                                      <h6 class="comment-name"><?php echo $cmt->name;?></h6>
                                                        <span><?php echo $cmt->created;?></span><?php if($user_id==$cmt->creator && $user_id !=0) { ?> <a onclick="loadcomment({!! $cmt->id !!})" class="right" href="javascript:void(0);"><i class="fa fa-pencil"></i></a><?php }?>
                                    </div>
                                    <div class="comment-content">
                                       <div id="comment_{!! $cmt->id !!}">{!! $cmt->comment !!}</div>
                                    </div>
                                 </div>
                              </div>
                              <!-- Respuestas de los comentarios -->
                           </li>
                           @endforeach