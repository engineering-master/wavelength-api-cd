<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */

        body,
        table,
        td,
        a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */

        a {
            text-decoration: none !important;
            font-family: Helvetica, Arial, sans-serif !important;
        }

        /* MOBILE STYLES */

        @media screen and (max-width: 525px) {

            /* ALLOWS FOR FLUID TABLES */
            .wrapper {
                width: 100% !important;
                max-width: 100% !important;
            }

            /* ADJUSTS LAYOUT OF LOGO IMAGE */
            .logo img {
                margin: 0 auto !important;
            }

            /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
            .mobile-hide {
                display: none !important;
            }

            .img-max {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }

            /* FULL-WIDTH TABLES */
            .responsive-table {
                width: 100% !important;
            }

            /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
            .padding {
                padding: 10px 5% 15px 5% !important;
            }

            .padding-meta {
                padding: 30px 5% 0px 5% !important;
                text-align: center;
            }

            .padding-copy {
                padding: 10px 5% 10px 5% !important;
                text-align: center;
            }

            .no-padding {
                padding: 0 !important;
            }

            .section-padding {
                padding: 50px 15px 50px 15px !important;
            }

            /* ADJUST BUTTONS ON MOBILE */
            .mobile-button-container {
                margin: 0 auto;
                width: 100% !important;
            }

            /*
            .mobile-button {
                font-family: Helvetica, Arial, sans-serif !important; 
                color: #ffffff !important; 
                text-decoration: none !important; 
                border-radius: 3px !important; 
                padding: 15px 30px !important;
/*                padding: 15px !important;*/
            /*                border: 0 !important;*/
            /*                font-size: 16px !important;
                display: block !important;
            }
*/
        }

        /* ANDROID CENTER FIX */

        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }

    </style>
</head>

<body style="margin: 0 !important; padding: 0 !important; background-color: #f7f7f7;">

    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr height="100"></tr>
        <tr>
            <td bgcolor="#F7F7F7" align="center" class="section-padding">
                <table bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="90%" class="responsive-table" style="box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.25);">
                    <tr>
                        <td align="center" style="padding: 60px 15px 80px 15px; ">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px; " class="responsive-table">
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <!-- Logo -->
                                            <tr>
                                                <td align="center" valign="top" style="padding: 15px 0;" class="logo">
                                                    <a href="http://fpsinc.com/" target="_blank">
                                                        <img alt="Logo" src="http://fpsinc.com/wp-content/uploads/2017/06/fps_final_logo-300x113.png" width="180" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;" border="0">
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- Notification -->
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                        <td align="center" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px; padding-bottom: 20px" class="padding"> <span style="color: #3C8DBC;">{{ $name }}</span>  requested to update profile
                                                        </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- Link to EVAL -->
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="center" style="font-size: 16px; font-family: Helvetica, Arial, sans-serif; color: #000; padding-top: 10px; padding-bottom: 30px" class="padding">
                                                                Please login to <a href="{{ config('app.front_end_url') }}" style="color: #3C8DBC">Wavelength</a> and Click <a href="{{ config('app.front_end_url') }}/settings/assignrole" style="color: #3C8DBC">here</a> to view the profile.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <!-- Footer -->
        <tr>
            <td align="center" style="padding: 50px 10px; text-align: center">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 500px;" class="responsive-table">
                    <tr>
                        <td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;">
                            If you have any queries, please <a href="#" target="_blank">contact us</a>
                            <br>2019 ©
                            <a href="http://fpsinc.com" target="_blank" style="color: #666666; text-decoration: none;">Full Potential Solutions Inc.</a>
                        </td>
                </table>
            </td>
        </tr>
    </table>

</body>

</html>
