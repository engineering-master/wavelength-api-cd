<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="{{ asset('favicon.ico') }}">
  <title>Call Search</title>
  <script src="{{ asset('js/jquery.min.js') }}"> </script>
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="{{ asset('js/app.js') }}"> </script>


  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" >
  <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css') }}"/>
  <script type="text/javascript" src="{{ asset('js/datatables.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" >

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
  <link rel="stylesheet" href="{{ asset('css/style.css') }}" >

<script type="text/javascript">

function datatable_load(search_info)
   {   

      $.fn.dataTableExt.sErrMode = 'throw';
      var table='';           
      table = $('#calltable').DataTable({
        processing: true,
        serverSide: true,
        deferRender: true,
        destroy: true,
        pageLength: 50,
        fixedHeader: true,
        ajax: { 
              url: "{{ url('dashboard/datatable') }}",
              dataType: "json",
                   type: "GET",     
                   data: search_info,
                  initComplete: function () {    

              }                 
               },
               rowCallback: function(row, data) {
                   $(row).attr('id',decodeURIComponent(data.resulturl));
                },  
        columns: [
                  {data: 'client', name: 'client', defaultContent: ""},
                  {data: 'agentname', name: 'agentname', defaultContent: ""},
                  {data: 'segstart', name: 'segstart', defaultContent: ""},
                  {data: 'segstop', name: 'segstop', defaultContent: ""},
                  {data: 'duration', name: 'duration', defaultContent: ""},
                  {data: 'queuetime', name: 'queuetime', defaultContent: ""},
                  {data: 'ringtime', name: 'ringtime', defaultContent: ""},
                  {data: 'talktime', name: 'talktime', defaultContent: ""},
                  {data: 'holdtime', name: 'holdtime', defaultContent: ""},
                  {data: 'direction', name: 'direction', defaultContent: ""},
                  {data: 'outbound_cid', name: 'outbound_cid', defaultContent: ""},
                  {data: 'calling_pty', name: 'calling_pty', defaultContent: ""},
                  {data: 'dialed_num', name: 'dialed_num', defaultContent: ""},
                  {data: 'recordingfileurl', name: 'recordingfileurl', defaultContent: ""},
                  {data: 'unique_id', name: 'unique_id', defaultContent: ""},
                  {data: 'surveyresponseid', name: 'surveyresponseid', defaultContent: "",render: function(data,type,row,meta) {
                      var surveyimg='';
                      if(row.surveyresponseid != null && row.surveyresponseid !='')
                      {
                        var surveyimg = '<a onclick="#" class="surveylink" prospect="'+row.surveyresponseid+'" href="javascript:void(0);"><img src="{{ asset("img/survey.png") }}"/></a>';
                        return surveyimg;
                      } 
                    }
                  },
                ],
                                
    language: {
                            "processing": ' Processing... please wait. <i class="fa fa-hourglass-start fa-pulse fa-1x fa-fw" style="color: #00b5e6;"></i>',
                        },
    });
   
    table.draw();
    return table;      
   }  

function search()
   {   
      search_info = [];

      if($('#client').val() !='All'){
        search_info['client'] = $('#client').val();
      }
      if($('#from_date').val() !=''){
        search_info['from_date'] = $('#from_date').val();
      }
      if($('#to_date').val() !=''){
        search_info['to_date'] = $('#to_date').val();
      }
      if($('#caller_ani').val() !=''){
        search_info['caller_ani']
      }
      if($('#agent_list').val() !=''){
        search_info['employee_name'] = $('#agent_list').val();
      }
      if($('#uniqueid').val() !=''){
        search_info['uniqueid'] = $('#uniqueid').val() ;
      }

      search_info['_token'] = "{{csrf_token()}}";

      datatable_load(search_info);

   };


 $(document).on( 'click', '#calltable tbody tr', function () {
   $(location).attr('href', this.id); 
  });

$(document).ready(function() {

//search();

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})

  $('#agent_list').select2();
            $("#from_date").datepicker({
                       maxDate: '0',
                       dateFormat: 'yy-mm-dd',
                       onSelect: function(dateStr) {
            var min = $(this).datepicker('getDate') || new Date(); // Selected date or today if none
            //var max = new Date(min.getTime());
            $('#to_date').datepicker('option', {
              minDate: min,
              maxDate: 0
            });
          }
        });
            $("#to_date").datepicker({
                       maxDate: '0',
                       dateFormat: 'yy-mm-dd',
                       onSelect: function(dateStr) {
            var max = $(this).datepicker('getDate') || new Date(); // Selected date or today if none
            // var max = new Date(min.getTime());
            $('#from_date').datepicker('option', {
              maxDate: max
            });
          }
        });

          $('.switchBtn').click(function(){
            var inputWrap = $(this).children('input');
            if (inputWrap.is(':checked')) {
             inputWrap.attr('checked' , false);
             $(this).removeClass('IsChecked');
           }
           else
           {
             inputWrap.attr('checked' , true);
             $(this).addClass('IsChecked');
           }
         });

 });

</script>
</head>
<body class="Cover_Bg">

 <div id="header" class="header">
   <!-- begin container-fluid -->
   <div class="container-fluid">
     <div class="row">
       <div class="logoWrap col-4 col-sm-2">
        <a href="/dashboard" class="navbar-logo"> <img src="{{ asset('img/fullsizelogo.png') }}" /> </a>
      </div>

      <!-- begin header navigation right -->
   </div>
   <!-- end header navigation right -->
 </div>
 <!-- end container-fluid -->
</div>

<!--Header CLosED -->
<div class="container">
 <div class="row">
  <main role="main" class="col-md-12 pt-3">
    <div class="container-fluid searcBar">
    <h4>Search</h4>
    <form id='searchform'>
    <section class="row search_btm searchOutterWrap">
        <div class="col-md-3">
          <div class="form-group">
           <label for="pwd">Client Name: </label>
           <select class="form-control" name="client" id="client">
             <option value="All">All Clients</option>
             <option value="Sprint">Sprint</option>
             <option value="Noodle">Noodle</option>
             <option value="Wayfair">Wayfair</option>
           </select>
         </div>
       </div>
		<div class="col-md-3">
         <div class="form-group">
          <label for="pwd">Agent Name:</label>
		  <span class="selector2Wrap">
          <select class="form-control" name="agent_id" id="agent_list">
            <option value="">All Agents</option>
            @foreach ($all_agents as $agent)
            <option value="{!! $agent->eid !!}">{{ $agent->employee_name }}</option>
            @endforeach

          </select>
		  </span>
        </div>
      </div>
		<div class="col-md-3">
		   <div class="form-group">
			<label for="pwd">Min Date:</label>
			<input name="from_date" id="from_date" value="@if(isset($from_date)) {{ date('Y-m-d',strtotime($from_date)) }} @else {{ date('Y-m-d') }} @endif" type="text" class="form-control">
		  </div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			  <label for="pwd">Max Date:</label>
			  <input name="to_date" id="to_date" value="@if(isset($to_date)) {{ date('Y-m-d',strtotime($to_date)) }} @else {{ date('Y-m-d') }} @endif" type="text" class="form-control">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			  <label for="pwd">Customer's Phone Number:</label>
			  <input name="caller_ani" id="caller_ani" value="@if(isset($caller_ani)) {{  $caller_ani }} @endif" type="text" class="form-control">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
			  <label for="pwd">Call ID:</label>
			  <input name="uniqueid" id="uniqueid" value="" type="text" class="form-control">
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group switchBtnWrap">
				<label for="pwd">Search with Comments</label>
				<div class="switchBtn @if(isset($check_comment)) @if($check_comment=='Y') {{ IsChecked }} @endif @endif">
					<input type="checkbox" value="Y" @if(isset($check_comment)) @if($check_comment=='Y') {{ checked }} @endif @endif name="check_comment">
					<span class="slider round">
						<span class=""> <i class="fa fa-times" aria-hidden="true"></i> </span>
					</span>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group btn_SubmitWrap">
				<input type="button" class="btn btn-info" value="Search" onclick="search()">
			</div>
		</div>
	</section>
	</form>
	
	<div class="table-responsive">
	
	<div class="tableBoxWrap app_table">
		<table class="table app_datatable" id="calltable">
			<thead>
			  <tr>
				<th>Client</th>
				<th>Agent Name</th>
				<th>Start DateTime</th>
				<th>End DateTime</th>
				<th>Duration</th>
				<th>Queue Time</th>
				<th>Ring Time</th>
				<th>Talk Time</th>
				<th>Hold Time</th>
				<th>Direction</th>
				<th>Outbound CID</th>
				<th>Calling Party CID</th>
				<th>Dialed Number</th>
				<th>Recording File</th>
				<th>Call ID</th>
        <th>Survey</th>
			  </tr>
			</thead>
		</table>
	</div>
	
	</div>
	
</body>
</html>
