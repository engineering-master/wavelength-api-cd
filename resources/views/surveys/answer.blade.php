<link rel="stylesheet" href="{{ asset('css/answerstyle.css') }}">
<h4>{{$survey->title}}</h4>
<form>
@foreach ($survey->groups as $group)
    <div class="FormOutterBoxWrap">

        <h4 class="title_h4">{{$group->title}}</h4>
        @if($group->description)
            <p class="text_p1">{{$group->description}}</p>
        @endif
        @php $iterate=0; @endphp
        @foreach ($group->questions as $question)
        @php $answer_text ='' @endphp
            @if(is_object($survey_answers))
                @foreach ($survey_answers as $answer)                
                    @if($answer->question_id == $question->id)
                        @php 
                        $answer_text = $answer->answer_text;
                        if(isset($answer->comments) && count($answer->comments))
                            $answer_comments = $answer->comments[0]->body;
                        else
                             $answer_comments = '';                     
                        @endphp
                    @endif
                @endforeach
            @endif
            @if($question->type == 'radio')
                @php 
                    $subquestions = $survey->subquestions($question->id); 
                    $subiterate = 0;
                @endphp
                @foreach ($subquestions as $subquestion)
                    @if(is_object($survey_answers))
                        @foreach ($survey_answers as $answer)                       
                            @if($answer->question_id == $subquestion->id)
                                @php $answer_text = $answer->answer_text; break; @endphp
                            @else
                                 @php $answer_text = ''; @endphp
                            @endif
                        @endforeach
                    @endif
                    @if($answer_text)
                        @if($subiterate == 0)
                            <p class="text_p3 semi-bold">{{$question->title}}</p>
                        @endif
                        <ul class="formBoxWrap SurveyFormWrap">
                            <li class="width_75">{{ $subquestion->title}}</li>
                            <li class="width_25">{{$answer_text}}</li>
                        </ul>                       
                    @endif
                    @php $subiterate++; @endphp
                @endforeach
                 <ul class="formBoxWrap">
                            <li class="width_75 semi-bold score">Score:</li>
                            <li class="width_25 semi-bold">100%</li>
                </ul>
            @elseif($question->type == 'text' || $question->type == 'email' || $question->type == 'number' || $question->type == 'tel' ||  $question->type == 'select')
                <ul class="formBoxWrap SurveyFormWrap">
                    <li class="semi-bold width_25">{{$question->title}}:</li>
                    <li class="width_25">{{$answer_text}}</li>
                </ul>
            @elseif($question->type == 'yesno')
                @if($iterate == 0)
                <ul class="formBoxWrap">
                    <li class="width_50 semi-bold">Question</li>
                    <li class="width_20 semi-bold">Answer</li>
                    <li class="width_30 semi-bold">Comment</li>
                </ul>
                @endif
                <ul class="formBoxWrap SurveyFormWrap">
                    <li class="width_50">{{$question->title}}</li>
                    <li class="width_20">{{$answer_text}}</li>
                    <li class="width_30">{{$answer_comments}}</li>
                </ul>
                @php $iterate++; @endphp
            @endif
        @endforeach
    </div>
@endforeach

<ul class="formBoxWrap">
    <li class="width_75 score scoreBold">Final Score:</li>
    <li class="width_25 scoreBold">100%</li>
</ul>