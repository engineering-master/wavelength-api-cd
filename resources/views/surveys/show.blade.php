<h4>{{$survey->title}}</h4>
<form method="post" action="/surveys/create" id="surveyform">
@foreach ($survey->groups as $group)


	<div class="FormOutterBoxWrap">
	   
		<h4 class="title_h4"> 
			{{$group->title}}
		</h4>
		<p class="text_p1">{{$group->description}}</p>
		<ul class="formBoxWrap SurveyFormWrap">

			@foreach ($group->questions as $question)

				@if($question->type)
				<li class="@if($question->type == 'yesno')YesNoWrap_list @elseif($question->type == 'radio' && $question->parent_question_id == 0) YesNoWrap_Table @endif" >
					
					@if($question->type == 'text' || $question->type == 'email' || $question->type == 'number' || $question->type == 'tel')
						<label>{{$question->title}} 
							@if($question->mandatory ==1)	
							<span>*</span>
							@endif
						</label>
						<input name="{{$question->id}}" type="{{$question->type}}" class="inputTypeTxt" />
					@elseif($question->type == 'textarea')
						<label> {{$question->title}} 
							@if($question->mandatory ==1)	
							<span>*</span>
							@endif
						</label>
						<textarea name="{{$question->title}}" class="inputTypeTxt"></textarea>
					@elseif($question->type == 'select')
						<label> {{$question->title}} 
							@if($question->mandatory ==1)	
							<span>*</span>
							@endif
						</label>
						<select class="inputTypeTxt" name="{{$question->id}}">
							<option value="">Please choose...</option>
								
								<?php 
									$soptvalues = explode('|',$question->values);
								?>
								@foreach ($soptvalues as $soptvalue)
									<option value="{{$soptvalue}}">{{$soptvalue}}</option>
								@endforeach
						</select>	
					@endif

				@if($question->type == 'radio')
						<div class="YesNoTable">
							<table>
								<thead>
									<tr>
										<th align="left" valign="middle"> <label> 			
												{{$question->title}} <b class='QusScore'>0%</b>
											</label> </th>
										<?php 
											$roptvalues = explode('|',$question->values);
										?>
										@foreach ($roptvalues as $roptvalue)

										<th align="center" valign="middle">{{$roptvalue}}</th>
										@endforeach
									</tr>
								</thead>
								<tbody>
								<?php
									$subquestions = $survey->subquestions($question->id);

								?>
									<tr>
											<th align="left" valign="middle" class="answerText"> Select All </th>
											@foreach ($roptvalues as $roptvalue)
											<td align="center" valign="middle">
												<span class="CustomRadioBox">
												<input class="inputStyle" name="{{$question->id}}" type="radio" @if($roptvalue == 'NA') checked @endif >
												<span class="checkedTick"> <i class="fa fa-circle"></i> </span>
												</span>
											</td>
											@endforeach
									</tr>

									@foreach ($subquestions as $subquestion)
										<tr>
											<th align="left" valign="middle" class="answerText"> {{$subquestion->title}} </th>
											@foreach ($roptvalues as $roptvalue)
											<td align="center" valign="middle">
												<span class="CustomRadioBox">
												<input class="inputStyle" name="{{$subquestion->id}}" type="radio" value="{{$roptvalue}}" data-score="@if($subquestion->score){{$subquestion->score}}@else{{0}}@endif" @if($roptvalue == 'NA') checked @endif>
												<span class="checkedTick"> <i class="fa fa-circle"></i> </span>
												</span>
											</td>
											@endforeach
										</tr>	
									@endforeach		
								</tbody>
							</table>
						</div>
					@endif
					@if($question->type == 'yesno')
							<label> {{$question->title}} 
								@if($question->mandatory ==1)	
								<span>*</span>
							@endif
						</label>
							<div class="YesNoBox">
								<?php 
									$optvalues = explode('|',$question->values);
								?>
								@foreach ($optvalues as $optvalue)
									<label name="{{$optvalue}}" class="radioTrig @if($optvalue=='NA') active @endif"for="qus_{{$question->id}}_{{$optvalue}}" >@if($optvalue =='NA')No Answer
									@else{{$optvalue}}@endif<input class="inputStyle" value="{{$optvalue}}" name="{{$question->id}}" type="radio" id="qus_{{$question->id}}_{{$optvalue}}""></label>
									
								@endforeach
							</div>
							<input id="selvalue" name="{{$question->id}}" type="hidden" value="" />
							<p class="text_p2">Comment</p>
							<textarea class="inputTypeTxt" name="{{ str_replace(' ','',$question->title).'__'.$question->id }}"></textarea>
					@endif

				</li>
			@endif
			@endforeach
		</ul>
	</div>
	
@endforeach	
	
	<ul class="formBoxWrap SurveyFormWrap">
		<li class="scorewrap">
			<div class="FinalTxtWrap">Observed Behaviors Score = <span class='OB_score'>0.00</span>%</div>
		</li>
		<li class="scorewrap">
			<div class="FinalTxtWrap"> Final Score = <span class='FL_score'>0.00</span>% </div>
		</li>
		<li class="scorewrap">
			<input type="submit" value="SUBMIT" class="BtnFormSearch" />
		</li>
	</ul>
	<input type="hidden" id="call_id" name="call_id" value="{{ $call_id }}"  />
	<input type="hidden" id="survey_id" name="survey_id" value="{{$survey->id}}"  />
	<input id="_token" name="_token" type="hidden" value="{{ csrf_token() }}" />
	<input type="hidden" id="creator" name="creator" value="{{ $user->id }}"  />
</form>
<script>
$(document).ready(function() {

	$(".radioTrig").click(function(){
		$(this).siblings('.radioTrig').removeClass('active');
		$(this).addClass('active');
		$(this).parent().parent().find("#selvalue").val($(this).attr('name'));

	});
	
	$('.switchBtn').click(function(){
		var inputWrap = $(this).children('input');
		if (inputWrap.is(':checked')) {
			inputWrap.attr('checked' , false);
			$(this).removeClass('IsChecked');
		}
		else
		{
			inputWrap.attr('checked' , true);
			$(this).addClass('IsChecked');
		}
	});

	/**************Back to Top****************/
	$(".backToTop").click(function () {
		$("html, body").animate({scrollTop: 0}, 1000);
	});
	
	/*******************************************/
	$("select.inputTypeTxt").change(function () {
		if ( $(this).val() == 'hasSubQuestion')
		{
			$('.SubQuestionWrap').slideDown();
		}
		else{
			$('.SubQuestionWrap').slideUp();
		}
	});
	
	$(document).on("click", ".deleteIconBox" , function() {
		$(this).parents('tr').remove();
	});
	
	$(document).on("click", ".addIconBox" , function() {
		var SubQuestionHTML = '<tr><td align="center" valign="middle" class="move-icon"><span class="moveIconBox"> <i class="fa fa-arrows" aria-hidden="true"></i> </span></td><td align="left" valign="middle"> <input type="text" class="inputTypeTxt" /></td><td align="left" valign="middle"> <input type="text" class="inputTypeTxt" /></td><td align="center" valign="middle"> <span class="addIconBox"> <i class="fa fa-plus" aria-hidden="true"></i> </span> <span class="deleteIconBox"> <i class="fa fa-trash-o" aria-hidden="true"></i> </span> </td> </tr>';
		$(this).parents('tbody').append( SubQuestionHTML );
	});

	$('.YesNoTable tr input[type="radio"]').change(function() {
		var GetParentDiv = $(this).parents('tbody');
		GetParentDiv.addClass('NrlActive');
		var countPosition = $(this).parents('td').index()+1;
		var InputRadioPosition = $('.NrlActive tr td:nth-child('+countPosition+')');
		var dynamicCount = InputRadioPosition.length - 1;

		var dynamicCountInput = InputRadioPosition.find(':radio:checked').length;

		if( dynamicCount == dynamicCountInput)
		{
			$('.NrlActive tr:first-child td:nth-child('+countPosition+') input[type="radio"]').prop('checked', true);
		}
		else
		{
			$('.NrlActive tr:first-child td input[type="radio"]').prop('checked', false);
		}

		setTimeout(function(){
			GetParentDiv.removeClass('NrlActive');
		},500);

	});


	$('.YesNoTable tr:first-child input[type="radio"]').change(function() {
		var GetParentDiv = $(this).parents('tbody');
		GetParentDiv.addClass('active');
		var countPosition = $(this).parents('td').index()+1;

		$('.active td input[type="radio"]').each(function(){
			$(this).prop('checked', false);
		});

		$('.active tr td:nth-child('+countPosition+') input[type="radio"]').prop('checked', true);

		setTimeout(function(){
			GetParentDiv.removeClass('active');
		},500);
	}); 



/*********Final Score *************/

function CheckValue() {
	var YesCount = 0;
    var NACount = 0;
    var OB_FinalScoreTotal = 0;
    var FinalScoreTotal = 0;

    //console.log(NACount);
    $('.YesNoTable').each(function() {
    	YesCount = 0;
    	NACount = 0;
    	$('.CustomRadioBox input[type="radio"]').each(function() {
	    	if($(this).val() == 'Yes')
	        {
	        	var DataNAVal = $(this).attr('data-score');
	        	NACount += parseInt(DataNAVal);
	        }
    	});

    	$('.CustomRadioBox input[type="radio"]').each(function() {
    		if ($(this).is(':checked')) {
    			var DataScore = $(this).attr('data-score');
                var DataVal = $(this).val();
    			if (DataVal == 'NA') {
                    NACount -= parseInt(DataScore);
                }
    		}
    	});
    	
    	$(this).find('.CustomRadioBox input[type="radio"]').each(function() {
    		if ($(this).is(':checked')) {
                var DataScore = $(this).attr('data-score');
                var DataVal = $(this).val();

                if(DataVal == 'Yes')
                {
                	YesCount += parseInt(DataScore);
                }
            }
    	});

    	var FinalScore = (YesCount / NACount) * 100;
    	OB_FinalScoreTotal += parseFloat(FinalScore);

    	FinalScore = FinalScore.toFixed(2);
    	if (FinalScore == 'NaN') {
	        $(this).find('.QusScore').text('0%');
	    } else {
	        $(this).find('.QusScore').text(FinalScore+'%');
	    }

    });

    OB_FinalScoreTotal = OB_FinalScoreTotal.toFixed(2);
    FinalScoreTotal = OB_FinalScoreTotal;

    $('.YesNoBox input[type="radio"]').each(function() {
        if ($(this).is(':checked')) {
            var CheckedVal = $(this).val();
            if (CheckedVal == 'No') {
                FinalScoreTotal = FinalScoreTotal * 0;
            } else {
                FinalScoreTotal = FinalScoreTotal;
            }
        }
    });

    $('.OB_score').text(OB_FinalScoreTotal);
    $('.FL_score').text(FinalScoreTotal);

    if (OB_FinalScoreTotal == 'NaN') {
        $('.OB_score').text('0');
    	$('.FL_score').text('0');
    } else {
        $('.OB_score').text(OB_FinalScoreTotal);
    	$('.FL_score').text(FinalScoreTotal);
    }
}


$('.YesNoTable input[type="radio"], .YesNoBox input[type="radio"]').change(function() {
    CheckValue();
});
		
} );
</script>