<!doctype html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="../../../../favicon.ico">
      <title>Call Search</title>
      <script src="<?php echo asset('js/jquery.min.js'); ?>"></script>

    	<!-- Bootstrap core CSS -->
    	<link rel="stylesheet" href="<?php echo asset('css/bootstrap.min.css'); ?>" >
    	<script src="<?php echo asset('js/bootstrap.bundle.min.js'); ?>"></script>
    	<script type="text/javascript" src="<?php echo asset('js/jquery-ui.min.js'); ?>"></script>
    	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/datatables.min.css'); ?>"/>
    	<link rel="stylesheet" type="text/css" href="<?php echo asset('css/jquery-ui.css'); ?>"/>
    	<script type="text/javascript" src="<?php echo asset('js/datatables.min.js'); ?>"></script>
    	<link rel="stylesheet" href="<?php echo asset('css/font-awesome.min.css'); ?>" >

		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css" />
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.js"></script>
		<link rel="stylesheet" href="<?php echo asset('css/style.css'); ?>" >
       <script>
      $(window).on('load', function(){
        $('.loadingWrap .loadingImg').css("display","none");
       });
       </script>

       <script>
   		$(document).ready(function() {
			 $('#agent_list').select2();
   			$('.DisplayTable').DataTable({
   				"pageLength": 50,
   				"bPaginate": false,
           "order":[[0,"desc"]]
   			});
   			$("#from_date").datepicker({
   				maxDate: '0',
   				dateFormat: 'mm-dd-yy',
   				onSelect: function(dateStr) {
   					var min = $(this).datepicker('getDate') || new Date(); // Selected date or today if none
   					//var max = new Date(min.getTime());
   					$('#to_date').datepicker('option', {
   						minDate: min,
   						maxDate: 0
   					});
   				}
   			});
   			$("#to_date").datepicker({
   				maxDate: '0',
   				dateFormat: 'mm-dd-yy',
   				onSelect: function(dateStr) {
   					var max = $(this).datepicker('getDate') || new Date(); // Selected date or today if none
   					// var max = new Date(min.getTime());
   					$('#from_date').datepicker('option', {
   						maxDate: max
   					});
   				}
   			});
   			$(".DisplayTable tr").click(function() {
          var redirect_url = this.id;
          $('.loadingWrap .loadingImg').css("display","table"); // loader on
				$.urlParam = function(name,url){
					var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
					  if (results==null){
						 return null;
					  }
					  else{
						 return results[1] || 0;
					  }
				  }
				 var tr_date = $.urlParam('selected_date',this.id); // get the selected_date from url
         var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}


         var recording_file_en = $.urlParam('recording_file',this.id); // get the recording_file from url along with base64_encoded value
         var recordingfile = Base64.decode(recording_file_en); // decode the recordingfile
         var folder_split = tr_date.split("-"); // to split by '-' from 'tr_date' into date,month and year
						<?php  $path = asset('sound_files') ."/";?>
						var wav_url = "<?php echo $path;?>" + folder_split[0] + "/" + folder_split[1] + "/" + folder_split[2] + "/" + recordingfile;
				 $.ajax({
					url:wav_url,
					type:'HEAD',
					error: function()
					{
						//file not exists
            $('.loadingWrap .loadingImg').css("display","none"); // loader display none
						$('#myModal').modal('show'); // show model
					},
					success: function()
					{
          $('.loadingWrap .loadingImg').css("display","none"); // loader display none
          $(location).attr('href', redirect_url); // redirect page
					}
				});

   			});
			$('.switchBtn').click(function(){
				var inputWrap = $(this).children('input');
				if (inputWrap.is(':checked')) {
					inputWrap.attr('checked' , false);
					$(this).removeClass('IsChecked');
				}
				else
				{
					inputWrap.attr('checked' , true);
					$(this).addClass('IsChecked');
				}
			});

			/**************Back to Top****************/
			$(".backToTop").click(function () {
				$("html, body").animate({scrollTop: 0}, 1000);
			});
   		});
   		jQuery.browser = {};
   		(function() {
   			jQuery.browser.msie = false;
   			jQuery.browser.version = 0;
   			if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
   				jQuery.browser.msie = true;
   				jQuery.browser.version = RegExp.$1;
   			}
   		})();
</script>


   </head>
   <body class="Cover_Bg loadingWrap">
   <div class="backToTop topBack" > <i class="fa fa-angle-up" aria-hidden="true"></i> </div>
     <div class="loadingImg"> <span> <img src="<?php echo asset('img/loading.png'); ?>" /> </span> </div>
     <div id="header" class="header">
	<!-- begin container-fluid -->
		<div class="container-fluid">
			<div class="row">
			<div class="logoWrap col-4 col-sm-2">
				<a href="/dashboard" class="navbar-logo"> <img src="<?php echo asset('img/fullsizelogo.png'); ?>" /> </a>
			</div>

			<!-- begin header navigation right -->
			<div class="col-8 col-sm-10">
				<div class="userInfoBox dropdown navbar-user">
					<span class="user-image">


						</span>
						<span class="hidden-xs">{{ $res->first_name }}></span> <a href="/auth/signout" class="signout"><i class="fa fa-sign-out" aria-hidden="true"></i></a><b class="caret"></b>
				</div>
			</div>
			</div>
			<!-- end header navigation right -->
		</div>
			<!-- end container-fluid -->
	</div>

	<!--Header CLosED -->
      <div class="container">
         <div class="row">
            <main role="main" class="col-md-12 pt-3">
              <div class="container-fluid searcBar">
                  <h4>Search</h4>
                  <form method="get">
                     <section class="row search_btm">
                       <?php $role = session()->get('user_data') ['cl_group'];
                       $span='2';
                       if($role=="All"){ $span='1';?>
                        <div class="col-4 col-sm-2">
                          <div class="form-group">
                             <label for="pwd">Client Name: </label>
                             <select class="form-control" name="client_name" id="client_name">
                               <option value="All">All Clients</option>
                               <option value="Sprint" <?php if($client_name=="Sprint"){echo "selected";}?>>Sprint</option>
							    <option value="Noodle" <?php if($client_name=="Noodle"){echo "selected";}?>>Noodle</option>
                               <option value="Wayfair" <?php if($client_name=="Wayfair"){echo "selected";}?>>Wayfair</option>
                            </select>
                          </div>
                       </div>
                     <?php } ?>
                        <div class="col-4 col-sm-2">
                           <div class="form-group">
                              <label for="pwd">Agent Name:</label>
                              <select class="form-control" name="agent_id" id="agent_list">
                                <option value="">All Agents</option>
                                 @foreach ($all_agents as $agent)
                                 <option division="{!! $agent->division !!}" value="{!! $agent->eid !!}" <?php if($agent_id!="" && $agent_id==$agent->eid){echo "selected";}?>>{!! $agent->employee_name !!}</option>
                                  @endforeach

                              </select>
                           </div>
                        </div>
                        <div class="col-4 col-sm-<?php echo $span;?>">
                           <div class="form-group">
                              <label for="pwd">Min Date:</label>
                              <input name="from_date" id="from_date" value="<?php if(isset($from_date)) { echo date('m-d-Y',strtotime($from_date)); }else{ echo date('m-d-Y');}?>" type="text" class="form-control">
                           </div>
                        </div>
                        <div class="col-4 col-sm-<?php echo $span;?>">
                           <div class="form-group">
                              <label for="pwd">Max Date:</label>
                              <input name="to_date" id="to_date" value="<?php if(isset($to_date)) { echo date('m-d-Y',strtotime($to_date)); }else{ echo date('m-d-Y');}?>" type="text" class="form-control">
                           </div>
                        </div>
                        <div class="col-4 col-sm-2">
                  				<div class="form-group">
                  				  <label for="pwd">Customer's Phone Number:</label>
                  				  <input name="caller_ani" id="caller_ani" value="<?php if(isset($caller_ani)) { echo $caller_ani; }?>" type="text" class="form-control">
                  				</div>
                  			</div>
                  			<div class="col-4 col-sm-2">
                  				<div class="form-group switchBtnWrap">
									<label for="pwd">Search with Comments</label>
									<div class="switchBtn <?php if(isset($check_comment)) { if($check_comment=='Y'){ echo "IsChecked"; }}?>">
										<input type="checkbox" value="Y" <?php if(isset($check_comment)) { if($check_comment=='Y'){ echo "checked"; }}?> name="check_comment">
										<span class="slider round">
											<span class=""> <i class="fa fa-times" aria-hidden="true"></i> </span>
										</span>
									</div>
								</div>
                  			</div>
						   <div class="col-4 col-sm-<?php echo $span;?>">
                           <div class="form-group btn_SubmitWrap">
                              <input type="submit" class="btn btn-info" value="Search">
                           </div>
                        </div>
                     </section>
                  </form>

                  <div class="table-responsive">
                    <?php if(isset($_GET['from_date']) && count($results)>0){ ?>
                    <div class="alert alert-info center" role="alert">
                       Please click on the row to listen to the call.
                    </div>
                  <?php } ?>
                     <?php
                         if(isset($_GET['from_date']) && isset($_GET['to_date'])){
                        $querystringArray = ['from_date' => $_GET['from_date'], 'to_date' => $_GET['to_date']];
                        $results->appends($querystringArray);
                        ?>

                     <table class="table table-striped DisplayTable">
                        <thead>
                           <tr>
                              <th>Client</th>
                              <th>Call Date</th>
                              <th>Duration</th>
                              <th>Recording File</th>
                              <th>Account Code</th>
                              <th>Employee Name</th>
                              <th>Direction</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach ($results as $cdr)

                              <tr>
                              <td>{{ $cdr->calldate }}</td>
                              <td>{{ $cdr->duration }}</td>
                              <td>{!! $cdr->recordingfile !!}</td>
                              <td>{!! $cdr->accountcode !!}</td>
                              <td>{!! $cdr->employee_name !!}</td>
                              <td>{!! $cdr->direction !!}</td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     <div class="text-right">
                        {!! $results->render() !!}
                     </div>
                   <?php }else{ ?>
                     <table class="table table-striped DisplayTable">
                        <thead>
                           <tr>
                              <th>Client</th>
                              <th>Call Date</th>
                              <th>Duration</th>
                              <th>Recording File</th>
                              <th>Account Code</th>
                              <th>Employee Name</th>
                              <th>Direction</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                     </table>
                   <?php } ?>
                  </div>
               </div>
            </main>
         </div>
      </div>
	  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body center">
          <h4>Oops!</h4><p>Audio doesn't exist.</p>
        </div>
        <div class="modal-footer center">
          <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
  <script>
   		$(document).ready(function() {
			$( "#client_name" ).change(function() {
				var cl_name = $( "#client_name" ).val();
				if(cl_name !=''){
					$('.loadingWrap .loadingImg').css("display","table");
					$.getJSON("/dashboard/get_agents?client_name=" + cl_name, function(data) {
						var items = [];
						items.push("<option value=''>All Agents</option>");
						$.each(data, function(key, val) {
							items.push("<option division='"+val.division+"' value="+val.eid+">"+val.employee_name+"</option>");
						});
						$( "#agent_list" ).html(items);
						$('.loadingWrap .loadingImg').css("display","none");
						$('#agent_list').focus();
					});
				}
			});
			
			$( "#agent_list" ).change(function() {
				var client = $('option:selected', this).attr('division');
				var clsp = client.split(" ");
				var resclient = clsp[0];
				
				$('#client_name option[value="'+resclient+'"]').prop('selected', true);
			});
		});		
  </script>
   </body>
</html>
