<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;

$GLOBALAPIs         = [];
$testToken          = "";

class EvaluationGetWithCallIDTest extends TestCase
{
    
     function testClearCache()
    {
        shell_exec("php artisan config:cache");
        $this->assertTrue(true);
    }

    function getToken($username='EC0001', $password='secret')
    {
    GLOBAL $testToken;
        if(!$testToken || ($username && $password))
        {
            $response = $this->call
            (
                'POST',
                'api/v1.1/auth/token',
                ['username'=>$username, 'password'=>$password]
            );

            $token = $response->baseResponse->getContent();
            $token = json_decode($token);
            if(isset($token->token) && $token->token)
                $testToken = $token->token;
            else
                $testToken = "";
        }
        return $testToken;
    } 

function testGetCallsEvaluationsWithSuccessData(){
        GLOBAL $GLOBALAPIs;  
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/call/131b1713e16c7e404da7735291d1a434/evaluations',
            'inputs'    => [],
            "output"    => []
        ];
        $response = $this->call
        (
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], //files
            ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
        );
        $evaluation = $response->baseResponse->getContent();
        $evaluation = json_decode($evaluation);
        $testResult  = (isset($evaluation->count) && $evaluation->count>=0);
        $this->assertTrue($testResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = $testResult;
        $GLOBALAPIs[] = $request_data;
    }

    function testGetCallsEvaluationsWithFailureData(){
        GLOBAL $GLOBALAPIs;  
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/call/f01128c34521a88f7d9a02de279.4a66e/evaluations',
            'inputs'    => [],
            "output"    => []
        ];
        $response = $this->call
        (
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], //files
            ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
        );
        $evaluation = $response->baseResponse->getContent();
        $evaluation = json_decode($evaluation);
        $testResult  = isset($evaluation->errors);
        $this->assertTrue($testResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = $testResult;
        $GLOBALAPIs[] = $request_data;
    }

    function testGetCallsEvaluationsWithFailureData2(){
        GLOBAL $GLOBALAPIs;  
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/call/0/evaluations',
            'inputs'    => [],
            "output"    => []
        ];
        $response = $this->call
        (
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], //files
            ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
        );
        $evaluation = $response->baseResponse->getContent();
        $evaluation = json_decode($evaluation);
        $testResult  = isset($evaluation->errors);
        $this->assertTrue($testResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = $testResult;
        $GLOBALAPIs[] = $request_data;
    }

    function testGetCallsEvaluationsWithFailureMethod(){
        GLOBAL $GLOBALAPIs;  
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'PUT',
            'url'       => 'api/v1.1/call/f01128c34521a88f7d9a02de2794a66e/evaluations',
            'inputs'    => [],
            "output"    => []
        ];
        $response = $this->call
        (
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], //files
            ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
        );
        $evaluation = $response->baseResponse->getContent();
        $evaluation = json_decode($evaluation);
        $testResult  = (isset($evaluation[0]) && $evaluation[0]=="Method Not Allowed");
        $this->assertTrue($testResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = $testResult;
        $GLOBALAPIs[] = $request_data;
    }
public function testGenerateReport(){
    $is_phpunit_report = config('services.phpunit.report');
          
        if ($is_phpunit_report) {
        GLOBAL $GLOBALAPIs;
        $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                ['api'=>$GLOBALAPIs])]
                ];
        $context  = stream_context_create($opts);
        $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false,$context);
      
        $GLOBALAPIs        = [];
    }
     $this->assertTrue(true);
       
    }
}