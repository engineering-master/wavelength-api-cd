<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;

$GLOBALAPIs         = [];

class SiteTest extends TestCase
{
    /**
     * Get Auth token for access Api.
     *
     * @return void
     */
    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }
    /**
     * Get Auth token for access Api.
     *
     * @return void
     */
    public function getSite($token, $request = [])
    {
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'Get',
            'url'       => '/api/v1.1/sites',
            'inputs'    => [],
            "output"    => []
        ];

        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;

    }
    /**
     * Get Auth token for access Api.
     *
     * @return void
     */
    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }
    /**
     * Get Auth token for access Api.
     *
     * @return Array
     */
    public function getTestCaseSite()
    {
        $requests = [
            [
                'method'    => 'Get',
                'url'       => '/api/v1.1/sites',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => '/api/v1.1/sites',
                'inputs'    => []
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/sites',
                'inputs'    => []
            ]
        ];
        
        return $requests;

    }
    /**
     * Get Auth token for access Api.
     *
     * @return Array
     */
    public function getTestCaseAgent($site = '')
    {
        $requests = [
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/sites/agent',
                'inputs'    => []
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/sites/agent',
                'inputs'    => ['site' => '']
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/sites/agent',
                'inputs'    => ['site' => $site]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/sites/agent',
                'inputs'    => ['site' => $site]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/sites/agent',
                'inputs'    => ['site' => $site,'flag' => 'evaluation']
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/sites/agent',
                'inputs'    => ['site' => 'Arul Manickam']
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/sites/agent',
                'inputs'    => ['sites' => 'Arul Manickam']
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/sites/agent',
                'inputs'    => ['site' => 'Arul@']
            ],
            [
                'method'    => 'Get',
                'url'       => '/api/v1.1/sites/agent',
                'inputs'    => ['site' => $site]
            ]
        ];
        return $requests;
    }
    /**
     * A Admin User Get Sites BasicTest.
     *
     * @return void
     */
    public function testAdminSiteTest()
    {
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseSite();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $responseAgent->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }   
    }
    /**
     * A Admin User Get Sites BasicTest.
     *
     * @return void
     */
    public function testAdminSiteAgentTest()
    {
        GLOBAL $GLOBALAPIs;  
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $response = $this->getSite($token);
        if ($response->status()==200) {
            $this->assertTrue(true);
            $site = [];
            $content = $response->baseResponse->getContent();
            $content = json_decode($content, true);
            $site = implode(",", $content);
            $requests = $this->getTestCaseAgent($site);
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $responseAgent = $this->accessMethod($token, $request);
                if ($responseAgent->status()==200) {
                    $this->assertTrue(true);
                    $request['result'] = true;
                }
                else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $responseAgent->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }   
        }
    }
    /**
     * A Admin User Get Sites BasicTest.
     *
     * @return void
     */
    public function testSupervisorSiteTest()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0002','password'=>'secret'];
        $token = $this->getToken($credentials);
        $response = $this->getSite($token);

        if ($response->status()==200) {
            $this->assertTrue(true);
            $site = [];
            $content = $response->baseResponse->getContent();
            $content = json_decode($content, true);
            $site = implode(",", $content);
            $requests = $this->getTestCaseAgent($site);
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $responseAgent = $this->accessMethod($token, $request);
                if ($responseAgent->status()==200) {
                    $this->assertTrue(true);
                    $request['result'] = true;
                }
                else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $responseAgent->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;
            }
        }
    }
    
    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        // dd($is_phpunit_report);
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                    'method'  => 'POST',
                    'header'  => 'Content-Type: application/x-www-form-urlencoded',
                    'content' => http_build_query(
                        ['api'=>$GLOBALAPIs]
                    )]
                    ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }
}
