<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;

$GLOBALAPIs         = [];

class EnumTest extends TestCase
{
    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCaseUser()
    {
        $requests = [
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/enums',
                'inputs'    => []
            ],
             [
                'method'    => 'POST',
                'url'       => 'api/v1.1/enums',
                'inputs'    => []
             ]
            
           ];
        return $requests;
    }

    public function testEvaluationAdminEnum()
    {
        
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseUser();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $responseAgent->baseResponse->getContent();
            $content= json_decode($content);
            //dd($content);
            $request['output'] = json_encode($content);

            $GLOBALAPIs[] = $request;  
        }   
    }

    public function testEvaluationSuprevisorEnum()
    {
        
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0004','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseUser();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $responseAgent->baseResponse->getContent();
            $content= json_decode($content);
            //dd($content);
            $request['output'] = json_encode($content);

            $GLOBALAPIs[] = $request;  
        }   
    }

    public function testEvaluationAgentEnum()
    {
        
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0008','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseUser();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $responseAgent->baseResponse->getContent();
            $content= json_decode($content);
            //dd($content);
            $request['output'] = json_encode($content);

            $GLOBALAPIs[] = $request;  
        }   
    }

    public function testGenerateReport()
    {
          $is_phpunit_report = config('services.phpunit.report');
          
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $this->assertTrue(true);
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }

    
}

