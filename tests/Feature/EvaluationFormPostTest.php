<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use App\Http\Traits\PHPOffice;

$GL_DATA = [];

class EvaluationFormPostTest extends TestCase
{
    use PHPOffice;

    function getToken($username='EC0001', $password='secret')
    {
        GLOBAL $GL_DATA;
        if(!isset($GL_DATA['token']) || ($username && $password))
        {
            $response = $this->call
            (
                'POST',
                'api/v1.1/auth/token',
                ['username'=>$username, 'password'=>$password]
            );

            $token = $response->baseResponse->getContent();
            $token = json_decode($token);
            if(isset($token->token) && $token->token)
                $GL_DATA['token'] = $token->token;
            else
                $GL_DATA['token'] = "";
        }
        return $GL_DATA['token'];
    }
    
    function testEvaluationFormsPostWithSuccessInputs(){
        GLOBAL $GL_DATA;
        $all_request_data = [
            //case 1
            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [
                        "title"=>"Test Form - Form Builder ".date("Y-m-d H:i:s"),
                        "description"=>"This is a test form",
                        "headers"=>"[]",
                        "ratings"=>"[5]",
                        "version"=>"2.0",
                        "created_by"=>"",
                        "questiongroups"=>[
                        [
                            "groupname"=>"COMPLIANCE",
                            "groupdescription"=> "",
                            "groupmode"=>"test",
                            "percentage"=>"100",
                            "questions"=>[
                                [
                                    "questionid"=>200,
                                    "questiontitle"=>"OPENING",
                                    "questiontype"=>"",
                                    "rawvalues"=>"",
                                    "showcommentbox"=>1,
                                    "commentmandatory"=>1,
                                    "weightage"=>"",
                                    "score"=>["Yes"=>100,"No"=>0,"NA"=>100],
                                    "options"=>[
                                        "Yes",
                                        "No",
                                        "NA"
                                    ],
                                    "subgroups"=>[
                                    [
                                        "questionid"=>201,
                                        "questiontype"=>"radio",
                                        "questiontitle"=>"Use correct call opening greeting \/ verbiage",
                                        "parent_question_id"=>200,
                                        "showcommentbox"=>1,
                                        "commentmandatory"=>1,
                                        "weightage"=>"",
                                        "score"=>[
                                          "Yes"=>100,
                                          "No"=>0,
                                          "NA"=>100
                                        ],
                                        "options"=>[
                                          "Yes",
                                          "No",
                                          "NA"
                                        ]
                                    ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                "output"    => []
            ],

            //case 2
            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [
                        "title"=>"Test Form - Form Builder ".date("Y-m-d H:i:s"),
                        "description"=>"This is a test form",
                        "headers"=>"[]",
                        "ratings"=>"[5]",
                        "version"=>"2.0",
                        "created_by"=>"",
                        "questiongroups"=>[
                        [
                            "groupid"=>25,
                            "groupname"=>"COMPLIANCE",
                            "groupdescription"=> "",
                            "groupmode"=>"test",
                            "percentage"=>"100",
                            "questions"=>[
                                [
                                    "questionid"=>200,
                                    "questiontitle"=>"OPENING",
                                    "questiontype"=>"",
                                    "rawvalues"=>"",
                                    "showcommentbox"=>1,
                                    "commentmandatory"=>1,
                                    "weightage"=>"",
                                    "score"=>["Yes"=>100,"No"=>0,"NA"=>100],
                                    "options"=>[
                                    "Yes",
                                    "No",
                                    "NA"
                                ],
                                "subgroups"=>[
                                    [
                                        "questionid"=>201,
                                        "questiontype"=>"radio",
                                        "questiontitle"=>"Use correct call opening greeting \/ verbiage",
                                        "parent_question_id"=>200,
                                        "showcommentbox"=>1,
                                        "commentmandatory"=>1,
                                        "weightage"=>"",
                                        "score"=>[
                                          "Yes"=>100,
                                          "No"=>0,
                                          "NA"=>100
                                        ],
                                        "options"=>[
                                          "Yes",
                                          "No",
                                          "NA"
                                        ]
                                    ]
                                ]
                                ]
                            ]
                        ]
                    ]
                ],
                "output"    => []
            ],

        ];
        foreach($all_request_data as $request_data)
        {
            $response = $this->call
            (
                $request_data['method'],
                $request_data['url'],
                $request_data['inputs'],
                [], //cookies
                [], //files
                ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
            );

            $evaluation_forms = $response->baseResponse->getContent();
            $GL_DATA['evaluation_forms'] = $evaluation_forms = json_decode($evaluation_forms);
            $testResult  = (isset($evaluation_forms) && !isset($evaluation_forms->errors));
            $request_data['output'] = json_encode($evaluation_forms);
            $request_data['result'] = $testResult;
            $GL_DATA['apis'][] = $request_data;

            $this->assertTrue($testResult);
        }
    }
    
    function testEvaluationFormsPostWithFailureInputs(){
        GLOBAL $GL_DATA;
        $all_request_data = [
            //case 1
            [
                'name'      => __FUNCTION__,
                'method'    => 'LOCK',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [],
                "output"    => []
            ],

            //case 2
            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [
                        "title"=>"",
                        "description"=>"This is a test form",
                        "headers"=>"[]",
                        "ratings"=>"[5]",
                        "version"=>"2.0",
                        "created_by"=>"",
                        "questiongroups"=>[
                        [
                            "groupid"=>25,
                            "groupname"=>"COMPLIANCE",
                            "groupdescription"=> "",
                            "groupmode"=>"test",
                            "percentage"=>"100",
                            "questions"=>[
                                [
                                    "questionid"=>200,
                                    "questiontitle"=>"OPENING",
                                    "questiontype"=>"",
                                    "rawvalues"=>"",
                                    "showcommentbox"=>1,
                                    "commentmandatory"=>1,
                                    "weightage"=>"",
                                    "score"=>["Yes"=>100,"No"=>0,"NA"=>100],
                                    "options"=>[
                                    "Yes",
                                    "No",
                                    "NA"
                                ],
                                "subgroups"=>[
                                    [
                                        "questionid"=>201,
                                        "questiontype"=>"radio",
                                        "questiontitle"=>"Use correct call opening greeting \/ verbiage",
                                        "parent_question_id"=>200,
                                        "showcommentbox"=>1,
                                        "commentmandatory"=>1,
                                        "weightage"=>"",
                                        "score"=>[
                                          "Yes"=>100,
                                          "No"=>0,
                                          "NA"=>100
                                        ],
                                        "options"=>[
                                          "Yes",
                                          "No",
                                          "NA"
                                        ]
                                    ]
                                ]
                                ]
                            ]
                        ]
                    ]
                ],
                "output"    => []
            ],

            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [
                        "title"=>"T",
                        "description"=>"This is a test form",
                        "headers"=>"[]",
                        "ratings"=>"[5]",
                        "version"=>"2.0",
                        "created_by"=>"",
                        "questiongroups"=>[ ]
                ],
                "output"    => []
            ],

            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [
                        "title"=>"100+ test test test test test test test test test test test test test test test test test test test+++++",
                        "description"=>"This is a test form",
                        "headers"=>"[]",
                        "ratings"=>"[5]",
                        "version"=>"2.0",
                        "created_by"=>"",
                        "questiongroups"=>[]
                ],
                "output"    => []
            ],

            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [
                        "title"=>"Test form",
                        "description"=>"This is a test form",
                        //"headers"=>"[]",
                        "ratings"=>"[5]",
                        "version"=>"2.0",
                        "created_by"=>"",
                        "questiongroups"=>[]
                ],
                "output"    => []
            ],

            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [
                        "title"=>"Test form",
                        "description"=>"This is a test form",
                        "headers"=>"[]",
                        //"ratings"=>"[5]",
                        "version"=>"2.0",
                        "created_by"=>"",
                        "questiongroups"=>[]
                ],
                "output"    => []
            ],

            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [
                        "title"=>"Test form",
                        "description"=>"This is a test form",
                        "headers"=>"[]",
                        "ratings"=>"[5]",
                        //"version"=>"2.0",
                        "created_by"=>"",
                        "questiongroups"=>[]
                ],
                "output"    => []
            ],

            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform',
                'inputs'    => [
                        "title"=>"Test form",
                        "description"=>"This is a test form",
                        "headers"=>"[]",
                        "ratings"=>"[5]",
                        "version"=>"2.0",
                        "created_by"=>"",
                        //"questiongroups"=>[]
                ],
                "output"    => []
            ],
        ];
        foreach($all_request_data as $request_data)
        {
            $response = $this->call
            (
                $request_data['method'],
                $request_data['url'],
                $request_data['inputs'],
                [], //cookies
                [], //files
                ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
            );

            $evaluation_forms = $response->baseResponse->getContent();
            $GL_DATA['evaluation_forms'] = $evaluation_forms = json_decode($evaluation_forms);
            $testResult  = (
                isset($evaluation_forms->errors)
                ||
                (is_array($evaluation_forms) && in_array("Method Not Allowed", $evaluation_forms))
            );
            $request_data['output'] = json_encode($evaluation_forms);
            $request_data['result'] = $testResult;
            $GL_DATA['apis'][] = $request_data;

            $this->assertTrue($testResult);
        }
    }

    public function testGenerateReport(){
        GLOBAL $GL_DATA;
        if ( config('services.phpunit.report') ) {
            $response = $this->export($GL_DATA['apis'], "save,unittesting");
            $this->assertTrue( ($response['status_id']=='1') );
        }
        $this->assertTrue(true);
    }
}