<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Log;


class EvaluationSearchTest extends TestCase
{

    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }
    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }


    public function getTestCase()
    {

        $request = [
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => "41"]
                    ],

                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["call_id" => "131b1713e16c7e404da7735291d1a434"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["agent_ids" => "EC0012"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["evaluation_form_id" => 1]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["severity_id" => 3]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["severity_id" => "test"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["severity_id" => 123456]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["severity_id" => 3.3]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["severity_id" => null]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["disposition_code" => "NI"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["status_id" => "2"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["minscore" => 14]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["maxscore" => 95]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["skip" => 6]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["take" => 8]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => "41.234"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => ""]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => "73@"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => "test"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => null]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => "4411"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => "-41"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => "758741"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["call_id" => "f01128c34521a.88f7d9a02de2794a66e"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["call_id" => "f01128c34521a88f7d9DDDa02de2794a66e"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["call_id" => "142796559856484465"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["call_id" => "TEST"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["call_id" => "00000000000"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["call_id" => null]
                    ],
                    [
                     'method'    => 'Post',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["eval_id" => "41"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["agent_ids" => "ec0012"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["agent_ids" => "!@#"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["agent_ids" => "TEST"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["agent_ids" => "ec.1234"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["agent_ids" => "21321"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["agent_ids" => null]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["disposition_code" => "HUQWE"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["disposition_code" => "HU!@#"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["disposition_code" => "test"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["disposition_code" => "123456"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["disposition_code" => "@NI"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["customer_number" => "test"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["customer_number"=>"9045459986"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["customer_number"=>"+9045459986"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["customer_number"=>"12345678912345"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["customer_number"=>"1234-56789"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["customer_number"=>"(1)2089978193"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["customer_number"=>"001-2089978193"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ['evaluation_form_id' => '0']
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["maxscore" => "test123"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["minscore" => "test"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["skip" => "skip1234"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["skip" => "10.123"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["skip" => "10#@123"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["take" => "test"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["take" => "100.19"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["take" => "100@19"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["take" => "100@19"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ['from_date' => '2019-02-15',
                                     'to_date' => '2019-07-01']
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["status_id"=>"2"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["status_id"=>"0"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["status_id"=>null]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["status_id"=>"TEST"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["status_id"=>"2.9"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => ["status_id"=>"2@9"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => [
                                     "evaluation_form_id"=>1,
                                     'disposition'=>"HU",
                                     'customer_number'=>'6156745261',
                                     'from_date' => '2018-02-15',
                                     'to_date' => '2019-07-01',
                                     'final_score'=>10,
                                     'status_id'=>"2"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => [
                                     "evaluation_form_id"=>1,
                                     "minscore"=>"10",
                                     "maxscore"=>"70"]
                    ],     
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => [
                                     "minscore"=>"70",
                                     "maxscore"=>"10"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => [
                                     "sortordercolumn"=>"status_id",
                                     "sortorder"=>"desc"]
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/evaluations/search',
                     'inputs'    => [
                                     "take"=>100,
                                     "sortordercolumn"=>"desc",
                                     "sortorder"=>"status_id"]
                    ]
        ];

        return $request;
    }

    public function testEvaluationSearch()
    {

        GLOBAL $GLOBALAPIs;
        //Log::Info("testEvaluationSearch");
        $credentials = ['username'=>'EC0001','password'=>'secret'];
    
        $token = $this->getToken($credentials);
            $requests = $this->getTestCase();
        foreach ($requests as $key => $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                 $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request; 
            if($key>0 && ($key % 30)==0) {
                sleep(30);
            }
        }
    }
    

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' => [
            'method' => 'POST',
            'header' => 'Content-Type: application/x-www-form-urlencoded',
            'content' => http_build_query(
                ['api' => $GLOBALAPIs]
            )
            ]];
            $context = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
       
            $GLOBALAPIs = [];
        }
        $this -> assertTrue(true);
    }
}
