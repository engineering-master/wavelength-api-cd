<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;
use Symfony\Component\HttpFoundation\Response;

class EvaluationScoreTest extends TestCase
{
    /**
     * Get Auth token for access Api.
     *
     * @return void
     */
    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }
    /**
     * Get Auth token for access Api.
     *
     * @return Response
     */
    public function evaluationScore($token, $request = [])
    {
        Log::Info("evaluationScore");
        //$route = '/api/v1.1/evaluation/' . $evaluation_id . '/score';
        $server = [
            'HTTP_Authorization' => 'Bearer ' . $token,
            'CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ];
        $response = $this->call(
            $request['method'],
            $request['url'],
            [], //parameters
            [], //cookies
            [], // files
            $server,
            $json = json_encode($request['inputs']) // content
        );
        return $response;
    }

    
    /**
     * Get Auth token for access Api.
     *
     * @return Array
     */
    public function getTestCase()
    {
        $group_score = [ 
            [
                "groupid" => 25,
                "groupname" => "COMPLIANCE",
                "obscore" => 5
            ]
        ];
        $answers = [ 
            [
                "questionid" => 210,
                "answertext" => "YES",
                "body" => "Test Comment",
                "score" => 0
            ]
        ];
        
        $requests = [
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => ["group_score" => $group_score]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => ["answers" => $answers]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => ["ratings" => ""]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => ["final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => ["answers" => $answers,
                                "ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => ["group_score" => $group_score,
                                "ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => ["ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => ["group_score" => $group_score,
                                "answers" => $answers,
                                "ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/evaluation/22/score',
                'inputs'    => ["group_score" => $group_score,
                                "answers" => $answers,
                                "ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/12345/score',
                'inputs'    => ["group_score" => $group_score,
                                "answers" => $answers,
                                "ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/test/score',
                'inputs'    => ["group_score" => $group_score,
                                "answers" => $answers,
                                "ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/123 .11/score',
                'inputs'    => ["group_score" => $group_score,
                                "answers" => $answers,
                                "ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/000/score',
                'inputs'    => ["group_score" => $group_score,
                                "answers" => $answers,
                                "ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/123@/score',
                'inputs'    => ["group_score" => $group_score,
                                "answers" => $answers,
                                "ratings" => "",
                                "final_score" => 10]
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/evaluation/20/score',
                'inputs'    => ["group_score" => $group_score,
                                "answers" => $answers,
                                "ratings" => "",
                                "final_score" => 10]
            ]
        ];
        return $requests;
    }
    /**
     * A basic test BasicTest.
     *
     * @return void
     */
    public function testEvaluationScoreTest()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCase();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            //Log::Info(print_r($request, true));
            $response = $this->evaluationScore($token, $request);
            if ($response->status()==Response::HTTP_OK) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else if ($response->status()==Response::HTTP_UNPROCESSABLE_ENTITY) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
             $content = $response->baseResponse->getContent();
             $request['output'] = json_encode($content);
             $GLOBALAPIs[] = $request;  
        }
        
    }

    public function testGenerateReport(){
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
        GLOBAL $GLOBALAPIs;
        $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                ['api'=>$GLOBALAPIs])]
                ];
        $context  = stream_context_create($opts);
        $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false,$context);
        $GLOBALAPIs        = [];
    }
    $this->assertTrue(true);
    }
}
