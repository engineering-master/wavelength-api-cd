<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use App\Http\Traits\PHPOffice;

$GL_DATA = [];

class EvaluationFormsGetTest extends TestCase
{
    use PHPOffice;

    function getToken($username='EC0001', $password='secret')
    {
        GLOBAL $GL_DATA;
        if(!isset($GL_DATA['token']) || ($username && $password))
        {
            $response = $this->call
            (
                'POST',
                'api/v1.1/auth/token',
                ['username'=>$username, 'password'=>$password]
            );

            $token = $response->baseResponse->getContent();
            $token = json_decode($token);
            if(isset($token->token) && $token->token)
                $GL_DATA['token'] = $token->token;
            else
                $GL_DATA['token'] = "";
        }
        return $GL_DATA['token'];
    }

    function getEvaluationForms()
    {
        GLOBAL $GL_DATA;
        if(!isset($GL_DATA['EvaluationForms']))
        {
            $response = $this->call
            (
                "GET",
                "api/v1.1/evaluationforms",
                [], //inputs
                [], //cookies
                [], //files
                ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
            );

            $GL_DATA['EvaluationForms'] = $response->baseResponse->getContent();
            $GL_DATA['EvaluationForms'] = json_decode($GL_DATA['EvaluationForms']);
        }
        if(!count($GL_DATA['EvaluationForms']))
            die("No forms found. Please create atleast one form & procced further.");

        return $GL_DATA['EvaluationForms'];
    }
    
    function testEvaluationFormsGetWithSuccessInputs(){
        GLOBAL $GL_DATA;
        $all_request_data = [
            [
                'name'      => __FUNCTION__,
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluationforms',
                'inputs'    => [],
                "output"    => []
            ],[
                'name'      => __FUNCTION__,
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluationforms',
                'inputs'    => ["form_id"=>$this->getEvaluationForms()[0]->id],
                "output"    => []
            ]
        ];
        foreach($all_request_data as $request_data)
        {
            $response = $this->call
            (
                $request_data['method'],
                $request_data['url'],
                $request_data['inputs'],
                [], //cookies
                [], //files
                ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
            );

            $evaluation_forms = $response->baseResponse->getContent();
            $GL_DATA['evaluation_forms'] = $evaluation_forms = json_decode($evaluation_forms);
            $testResult  = (isset($evaluation_forms) && !isset($evaluation_forms->errors));
            $request_data['output'] = json_encode($evaluation_forms);
            $request_data['result'] = $testResult;
            $GL_DATA['apis'][] = $request_data;

            $this->assertTrue($testResult);
        }
    }
    
    function testEvaluationFormsGetWithFailureInputs(){
        GLOBAL $GL_DATA;
        $all_request_data = [
            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationforms',
                'inputs'    => [],
                "output"    => []
            ],[
                'name'      => __FUNCTION__,
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluationforms',
                'inputs'    => ["form_id"=>"0"],
                "output"    => []
            ]
        ];
        foreach($all_request_data as $request_data)
        {
            $response = $this->call
            (
                $request_data['method'],
                $request_data['url'],
                $request_data['inputs'],
                [], //cookies
                [], //files
                ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
            );

            $evaluation_forms = $response->baseResponse->getContent();
            $GL_DATA['evaluation_forms'] = $evaluation_forms = json_decode($evaluation_forms);
            $testResult  = (isset($evaluation_forms->errors) || in_array("Method Not Allowed", $evaluation_forms));
            $request_data['output'] = json_encode($evaluation_forms);
            $request_data['result'] = $testResult;
            $GL_DATA['apis'][] = $request_data;

            $this->assertTrue($testResult);
        }
    }

    public function testGenerateReport(){
        GLOBAL $GL_DATA;
        if ( config('services.phpunit.report') ) {
            $response = $this->export($GL_DATA['apis'], "save,unittesting");
            $this->assertTrue( ($response['status_id']=='1') );
        }
        $this->assertTrue(true);
    }
}