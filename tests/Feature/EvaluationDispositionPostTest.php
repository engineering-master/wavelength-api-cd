<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;
use DB;
$GLOBALAPIs         = [];

class EvaluationDispositionPostTest extends TestCase
{
    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function getDispositionPost($token, $request = [])
    {
        $request_data = [
           'name'      => __FUNCTION__,
           'method'    => 'Post',
           'url'       => 'api/v1.1/evaluation/16/disposition',
           'inputs'    => [],
           "output"    => []
        ];

        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;

    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCaseDispositionPost()
    {
        $requests = [
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>1, "evaluationdispositionid"=>3]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/17/disposition',
                'inputs'    => ["severityid"=>1, "evaluationdispositionid"=>3]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>1, "evaluationdispositionid"=>3]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/1645/disposition',
                'inputs'    => ["severityid"=>1, "evaluationdispositionid"=>3]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>4, "evaluationdispositionid"=>3]
            ],
             [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>1, "evaluationdispositionid"=>8]
             ],
             [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>3.875, "evaluationdispositionid"=>3]
             ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>1, "evaluationdispositionid"=>3.876]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>"@#$%", "evaluationdispositionid"=>3]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>1, "evaluationdispositionid"=>"@#$%"]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>"test", "evaluationdispositionid"=>3]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>1, "evaluationdispositionid"=>"test"]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>"", "evaluationdispositionid"=>3]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => ["severityid"=>1, "evaluationdispositionid"=>""]
            ],

            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/evaluation/16/disposition',
                'inputs'    => []
            ]
        ];
        
        return $requests;

    }

    public function deleteDispositionData()
    {
        $GLOBALEvaluation_id = 16;
        DB::table('evaluation_dispositions')->where('evaluations_id', $GLOBALEvaluation_id)->delete();
        $this->assertTrue(true);
    }

    public function testEvaluationDispositionPost()
    {
        
        GLOBAL $GLOBALAPIs; 
        $this->deleteDispositionData();
        $credentials = ['username'=>'EC0001','password'=>'secret'];
           $token = $this->getToken($credentials);
           $requests = $this->getTestCaseDispositionPost();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                   $this->assertTrue(true);
                   $request['result'] = true;
            } else {
                   $this->assertFalse(false);
                   $request['result'] = true;
            }
               $content = $responseAgent->baseResponse->getContent();
               $content= json_decode($content);
               $request['output'] = json_encode($content);
               $GLOBALAPIs[] = $request;  
        }
           
    }

    public function testGenerateReport()
    {
         $is_phpunit_report = config('services.phpunit.report');
          
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
       
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }


}
