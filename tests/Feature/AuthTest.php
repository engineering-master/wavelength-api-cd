<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;

$GLOBALAPIs         = [];
class AuthTest extends TestCase
{
    /**
     * Get Auth token for access Api.
     *
     * @return string
     */
    /*public function testToken($credentials = [])
    {
        if (empty($credentials)) {
            $credentials = [
                ['username'=>'EC0001', 'password'=>'secret'],
                ['username'=>'EC0002', 'password'=>'secret']
           ];
        }
        foreach ($credentials as $credential) {
            $response = $this->call(
                'POST',
                'api/v1.1/auth/token',
                $credentials
            );
            $token = $response->baseResponse->getContent();
            $token = json_decode($token, true);
            if (isset($token['token']) && $token['token']) {
                $this->assertTrue(true);
            } else {
                $this->assertFalse(false);
            }
        }
    }*/

    function testToken()
    {
        GLOBAL $GLOBALAPIs;  
        $credentials = [
            ['username'=>'EC0001', 'password'=>'secret'],// ADMIN
            ['username'=>'EC0002', 'password'=>'secret'],// SUPERVISIOR
            ['username'=>'EC0003', 'password'=>'secret'],// QA MANAGER
            ['username'=>'EC0004', 'password'=>'secret'],// QA SPECIALIST
            ['username'=>'EC0008', 'password'=>'secret'],// AGENT
            ['username'=>'0008', 'password'=>'secret'],
            ['username'=>'ECOOO1', 'password'=>'SECRET'],
            ['username'=>'EC_0001', 'password'=>'secret'],
            ['username'=>'EC0001', 'password'=>'se_cret'],
            ['username'=>'EC0002', 'password'=>'1234']
        ];
        foreach ($credentials as $key => $credential) {
            $request_data = [
                'name'      => __FUNCTION__,
                 'method'    => 'POST',
                 'url'       => 'api/v1.1/auth/token',
                 'inputs'    => $credential,
                 "output"    => []
            ];
            $response = $this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], //files
            [] 
            );
            if($response->status()==200){
                $evaluation = $response->baseResponse->getContent();
                $evaluation = json_decode($evaluation);
                $testResult  = isset($evaluation->token);
                $this-> meEndPoint($evaluation->token);
                $this->assertTrue($testResult);
                $request_data['output'] = json_encode($evaluation);
                $request_data['result'] = $testResult;
                $GLOBALAPIs[] = $request_data;
            } else {
                $evaluation = $response->baseResponse->getContent();
                $this->assertTrue($testResult);
                $request_data['output'] = json_encode($evaluation);
                $request_data['result'] = $evaluation;
                $GLOBALAPIs[] = $request_data;
            }
        }
        $this-> meEndPoint('WrongToken');
    }

    public function meEndPoint($token)
    {
        GLOBAL $GLOBALAPIs;  
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'Get',
            'url'       => '/api/v1.1/me',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        $evaluation = $response->baseResponse->getContent();
        $evaluation = json_decode($evaluation);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = $evaluation;
        $GLOBALAPIs[] = $request_data;
    }

    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCase()
    {
        $requests = [
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/me',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => '/api/v1.1/me',
                'inputs'    => []
            ],
            [
                'method'    => 'DELETE',
                'url'       => '/api/v1.1/me',
                'inputs'    => []
            ]
        ];
        
        return $requests;

    }

     public function testAdminMe()
    {
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCase();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $responseAgent->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }   
    }

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }
}
