<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use DB;

$eval_id            = 0;
$GLOBALAPIs         = [];
$testToken          = "";
$TransitionId       = 0;
$DropStatus         = 0;
$comment_id         = 0;

class EvaluationTransitionTest extends TestCase
{
    function getToken($username='EC0001', $password='secret')
    {
        GLOBAL $testToken;
        if(!$testToken || ($username && $password)) {
            $response = $this->call(
                'POST',
                'api/v1.1/auth/token',
                ['username'=>$username, 'password'=>$password]
            );
            $token = $response->baseResponse->getContent();
            $token = json_decode($token);
            if(isset($token->token) && $token->token) {
                $testToken = $token->token;
            } else {
                $testToken = "";
            }
        }
        return $testToken;
    }
    public function revertUserData($status_id=0)
    {
        GLOBAL $eval_id;
        DB::table('evaluations')->where('id', $eval_id)->update(['status_id' =>$status_id]);
        $this->assertTrue(true);
    }

    public function getEvaluationId($status_id=3, $eid=0)
    {   
        GLOBAL $eval_id;
        if($eid) {
            $eval_id = $eid;
        }
        if(isset($eval_id) && $eval_id) {
            return $eval_id;
        } else {
            $eval_id = DB::table('evaluations')->where('status_id', $status_id)->pluck('id')->first();
            return $eval_id;
        }
    }
    function testGetEvaluationsWithSuccessData($which_drop_status=0)
    {  
        $evaluation_id = $this->getEvaluationId();
        GLOBAL $GLOBALAPIs;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/evaluations/'.$evaluation_id,
            'inputs'    => [],
            "output"    => []
        ];
        $response = $this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], //files
            ['HTTP_Authorization' => 'Bearer '.$this->getToken()] // ['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
        );
        $evaluation = $response->baseResponse->getContent();
        $evaluation = json_decode($evaluation);
        $testResult  = isset($evaluation->evaluationcallresults[0]);
        $this->assertTrue($testResult);
        if($evaluation->evaluationcallresults[0]->status->id<3) {
            dd("The given evaluation must be in assigned status(status_id=1). Please provide valid evaluation id");
        }
        if($evaluation->evaluationcallresults[0]->status->id==10) {
            dd("This evaluation has been closed. Please proceed further with other evaluation");
        }
        if(!isset($evaluation->evaluationcallresults[0]->allowed_transition->DropStatus[$which_drop_status])) {
            dd("The drop status not found");
        }
        //get TransitionId & DropStatus dynamic values if $testResult==true
        if(isset($evaluation->evaluationcallresults[0]->allowed_transition)) {
            $TransitionId = $evaluation->evaluationcallresults[0]->allowed_transition->TransitionId;    
            $DropStatus = $evaluation->evaluationcallresults[0]->allowed_transition->DropStatus[$which_drop_status];
        }
        //Generate report
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = $testResult;
        $GLOBALAPIs[] = $request_data;
    }

    function testEvaluationCommentPostSuccess()
    {
        $evaluation_id = $this->getEvaluationId();
        GLOBAL $GLOBALAPIs;
        GLOBAL $comment_id;
        $data=['body'=>[["comment"=>"Next Step Has been processed"]]];
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$evaluation_id.'/comment',
            'inputs'    =>  $data,
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $comment= $response->baseResponse->getContent();
        $comment= json_decode($comment); 
        $tesResult  =isset($comment->id);
        $this->assertTrue($tesResult);
        if ($tesResult) {
            $comment_id = $comment->id;
        }
        $request_data['output'] = json_encode($comment);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }
    // This Transition is done by Acknowledging and closing the evaluations
    function testEvaluationTransitionSuccessId3()
    {
        GLOBAL $GLOBALAPIs; 
        $evaluation_id = $this->getEvaluationId();
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testEvaluationTransitionSuccess4()
    {
        GLOBAL $GLOBALAPIs; 
        $evaluation_id = $this->getEvaluationId();
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $this->testGetEvaluationsWithSuccessData();
        $this->testEvaluationCommentPostSuccess();
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation); 
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;
        $this->revertUserData(3);
    }
    // From here the transition would be Dispute has been escalted and rejected and evaluation has been closed
    function testEvaluationTransitionSuccess5()
    {
        GLOBAL $GLOBALAPIs; 
        $evaluation_id = $this->getEvaluationId();
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        //To get seconds drop status(1)
        $this->testGetEvaluationsWithSuccessData(1);
        $this->testEvaluationCommentPostSuccess();
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;   
    }
    function testEvaluationTransitionSuccess7()
    {
        GLOBAL $GLOBALAPIs; 
        $evaluation_id = $this->getEvaluationId();
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        
        //To get seconds drop status(1)
        $this->testGetEvaluationsWithSuccessData(1);
        $this->testEvaluationCommentPostSuccess();
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;  
    }

    function testEvaluationTransitionSuccess10()
    {
        GLOBAL $GLOBALAPIs; 
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $evaluation_id = $this->getEvaluationId();
        $this->testGetEvaluationsWithSuccessData();
        $this->testEvaluationCommentPostSuccess();

        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;
        $this->revertUserData(3);
    }
    // In this transition the dispute has been accepted and evaluation has been re-evaluated and evaluation has been closed
    function testEvaluationTransitionDisputeAccepted3()
    {
        GLOBAL $GLOBALAPIs; 
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $evaluation_id = $this->getEvaluationId();
        // To get seconds drop status(1)
        $this->testGetEvaluationsWithSuccessData(1);
        $this->testEvaluationCommentPostSuccess();
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data; 
    }

    function testEvaluationTransitionDisputeAccepted5()
    {
        GLOBAL $GLOBALAPIs; 
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $evaluation_id = $this->getEvaluationId();
        $this->testGetEvaluationsWithSuccessData();
        $this->testEvaluationCommentPostSuccess();
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        if(isset($evaluation->eval_id) && $evaluation->eval_id) {
            $this->getEvaluationId(0, $evaluation->eval_id);
        }
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;  
    }

    function testEvaluationTransitionDisputeAccepted6()
    {
        GLOBAL $GLOBALAPIs; 
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $Evaluation_id = $this->getEvaluationId();
        $this->testGetEvaluationsWithSuccessData();
        $this->testEvaluationCommentPostSuccess();
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$Evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;
    }

    function testEvaluationTransitionDisputeAccepted8()
    {
        GLOBAL $GLOBALAPIs; 
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $Evaluation_id = $this->getEvaluationId();
        // To get seconds drop status(1)
        $this->testGetEvaluationsWithSuccessData(1);
        $this->testEvaluationCommentPostSuccess();
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$Evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;
    }

    function testEvaluationTransitionDisputeAccepted9()
    {
        GLOBAL $GLOBALAPIs; 
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $Evaluation_id = $this->getEvaluationId();
        $this->testGetEvaluationsWithSuccessData();
        $this->testEvaluationCommentPostSuccess();
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$Evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;
    }

    function testEvaluationTransitionDisputeAccepted10()
    {
        GLOBAL $GLOBALAPIs; 
        GLOBAL $comment_id;
        GLOBAL $TransitionId;
        GLOBAL $DropStatus;
        $Evaluation_id = $this->getEvaluationId();
        $this->testGetEvaluationsWithSuccessData();
        $this->testEvaluationCommentPostSuccess();
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/'.$Evaluation_id.'/transition/'.$TransitionId,
            'inputs'    => ["status_id"=>$DropStatus,"comment_id"=>$comment_id],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =isset($evaluation->message);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;
    }

    public function getTokenFail($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }
    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCase()
    {
        $requests = [
            [
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluation/36/transition/7',
                'inputs'    => ["status_id"=>"10","comment_id"=>"1121"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/2200/transition/7',
                'inputs'    => ["status_id"=>"7","comment_id"=>"1124"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/7',
                'inputs'    => ["status_id"=>"4","comment_id"=>"1124"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3.3',
                'inputs'    => ["status_id"=>"4","comment_id"=>"1124"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/0',
                'inputs'    => ["status_id"=>"4","comment_id"=>"1124"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/test',
                'inputs'    => ["status_id"=>"4","comment_id"=>"1124"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3',
                'inputs'    => ["status_id"=>"4","comment_id"=>"test"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3',
                'inputs'    => ["status_id"=>"4","comment_id"=>null]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3',
                'inputs'    => ["status_id"=>"4","comment_id"=>0]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3',
                'inputs'    => ["status_id"=>"4","comment_id"=>"@#$%"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3',
                'inputs'    => ["status_id"=>"4","comment_id"=>"1121.254"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3',
                'inputs'    => ["status_id"=>0,"comment_id"=>"1121"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3',
                'inputs'    => ["status_id"=>"4.5","comment_id"=>"1121"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3',
                'inputs'    => ["status_id"=>null,"comment_id"=>"1121.254"]
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/33/transition/3',
                'inputs'    => ["status_id"=>null,"comment_id"=>null]
            ]
        ];

        return $requests;
    }

    public function testEvaluationTransitionFailureMethods()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        if ($token) {
            $requests = $this->getTestCase();
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $response = $this->accessMethod($token, $request);
                if ($response->status()==200) {
                     $this->assertTrue(true);
                    $request['result'] = true;
                } else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $response->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }
        }
    }

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' => [
                'method' => 'POST',
                'header' => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api' => $GLOBALAPIs]
                )
            ]];
            $context = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs = [];
        }
        $this -> assertTrue(true);
    }       
}
