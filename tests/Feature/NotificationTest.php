<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use DB;
use Log;

$GLOBALAPIs         = [];

class NotificationTest extends TestCase
{
    
    public function getToken($credentials)
    {
        
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token] // server
        );
        return $response;
    }

    public function getTestCase()
    {

        $requests = [
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notifications/latest',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notifications/oldest',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notifications/unread',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notifications/read',
                'inputs'    => []
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/notifications/read',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notifications/Read',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notifications/Un-Read',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notifications/Un-Read',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notifications/1234',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notifications/12.34',
                'inputs'    => []
            ]
        ];
        return $requests;
    }

    public function testNotificationAllAsAdmin()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];  
        $token = $this->getToken($credentials);
        if ($token) {
            $requests = $this->getTestCase();
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $response = $this->accessMethod($token, $request);
                if ($response->status()==200) {
                     $this->assertTrue(true);
                    $request['result'] = true;
                } else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $response->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }
        }
    }

    public function testNotificationAllAsAgent()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0008','password'=>'secret'];  
        $token = $this->getToken($credentials);
        if ($token) {
            $requests = $this->getTestCase();
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $response = $this->accessMethod($token, $request);
                if ($response->status()==200) {
                     $this->assertTrue(true);
                    $request['result'] = true;
                } else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $response->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }
        }
    }

    public function getNotificationTestCase($dim_user_id)
    {
        $notification = $this->getReadAsNull($dim_user_id);
        $requests = [
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notification/'.$notification.'',
                'inputs'    => []
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/notification/0032d5ab-5c0d-426f-878d-5bef8a45e22d',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notification/00f4fdd8-e6c9-4e18-9d05-be5594b6e6fsdf01',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notification/NULL',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notification/Test',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notification/0032d$%^5ab-5c0d-426f-878d-5bef8a4%^&5e22d',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/notification/1234567789',
                'inputs'    => []
            ]
        ];
        return $requests;
    }

    public function testGetNotificationAdmin()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        if ($token) {
            $requests = $this->getNotificationTestCase('EC0001');
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $response = $this->accessMethod($token, $request);
                if ($response->status()==200) {
                     $this->assertTrue(true);
                    $request['result'] = true;
                } else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $response->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }
        }
    }

    public function testGetNotificationAgent()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0008','password'=>'secret'];
        $token = $this->getToken($credentials);
        if ($token) {
            $requests = $this->getNotificationTestCase('EC0008');
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $response = $this->accessMethod($token, $request);
                if ($response->status()==200) {
                     $this->assertTrue(true);
                    $request['result'] = true;
                } else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $response->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }
        }
    }

    public function getReadAsNull($dim_user_id)
    { 
        $user = DB::table('users')->where('dim_user_id', $dim_user_id)->first();
        $notification = DB::table('notifications')->whereNull('read_at')->where('notifiable_id', $user->user_id)->first();
        return $notification->id;
    }

    public function getTestCaseReadAsNull($dim_user_id) 
    {
        $notification = $this->getReadAsNull($dim_user_id);
        $requests = [
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/notification/'.$notification.'/markasread',
                'inputs'    => []
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/notification/1ef07134-ae1d-4cea-ac83-680a5e9999a5/markasread',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/notification/ae1d-4cea-ac83-680a5e9999a5/markasread',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/notification/NULL/markasread',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/notification/1ef07134!ae1d@4cea-ac83%680a5e9999a5/markasread',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/notification/0000000-000-000000-0000000-000/markasread',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/notification/test/markasread',
                'inputs'    => []
            ]   
        ];
        return $requests;
    }

    public function testNotificationMarkAsReadWithNullAdmin()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseReadAsNull('EC0001');
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }
    }

    public function testNotificationMarkAsReadWithNullAgent()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0008','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseReadAsNull('EC0008');
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }
    }

    public function getReadAsNullForMultipleUser($dim_user_id)
    { 
        $user = DB::table('users')->where('dim_user_id', $dim_user_id)->first();
        $notification = DB::table('notifications')->whereNull('read_at')->where('notifiable_id', $user->user_id)->limit(2)->pluck('id')->toArray();
        $notification = implode(",", $notification);
        return $notification;
    }

    public function getTestCaseReadAsNullWithMultipleUser($dim_user_id) 
    {
        $notification = $this->getReadAsNullForMultipleUser($dim_user_id);
        $requests = [
           [
               'method'    => 'Put',
               'url'       => 'api/v1.1/notifications/'.$notification.'/markallasread',
               'inputs'    => []
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/notifications/0032d5ab-5c0d-426f-878d-5bef8a45e22d,04ede829-d075-4198-8a52-daec7d22a29e,/markallasread',
               'inputs'    => []
           ],
           [
               'method'    => 'Put',
               'url'       => 'api/v1.1/notifications/37brtdyhy0-403uytht7464-b5565457b-2c66c3324a29/markallasread',
               'inputs'    => []
           ],
           [
               'method'    => 'Put',
               'url'       => 'api/v1.1/notifications/NULL/markallasread',
               'inputs'    => []
           ],
           [
               'method'    => 'Put',
               'url'       => 'api/v1.1/notifications/Test/markallasread',
               'inputs'    => []
           ],
           [
               'method'    => 'Put',
               'url'       => 'api/v1.1/notifications/0032$d5ab-5c0d-426f-878d-5bef8a45e22d/markallasread',
               'inputs'    => []
           ],
           [
               'method'    => 'Put',
               'url'       => 'api/v1.1/notifications/1234567/markallasread',
               'inputs'    => []
           ],
           [
               'method'    => 'Put',
               'url'       => 'api/v1.1/notifications/0032d5ab.5c0d.426f.878d.5bef8a45e22d/markallasread',
               'inputs'    => []
           ]
        ];
        return $requests;
    }

    public function testNotificationMarkAsReadWithMultipleIdAsAdmin()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseReadAsNullWithMultipleUser('EC0001');
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }
    }

    public function testNotificationMarkAsReadWithMultipleIdAsAgent()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0008','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseReadAsNullWithMultipleUser('EC0008');
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }
    }

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }
}
