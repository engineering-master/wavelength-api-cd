<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;

$GLOBALAPIs         = [];

class EvaluationDispositionTest extends TestCase
{
    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function getDisposition($token, $request = [])
    {
        $request_data = [
           'name'      => __FUNCTION__,
           'method'    => 'Get',
           'url'       => 'api/v1.1/evaluation/disposition',
           'inputs'    => [],
           "output"    => []
        ];

        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;

    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCaseDisposition()
    {
        $requests = [
            [
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluation/disposition',
                'inputs'    => []
            ],
            [
                'method'    => 'PUT',
                'url'       => 'api/v1.1/evaluation/disposition/16',
                'inputs'    => []
            ],
            [
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluation/disposition',
                'inputs'    => []
            ],
            [
                'method'    => 'PATCH',
                'url'       => 'api/v1.1/evaluation/disposition/16',
                'inputs'    => []
            ],
            [
                'method'    => 'HEAD',
                'url'       => 'api/v1.1/evaluation/disposition/create',
                'inputs'    => []
            ],
            [
                'method'    => 'DELETE',
                'url'       => 'api/v1.1/evaluation/disposition/16',
                'inputs'    => []
            ],
            [
                'method'    => 'HEAD',
                'url'       => 'api/v1.1/evaluation/disposition/16/edit',
                'inputs'    => []
            ],
            [
                'method'    => 'HEAD',
                'url'       => 'api/v1.1/evaluation/disposition/16',
                'inputs'    => []
            ]
        ];
        
        return $requests;

    }

    public function testEvaluationDisposition()
    {
        
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0001','password'=>'secret'];
           $token = $this->getToken($credentials);
           $requests = $this->getTestCaseDisposition();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                   $this->assertTrue(true);
                   $request['result'] = true;
            } else {
                   $this->assertFalse(false);
                   $request['result'] = true;
            }
               $content = $responseAgent->baseResponse->getContent();
               $content= json_decode($content);
               $request['output'] = json_encode($content);
               $GLOBALAPIs[] = $request;  
        }
           
    }

    public function testGenerateReport()
    {
         $is_phpunit_report = config('services.phpunit.report');
          
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
       
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }




}
