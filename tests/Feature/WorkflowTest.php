<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;

$GLOBALAPIs         = [];

class WorkflowTest extends TestCase
{
    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function getWorkflow($token, $request = [])
    {
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'Post',
            'url'       => 'api/v1.1/workflow',
            'inputs'    => [],
            "output"    => []
        ];

        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;

    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCaseWorkflow()
    {
        $requests = [
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>"Evaluation" ,"start_date"=>"2019-08-21" ,"wf_details"=>"{\"1\":{\"TransitionId\":\"1\",\"PickUpStatus\":\"1\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"2\":{\"TransitionId\":\"2\",\"PickUpStatus\":\"2\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"3\":{\"TransitionId\":\"3\",\"PickUpStatus\":\"3\",\"DropStatus\":[\"4\",\"5\"],\"ButtonLabel\":[\"Acknowledge\",\"Dispute\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"4\":{\"TransitionId\":\"4\",\"PickUpStatus\":\"4\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"5\":{\"TransitionId\":\"5\",\"PickUpStatus\":\"5\",\"DropStatus\":[\"6\",\"7\"],\"ButtonLabel\":[\"Accept\",\"Reject\"],\"Roles\":[\"1\",\"6\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"6\":{\"TransitionId\":\"6\",\"PickUpStatus\":\"6\",\"DropStatus\":[\"8\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"7\":{\"TransitionId\":\"7\",\"PickUpStatus\":\"7\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"8\":{\"TransitionId\":\"8\",\"PickUpStatus\":\"8\",\"DropStatus\":[\"9\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"9\":{\"TransitionId\":\"9\",\"PickUpStatus\":\"9\",\"DropStatus\":[\"4\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}}}"]
           ],
           [
               'method'    => 'PATCH',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>"Evaluation" ,"start_date"=>"2019-08-21" ,"wf_details"=>"{\"1\":{\"TransitionId\":\"1\",\"PickUpStatus\":\"1\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"2\":{\"TransitionId\":\"2\",\"PickUpStatus\":\"2\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"3\":{\"TransitionId\":\"3\",\"PickUpStatus\":\"3\",\"DropStatus\":[\"4\",\"5\"],\"ButtonLabel\":[\"Acknowledge\",\"Dispute\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"4\":{\"TransitionId\":\"4\",\"PickUpStatus\":\"4\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"5\":{\"TransitionId\":\"5\",\"PickUpStatus\":\"5\",\"DropStatus\":[\"6\",\"7\"],\"ButtonLabel\":[\"Accept\",\"Reject\"],\"Roles\":[\"1\",\"6\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"6\":{\"TransitionId\":\"6\",\"PickUpStatus\":\"6\",\"DropStatus\":[\"8\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"7\":{\"TransitionId\":\"7\",\"PickUpStatus\":\"7\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"8\":{\"TransitionId\":\"8\",\"PickUpStatus\":\"8\",\"DropStatus\":[\"9\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"9\":{\"TransitionId\":\"9\",\"PickUpStatus\":\"9\",\"DropStatus\":[\"4\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}}}"]
           ],
           [
               'method'    => 'POST',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>123 ,"start_date"=>"2019-08-21" ,"wf_details"=>"{\"1\":{\"TransitionId\":\"1\",\"PickUpStatus\":\"1\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"2\":{\"TransitionId\":\"2\",\"PickUpStatus\":\"2\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"3\":{\"TransitionId\":\"3\",\"PickUpStatus\":\"3\",\"DropStatus\":[\"4\",\"5\"],\"ButtonLabel\":[\"Acknowledge\",\"Dispute\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"4\":{\"TransitionId\":\"4\",\"PickUpStatus\":\"4\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"5\":{\"TransitionId\":\"5\",\"PickUpStatus\":\"5\",\"DropStatus\":[\"6\",\"7\"],\"ButtonLabel\":[\"Accept\",\"Reject\"],\"Roles\":[\"1\",\"6\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"6\":{\"TransitionId\":\"6\",\"PickUpStatus\":\"6\",\"DropStatus\":[\"8\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"7\":{\"TransitionId\":\"7\",\"PickUpStatus\":\"7\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"8\":{\"TransitionId\":\"8\",\"PickUpStatus\":\"8\",\"DropStatus\":[\"9\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"9\":{\"TransitionId\":\"9\",\"PickUpStatus\":\"9\",\"DropStatus\":[\"4\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}}}"]
           ],
           [
               'method'    => 'POST',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>1.547 ,"start_date"=>"2019-08-21" ,"wf_details"=>"{\"1\":{\"TransitionId\":\"1\",\"PickUpStatus\":\"1\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"2\":{\"TransitionId\":\"2\",\"PickUpStatus\":\"2\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"3\":{\"TransitionId\":\"3\",\"PickUpStatus\":\"3\",\"DropStatus\":[\"4\",\"5\"],\"ButtonLabel\":[\"Acknowledge\",\"Dispute\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"4\":{\"TransitionId\":\"4\",\"PickUpStatus\":\"4\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"5\":{\"TransitionId\":\"5\",\"PickUpStatus\":\"5\",\"DropStatus\":[\"6\",\"7\"],\"ButtonLabel\":[\"Accept\",\"Reject\"],\"Roles\":[\"1\",\"6\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"6\":{\"TransitionId\":\"6\",\"PickUpStatus\":\"6\",\"DropStatus\":[\"8\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"7\":{\"TransitionId\":\"7\",\"PickUpStatus\":\"7\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"8\":{\"TransitionId\":\"8\",\"PickUpStatus\":\"8\",\"DropStatus\":[\"9\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"9\":{\"TransitionId\":\"9\",\"PickUpStatus\":\"9\",\"DropStatus\":[\"4\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}}}"]
           ],
           [
               'method'    => 'POST',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>"" ,"start_date"=>"2019-08-21" ,"wf_details"=>"{\"1\":{\"TransitionId\":\"1\",\"PickUpStatus\":\"1\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"2\":{\"TransitionId\":\"2\",\"PickUpStatus\":\"2\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"3\":{\"TransitionId\":\"3\",\"PickUpStatus\":\"3\",\"DropStatus\":[\"4\",\"5\"],\"ButtonLabel\":[\"Acknowledge\",\"Dispute\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"4\":{\"TransitionId\":\"4\",\"PickUpStatus\":\"4\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"5\":{\"TransitionId\":\"5\",\"PickUpStatus\":\"5\",\"DropStatus\":[\"6\",\"7\"],\"ButtonLabel\":[\"Accept\",\"Reject\"],\"Roles\":[\"1\",\"6\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"6\":{\"TransitionId\":\"6\",\"PickUpStatus\":\"6\",\"DropStatus\":[\"8\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"7\":{\"TransitionId\":\"7\",\"PickUpStatus\":\"7\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"8\":{\"TransitionId\":\"8\",\"PickUpStatus\":\"8\",\"DropStatus\":[\"9\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"9\":{\"TransitionId\":\"9\",\"PickUpStatus\":\"9\",\"DropStatus\":[\"4\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}}}"]
           ],
           [
               'method'    => 'POST',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>"Evaluation" ,"start_date"=>"@#$%" ,"wf_details"=>"{\"1\":{\"TransitionId\":\"1\",\"PickUpStatus\":\"1\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"2\":{\"TransitionId\":\"2\",\"PickUpStatus\":\"2\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"3\":{\"TransitionId\":\"3\",\"PickUpStatus\":\"3\",\"DropStatus\":[\"4\",\"5\"],\"ButtonLabel\":[\"Acknowledge\",\"Dispute\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"4\":{\"TransitionId\":\"4\",\"PickUpStatus\":\"4\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"5\":{\"TransitionId\":\"5\",\"PickUpStatus\":\"5\",\"DropStatus\":[\"6\",\"7\"],\"ButtonLabel\":[\"Accept\",\"Reject\"],\"Roles\":[\"1\",\"6\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"6\":{\"TransitionId\":\"6\",\"PickUpStatus\":\"6\",\"DropStatus\":[\"8\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"7\":{\"TransitionId\":\"7\",\"PickUpStatus\":\"7\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"8\":{\"TransitionId\":\"8\",\"PickUpStatus\":\"8\",\"DropStatus\":[\"9\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"9\":{\"TransitionId\":\"9\",\"PickUpStatus\":\"9\",\"DropStatus\":[\"4\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}}}"]
           ],
           [
               'method'    => 'POST',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>"Evaluation" ,"start_date"=>"test" ,"wf_details"=>"{\"1\":{\"TransitionId\":\"1\",\"PickUpStatus\":\"1\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"2\":{\"TransitionId\":\"2\",\"PickUpStatus\":\"2\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"3\":{\"TransitionId\":\"3\",\"PickUpStatus\":\"3\",\"DropStatus\":[\"4\",\"5\"],\"ButtonLabel\":[\"Acknowledge\",\"Dispute\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"4\":{\"TransitionId\":\"4\",\"PickUpStatus\":\"4\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"5\":{\"TransitionId\":\"5\",\"PickUpStatus\":\"5\",\"DropStatus\":[\"6\",\"7\"],\"ButtonLabel\":[\"Accept\",\"Reject\"],\"Roles\":[\"1\",\"6\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"6\":{\"TransitionId\":\"6\",\"PickUpStatus\":\"6\",\"DropStatus\":[\"8\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"7\":{\"TransitionId\":\"7\",\"PickUpStatus\":\"7\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"8\":{\"TransitionId\":\"8\",\"PickUpStatus\":\"8\",\"DropStatus\":[\"9\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"9\":{\"TransitionId\":\"9\",\"PickUpStatus\":\"9\",\"DropStatus\":[\"4\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}}}"]
           ],
           [
               'method'    => 'POST',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>"Evaluation" ,"start_date"=>"" ,"wf_details"=>"{\"1\":{\"TransitionId\":\"1\",\"PickUpStatus\":\"1\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"2\":{\"TransitionId\":\"2\",\"PickUpStatus\":\"2\",\"DropStatus\":[\"2\",\"3\"],\"ButtonLabel\":[\"In Progress\",\"Submit\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"3\":{\"TransitionId\":\"3\",\"PickUpStatus\":\"3\",\"DropStatus\":[\"4\",\"5\"],\"ButtonLabel\":[\"Acknowledge\",\"Dispute\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"4\":{\"TransitionId\":\"4\",\"PickUpStatus\":\"4\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"5\":{\"TransitionId\":\"5\",\"PickUpStatus\":\"5\",\"DropStatus\":[\"6\",\"7\"],\"ButtonLabel\":[\"Accept\",\"Reject\"],\"Roles\":[\"1\",\"6\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"6\":{\"TransitionId\":\"6\",\"PickUpStatus\":\"6\",\"DropStatus\":[\"8\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"7\":{\"TransitionId\":\"7\",\"PickUpStatus\":\"7\",\"DropStatus\":[\"10\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"2\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"8\":{\"TransitionId\":\"8\",\"PickUpStatus\":\"8\",\"DropStatus\":[\"9\"],\"ButtonLabel\":[\"Submit Revaluation\"],\"Roles\":[\"1\",\"6\",\"7\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}},\"9\":{\"TransitionId\":\"9\",\"PickUpStatus\":\"9\",\"DropStatus\":[\"4\"],\"ButtonLabel\":[\"Acknowledge\"],\"Roles\":[\"1\",\"8\"],\"EscalationPolicy\":{\"Threshold\":\"5\",\"NotifyTo\":[\"1\",\"2\"]}}}"]
           ],
            
           [
               'method'    => 'POST',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>"Evaluation" ,"start_date"=>"2019-08-21" ,"wf_details"=>""]
           ],
           [
               'method'    => 'POST',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description
                "=>"Evaluation" ,"start_date"=>"2019-08-21" ,"wf_details"=>"test"]
           ],
           [
               'method'    => 'POST',
               'url'       => 'api/v1.1/workflow',
               'inputs'    => ["description"=>"Evaluation" ,"start_date"=>"2019-08-21" ,"wf_details"=>"@#$%"]
           ],
           [
               'method'    => 'GET',
               'url'       => 'api/v1.1/workflow?status=1',
               'inputs'    => []
           ],
           [
               'method'    => 'PUT',
               'url'       => 'api/v1.1/workflow?status=1',
               'inputs'    => []
           ],
           [
               'method'    => 'GET',
               'url'       => 'api/v1.1/workflow?status=test',
               'inputs'    => []
           ],
           [
               'method'    => 'GET',
               'url'       => 'api/v1.1/workflow?status=#$%',
               'inputs'    => []
           ],
           [
               'method'    => 'GET',
               'url'       => 'api/v1.1/workflow?status=1.546',
               'inputs'    => []
           ],
           [
               'method'    => 'GET',
               'url'       => 'api/v1.1/workflow?status=',
               'inputs'    => []
           ],
            [
               'method'    => 'DELETE',
               'url'       => 'api/v1.1/workflow/1',
               'inputs'    => []
            ],
           [
               'method'    => 'GET',
               'url'       => 'api/v1.1/workflow/create',
               'inputs'    => []
           ],
           [
               'method'    => 'PUT',
               'url'       => 'api/v1.1/workflow/1',
               'inputs'    => []
           ],
            [
               'method'    => 'HEAD',
               'url'       => 'api/v1.1/workflow/1',
               'inputs'    => []
            ],
            [
               'method'    => 'GET',
               'url'       => 'api/v1.1/workflow/1/edit',
               'inputs'    => []
            ]




           
        ];
        
        return $requests;

    }


    public function testEvaluationDisposition()
    {
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseWorkflow();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $responseAgent->baseResponse->getContent();
            $content= json_decode($content);
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }
    }

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        // dd($is_phpunit_report);
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                    'method'  => 'POST',
                    'header'  => 'Content-Type: application/x-www-form-urlencoded',
                    'content' => http_build_query(
                        ['api'=>$GLOBALAPIs]
                    )]
                    ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }
}
