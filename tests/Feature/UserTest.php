<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;

$GLOBALAPIs         = [];

class UserTest extends TestCase
{
    function testClearCache()
    {
        shell_exec("php artisan config:cache");
        $this->assertTrue(true);
    }

    public function getToken($credentials)
    {

        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCaseUser()
    {
        $requests = [
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7',
                'inputs'    => []
            ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=19',
                'inputs'    => []
             ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=19.785',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=dsgsd',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=#$%@',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,7',
                'inputs'    => []
            ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7.547,2',
                'inputs'    => []
             ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7.$%#&,2',
                'inputs'    => []
             ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=$%#&,2',
                'inputs'    => []
             ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=test,2',
                'inputs'    => []
             ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=,2',
                'inputs'    => []
             ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2.578',
                'inputs'    => []
            ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2.@#$%',
                'inputs'    => []
             ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,@#$%',
                'inputs'    => []
            ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,test',
                'inputs'    => []
             ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,',
                'inputs'    => []
             ],
             [
                'method'    => 'POST',
                'url'       => 'api/v1.1/users?role_id=7',
                'inputs'    => []
             ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2,6',
                'inputs'    => []
            ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=6,2,6',
                'inputs'    => []
             ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=6,6,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2,6.457',
                'inputs'    => []
            ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2,6.@#$',
                'inputs'    => []
             ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2,@#$',
                'inputs'    => []
            ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2,test',
                'inputs'    => []
             ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2.745,6',
                'inputs'    => []
             ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2.@#$%,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,@#$%,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,test,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7.845,2,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7.@#$%,2,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=@#$%,2,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=test,7,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=,7,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,,6',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7,2,',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,1',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,1,7',
                'inputs'    => []
            ],
             [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,1,7.765',
                'inputs'    => []
             ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,1,7.@#$%',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,1,@#$%',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,1,test',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,1.456,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,1.@#$%,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,@#$%,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,test,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6.456,1,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6.@#$%,1,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,@#$%,1,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,test,1,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8.456,6,1,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8.@#$%,6,1,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=@#$%,6,1,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=test,6,1,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,1,',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,6,,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=8,,1,7',
                'inputs'    => []
            ],
            [
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=,6,1,7',
                'inputs'    => []
            ]

           
            
        ];
        return $requests;
    }

    public function testEvaluationUser()
    {
        
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseUser();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } 
            else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $responseAgent->baseResponse->getContent();
            $content= json_decode($content);
            //dd($content);
            $request['output'] = json_encode($content);

            $GLOBALAPIs[] = $request;  
        }   
    }

    public function testGenerateReport()
    {
          $is_phpunit_report = config('services.phpunit.report');
          
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $this->assertTrue(true);
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }
}
