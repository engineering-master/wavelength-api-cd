<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;
use DB;

$GLOBALAPIs         = [];

class CampaignSkillTest extends TestCase
{
    function testClearCache()
    {
        shell_exec("php artisan config:cache");
        $this->assertTrue(true);
    }

    public function getToken($credentials)
    {

        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = '';
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCaseCampaignSkill()
    {
        $name="campaignsgroups".rand();
        $name1="campaignsgroups".rand();
        $name2="campaignsgroups".rand();
        $requests = [
           [
               'method'    => 'Get',
               'url'       => 'api/v1.1/campaign/skills',
               'inputs'    => []
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/campaign/skills',
               'inputs'    => []
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/campaign/group/1/skill',
               'inputs'    => ["name"=>$name,"description"=>"campaign intake"]
           ],
           [
               'method'    => 'Get',
               'url'       => 'api/v1.1/campaign/group/1/skill',
               'inputs'    => ["name"=>"campaign group","description"=>"campaign intake"]
           ],
           [
               'method'    => 'Put',
               'url'       => 'api/v1.1/campaign/skill/1',
               'inputs'    => ["name"=>$name1,"description"=>"campaign intake"]
           ],
           [
               'method'    => 'Patch',
               'url'       => 'api/v1.1/campaign/skill/{id}',
               'inputs'    => ["name"=>"campaign group","description"=>"campaign intake"]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/campaign/group/1/skill',
               'inputs'    => ["name"=>123,"description"=>123]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/campaign/group/1/skill',
               'inputs'    => ["name"=>"","description"=>""]
           ],
               
        ];
        return $requests;
    }

    /**
     * Get Test Case get Disposition using Skill
     * {type}/{type_ids}/disposition
     * 
     * @return []
     */
    public function getTestCaseSkillDisposition($type, $type_ids)
    {
        $url = 'api/v1.1/campaign/'. $type . '/' . $type_ids . '/disposition';
        $requests = [
           [
               'method'    => 'Get',
               'url'       => $url,
               'inputs'    => []
           ],
           [
               'method'    => 'Post',
               'url'       => $url,
               'inputs'    => []
           ]
        ];
        return $requests;
    }

    public function testCampaignSkill()
    {
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseCampaignSkill();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $responseAgent->baseResponse->getContent();
            $content= json_decode($content);
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }   
    }

    public function testSkillDisposition()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requestSkill = $this->getTestCaseSkillDisposition('skill', '1,2,3');
        $requestGroup = $this->getTestCaseSkillDisposition('group', '1,2,3');
        $requests = array_merge($requestSkill, $requestGroup);
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $responseAgent = $this->accessMethod($token, $request);
            if ($responseAgent->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $responseAgent->baseResponse->getContent();
            $content= json_decode($content);
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }   
    }

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => http_build_query(
                ['api'=>$GLOBALAPIs]
            )]
            ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs        = [];
        }
           $this->assertTrue(true);
    }

}
