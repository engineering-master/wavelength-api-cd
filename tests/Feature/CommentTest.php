<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;

$GLOBALAPIs         = [];
$testToken          = "";
class CommentTest extends TestCase
{

    function getToken($username='EC0001', $password='secret')
    {
        GLOBAL $testToken;
        if(!$testToken || ($username && $password)) {
            $response = $this->call(
                'POST',
                'api/v1.1/auth/token',
                ['username'=>$username, 'password'=>$password]
            );

            $token = $response->baseResponse->getContent();
            $token = json_decode($token);
            if(isset($token->token) && $token->token) {
                $testToken = $token->token;
            } else {
                $testToken = "";
            }
        }
        return $testToken;
    }
    
    function testCommentSuccess()
    {
        GLOBAL $GLOBALAPIs;
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/comments/evaluation/16',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            [],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }


    function testCommentAsWrongMethodFailure()
    {
        GLOBAL $GLOBALAPIs;
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/comments/evaluation/41',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            [],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation[0])  && $evaluation[0]=="Method Not Allowed");
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testCommentIdAsDecimalFailure()
    {
        GLOBAL $GLOBALAPIs;
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/comments/evaluation/41.987',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            [],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation->errors));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testCommentIdAsCharacterFailure()
    {
        GLOBAL $GLOBALAPIs;
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/comments/evaluation/fhois',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            [],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation->errors));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testCommentIdAsWrongIdFailure()
    {
        GLOBAL $GLOBALAPIs;
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/comments/evaluation/132365',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            [],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation->message));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testCommentPostSuccess()
    {
        GLOBAL $GLOBALAPIs;
        $data=['body'=>[["comment"=>"Hello"],["comment"=>"Welcome"]]];
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/21/comment',
            'inputs'    =>  $data,
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testCommentPostMethodFailure()
    {
        GLOBAL $GLOBALAPIs;
        $data=['body'=>[["comment"=>"Hello"],["comment"=>"Welcome"]]];
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/evaluation/21/comment',
            'inputs'    =>  $data,
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testCommentPostEmptyFailure()
    {
        GLOBAL $GLOBALAPIs;
        
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/21/comment',
            'inputs'    =>  [],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            [],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation->errors));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testCommentPostNullSuccess()
    {
        GLOBAL $GLOBALAPIs;
        $data=['body'=>[["comment"=>""],["comment"=>""]]];
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/21/comment',
            'inputs'    =>  $data,
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testCommentPostApiFailure()
    {
        GLOBAL $GLOBALAPIs;
        $data=['body'=>[["comment"=>"Hello"],["comment"=>"Welcome"]]];
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/2176543/comment',
            'inputs'    =>  $data,
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation->message));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }

    function testCommentPostApiDecimalFailure()
    {
        GLOBAL $GLOBALAPIs;
        $data=['body'=>[["comment"=>"Hello"],["comment"=>"Welcome"]]];
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluation/21.678/comment',
            'inputs'    =>  $data,
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        $tesResult  =(isset($evaluation->errors));
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;

    }


    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
          
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
       
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }

}
