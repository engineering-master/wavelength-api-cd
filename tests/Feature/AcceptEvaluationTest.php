<?php
namespace Tests\Feature;

ini_set('memory_limit', -1);
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use DB;

$GLOBALAPIs         = [];

class AcceptEvaluationTest extends TestCase
{

    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getCallId()
    {
        $call_id = DB::table('fact_call_details')
            ->leftJoin('evaluations', 'evaluations.call_id', '=', 'fact_call_details.ucid')
            ->whereNULL('evaluations.call_id')->pluck('fact_call_details.ucid')->first();
        return $call_id;
    }

    public function getUserId($dim_user_id)
    {
        $user_id = DB::table('users')->where('dim_user_id', $dim_user_id)->pluck('user_id')->first();
        return $user_id;
    }

    public function getTestCaseForSuccess($dim_user_id)
    {
        $call_id = $this->getCallId();
        $user_id = $this->getUserId($dim_user_id);
        $request = [
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
                'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>$user_id]
            ],
            [
             'method'    => 'Get',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>$user_id]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/fff6e8d4f027.cd38908c77a30900ae95/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>$user_id]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>1.10,"assigned_to"=>$user_id]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>1000.67]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>"TEST","assigned_to"=>$user_id]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>"TEST"]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>"0@!","assigned_to"=>$user_id]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>"0@13"]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>null,"assigned_to"=>null]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>$user_id]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>0,"assigned_to"=>$user_id]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>0]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/0/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>$user_id]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/test/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>$user_id]
            ],

            [
             'method'    => 'Post',
             'url'       => 'api/v1.1/call/'.$call_id.'/evaluation',
             'inputs'    => ["evaluation_form_id"=>1,"assigned_to"=>5]
            ]
        ];
        return $request;
    }

    public function testAdminAcceptEvaluation()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseForSuccess('EC0001');
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }
    }

    public function testSuperVisorAcceptEvaluation()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0004','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseForSuccess('EC0004');
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }
    }


    /*public function getCredentials()
    {
        $credentials = [
            [ 'username'=>'EC0001', 'password'=>'secret'],
            // [ 'username'=>'EC0008', 'password'=>'secret']
        ];
        return $credentials;
    }

    public function testAcceptEvaluationSuccess(){
        $this->deleteCallData();
        GLOBAL $GLOBALAPIs;
        $credentials = $this->getCredentials();
        foreach ($credentials as $credential) {
            $token = $this->getToken($credential);
            $requests = $this->getTestCaseForSuccess();
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $response = $this->accessMethod($token, $request);
                if ($response->status()==200) {
                    $this->assertTrue(true);
                    $request['result'] = true;
                } else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $response->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }
        }
    }*/
    
    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' => [
                'method' => 'POST',
                'header' => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api' => $GLOBALAPIs]
                )
            ]];
            $context = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs = [];
        }
        $this -> assertTrue(true);
    }
}
