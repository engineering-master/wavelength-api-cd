<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use App\Http\Traits\PHPOffice;

$GL_DATA = [];

class EvaluationFormGetTest extends TestCase
{
    use PHPOffice;

    function getToken($username='EC0001', $password='secret')
    {
        GLOBAL $GL_DATA;
        if(!isset($GL_DATA['token']) || ($username && $password))
        {
            $response = $this->call
            (
                'POST',
                'api/v1.1/auth/token',
                ['username'=>$username, 'password'=>$password]
            );

            $token = $response->baseResponse->getContent();
            $token = json_decode($token);
            if(isset($token->token) && $token->token)
                $GL_DATA['token'] = $token->token;
            else
                $GL_DATA['token'] = "";
        }
        return $GL_DATA['token'];
    }

    function getEvaluationForms()
    {
        GLOBAL $GL_DATA;
        if(!isset($GL_DATA['EvaluationForms']))
        {
            $response = $this->call
            (
                "GET",
                "api/v1.1/evaluationforms",
                [], //inputs
                [], //cookies
                [], //files
                ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
            );

            $GL_DATA['EvaluationForms'] = $response->baseResponse->getContent();
            $GL_DATA['EvaluationForms'] = json_decode($GL_DATA['EvaluationForms']);
        }
        if(!count($GL_DATA['EvaluationForms']))
            die("No forms found. Please create atleast one form & procced further.");
        return $GL_DATA['EvaluationForms'];
    }
    
    function testEvaluationFormGetWithSuccessInputs(){
        GLOBAL $GL_DATA;
        $all_request_data = [
            [
                'name'      => __FUNCTION__,
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluationform/'.$this->getEvaluationForms()[0]->id,
                'inputs'    => [],
                "output"    => []
            ]
        ];
        foreach($all_request_data as $request_data)
        {
            $response = $this->call
            (
                $request_data['method'],
                $request_data['url'],
                $request_data['inputs'],
                [], //cookies
                [], //files
                ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
            );

            $evaluation_forms = $response->baseResponse->getContent();
            $GL_DATA['evaluation_forms'] = $evaluation_forms = json_decode($evaluation_forms);
            $testResult  = (isset($evaluation_forms) && !isset($evaluation_forms->errors));
            $request_data['output'] = json_encode($evaluation_forms);
            $request_data['result'] = $testResult;
            $GL_DATA['apis'][] = $request_data;

            $this->assertTrue($testResult);
        }
    }
    
    function testEvaluationFormGetWithFailureInputs(){
        GLOBAL $GL_DATA;
        $all_request_data = [
            [
                'name'      => __FUNCTION__,
                'method'    => 'POST',
                'url'       => 'api/v1.1/evaluationform/'.$this->getEvaluationForms()[0]->id,
                'inputs'    => [],
                "output"    => []
            ],[
                'name'      => __FUNCTION__,
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluationform/12345567890',
                'inputs'    => [],
                "output"    => []
            ],[
                'name'      => __FUNCTION__,
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluationform/-',
                'inputs'    => [],
                "output"    => []
            ],[
                'name'      => __FUNCTION__,
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluationform/0',
                'inputs'    => [],
                "output"    => []
            ],[
                'name'      => __FUNCTION__,
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluationform/0-!@#$%^&*()',
                'inputs'    => [],
                "output"    => []
            ]
        ];
        foreach($all_request_data as $request_data)
        {
            $response = $this->call
            (
                $request_data['method'],
                $request_data['url'],
                $request_data['inputs'],
                [], //cookies
                [], //files
                ['HTTP_Authorization' => 'Bearer '.$this->getToken()] ##['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
            );

            $evaluation_forms = $response->baseResponse->getContent();
            $GL_DATA['evaluation_forms'] = $evaluation_forms = json_decode($evaluation_forms);
            $testResult  = (
                isset($evaluation_forms->errors)
                ||
                (is_array($evaluation_forms) && in_array("Method Not Allowed", $evaluation_forms))
                ||
                (isset($evaluation_forms->status) && $evaluation_forms->status=='0')
            );
            $request_data['output'] = json_encode($evaluation_forms);
            $request_data['result'] = $testResult;
            $GL_DATA['apis'][] = $request_data;

            $this->assertTrue($testResult);
        }
    }

    public function testGenerateReport(){
        GLOBAL $GL_DATA;
        if ( config('services.phpunit.report') ) {
            $response = $this->export($GL_DATA['apis'], "save,unittesting");
            $this->assertTrue( ($response['status_id']=='1') );
        }
        $this->assertTrue(true);
    }
}