<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;
use DB;

$GLOBALAPIs         = [];

class CampaignGroupTest extends TestCase
{
    function testClearCache()
    {
        shell_exec("php artisan config:cache");
        $this->assertTrue(true);
    }

    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = '';
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCaseCampaignGroup()
    {
        $name="campaignsgroups".rand();
        $name1="campaignsgroups".rand();
        $name2="campaignsgroups".rand();
        $requests = [
          [
              'method'    => 'Get',
              'url'       => 'api/v1.1/campaign/groups',
              'inputs'    => []
          ],
          [
              'method'    => 'Post',
              'url'       => 'api/v1.1/campaign/groups',
              'inputs'    => []
          ],
          [
              'method'    => 'Post',
              'url'       => 'api/v1.1/campaign/group',
              'inputs'    => ["name"=>$name,"description"=>"campaign intake"]
          ],
          [
              'method'    => 'Get',
              'url'       => 'api/v1.1/campaign/group',
              'inputs'    => ["name"=>"campaignsgroups","description"=>"campaign intake"]
          ],
          [
              'method'    => 'Put',
              'url'       => 'api/v1.1/campaign/group/1',
              'inputs'    => ["name"=>$name1,"description"=>"campaign intake"]
          ],
          [
              'method'    => 'Patch',
              'url'       => 'api/v1.1/campaign/group/1',
              'inputs'    => ["name"=>"campaignnhjgroup","description"=>"campaign intake"]
          ],
          [
              'method'    => 'Post',
              'url'       => 'api/v1.1/campaign/group/1/form/1',
              'inputs'    => []
          ],
          [
            'method'    => 'Post',
            'url'       => 'api/v1.1/campaign/skill/1/form/1',
            'inputs'    => []
          ],
          [
              'method'    => 'Get',
              'url'       => 'api/v1.1/campaign/group/1/form/1',
              'inputs'    => []
          ],
          [
              'method'    => 'Post',
              'url'       => 'api/v1.1/campaign/group',
              'inputs'    => ["name"=>123,"description"=>"campaign intake"]
          ],
          [
              'method'    => 'Post',
              'url'       => 'api/v1.1/campaign/group',
              'inputs'    => ["name"=>$name2,"description"=>123]
          ],
          [
              'method'    => 'Post',
              'url'       => 'api/v1.1/campaign/group',
              'inputs'    => ["name"=>"","description"=>""]
          ],
        ];
        return $requests;
    }

    
    /**
     * Get Test Case get Disposition using Skill
     * group/{group_ids}/skills
     * 
     * @return []
     */
    public function getTestCaseCampaignSkill($group_ids)
    {
        $url = 'api/v1.1/campaign/group/' . $group_ids . '/skills';
        $requests = [
           [
               'method'    => 'Get',
               'url'       => $url,
               'inputs'    => []
           ],
           [
               'method'    => 'Post',
               'url'       => $url,
               'inputs'    => []
           ]
        ];
        return $requests;
    }

    /**
     * Get Test Case get Disposition using Skill
     * {type}/{type_ids}/forms
     * 
     * @return []
     */
    public function getTestCaseCampaignForm($type, $type_ids)
    {
        $url = 'api/v1.1/campaign/'. $type . '/' . $type_ids . '/forms';
        $requests = [
           [
               'method'    => 'Get',
               'url'       => $url,
               'inputs'    => []
           ],
           [
               'method'    => 'Post',
               'url'       => $url,
               'inputs'    => []
           ]
        ];
        return $requests;
    }

    public function testCampaignGroup()
    {
        GLOBAL $GLOBALAPIs; 
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseCampaignGroup();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            //Log::Info($token);
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                //Log::Info("200");
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                //Log::Info("else");
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $content= json_decode($content);
            $request['output'] = json_encode($content);

            $GLOBALAPIs[] = $request;  
        }
    }

    public function testCampaignSkill()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requests = $this->getTestCaseCampaignSkill('1,2,3');
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $content= json_decode($content);
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }   
    }

    public function testCampaignForm()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        $requestSkill = $this->getTestCaseCampaignForm('skill', '1,2,3');
        $requestGroup = $this->getTestCaseCampaignForm('group', '1,2');
        $requests = array_merge($requestSkill, $requestGroup);
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);
            if ($response->status()==200) {
                $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $content= json_decode($content);
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }   
    }

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => http_build_query(
                ['api'=>$GLOBALAPIs]
            )]
            ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs        = [];
        }
           $this->assertTrue(true);
    }

}
