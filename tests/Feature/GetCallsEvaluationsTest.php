<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use Log;

$GLOBALAPIs         = [];

class GetCallsEvaluationsTest extends TestCase
{
    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }

    public function getTestCase()
    {

        $request = [
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/call/608b6c8ff60d621a90f7eba9c9e1c3a7/evaluations',
                     'inputs'    => []
                    ],
                    [
                     'method'    => 'Post',
                     'url'       => 'api/v1.1/call/608b6c8ff60d621a90f7eba9c9e1c3a7/evaluations',
                     'inputs'    => []
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/call/608b6c8ff60d621a.90f7eba9c9e1c3a7/evaluations',
                     'inputs'    => []
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/call/608b6c8ff60d621a#@$90f7eba9c9e1c3a7/evaluations',
                     'inputs'    => []
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/call/00/evaluations',
                     'inputs'    => []
                    ],
                    [
                     'method'    => 'Get',
                     'url'       => 'api/v1.1/call/test/evaluations',
                     'inputs'    => []
                    ],

                 ];

        return $request;
    }

    public function testCallEvaluation()
    {

        GLOBAL $GLOBALAPIs;
        //Log::Info("testEvaluationSearch");
        $credentials = ['username'=>'EC0001','password'=>'secret'];
    
        $token = $this->getToken($credentials);
            $requests = $this->getTestCase();
        foreach ($requests as $request) {
            $request['name'] = __FUNCTION__;
            $response = $this->accessMethod($token, $request);

            if ($response->status()==200) {
                 $this->assertTrue(true);
                $request['result'] = true;
            } else {
                $this->assertFalse(false);
                $request['result'] = true;
            }
            $content = $response->baseResponse->getContent();
            $request['output'] = json_encode($content);
            $GLOBALAPIs[] = $request;  
        }
    }
    

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' => [
            'method' => 'POST',
            'header' => 'Content-Type: application/x-www-form-urlencoded',
            'content' => http_build_query(
                ['api' => $GLOBALAPIs]
            )
            ]];
            $context = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
       
            $GLOBALAPIs = [];
        }
        $this -> assertTrue(true);
    }
}

