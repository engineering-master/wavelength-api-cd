<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use DB;

$GLOBALAPIs         = [];

class AllowDisputeTest extends TestCase
{
	public function getToken($credentials){
        
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]// server
        );
        return $response;
    }

    public function getTestCaseSuccess() {
        $requests = [
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/evaluation/68/allowdispute',
                'inputs'    => []
            ]
        ];
        return $requests;
    }

    public function getCredentials()
    {
        $credentials = [
        	[ 'username'=>'EC0001', 'password'=>'secret'],
            [ 'username'=>'prabu', 'password'=>'secret']
            
        ];
        return $credentials;
    }


    public function updateAllowDisputeAsFalse()
    {
        $id = "68";
        DB::table('evaluations')->where('id',$id)->update(['allow_dispute'=> 'False']);
        $this->assertTrue(true);
    }

    public function testAllowDisputeSuccess()
    {
        GLOBAL $GLOBALAPIs;
        $this->updateAllowDisputeAsFalse();
        $credentials = $this-> getCredentials();
        foreach ($credentials as $credential) {
        	$token = $this->getToken($credential);
        	if ($token) {
            	$requests = $this->getTestCaseSuccess();
            	foreach ($requests as $request) {
                	$request['name'] = __FUNCTION__;
                	$response = $this->accessMethod($token, $request);
                		if ($response->status()==200) {
                     	$this->assertTrue(true);
                    	$request['result'] = true;
                	} else {
                    	$this->assertFalse(false);
                    	$request['result'] = true;
                	}
                	$content = $response->baseResponse->getContent();
                	$request['output'] = json_encode($content);
                	$GLOBALAPIs[] = $request;  
            	}
			}
        }
    }

    public function getTestCaseFailure() {
        $requests = [
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/evaluation/68/allowdispute',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/evaluation/6.8/allowdispute',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/evaluation/20/allowdispute',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/evaluation/0/allowdispute',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/evaluation/test/allowdispute',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/evaluation/NULL/allowdispute',
                'inputs'    => []
            ],
            [
                'method'    => 'Put',
                'url'       => 'api/v1.1/evaluation/6*^8/allowdispute',
                'inputs'    => []
            ]

        ];
        return $requests;
    }

    public function testAllowDisputeFailure()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        
        $token = $this->getToken($credentials);
        if ($token) {
            $requests = $this->getTestCaseFailure();
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $response = $this->accessMethod($token, $request);
                if ($response->status()==200) {
                     $this->assertTrue(true);
                    $request['result'] = true;
                } else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $response->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }

        }

    }

    public function testGenerateReport(){
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
        GLOBAL $GLOBALAPIs;
        $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                ['api'=>$GLOBALAPIs])]
                ];
        $context  = stream_context_create($opts);
        $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false,$context);
        $GLOBALAPIs        = [];
    }
    $this->assertTrue(true);
    }
}