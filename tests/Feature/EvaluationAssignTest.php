<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;
use DB;


$GLOBALAPIs         = [];

class EvaluationAssignTest extends TestCase
{
    function testClearCache()
    {
        shell_exec("php artisan config:cache");
        $this->assertTrue(true);
    }

    public function getToken($credentials)
    {

        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } 
        else {
            return "";
        }
    }

    public function accessMethod($token, $request = [])
    {
        $response = '';
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]
        );
        return $response;
    }
    public function getEvaluationbystatus($status_id=0)
    {
        $evaluations = DB::table('evaluations')->where('status_id', $status_id)->first();
        return $evaluations->id;
    }

    public function getTestCaseAssign($e_id='')
    {
        $evaluation = $this->getEvaluationbystatus(8);
        $requests = [
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/'.$evaluation.'/assign',
               'inputs'    => ["user_id"=>$e_id]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/18/assign',
               'inputs'    => ["user_id"=>5]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/19/assign',
               'inputs'    => ["user_id"=>5.09]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/19/assign',
               'inputs'    => ["user_id"=>"test"]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/19/assign',
               'inputs'    => ["user_id"=>"@#$%&"]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/19/assign',
               'inputs'    => ["user_id"=>5578965]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/19.000/assign',
               'inputs'    => ["user_id"=>5]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/1965432/assign',
               'inputs'    => ["user_id"=>5]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/test/assign',
               'inputs'    => ["user_id"=>5]
           ],
           [
               'method'    => 'Post',
               'url'       => 'api/v1.1/evaluation/19/assign',
               'inputs'    => ["user_id"=>""]
           ],
           [
               'method'    => 'Get',
               'url'       => 'api/v1.1/evaluation/19/assign',
               'inputs'    => ["user_id"=>5]
           ]
            
        ];
        return $requests;
    }

    /*public function testEvaluationAssign()
    {
        
        GLOBAL $GLOBALAPIs;
        $credentials = $this->getCredentialsForAssign();
        foreach ($credentials as $credential) {
            $token = '';
            $token = $this->getToken($credential);
            if ($token) {
                $requests = $this->getTestCaseAssign();
                foreach ($requests as $request) {
                    $request['name'] = __FUNCTION__;
                    $responseAgent = $this->accessMethod($token, $request);
                    if ($responseAgent->status()==200) {
                        $this->assertTrue(true);
                        $request['result'] = true;
                    } else {
                        $this->assertFalse(false);
                        $request['result'] = true;
                    }
                    $content = $responseAgent->baseResponse->getContent();
                    $content = json_decode($content);
                    Log::Info(print_r($content, true));
                    $request['output'] = json_encode($content);
                    $GLOBALAPIs[] = $request;  
                }
            }
        }   
    } */
        
    public function testAdminEvaluationAssign()
    {
        
        GLOBAL $GLOBALAPIs; 
        $credential = ['username'=>'EC0001', 'password'=>'secret'];
        $token = $this->getToken($credential);
        if ($token) {
                $request_data = [
                'name'      => __FUNCTION__,
                'method'    => 'Get',
                'url'       => 'api/v1.1/users?role_id=7',
                'inputs'    => [],
                "output"    => []
                ];
                $response = $this->accessMethod($token, $request_data);
                if ($response->status()==200) {
                    $this->assertTrue(true);
                    $users = [];
                    $content = $response->baseResponse->getContent();
                    $content = json_decode($content, true);
                    $e_ids = array_column($content, 'eid');
                    $requests = $this->getTestCaseAssign($e_ids[0]);
                    foreach ($requests as $request) {
                        $request['name'] = __FUNCTION__;
                        $response = $this->accessMethod($token, $request);
                        if ($response->status()==200) {
                            $this->assertTrue(true);
                            $request['result'] = true;
                        } else {
                            $this->assertFalse(false);
                            $request['result'] = true;
                        }
                        $content = $response->baseResponse->getContent();
                        $content = json_decode($content);
                        $request['output'] = json_encode($content);
                        $GLOBALAPIs[] = $request;  
                    }
                }
        }
    }

    public function testSupervisorEvaluationAssign()
    {
        
        GLOBAL $GLOBALAPIs; 
        $credential = ['username'=>'EC0004', 'password'=>'secret'];
        $token = $this->getToken($credential);
        if ($token) {
            $requests = $this->getTestCaseAssign();
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $response = $this->accessMethod($token, $request);
                if ($response->status()==200) {
                    $this->assertTrue(true);
                    $request['result'] = true;
                } else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $response->baseResponse->getContent();
                $content = json_decode($content);
                //Log::Info(print_r($content, true));
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }
        }
    }
    
    /*public function getCredentialsForAssign()
    {
        $credentials = [
            ['username'=>'prabu', 'password'=>'secret'],
            ['username'=>'EC0004', 'password'=>'secret']
        ];
        return $credentials;
    }*/

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
            'method'  => 'POST',
            'header'  => 'Content-Type: application/x-www-form-urlencoded',
            'content' => http_build_query(
                ['api'=>$GLOBALAPIs]
            )]
            ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs        = [];
        }
            $this->assertTrue(true);
    }

}
