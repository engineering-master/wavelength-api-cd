<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;

$GLOBALAPIs         = [];

class ShowEvaluationsTest extends TestCase
{

    public function getToken($credentials)
    {
        
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }
    
    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token]// server
        );
        return $response;
    }

    public function getTestCase()
    {
        $requests = [
            [
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluations/41',
                'inputs'    => []
            ],
            [
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluations/75',
                'inputs'    => [
                                "call_id"=>"6f3ba9551f6ae1d5bd0f8f83a596b885",
                                "agent_ids"=>"EC0012",
                                "status_id"=>"10",
                                "evaluation_form_id"=>1,
                                "customer_number"=>"9548738081",
                                "minscore" =>"70",
                                "maxscore" => "90",
                                "from_date"=>"2019-07-25 15:13:45",
                                "to_date"=>"2019-08-25 15:13:45",
                                "skip"=>0,
                                "take"=>25,
                                "total"=>0,
                                "sortordercolumn"=>"createduser.name",
                                "sortorder"=>"desc"
                                ]
            ],
            [
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluations/75',
                'inputs'    => [
                                "status_id"=>"4",
                                ]
            ],
            [
                'method'    => 'Post',
                'url'       => 'api/v1.1/evaluations/44',
                'inputs'    => []
            ],
            [
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluations/4100000',
                'inputs'    => []
            ],
            [
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluations/4.4',
                'inputs'    => []
            ],
            [
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluations/0',
                'inputs'    => []
            ],
            [
                'method'    => 'GET',
                'url'       => 'api/v1.1/evaluations/75',
                'inputs'    => [
                                "call_id"=>"6f3ba9551f6ae1d5bd0f8f83a596b885",
                                "agent_ids"=>"EC0012",
                                "status_id"=>"10",
                                "evaluation_form_id"=>1,
                                "customer_number"=>"9548738081",
                                "minscore" =>"70",
                                "maxscore" => "90",
                                "from_date"=>"2019-07-25 15:13:45",
                                "to_date"=>"2019-08-25 15:13:45",
                                "skip"=>0,
                                "take"=>25,
                                "total"=>0,
                                "sortordercolumn"=>"createduser.name",
                                "sortorder"=>"desc"
                                ]
            ]

        ];
        return $requests;
    }

    public function getCredentials()
    {
        $credentials = [
            // ['username'=>'EC0001', 'password'=>'secret'],
            ['username'=>'EC0004', 'password'=>'secret']
        ];
        return $credentials;
    }

    public function testShowEvaluation()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = $this->getCredentials();
        foreach ($credentials as $credential) {
            $token = $this->getToken($credential);
            if ($token) {
                $requests = $this->getTestCase();
                foreach ($requests as $request) {
                    $request['name'] = __FUNCTION__;
                    $response = $this->accessMethod($token, $request);
                    if ($response->status()==200) {
                         $this->assertTrue(true);
                        $request['result'] = true;
                    } else {
                        $this->assertFalse(false);
                        $request['result'] = true;
                    }
                    $content = $response->baseResponse->getContent();
                    
                    $request['output'] = json_encode($content);
                    $GLOBALAPIs[] = $request;  
                }
            }
        }
    }

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }
}
