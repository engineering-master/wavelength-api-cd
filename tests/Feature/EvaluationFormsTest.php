<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;
use DB;

$GLOBALAPIs         = [];
$testToken          = "";


/**
 * 
 */
class EvaluationFormsTest extends TestCase
{
    function testClearCache()
    {
        shell_exec("php artisan config:cache");
        $this->assertTrue(true);
    }

    function getToken($username='EC0001', $password='secret')
    {
        GLOBAL $testToken;
        if(!$testToken || ($username && $password)) {
            $response = $this->call(
                'POST',
                'api/v1.1/auth/token',
                ['username'=>$username, 'password'=>$password]
            );

            $token = $response->baseResponse->getContent();
            $token = json_decode($token);
            if(isset($token->token) && $token->token) {
                $testToken = $token->token;
            } else {
                $testToken = "";
            }
        }
        return $testToken;
    }

    function testGetEvaluationForms()
    {
        GLOBAL $GLOBALAPIs;  
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/evaluationforms',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        // dd($evaluation);
        $tesResult  = isset($evaluation);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;
    }

    function testGetEvaluationFormsWithWrongMethod()
    {
        GLOBAL $GLOBALAPIs;  
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/evaluationforms',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [],
            [],
            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]
        );
        $evaluation= $response->baseResponse->getContent();
        $evaluation= json_decode($evaluation);
        // dd($evaluation);
        $tesResult  = isset($evaluation);
        $this->assertTrue($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $GLOBALAPIs[] = $request_data;
    }

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
          
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
     
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
       
    }
}
