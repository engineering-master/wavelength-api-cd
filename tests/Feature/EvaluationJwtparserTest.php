<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;

$GLOBALAPIs        = [];
$testToken          = "";

class EvaluationJwtparserTest extends TestCase
{
    function clearCache()
    {
        shell_exec("php artisan config:cache");
    }

    function getToken($username='EC0001', $password='secret')
    {
        GLOBAL $testToken;
        if(!$testToken || ($username && $password)) {
            $response = $this->call(
                'POST',
                'api/v1.1/auth/token',
                ['username'=>$username, 'password'=>$password]
            );

            $token = $response->baseResponse->getContent();
            $token = json_decode($token);
            if(isset($token->token) && $token->token) {
                $testToken = $token->token;
            } else {
                $testToken = "";
            }
            
        }
        return $testToken;
    }

    function testJwtparserSuccess()
    {
        GLOBAL $GLOBALAPIs; 
        $request_data = [
        'name' => __FUNCTION__,
        'method' => 'GET',
        'url' => 'api/v1.1/jwtparser',
        'inputs' =>[],
        'output' =>[]
        ];
        $response = $this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer '.$this->getToken()] // ['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
        );
        $jwtparser = $response->baseResponse->getContent();
        $jwtparser = json_decode($jwtparser);
        $testdata = isset($jwtparser);
        $this->assertTrue($testdata);
        $request_data['output'] = json_encode($jwtparser);
        $request_data['result'] = $testdata;
        $GLOBALAPIs[] = $request_data;
    }

    function testJwtparserWithInvalidMethod()
    {
        GLOBAL $GLOBALAPIs; 
        $request_data = [
        'name' => __FUNCTION__,
        'method' => 'POST',
        'url' => 'api/v1.1/jwtparser',
        'inputs' =>[],
        'output' =>[]
        ];
        $response = $this->call(
            $request_data['method'],
            $request_data['url'],
            $request_data['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer '.$this->getToken()] // ['Authorization' => 'Bearer '.\JWTAuth::fromUser($user)]
        );
        $jwtparser = $response->baseResponse->getContent();
        $jwtparser = json_decode($jwtparser);

        $testdata = isset($jwtparser);
        $this->assertTrue($testdata);
        $request_data['output'] = json_encode($jwtparser);
        $request_data['result'] = $testdata;
        $GLOBALAPIs[] = $request_data;
    }
    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
          
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $this->assertTrue(true);
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
       
    }
}
