<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Log;

$GLOBALAPIs         = [];

class CallSearchTest extends TestCase
{
    /**
     * Get Auth token for access Api.
     *
     * @return string
     */
    public function getToken($credentials)
    {
        $response = $this->call(
            'POST',
            'api/v1.1/auth/token',
            $credentials
        );
        $token = $response->baseResponse->getContent();
        $token = json_decode($token, true);
        if (isset($token['token']) && $token['token']) {
            return $token['token'];
        } else {
            return "";
        }
    }
    /**
     * Get Auth token for access Api.
     *
     * @return Response
     */
  
    public function accessMethod($token, $request = [])
    {
        $response = $this->call(
            $request['method'],
            $request['url'],
            $request['inputs'],
            [], //cookies
            [], // files
            ['HTTP_Authorization' => 'Bearer ' . $token], // server
            []
        );
        return $response;
    }
    /**
     * Get Auth token for access Api.
     *
     * @return Array
     */
    public function getTestCase()
    {
        $requests = [
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    => []
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                    'from_date' => '2019-07-15 23:59:59',
                    'to_date' => '2019-02-01 00:00:00',
                    "evaluation_form_id" => 1,
                    "skip" => 0,
                    "take" => 25,
                    "total" => 0,
                    "disposition" => "RNA,HU",
                    "sortordercolumn" => "callenddatetime",
                    "sortorder" => "desc",
                    "pageOffsetValue" => 0,
                    "minduration" => 15,
                    "site" => "TOG"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "disposition" => "RNA,HU",
                "sortordercolumn" => "callenddatetime",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "Arul Manickam"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "disposition" => "RNA,HU",
                "sortordercolumn" => "callenddatetime",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "disposition" => "RNA,HU",
                "sortordercolumn" => "callenddatetime",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "+12089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "disposition" => "RNA,HU",
                "sortordercolumn" => "callenddatetime",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    => [
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "(1)2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "disposition" => "RNA,HU",
                "sortordercolumn" => "callenddatetime",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "001-2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "disposition" => "RNA,HU",
                "sortordercolumn" => "employee.agentname",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "disposition" => "RNA,HU",
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG",
                "agent_ids" => "TOC171132,TOC171137,TOC171139"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 'test',
                "customer_num" => "2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG",
                "agent_ids" => "TOC171132,TOC171137,TOC171139"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => 'test',
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG",
                "agent_ids" => "TOC171132,TOC171137,TOC171139"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "Test@123",
                "agent_ids" => "TOC171132,TOC171137,TOC171139"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "2089978193",
                "skip" => 0,
                "take" => -25,
                "total" => 0,
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG",
                "agent_ids" => "TOC171132,TOC171137,TOC171139"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "208997$8193",
                "skip" => 0,
                "take" => 25,
                "total" => 1000,
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG",
                "agent_ids" => "TOC171132,TOC171137,TOC171139"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => 15,
                "site" => "TOG",
                "agent_ids" => "TOC171132,TOC171137,TOC171139"]
            ],
            [
                'method'    => 'Post',
                'url'       => '/api/v1.1/calls',
                'inputs'    =>[
                'from_date' => '2019-07-15 23:59:59',
                'to_date' => '2019-02-01 00:00:00',
                "evaluation_form_id" => 1,
                "customer_num" => "2089978193",
                "skip" => 0,
                "take" => 25,
                "total" => 0,
                "sortordercolumn" => "employee.vendor",
                "sortorder" => "desc",
                "pageOffsetValue" => 0,
                "minduration" => -15,
                "site" => "TOG",
                "agent_ids" => "TOC171132,TOC171137,TOC171139"]
            ],
            [
                'method'    => 'Get',
                'url'       => '/api/v1.1/calls',
                'inputs'    => [
                    'from_date' => '2019-07-15 23:59:59',
                    'to_date' => '2019-02-01 00:00:00',
                    "evaluation_form_id" => 1,
                    "customer_num" => "2089978193",
                    "skip" => 0,
                    "take" => 25,
                    "total" => 0,
                    "sortordercolumn" => "employee.vendor",
                    "sortorder" => "desc",
                    "pageOffsetValue" => 0,
                    "minduration" => 15,
                    "site" => "TOG",
                    "agent_ids" => "TOC171132,TOC171137,TOC171139"
                ]
            ]

        ];
        return $requests;
    }
    /**
     * A basic test example.
     *
     * @return void
     */

    /*public function getCredentials()
    {
        $credentials = [
            [ 'username'=>'EC0001', 'password'=>'secret']
            // [ 'username'=>'EC0004', 'password'=>'secret']
        ];
        return $credentials;
    }

    public function testCallSearch()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = $this->getCredentials();
        foreach ($credentials as $credential) {
            $token = $this->getToken($credential);
            if ($token) {
                $requests = $this->getTestCase();
                foreach ($requests as $request) {
                    $request['name'] = __FUNCTION__;
                    $response = $this->accessMethod($token, $request);
                    if ($response->status()==200) {
                         $this->assertTrue(true);
                        $request['result'] = true;
                    } else {
                        $this->assertFalse(false);
                        $request['result'] = true;
                    }
                    $content = $response->baseResponse->getContent();
                    // print_r($content);
                    $request['output'] = json_encode($content);
                    $GLOBALAPIs[] = $request;  
                }

            }
        }

    }
    */
    public function testAdminCallSearch()
    {
        GLOBAL $GLOBALAPIs;
        $credentials = ['username'=>'EC0001','password'=>'secret'];
        $token = $this->getToken($credentials);
        if ($token) {
            $requests = $this->getTestCase();
            foreach ($requests as $request) {
                $request['name'] = __FUNCTION__;
                $response = $this->accessMethod($token, $request);
                if ($response->status()==200) {
                     $this->assertTrue(true);
                    $request['result'] = true;
                } else {
                    $this->assertFalse(false);
                    $request['result'] = true;
                }
                $content = $response->baseResponse->getContent();
                $request['output'] = json_encode($content);
                $GLOBALAPIs[] = $request;  
            }

        }

    }

    public function testGenerateReport()
    {
        $is_phpunit_report = config('services.phpunit.report');
        if ($is_phpunit_report) {
            GLOBAL $GLOBALAPIs;
            print_r($GLOBALAPIs);
            $opts = ['http' =>[
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>$GLOBALAPIs]
                )]
                ];
            $context  = stream_context_create($opts);
            $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, $context);
            $GLOBALAPIs        = [];
        }
        $this->assertTrue(true);
    }
}
