<?php
namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Cache;

class EnumTest extends TestCase
{

    function testInit()
    {
        Cache::store('file')->put('api', [], 3600);
        $this->assertTrue(true);
    }

 function getToken($username='EC0001', $password='secret')
    {
        $testToken = Cache::store('file')->get('testToken');
        if(!$testToken || ($username && $password))
        {
            $response = $this->call
            (
                'POST',
                'api/v1.1/auth/token',
                ['username'=>$username, 'password'=>$password]
            );

            $token = $response->baseResponse->getContent();
            $token = json_decode($token);
            if(isset($token->token) && $token->token)
                $testToken = $token->token;
            else
                $testToken = "";
            
            Cache::store('file')->put('testToken', $testToken, 3600);

        }
        return $testToken;
    }
    public function saveReport($data=[])
    {
        $APIs = Cache::store('file')->get('api');
        $APIs[] = $data;

        Cache::store('file')->put('api', $APIs, 3600);
    }

    function testEnumSuccess(){
       
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'GET',
            'url'       => 'api/v1.1/enums',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call
        (
            $request_data['method'],
            $request_data['url'],
            [],
            [],
            [],

            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]

        );
         $evaluation= $response->baseResponse->getContent();
        //dd($evaluation);
        $evaluation= json_decode($evaluation);
        //dd($evaluation);
        $tesResult  =(isset($evaluation));
        $this->assertTrue($tesResult);
        //dd($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $this->saveReport($request_data);

    }

    function testEnumMethodFailure(){
       
        $request_data = [
            'name'      => __FUNCTION__,
            'method'    => 'POST',
            'url'       => 'api/v1.1/enums',
            'inputs'    => [],
            "output"    => []
        ];
        $response=$this->call
        (
            $request_data['method'],
            $request_data['url'],
           [],
            [],
            [],

            ['HTTP_Authorization'=>'Bearer ' .$this->getToken()]

        );
         $evaluation= $response->baseResponse->getContent();
        //dd($evaluation);
        $evaluation= json_decode($evaluation);
        //dd($evaluation);
        $tesResult  =(isset($evaluation[0])  && $evaluation[0]=="Method Not Allowed");
        $this->assertTrue($tesResult);
        //dd($tesResult);
        $request_data['output'] = json_encode($evaluation);
        $request_data['result'] = (int)$tesResult;
        $this->saveReport($request_data);

    }


public function testGenerateReport()
    {
        $opts = ['http' =>
            [
                'method'  => 'POST',
                'header'  => 'Content-Type: application/x-www-form-urlencoded',
                'content' => http_build_query(
                    ['api'=>Cache::store('file')->get('api')]
                )
            ]
        ];
        $context  = stream_context_create($opts);
        $result = file_get_contents('http://localhost/PhpSpreadsheet/samples/Basic/generateReport.php', false, 

            $context);
        
        $this->assertTrue(true);
    }

}