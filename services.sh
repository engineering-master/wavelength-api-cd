#!/bin/bash

#queue: checks it is running or not, if not will start running
if [ $(ps -ef | grep artisan | grep queue -c) -gt 0 ] ;
then
    echo "queue is running...";
else
    SCRIPT=$(readlink -f "$0")
    SCRIPTPATH=$(dirname "$SCRIPT")
    nohup /bin/php "$SCRIPTPATH"/artisan queue:work > nohup.out 2> nohup.err < /dev/null &
    echo "queue was stopped, now it is running...";
fi;