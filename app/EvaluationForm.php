<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EvaluationForm extends Model
{
    protected $connection = 'mysql';
    use SoftDeletes;

    protected $fillable = [
        'slug',
        'title',
        'description',
        'headers',
        'ratings',
        'active',
        'version',
        'created_by',
        'client_id'
    ];

    protected $dates = ['deleted_at'];

    public function user(){
        return $this->hasOne('App\User', 'user_id', 'created_by');
    }

    public function client(){
        return $this->hasOne('App\WavClient', 'id', 'client_id');
    }

    public function groups()
    {
        return $this->hasMany('App\QuestionGroup', 'evaluation_form_id')->orderBy('order','asc');
    }

    public function questions()
    {
        return $this->hasManyThrough(
            'App\Question', // Table you wish to access
            'App\QuestionGroup', // Intermediate Table
            'evaluation_form_id', // Foreign key on intermediate table...
            'group_id', // Foreign key on target table...
            'id', // Local key on local table...
            'id' // Local key on intermediate table...
        );
    }

    public function subquestions($id)
    {
        return $this->hasManyThrough(
            'App\Question', // Table you wish to access
            'App\QuestionGroup', // Intermediate Table
            'evaluation_form_id', // Foreign key on intermediate table...
            'group_id', // Foreign key on target table...
            'id', // Local key on local table...
            'id' // Local key on intermediate table...
        )->where('questions.parent_question_id','=',$id)->orderBy('questions.order','asc')->get();
    }
}