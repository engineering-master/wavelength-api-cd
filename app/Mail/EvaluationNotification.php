<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Evaluation;

class EvaluationNotification extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * The evaluation instance.
     *
     * @var Evaluation
     */
    public $evaluation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($evaluation)
    {
        $this->evaluation = $evaluation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.feedback');
    }
}
