<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace App\Extensions;

use Illuminate\Http\Request;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Keychain; // just to make our life simpler
use Lcobucci\JWT\Signer\Rsa\Sha256; // you can use Lcobucci\JWT\Signer\Ecdsa\Sha256 if you're using ECDSA keys
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Claims\Claim;
use App\Employee;
use App\NonEmployeeUser;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use JWTAuth;
use Illuminate\Contracts\Auth\Guard;

class JWTGuard //extends Guard
{
   public function attempt($token, $remember = false, $login = true)
    {
      
        $response = $this->checkByToken($token);
        $user=$credentials=array();

        if (isset($response->getData()->error)) {
            $this->fireFailedEvent($user, $credentials);
            return false;
        }
        else if (isset($response->getData()->userid)){
            $user = $this->checkUsertypeByClaim($response->getData());
            return $user;
        }
    }

    /**
    * Parse token, get claims
    *
    * @return JSON Obj
    **/

    public function checkByToken($token)
    {
       try {
            $claims = JWTAuth::getJWTProvider()->decode($token);
            
            if(empty($claims))
            {
                $error = array(['error' => 'Unauthorized']);
                return response()->json(['error' => 'Unauthorized'], 401);
            }
            // Test Code - need to fail if these are here

            $userid = $claims['sub'];
            $groups = $claims['groups'];
            $userType = $claims['Usertype'];
            
            return response()->json(['userid' => $userid,'groups'=> $groups,'usertype' => $userType ], 200);
        }
        catch (TokenInvalidException $e) {
            $error = array(['error' => 'Token Invalid']);
            return response()->json(['error' => 'Token Invalid'], 401);

        } catch (JWTException $e) {
            $error = array(['error' => 'Token Absent']);
            return response()->json(['error' => 'Token Absent'], 401);
        }

       //return $userid;
    }
    

    /**
    * Set user based on usertype claims
    *
    * @return Obj
    **/
    private function checkUsertypeByClaim($claims) {
      
        if ($claims->usertype === 'employee') {
            return $this->setUserfromEmployee($claims->userid);
        } 
        elseif ($claims->usertype === 'client' || $claims->usertype === 'api') {            
             return $this->setUserfromNonEmployeeUser($claims->userid);
        } else {
            return false;
        }
    }

    /**
    * Set Employee based on Employee model
    *
    * @return User Obj
    **/
    private function setUserfromEmployee($eid) {

        $user = Employee::Active()->where('eid', $eid)->first();
        return $user;
    }


    //Begining for authenicating Clients and API accounts - need to create this table
    private function setUserfromNonEmployeeUser($externalid) {
        $user = NonEmployeeUser::where('id', $externalid)->first();        
        return $user;
    }

    /**
     * Fire the failed authentication attempt event with the given arguments.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable|null  $user
     * @param  array  $credentials
     * @return void
     */
    protected function fireFailedEvent($user, array $credentials)
    {
        if (isset($this->events)) {
            $this->events->dispatch(new Events\Failed($user, $credentials));
        }
    }


}
