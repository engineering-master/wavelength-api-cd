<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemId extends Model
{
    protected $connection = 'mysql3';
	protected $table = 'ascentis.system_ids';


    public function employee()
    {
        return $this->belongsTo('App\Employee','eid');
    }


}
