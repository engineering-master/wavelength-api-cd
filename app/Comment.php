<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    //protected $dates = ['deleted_at'];
    //public $timestamps = false;
    protected $connection = 'mysql';
    protected $table = 'comments';
    protected $fillable = ['body','created_by'];


    public function commentable() {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User','created_by','user_id'); //->withTrashed()
    }
    public function status(){
        return $this->belongsTo('App\EvaluationStatusAudit','id','comment_id') ->with(['status'=> function($q){
            $q->select('id','status','description');
         }]);
    }
}
