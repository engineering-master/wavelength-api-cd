<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Cache;

class EvaluationWorkflows extends Model
{
    protected $connection = 'mysql';
    use SoftDeletes;

    protected $fillable = [
         'description',
         'status',
         'start_date',
         'wf_details',
         'created_by',
         'updated_by',
         'deleted_at',
         'created_at',
         'updated_at'
    ];

    public static function workflow($workflowId = "")
    {
        try {
            $cacheExpireTime = 60 * 12;
            if ($workflowId) {
                $wokflow_details = [];
                $cache_key_workflow = 'workflow_'.$workflowId;
                if (!Cache::has($cache_key_workflow)) {
                    $wokflow = EvaluationWorkflows::where('status', 1)->where('id', $workflowId)->first();
                    if ($wokflow) {
                        $wokflow_details = json_decode($wokflow->wf_details, true);
                        Cache::put($cache_key_workflow, $wokflow_details, $cacheExpireTime);
                    }
                } else {
                    $wokflow_details = Cache::get($cache_key_workflow);
                }
                return $wokflow_details;
            } else {
                $wokflows = EvaluationWorkflows::where('status', 1)->get();
                $cache_key_workflow = '';
                foreach ($wokflows as $wf) {
                    $wfdetails = json_decode($wf->wf_details, true);
                    $cache_key_workflow = 'workflow_'.$wf->id;
                    Cache::put($cache_key_workflow, $wfdetails, $cacheExpireTime);
                }
            }
        } catch (Exception $ex) { // Anything that went wrong
            return response()->json(['error' => 'Something went wrong,try again later'], 422);
        }
    }
}
