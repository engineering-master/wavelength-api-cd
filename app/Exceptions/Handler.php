<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        //Handle Internal Server error
        if ($e instanceof \ErrorException) {
            return response()->json(['Some thing went wrong. Please contact Administrator'], 500);
        }
        else if ($e instanceof Illuminate\Database\Eloquent\ModelNotFoundException) {
            return response()->json(['Model Not Found'], $e->getStatusCode());
        }elseif ($e instanceof Illuminate\Database\Eloquent\QueryException) {
            return response()->json(['Some thing went wrong. Please contact Administrator'], $e->getStatusCode());
        }
        elseif ($e instanceof  \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            return response()->json(['Method Not Allowed'], 405);
        }
        elseif ($e instanceof  \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            return response()->json(['Page Not Found'], 404);
        }elseif ($e instanceof  \Symfony\Component\HttpKernel\Exception\BadRequestHttpException) {
            return response()->json(['HTTP Bad Request'], 400);
        }
        elseif ($e instanceof Tymon\JWTAuth\Exceptions\TokenExpiredException) {
            return response()->json(['Token has expired'], $e->getStatusCode());
        } else if ($e instanceof Tymon\JWTAuth\Exceptions\TokenInvalidException) {
            return response()->json(['Token is Invalid'], $e->getStatusCode());
        }
        if ($e instanceof \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException) {
            return response()->json(['Unauthorized'], 401);
        }
        if ($e instanceof \Illuminate\Auth\Access\AuthorizationException) {
            return response()->json(['errors' => $e->getMessage()], 403);
        }
    
        return parent::render($request, $e);
    }
}
