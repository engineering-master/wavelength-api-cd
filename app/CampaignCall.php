<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignCall extends Model
{
	protected $table = 'campaign_calls';
    protected $fillable = ['call_id', 'campaign_id', 'status', 'created_by'];
    public $timestamps = true;

    public function campaign(){
    	return $this->hasOne('App\Campaign', 'id','campaign_id');
    }

    public function calls(){
    	return $this->hasOne('App\Call', 'unique_id','call_id')->with(['agent' => function($query){
    		$query->select('eid', 'employee_name');
    	}]);
    }

    public function callworklist(){
        return $this->hasOne('App\Worklist', 'call_id', 'call_id')->with('user');
    }
}
