<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluationFormCampaign extends Model
{
    protected $table = 'evaluation_form_campaign_mappings';
    protected $fillable=['type','type_id'];
}
