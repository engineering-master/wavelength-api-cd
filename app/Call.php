<?php
namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Call extends Model
{

  protected $connection = 'mysql';
  protected $table = 'fact_call_details';
  protected $primaryKey = 'unique_id';
  public $incrementing = false;

  public function employee()
    {
        return $this->belongsTo('App\Employee', 'agent_id','user_id')->with(['supervisor' => function($query){
          $query->select('name as manager_name', 'user_id', 'supervisor_user_id');
        }]);
    }

    public function emp(){
      return $this->belongsTo('App\Employee', 'agent_id','user_id');
    }

  public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function agent(){
      return $this->hasOne('App\Employee', 'eid', 'agent_id');
    }

    public function evaluations(){
      return $this->hasOne('App\Evaluation', 'call_id', 'ucid')->with('status');
    }

    /**
     * Get campaign skill
     * 
     * @return Object
     */
    public function campaignSkill()
    {
        return $this->belongsTo('App\CampaignSkill', 'skill_campaign', 'name');
    }
  public static function multiAudioFile($audiofileurl, $employee_agentname, $usr_grp) {
    $audio_file_url = [];
    $audiofileurls = explode(",", $audiofileurl);
    foreach ($audiofileurls as $key => $audiofile) {
      $audio_file_url[$key] = [];

      if($usr_grp == 'F9'){ // Authendicate the intake call urls
        $disk = Storage::disk('s3');
        $parsed_url = parse_url($audiofile);
        $filePath = substr($parsed_url['path'], 1);
        $fileName = pathinfo($filePath);
        $audio_file_url[$key]['url'] = $disk->temporaryUrl($filePath, now()->addMinutes(120), [
          'ResponseContentType' => 'application/octet-stream',
          'ResponseContentDisposition' => 'attachment; filename='.$fileName['basename'],
        ]);
      }else{
        $audio_file_url[$key]['url'] = $audiofile;  
      }
      //$audio_file_url[$key]['url'] = $audiofile;
      $basename = basename($audiofile);
      $names = explode('_', $basename);
      $time = str_replace("-", " ", $names[0]);
      $audiofilename = date( "Y-m-d_H_i_s", strtotime($time));
      $audio_file_url[$key]['name'] = $employee_agentname ."_". $audiofilename;
    }
    return $audio_file_url;
  }
}
