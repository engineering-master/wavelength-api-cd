<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluationDispositions extends Model
{
    protected $table = 'evaluation_dispositions';
    public $timestamps = false;
    protected $fillable = [
        'evaluations_id',
        'evaluation_disposition_definitions_id',
        'severity_id',
        'created_by'
    ];
    public function evaluationdispositiondefinition()
    {
        return $this->belongsTo('App\EvaluationDispositionDefinitions', 'evaluation_disposition_definitions_id');
    }
}
