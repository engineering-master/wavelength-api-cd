<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class UserProfileAudit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users_profile_audits';
    public $timestamps = false;
    protected $fillable = [
        'user_id', 'prev_email', 'change_email', 'prev_unite', 'change_unite', 'status', 'updated_at', 'updated_by', 'created_at'
    ];

}
