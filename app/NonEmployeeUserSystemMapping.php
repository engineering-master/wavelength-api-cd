<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NonEmployeeUserSystemMapping extends Model
{
    protected $connection = 'mysql_nonemployee';
    protected $table = 'user_system_mapping';
    protected $guard_name = 'api';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'system_id', 'system_user_id'
    ];

}
