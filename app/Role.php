<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	public function rolepermissions(){
		return $this->hasMany('App\RoleHasPermissions', 'role_id', 'id')->with('permissions');
	}
}
