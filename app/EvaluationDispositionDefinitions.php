<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluationDispositionDefinitions extends Model
{
    protected $table = 'evaluation_disposition_definitions';
}
