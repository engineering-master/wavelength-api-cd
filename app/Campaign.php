<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $connection = 'mysql';
    protected $fillable = ['name', 'version', 'survey_id', 'description', 'start_date', 'end_date', 'activated_at', 'frequency_type', 'frequency_val', 'type', 'type_val', 'created_by', 'activated_by', 'status', 'pauses_log'];
    public $timestamps = true;

    public function evaluation_form(){
    	return $this->hasOne('App\EvaluationForm', 'id', 'survey_id')->with('client');
    }

    public function createduser(){
    	return $this->hasOne('App\User', 'user_id', 'created_by');
    }

    public function campaigncall(){
    	return $this->hasMany('App\CampaignCall', 'campaign_id', 'id');
    }
}
