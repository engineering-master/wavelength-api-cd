<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $connection = 'mysql';
    protected $fillable = [
        'parent_question_id',
        'group_id',
        'title',
        'description',
        'type',
        'questionmode',
        'order',
        'mandatory',
        'values',
        'rawvalues',
        'score',
        'comment',
        'commentmandatory',
        'actual_score'
    ];

	public function getMandatoryAttribute($value){
        return ($value == 1) ? true : false;
    }

    public function getCommentAttribute($value){
        return ($value == 1) ? true : false;
    }

    public function getShowcommentboxAttribute($value){
        return ($value == 1) ? true : false;
    }

    public function getCommentmandatoryAttribute($value){
        return ($value == 1) ? true : false;
    }

    public function answers()
    {
        return $this->hasMany('App\Answer')->with('comments');
    }

    public function questiongroup()
    {
    	return $this->belongsTo('App\QuestionGroup', 'group_id');
    }
}
