<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSystemMapping extends Model
{
    public $timestamps = true;
    protected $fillable = ['user_id','system_user_id'];
    protected $table = 'user_system_mapping';
}
