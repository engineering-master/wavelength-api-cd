<?php
namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\SystemId;
use App\Calls;
use App\Extension;
use Carbon\Carbon;

class Employee extends Model

{
	protected $connection = 'mysql';
	protected $table = 'portal_user';
	protected $primaryKey = 'user_id';
	public $incrementing = false;

    public function scopeActive($query)
    {
        return $query->whereNull('termination_date')->where('to_date', '>' ,Carbon::now());
    }

    // Helper Methods
    public function agents()
    {
        $result = Employee::Active()->where('job_title', 'like', '%Advisor%')->orWhere('job_title', 'like', '%Contractor%')->get();                         
        return $result;
    }

    public function managers()
    {
        $result = Employee::Active()->where('job_title', 'like', '%Manager%')->get();                           
        return $result;
    }

    public function agents_manager($eid)
    {
        $result = Employee::Active()->where('eid',$eid)->first() ?? NULL;
        return $result;
    }
    
    public function client_agents($client_name)
    {
        $result = Employee::Active()->where('division',$client_name)->get();                                
        return $result;
    }

    public function subordinates($eid)
    {
        $result = Employee::Active()->where('supervisor_eid',$eid)->get();
        return $result;
    }
	public function systemids()
    {
        return $this->hasMany('App\SystemId', 'eid');
    }

    public function calls(){

    	return $this->hasMany('App\Call', 'agent_id');

    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable')->orderBy('created_at', 'desc' );
    }

    public function supervisor()
    {
        return $this->belongsTo('App\Employee', 'supervisor_user_id');
    }
    public function evaluation_agents()
    {
        return $this->belongsTo('App\Evaluation', 'agent_id');
    }


/*    // Need to have a relationship via explicit SystemID i.e. Extension and it be on src/dst for cdr record
    public function calls()
    {
        return $this->hasManyThrough(
            'App\Call', // Table you wish to access
            'App\Extension', // Intermediate Table
            'extension', // Foreign key on intermediate table...
            'extension', // Foreign key on target table...
            'eid', // Local key on local table...
            'eid' // Local key on intermediate table...
        );
    }*/




}
