<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worklist extends Model
{
    protected $fillable = ['call_id', 'campaign_id', 'assigned_id', 'created_by', 'evaluation_id', 'status', 'worklist_log'];
    public $timestamps = true;

    public function calls(){
    	return $this->hasOne('App\Call', 'unique_id','call_id')->with(['agent' => function($query){
    		$query->select('eid', 'employee_name');
    	}]);
    }

    public function campaign(){
    	return $this->hasOne('App\Campaign', 'id', 'campaign_id');
    }

    public function user(){
        return $this->hasOne('App\User', 'user_id', 'created_by');
    }
}
