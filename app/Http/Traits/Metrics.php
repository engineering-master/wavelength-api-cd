<?php
namespace App\Http\Traits;

use Aws\CloudWatch\CloudWatchClient; 
use Aws\Exception\AwsException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

#Reference : https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/cw-examples-publishing-custom-metrics.html
/** Usage#---------
    namespace App;

    use App\Http\Traits\Metrics;

    class TestController extends Controller
    {
        use Metrics;

        public function testMethod()
        {
            #Method 1 - Logs to CW with test metrics :--------------------------------------------------------
            $this->helloUniverse();

            #Method 2 - Logs to CW with current url & current elapsed time :----------------------------------
            $this->putMetricData();

            #Method 3 - Log to CW with given metrics array(Multiple metrics can be given). If any of array missing below metrics it fill & log to CW :-
            #   'MetricName'=> request()->path(),
            #   'Timestamp' => time(),
            #   'Value'     => $this->get_elapsed_time(),
            #   'Unit'      => 'Seconds',
            #------------------------------------------------------------------------------------------------
            $StartTime = microtime(true);
            #Some other statements
            $metrics = array(
                array(
                    'MetricName'=> "WL/API/auth",
                    'Timestamp' => time(),
                    'Value'     => "1",
                    'Unit'      => 'Seconds'
                ),
                array(
                    'MetricName'=> "WL/API/auth",
                    'StartTime' => $StartTime #the 'Value'(elapsed time) will be calculated from this time
                )
            );
            $this->putMetricData($metrics);
        }
    }
    //end
 */

trait Metrics
{
    public $aws_CloudWatchClient;
    
    public function aws_cw_connect()
    {
        if(!isset($this->aws_CloudWatchClient))
        {
            $this->aws_CloudWatchClient = new \Aws\CloudWatch\CloudWatchClient([
                //'profile' => 'default',
                'region' => config('auth.aws.region'),
                'version' => '2010-08-01',
                'credentials' => [
                    'key'    => config('auth.aws.key'),
                    'secret' => config('auth.aws.secret')
                ]
            ]);
        }
        return $this->aws_CloudWatchClient;
    }

    #created for testing purpose
    public function helloUniverse()
    {
        $this->aws_CloudWatchClient = $this->aws_cw_connect();
        
        $response = array();
        try {
            $result = $this->aws_CloudWatchClient->putMetricData(array(
                'Namespace' => config('app.name'),
                'MetricData' => array(
                    array(
                        'MetricName' => 'HelloUniverse',
                        'Timestamp' => time(),
                        'Value' => rand(0, 100),
                        'Unit' => 'Count'
                    )
                )
            ));
            $response = array("status_id"=>"1", "status_message"=>"Metrics pushed to CloudWatch", "helloUniverse"=>$result);
        } catch (AwsException $e) {
            $response = array("status_id"=>"0", "status_message"=>$e->getMessage());
            error_log($e->getMessage());
        }
        return $response;
    }

    public function putMetricData($metrics = array())
    {
        $this->aws_CloudWatchClient = $this->aws_cw_connect();

        $default_metric = array
        (
            'MetricName'=> request()->path(),
            'Timestamp' => time(),
            'Value'     => $this->get_elapsed_time(),
            'Unit'      => 'Seconds',
        );
        if(!count($metrics)){
            $metrics = array( $default_metric );
        }
        else #Add the missed parameters
        {
            foreach($metrics as $metric_key => $metric) {
                if(isset($metric['StartTime']))
                {
                    $metric['Value'] = $this->get_elapsed_time($metric['StartTime']);
                    unset($metric['StartTime']);    
                }
                $metrics[$metric_key] = array_merge($default_metric, $metric);
            }
        }

        #1) Logs to a file
        Log::channel("metrics")->Info( json_encode($metrics, true) );

        #2) Logs to CloudWatch
        $response = array();
        try {
            $result = $this->aws_CloudWatchClient->putMetricData(array(
                'Namespace' => config('app.name'),
                'MetricData'=> $metrics
            ));
            $response = array("status_id"=>"1", "status_message"=>"Metrics pushed to CloudWatch", "putMetricData"=>$result);
        } catch (AwsException $e) {
            $response = array("status_id"=>"0", "status_message"=>$e->getMessage());
            error_log($e->getMessage());
        }
        return $response;
    }

    public function putMetricAlarm($alarms = array())
    {
        $this->aws_CloudWatchClient = $this->aws_cw_connect();

        if(!count($alarms)){
            $alarms = array( $default_alarm );
        }

        $default_alarm = array
        (
            'AlarmName'         => "Common Alarm",
            'MetricName'        => request()->path(),
            'Namespace'         => config('app.name'),
            //string: SampleCount | Average | Sum | Minimum | Maximum
            'Statistic'         => 'SampleCount',
            'Period'            => 1,
            //Unit: Count/Second
            'Unit'              => 'Count',
            'Threshold'         => 1,
            // string: GreaterThanOrEqualToThreshold | GreaterThanThreshold | LessThanThreshold | LessThanOrEqualToThreshold
            'ComparisonOperator'=> 'GreaterThanOrEqualToThreshold',
            'EvaluationPeriods' => 1,
        );
        $responses = array();
        try {
            if(!count($alarms)){
                $alarms = array( $default_metric );
            }
            else #Add the missed parameters
            {
                foreach($alarms as $alarm_key => $alarm) {
                    $alarms[$alarm_key] = array_merge($default_alarm, $alarm);
                    #1) Logs to CloudWatch
                    $responses[] = $this->aws_CloudWatchClient->putMetricAlarm($alarm);
                }
            }
            #2) Logs to a file
            Log::channel("metrics")->Info( json_encode($alarms, true) );

            $response = array("status_id"=>"1", "status_message"=>"", "putMetricAlarm"=>$responses);
        } catch (AwsException $e) {
            $response = array("status_id"=>"0", "status_message"=>$e->getMessage());
            error_log($e->getMessage());
        }
        return $response;
    }

    function listMetrics()
    {
        $this->aws_CloudWatchClient = $this->aws_cw_connect();
        try {
            $result = $this->aws_CloudWatchClient->listMetrics();
            $response = array("status_id"=>"1", "status_message"=>"", "listMetrics"=>$result);
        } catch (AwsException $e) {
            $response = array("status_id"=>"0", "status_message"=>$e->getMessage());
            error_log($e->getMessage());
        }
        return $response;
    }

    function get_elapsed_time($start_time=0)
    {
        $elapsed_seconds = $start_time ? (microtime(true) - $start_time) : (microtime(true) - LARAVEL_START);
        return number_format((float)$elapsed_seconds, 3, '.', '');
    }

}