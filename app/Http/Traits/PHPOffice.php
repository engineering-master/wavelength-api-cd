<?php
namespace App\Http\Traits;

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

/** Usage#---------
    namespace App;

    use App\Http\Traits\PHPOffice;

    class TestController extends Controller
    {
        use PHPOffice;

        public function testMethod()
        {
            #Method 1 - Simple Save :--------------------------------------------------------
            $sheetDatas = [[
                'A1'=>'Title 1',
                'B1'=>'Title 2',
                'C1'=>'Title 3',
                'D1'=>'Title 4',
                'E1'=>'Title 5',
                'F1'=>'Title 6',
                'G1'=>'Title 7',
            ], [...]];
            $this->export($sheetDatas);

            #Method 2 - Save as unittesting format :--------------------------------------------------------
            $unitTestingData = [[
                'method'=>"GET",
                'url'=>"api/v1.1/auth/token",
                'inputs'=>["username"=>"", "password"=>""],
                'output'=>"",
                'result'=>""
            ]];
            $this->export($unitTestingData, 'save,unittesting,download');
        }
    }
    //end
 */

trait PHPOffice
{
    public $spreadsheet;

    public function spreadsheetInit($customProperties=[], $action="")
    {
        if(!$this->spreadsheet)
        {
            // Create new Spreadsheet object
            $this->spreadsheet = new Spreadsheet();
            // Rename worksheet
            $this->spreadsheet->getActiveSheet()->setTitle('Wavelength');

            $properties = [
                "Creator"    => config('app.name'),
                "ModifiedBy" => config('app.name'),
                "Title"      => "",
                "Subject"    => "",
                "Description"=> "",
                "Keywords"   => "",
                "Category"   => "",
                "Sheet"      => []
            ];
            if(strpos($action, "unittesting")>-1)
            {
                $properties["Sheet"] = [
                [
                    'A6'=>'S.No.',
                    'B6'=>'Name',
                    'C6'=>'API URL',
                    'D6'=>'Method',
                    'E6'=>'Inputs',
                    'F6'=>'Output',
                    'G6'=>'Result',
                ] ];
            }
            $properties = array_merge($properties, $customProperties);

            // Set document properties
            $this->spreadsheet->getProperties()->setCreator($properties['Creator'])
            ->setLastModifiedBy($properties['ModifiedBy'])
            ->setTitle($properties['Title'])
            ->setSubject($properties['Subject'])
            ->setDescription($properties['Description'])
            ->setKeywords($properties['Keywords'])
            ->setCategory($properties['Category']);

            // Add some data
            foreach ($properties['Sheet'] as $sheetInc => $sheetData) {
                foreach($sheetData as $sheetAddr=>$sheetVal)
                    $this->spreadsheet->setActiveSheetIndex($sheetInc)->setCellValue($sheetAddr, $sheetVal);
            }
        }
        return $this->spreadsheet;
    }

    public function export($sheetDatas=[], $action='save', $customProperties=[])
    {
        $this->spreadsheet = $this->spreadsheetInit($customProperties, $action);

        if(strpos($action, 'unittesting')>-1)
            $this->spreadsheetExportUnittesting($sheetDatas, $action);
        else
        {
            foreach ($sheetDatas as $sheetInc => $sheetData) {
                foreach($sheetData as $sheetAddr=>$sheetVal)
                    $this->spreadsheet->setActiveSheetIndex($sheetInc)->setCellValue($sheetAddr, $sheetVal);
            }
        }
        
        return $this->spreadsheetEnd($action);
    }

    public function spreadsheetExportUnittesting($sheetDatas=[])
    {
        $sheetInc = $totalPassed = $totalFailed = $percentageOfCoverage = 0;
        $sheet_i   = 6; //start
        foreach($sheetDatas as $EachData)
        {
            $sheet_i++;
            $sheetInc++;
            $EachData['result'] ? $totalPassed++ : $totalFailed++;
            $EachData['result'] = $EachData['result'] ? "Passed" : "Failed";
            $EachData['inputs'] = $EachData['inputs'] ?? [];
            $this->spreadsheet->setActiveSheetIndex(0)
            ->setCellValue("A$sheet_i", $sheetInc)
            ->setCellValue("B$sheet_i", $EachData['name'])
            ->setCellValue("C$sheet_i", $EachData['url'])
            ->setCellValue("D$sheet_i", $EachData['method'])
            ->setCellValue("E$sheet_i", json_encode($EachData['inputs']))
            ->setCellValue("F$sheet_i", $EachData['output'])
            ->setCellValue("G$sheet_i", $EachData['result']);
        }

        if($sheetInc)
            $percentageOfCoverage = ($sheetInc == $totalPassed) ? 100 : ($totalPassed*100)/$sheetInc ;
        else
            $percentageOfCoverage = 0;
        $this->spreadsheet->setActiveSheetIndex(0)
        ->setCellValue("B1", "Total Unit Test Cases")
        ->setCellValue("C1", $sheetInc)
        
        ->setCellValue("B2", "Total Passed")
        ->setCellValue("C2", $totalPassed)
        
        ->setCellValue("B3", "Total Failed")
        ->setCellValue("C3", $totalFailed)
        
        ->setCellValue("B4", "% of Coverage")
        ->setCellValue("C4", "$percentageOfCoverage%");

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->spreadsheet->setActiveSheetIndex(0);
    }

    public function spreadsheetEnd($action="save")
    {
        $fileName = "Wavelength-APIs-UT-Cases".date("Y-m-d-H-i-s").".xlsx";
        if(strpos($action, 'download')>-1)
        {
            // Redirect output to a client’s web browser (Xlsx)
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$fileName.'"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0
        }
        else if(strpos($action, 'save')>-1)
        { }

        $writer = IOFactory::createWriter($this->spreadsheet, 'Xlsx');
        if(strpos($action, 'download')>-1)
            $writer->save('php://output');
        else
            $writer->save($fileName);

        $response = ["status_id"=>"1", "status_message"=>"Created the $fileName"];
        return $response;
    }

}
