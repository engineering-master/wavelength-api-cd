<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
            /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    public function auth(Request $request)
    {
        try{
            $credentials = $request->only('username', 'password');
      
            $client = new Client();
            $authConfig = config('auth.authentication');
            $host = $authConfig['portal']['url'].'/api/v1.1/auth/token';
            $result = $client->request('POST',$host, [
                'form_params' => ['username' => $request->username, 'password' => $request->password]
            ]);
            return response($result->getBody());
        } catch(Exception $e) {
            return response($e->getMessage(), 401);
       }
    }

    /**
    * Get Hash from JWT Auth server
    * Call JWT Auth server with has and get token
    * Store the token in session to avoid authendication on each call
    * @return
    */
    public function getToken(Request $request){

        $authCode = $request->code;  // Hash from Auth server
        $authServer = $request->auth;

        $destinationUrl = request()->headers->get('referer')?(
                strpos(request()->headers->get('referer'),'login')?request()->headers->get('referer'):request()->headers->get('referer').'login'):$request->fullUrl();

        $destinationUrl = explode('?',$destinationUrl);

        $authConfig = config('auth.authentication');

        if($authServer == 'jwt'){
            $host = $authConfig['portal']['url'].'/api/token';
            $clientId = $authConfig['portal']['clientid'];
        } else{
            $host = $authConfig['adfs']['url'].'/token';
            $clientId = $authConfig['adfs']['clientid'];
        }

        $client = new Client();
        
        //dd($host);
        $result = $client->request('POST',$host, [
        'form_params' => ['grant_type' => 'authorization_code', 'code' => $authCode,'client_id' => $clientId,'redirect_uri'=>$destinationUrl[0]]
        ]);
        return response($result->getBody());
    }

    public function refreshToken(Request $request){

        $token = $request->refresh_token;  // Refresh token from client
        $authServer = $request->auth;
        
        $destinationUrl = request()->headers->get('referer')?(
                strpos(request()->headers->get('referer'),'login')?request()->headers->get('referer'):request()->headers->get('referer').'login'):$request->fullUrl();

        $destinationUrl = explode('?',$destinationUrl);

        $authConfig = config('auth.authentication');
        if($authServer == 'jwt'){
            $host = $authConfig['portal']['url'].'/api/token/refresh';
            $clientId = $authConfig['portal']['clientid'];
        } else{
            $host = $authConfig['adfs']['url'].'/token';
            $clientId = $authConfig['adfs']['clientid'];
        }

        $client = new Client();

        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept'        => 'application/json',
        ];

        $result = $client->request('POST',$host, [
            'headers'=>$headers,'form_params' => ['grant_type' => 'refresh_token', 'refresh_token' => $token,'client_id' => $clientId]
        ]);
        return response($result->getBody());
    }
}
