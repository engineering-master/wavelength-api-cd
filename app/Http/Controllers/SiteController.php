<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\User;
use App\UserSystemMapping;
use App\Client;
use Auth;
use Cache;

class SiteController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
        $cacheExpireTime = 60 * 12;
        $cache_key = 'site_'. strtolower(Auth::user()->user_id);
        if (!Cache::has($cache_key)) {
            $clients = new User;
            if (Auth::user()->hasRole('Supervisor')) {
                $userSystemMapping = UserSystemMapping::select('system_user_id')
                    ->where('user_id', Auth::user()->user_id)->get();
                $clients = $clients->whereIn('suerpvisorid', $userSystemMapping);
            } else if (Auth::user()->hasRole('Agent')) {
                $clients = $clients->where('user_id', Auth::user()->user_id);
            }
            $clients = $clients->where('site', '!=', '')->whereNotNull('site')->groupBy('site')->pluck('site');
            if (Auth::user()->hasRole('Supervisor')) {
                if (Auth::user()->site && !$clients->contains(Auth::user()->site)) {
                    $clients->push(Auth::user()->site);
                }
            }
            Cache::put($cache_key, $clients, $cacheExpireTime);
        } else {
            $clients = Cache::get($cache_key);
        }
        return $clients;
    }
    
    public function getAgent(Request $request)
    {
        $cacheExpireTime = 60 * 12;
        if ($request->has('site')) {
            $site = $request->input('site');
            if (!empty($site)) {
                $cache_key = 'agent_'. strtolower(Auth::user()->user_id);
                $cache_key .= '_' . strtolower(str_replace(' ', '_', str_replace(',', '_', $site)));
                if ($request->flag == 'evaluation') {
                    $cache_key .=  '_' . $request->flag;
                }
                $sites = explode(',', $site);
                foreach ($sites as $site) {
                    $cache_key_site = 'site_' . $site;
                    if (!Cache::has($cache_key_site)) {
                        $client = User::where('site', $site)->first();
                        if (!$client) {
                            $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
                            $data['message'] = 'Site not found, Please select an available site';
                            return response($data, 422);
                        }
                        Cache::put($cache_key_site, $client, $cacheExpireTime);
                    }
                }
                if (!Cache::has($cache_key) || $request->flag == 'evaluation') {
                    $employees = User::select('name as employee_name', 'users.user_id as eid')->whereIn('site', $sites);
                    if (Auth::user()->hasRole('Supervisor')) {
                        $userSystemMapping = UserSystemMapping::where('user_id', Auth::user()->user_id)->pluck('system_user_id');
                        $employees = $employees->whereIn('suerpvisorid', $userSystemMapping);
                    } else if (Auth::user()->hasRole('Agent')) {
                        $employees = $employees->where('users.user_id', Auth::user()->user_id);
                    }
                    if ($request->flag == 'evaluation') {
                        $employees =  $employees->join('user_system_mapping', 'user_system_mapping.user_id', '=', 'users.user_id')
                            ->join('evaluations', 'evaluations.agent_id', '=', 'user_system_mapping.system_user_id')
                            ->distinct();
                    }
                    $employees = $employees->orderBy('name', 'ASC')->get();
                    Cache::put($cache_key, $employees, $cacheExpireTime);
                } else {
                    $employees = Cache::get($cache_key);
                }
                return $employees;
            } else {
                $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
                $data['message'] = 'Site can`t be empty, Please select at least one site';
                return response($data, 422);
            }
        } else {
            $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
            $data['message'] = 'Site can`t be empty, Please select at least one site';
            return response($data, 422);
        }
    }
}
