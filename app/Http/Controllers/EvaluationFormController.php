<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\EvaluationForm;
use App\Employee;
use App\Answer;
use App\Call;
use App\User;
use Auth;
use App\Comment;
use App\Evaluation;
use App\Disposition;
use App\Question;
use App\QuestionGroup;
use App\Campaign;
use App\CampaignCall;
use App\CampaignSearch;
use App\Worklist;
use App\WavClient;
use DB;
use DataTables;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\EvaluationDispositions;
use App\Http\Requests\EvaluationFormsGetRequest;
use App\Http\Requests\EvaluationFormCreateRequest;
use App\Http\Requests\EvaluationFormGetRequest;


class EvaluationFormController extends Controller
{
    
    public function getAllEvaluationForms(){
        $evaluation_forms = EvaluationForm::select('id', 'title', 'description', 'version', 'created_by', 'active', 'created_at')->with(['user' => function($query){$query->select('user_id', 'name');
        }])->get();
        return DataTables::of($evaluation_forms)->make(true);
    }
    
    public function createEvaluationForm(Request $request){
        if($request->id == ''){
            $evaluation_forms['title'] = $request->title;
            $evaluation_forms['description'] = $request->description;
            $headers = collect($request->headervalues)->toJson();
            $ratings = collect($request->ratings)->toJson();
            $evaluation_forms['headers'] = $headers;
            $evaluation_forms['ratings'] = $ratings;
            $evaluation_forms['version'] = $request->version;
            $evaluation_forms['active'] = $request->active;
            //$evaluation_forms['client_id'] = $request->client_id;
            $evaluation_forms['created_by'] = Auth::user()->user_id;
            $evaluation_form = EvaluationForm::create($evaluation_forms);
            $qgroup = [];
            $gquestion = [];
            $sgroup = [];
            foreach($request->questiongroups as $key => $questiongroup){
                $qgroup['evaluation_form_id'] = $evaluation_form->id;
                $qgroup['title'] = $questiongroup['groupname'];
                $qgroup['description'] = $questiongroup['groupdescription'];
                $qgroup['groupmode'] = $questiongroup['groupmode'];
                $qgroup['percentage'] = $questiongroup['percentage'];
                $qgroup['groupmode'] = $questiongroup['groupmode'];
                $groupid = QuestionGroup::create($qgroup);
                foreach($questiongroup['questions'] as $qkey => $question){
                    $gquestion['group_id'] = $groupid->id;
                    $gquestion['title'] = $question['questiontitle'];
                    $gquestion['type'] = $question['questiontype'];
                    $gquestion['mandatory'] = (isset($question['mandatory'])) ? $question['mandatory'] : 0;
                    $gquestion['values'] = json_encode($question['options']);
                    $gquestion['score'] = json_encode($question['score']);
                    $gquestion['rawvalues'] = $question['rawvalues'];
                    $gquestion['comment'] = $question['showcommentbox'];
                    $gquestion['commentmandatory'] = $question['commentmandatory'];
                    $gquestion['actual_score'] = $question['weightage'];
                    $createquestion = Question::create($gquestion);
                    if(isset($question['subgroups'])){
                        $i = 0;
                        foreach($question['subgroups'] as $skey => $subgroup){
                            $sgroup[$i]['group_id'] = $groupid->id;
                            $sgroup[$i]['parent_question_id'] = $createquestion->id;
                            $sgroup[$i]['title'] = $subgroup['questiontitle'];
                            $sgroup[$i]['mandatory'] = (isset($subgroup['mandatory'])) ? $subgroup['mandatory'] : 0;
                            $sgroup[$i]['type'] = $subgroup['questiontype'];
                            $sgroup[$i]['values'] = json_encode($subgroup['options']);
                            $sgroup[$i]['score'] = json_encode($subgroup['score']);
                            $sgroup[$i]['comment'] = $subgroup['showcommentbox'];
                            $sgroup[$i]['commentmandatory'] = $subgroup['commentmandatory'];
                            $sgroup[$i]['actual_score'] = $subgroup['weightage'];
                            $i++;
                        }
                        $createsubgroup = Question::insert($sgroup);
                        unset($sgroup);
                        $sgroup = [];
                    }
                }
            }    
        }else{
            $checksurvey =  Evaluation::where('evaluation_form_id', $request->id)->count();
            if($checksurvey == 0){
                $evaluation_forms['title'] = $request->title;
                $evaluation_forms['description'] = $request->description;
                $headers = collect($request->headervalues)->toJson();
                $ratings = collect($request->ratings)->toJson();
                $evaluation_forms['headers'] = $headers;
                $evaluation_forms['ratings'] = $ratings;
                $evaluation_forms['version'] = $request->version;
                //$evaluation_forms['client_id'] = $request->client_id;
                $evaluation_forms['active'] = $request->active;
                $evaluation_forms['created_by'] = Auth::user()->user_id;
                $evaluation_form = EvaluationForm::where('id', $request->id)->update($evaluation_forms);
                $qgroup = [];
                $gquestion = [];
                $sgroup = [];
                $qids = QuestionGroup::where('evaluation_form_id', $request->id)->pluck('id');
                $delq = Question::whereIn('group_id', $qids)->whereNotNull('parent_question_id')->delete();
                $delq = Question::whereIn('group_id', $qids)->delete();
                $delqgroup = QuestionGroup::where('evaluation_form_id', $request->id)->forcedelete();
                foreach($request->questiongroups as $key => $questiongroup){
                    $qgroup['evaluation_form_id'] = $request->id;
                    $qgroup['title'] = $questiongroup['groupname'];
                    $qgroup['description'] = $questiongroup['groupdescription'];
                    $qgroup['percentage'] = $questiongroup['percentage'];
                    $qgroup['groupmode'] = $questiongroup['groupmode'];
                    $groupid = QuestionGroup::create($qgroup);
                    foreach($questiongroup['questions'] as $qkey => $question){
                        $gquestion['group_id'] = $groupid->id;
                        $gquestion['title'] = $question['questiontitle'];
                        $gquestion['type'] = $question['questiontype'];
                        $gquestion['mandatory'] = (isset($question['mandatory'])) ? $question['mandatory'] : 0;
                        $gquestion['values'] = json_encode($question['options']);
                        $gquestion['score'] = json_encode($question['score']);
                        $gquestion['rawvalues'] = $question['rawvalues'];
                        $gquestion['comment'] = $question['showcommentbox'];
                        $gquestion['commentmandatory'] = $question['commentmandatory'];
                        $gquestion['actual_score'] = $question['weightage'];
                        $createquestion = Question::create($gquestion);
                        if(isset($question['subgroups'])){
                            $i = 0;
                            foreach($question['subgroups'] as $skey => $subgroup){
                                $sgroup[$i]['group_id'] = $groupid->id;
                                $sgroup[$i]['parent_question_id'] = $createquestion->id;
                                $sgroup[$i]['title'] = $subgroup['questiontitle'];
                                $sgroup[$i]['mandatory'] = (isset($subgroup['mandatory'])) ? $subgroup['mandatory'] : 0;
                                $sgroup[$i]['type'] = $subgroup['questiontype'];
                                $sgroup[$i]['values'] = json_encode($subgroup['options']);
                                $sgroup[$i]['score'] = json_encode($subgroup['score']);
                                $sgroup[$i]['comment'] = $subgroup['showcommentbox'];
                                $sgroup[$i]['commentmandatory'] = $subgroup['commentmandatory'];
                                $sgroup[$i]['actual_score'] = $subgroup['weightage'];
                                $i++;
                            }
                            $createsubgroup = Question::insert($sgroup);
                            unset($sgroup);
                            $sgroup = [];
                        }
                    }
                }
            }else{
                $data['errors']['Invalid Save']['0'] = 'Evaluation added for a call so cannot be updated'; 
                $data['message'] = 'Validation Failed';
                return response($data, 422);
            }
        }
        

        if(isset($evaluation_form->id)){
            $data['message'] = 'Evaluation Created Successfully'; 
            return response($data, 200);
        }elseif($request->id != ''){
            $data['message'] = 'Evaluation Updated Successfully'; 
            return response($data, 200);
        }else{
            $data['errors']['Invalid Search']['0'] = 'Evaluation not saved successfully'; 
            $data['message'] = 'Validation Failed';
            return response($data, 422);
        }
    }

    public function getOneEvaluationForm($id){
        $evaluation_form = EvaluationForm::where('id', $id)->first();
        if(count($evaluation_form) != 0){
        $surveyquestions = QuestionGroup::select('title as groupname', 'description as groupdescription', 'groupmode','percentage','id')->where('evaluation_form_id', $id)->with(['questions' => function($query){
            $query->select('id as questionid', 'type as questiontype', 'group_id', 'title as questiontitle', 'mandatory', 'score', 'values', 'comment', 'commentmandatory', 'actual_score', 'rawvalues');
        }])->get();

        $subgroups = Question::select('id as questionid', 'type as questiontype', 'title as questiontitle', 'parent_question_id','score','values as options', 'comment as showcommentbox', 'commentmandatory', 'mandatory', 'actual_score as weightage')->whereNotNull('parent_question_id')->get();
        $subgroupcollection = collect($subgroups);

        $questions = [];
        $v = json_decode($evaluation_form->headers, true);
        $r = json_decode($evaluation_form->ratings, true);
        $val = [];
        $val['id'] = $id;
        $val['headers'] = $v;
        $val['ratings'] = $r;
        $val['title'] = $evaluation_form['title'];
        $val['description'] = $evaluation_form['description'];
        $val['version'] = $evaluation_form['version'];
        //$val['client_id'] = $evaluation_form['client_id'];
        $val['active'] = $evaluation_form['active'];
        foreach($surveyquestions as $key => $values){
            $questions[$key]['groupname'] = $values['groupname'];
            $questions[$key]['groupdescription'] = $values['groupdescription'];
            $questions[$key]['groupid'] = $values['id'];
            $questions[$key]['percentage'] = $values['percentage'];
            $questions[$key]['groupmode'] = $values['groupmode'];
            foreach($values['questions'] as $qkey => $qvalues){
                $questions[$key]['questions'][$qkey]['questionid'] = $qvalues['questionid'];
                $questions[$key]['questions'][$qkey]['questiontype'] = $qvalues['questiontype'];
                $questions[$key]['questions'][$qkey]['questiontitle'] = $qvalues['questiontitle'];
                $questions[$key]['questions'][$qkey]['options'] = json_decode($qvalues['values'], true);
                $questions[$key]['questions'][$qkey]['comment'] = '';
                $questions[$key]['questions'][$qkey]['score'] = json_decode($qvalues['score'], true);
                $questions[$key]['questions'][$qkey]['rawvalues'] = $qvalues['rawvalues'];
                $questions[$key]['questions'][$qkey]['weightage'] = $qvalues['actual_score'];
                $questions[$key]['questions'][$qkey]['mandatory'] = $qvalues['mandatory'];
                $questions[$key]['questions'][$qkey]['showcommentbox'] = $qvalues['comment'];
                $questions[$key]['questions'][$qkey]['commentmandatory'] = $qvalues['commentmandatory'];
                $questions[$key]['questions'][$qkey]['subgroups'] = $subgroupcollection->where('parent_question_id', $qvalues['questionid'])->each(function($item, $key){
                    $item->options = json_decode($item->options);
                    if($item->score){
                        $item->score = json_decode($item->score);
                    }
                })->values()->all();

            }
        }
        $val['questiongroups'] = $questions;
        return $val;
        }else{
            $data['message'] = 'Evaluation does not exist';
            return $data; 
        }
    }
    public function updateWorklist($request, $id){
        $checkworklist = Worklist::where('call_id', $request->callid)->where('created_by', Auth::user()->user_id)->with(['campaign' => function($query)use($request){
            $query->where('survey_id', $request->surveyid);
        }])->get();
        $cwc = count($checkworklist);
        if($cwc > 0){
            foreach($checkworklist as $cwk => $cwval){
                if($cwval['campaign'] != null){
                    $cid = $cwval['campaign']['id'];
                    $wid = $cwval['id'];
                }
            }
            $checkcampaign = CampaignCall::where('call_id', $request->callid)->where('campaign_id', $cid)->count();
            $wc = Worklist::where('call_id', $request->callid)->where('campaign_id', $cid)->count();
            if($checkcampaign == 1 && $wc == 0){
                $worklist['call_id'] = $request->callid;
                $worklist['campaign_id'] = $cid;
                $worklist['created_by'] = Auth::user()->user_id;
                $worklist['status'] = ($request->status == 'Submitted') ? 'completed' : 'inprogress';
                $worklist['evaluation_id'] = $id;
                Worklist::create($worklist);
                $campaign['status'] = ($request->status == 'Submitted') ? 'completed' : 'inprogress';
                CampaignCall::where('call_id', $request->callid)->where('campaign_id', $cid)->update($campaign);
                return;
            }
            if($wc == 1){
                $worklist['status'] = ($request->status == 'Submitted') ? 'completed' : 'inprogress';
                $worklist['evaluation_id'] = $id;
                $wl = Worklist::where('id', $wid)->update($worklist);
                $campaign['status'] = ($request->status == 'Submitted') ? 'completed' : 'inprogress';
                CampaignCall::where('call_id', $request->callid)->where('campaign_id', $cid)->update($campaign);
                return;
            }
        }
        return;
    }

    public function getEvaluationForms(Request $request){
        if(isset($request->callid)){
            $call = Call::select('unique_id', 'client')->where('unique_id', $request->callid)->first();
            $evaluation_form_id = Evaluation::where('call_id', $request->callid)->pluck('evaluation_form_id');
            if(count($evaluation_form_id) > 0){
                $evaluation_forms = EvaluationForm::select('id', 'title')->where('active', 1)->whereNotIn('id', $evaluation_form_id)->get();
            }else{
                $evaluation_forms = EvaluationForm::select('id', 'title')->where('active', 1)->get();
            }
        }else{
            $evaluation_forms = EvaluationForm::select('id', 'title')->where('active', 1)->get();
        }
        return $evaluation_forms;
    }

    public function getEvaluationFormQuestions($id){
        $evaluation_form = EvaluationForm::where('id', $id)->first();
        if(count($evaluation_form) != 0){
        $surveyquestions = QuestionGroup::select('title as groupname', 'description as groupdescription', 'groupmode','percentage','id')->where('evaluation_form_id', $id)->with(['questions' => function($query){
            $query->select('id as questionid', 'type as questiontype', 'group_id', 'title as questiontitle', 'mandatory', 'score', 'values', 'comment', 'commentmandatory', 'actual_score', 'rawvalues');
        }])->get();

        $subgroups = Question::select('id as questionid', 'type as questiontype', 'title as questiontitle', 'parent_question_id','score','values as options', 'comment as showcommentbox', 'commentmandatory', 'mandatory', 'actual_score as weightage')->whereNotNull('parent_question_id')->get();
        $subgroupcollection = collect($subgroups);

        $questions = [];
        $v = json_decode($evaluation_form->headers, true);
        $r = json_decode($evaluation_form->ratings, true);
        $val = [];
        if(isset($_GET['callid'])){
            $calldetails = Call::select('ucid as callid','unique_id','agent_id','segstart as callstartdatetime','segstop as callenddatetime','duration as callduaration','direction as calldirection','outbound_cid as outboundcid','calling_pty as callingpartycid','dialed_num as diallednumber','screenfileurl as screenfileurl','recordingfileurl as recordingfile','recordingfileurl as audiofileurl','client as client', 'dialed_num', 'extension', 'disposition', 'customer_num','usr_grp')->with(['employee' => function($query){
            $query->select('name as agentname','user_id', 'supervisor_user_id', 'user_id as eid', 'vendor');
            }, 'comments' => function($query){
                $query->select('commentable_id', 'commentable_type');
            }])->where('ucid', $_GET['callid'])->first();
            $audiofileurl = Call::multiAudioFile($calldetails->audiofileurl, $calldetails->employee->agentname,$calldetails->usr_grp);
            $calldetails->audiofileurl = $audiofileurl;
            $val['calldetails'] = $calldetails;
        }

        $val['headers'] = $v;
        $val['ratings'] = $r;
        $val['description'] = $evaluation_form['title'];
        foreach($surveyquestions as $key => $values){
            if ($values['groupname'] == 'Header Details') {
                continue;
            }
            $questions[$key]['groupname'] = $values['groupname'];
            $questions[$key]['groupdescription'] = $values['groupdescription'];
            $questions[$key]['groupid'] = $values['id'];
            $questions[$key]['percentage'] = $values['percentage'];
            $questions[$key]['groupmode'] = $values['groupmode'];
            foreach($values['questions'] as $qkey => $qvalues){
                $questions[$key]['questions'][$qkey]['questionid'] = $qvalues['questionid'];
                $questions[$key]['questions'][$qkey]['questiontype'] = $qvalues['questiontype'];
                $questions[$key]['questions'][$qkey]['questiontitle'] = $qvalues['questiontitle'];
                $questions[$key]['questions'][$qkey]['options'] = json_decode($qvalues['values'], true);
                $questions[$key]['questions'][$qkey]['comment'] = '';
                $questions[$key]['questions'][$qkey]['score'] = '';
                $questions[$key]['questions'][$qkey]['rawvalues'] = $qvalues['rawvalues'];
                $questions[$key]['questions'][$qkey]['weightage'] = $qvalues['actual_score'];
                $questions[$key]['questions'][$qkey]['mandatory'] = $qvalues['mandatory'];
                $questions[$key]['questions'][$qkey]['showcommentbox'] = $qvalues['comment'];
                $questions[$key]['questions'][$qkey]['commentmandatory'] = $qvalues['commentmandatory'];
                $questions[$key]['questions'][$qkey]['subgroups'] = $subgroupcollection->where('parent_question_id', $qvalues['questionid'])->each(function($item, $key){
                    $item->options = json_decode($item->options);
                    if($item->score){
                        $item->score = json_decode($item->score);
                    }
                })->values()->all();

            }
        }
        $val['questiongroups'] = array_values($questions);
        return $val;
        }else{
            $data['message'] = 'Evaluation does not exist';
            return $data; 
        }
    }

    /**
     * Evaluation Form List :
     * This function will return all form ids based on the form status input
     * If additional parameter expands as true is passed then it will return form details as well
    **/
    public function evaluationFormsListGet(EvaluationFormsGetRequest $request){
        $evaluationForms = EvaluationForm::select('id', 'title')->where('active', 1);
        if($request->form_id){
            $evaluationForms->whereIn('id', explode(",", $request->form_id));
        }
        $data = $evaluationForms->get();
        return response($data, 200);
    }

    /**
     * Evaluation Form Detail :
     * This function will return form details for the given form id
     * Response - form details + application transitions
    **/
    public function evaluationFormGet(EvaluationFormGetRequest $request){
        $evaluationForm = EvaluationForm::where('id', $request->form_id)->first();
        if(isset($evaluationForm->id)){
            //Question groups list
            $questionGroups = QuestionGroup::select('title as groupname', 'description as groupdescription', 'groupmode','percentage','id')->where('evaluation_form_id', $request->form_id)->with(['questions' => function($query){
                $query->select('id as questionid', 'type as questiontype', 'group_id', 'title as questiontitle', 'mandatory', 'score', 'values', 'comment', 'commentmandatory', 'actual_score', 'rawvalues');
            }])->get();

            //Question sub-groups list
            $questionSubGroups = Question::select('id as questionid', 'type as questiontype', 'title as questiontitle', 'parent_question_id','score','values as options', 'comment as showcommentbox', 'commentmandatory', 'mandatory', 'actual_score as weightage')->whereNotNull('parent_question_id')->get();
            $questionSubGroupsCollection = collect($questionSubGroups);

            $formResponse =
            [
                'title'      => $evaluationForm['title'],
                'description'=> $evaluationForm['description'],
                'headers'    => json_decode($evaluationForm->headers, true),
                'ratings'    => json_decode($evaluationForm->ratings, true),
                'version'    => $evaluationForm->version
            ];
            //Question groups
            $questions = [];
            foreach($questionGroups as $groupKey => $group){
                if ($group['groupname'] == 'Header Details') {
                    continue;
                }
                $questions[$groupKey] = [
                    'groupid'           => $group['id'],
                    'groupname'         => $group['groupname'],
                    'groupmode'         => $group['groupmode'],
                    'groupdescription'  => $group['groupdescription'],
                    'percentage'        => $group['percentage'],
                ];
                //Questions
                foreach($group['questions'] as $questionKey => $question){
                    $questions[$groupKey]['questions'][$questionKey] = [
                        'questionid'        => $question['questionid'],
                        'questiontitle'     => $question['questiontitle'],
                        'questiontype'      => $question['questiontype'],
                        'options'           => json_decode($question['values'], true),
                        'comment'           => "",
                        'score'             => "",
                        'rawvalues'         => $question['rawvalues'],
                        'weightage'         => $question['actual_score'],
                        'mandatory'         => $question['mandatory'],
                        'showcommentbox'    => $question['comment'],
                        'commentmandatory'  => $question['commentmandatory'],
                        'subgroups'         => $questionSubGroupsCollection->where('parent_question_id', $question['questionid'])->each(function($item, $key)
                                            {
                                                $item->options = json_decode($item->options);
                                                if($item->score){
                                                    $item->score = json_decode($item->score);
                                                }
                                            })->values()->all(),
                    ];
                }
            }
            $formResponse['questiongroups'] = array_values($questions);
            return $formResponse;
        }else{
            $data = ['status'=>'0', 'message' => 'Evaluation form does not exist'];
            return response($data, 200);
        }
    }

    /**
     * Evaluation Form Create :
     * Used to create a new form with form, question groups, quesions & question sub-groups inputs
    **/
    public function evaluationFormCreate(EvaluationFormCreateRequest $request){
        try {
            //Form : Insert
            $evaluationFormsData = [
                'title'         => $request->title,
                'description'   => $request->description,
                'headers'       => collect($request->headers)->toJson(),
                'ratings'       => collect($request->ratings)->toJson(),
                'version'       => $request->version,
                'active'        => 1,
                'created_by'    => Auth::user()->user_id,
            ];
            $evaluationFormsCreate = EvaluationForm::create($evaluationFormsData);
            
            //Question Groups
            foreach($request->questiongroups as $key => $questiongroup)
            {
                //Question Groups : Insert
                $qGroupData = [
                    'evaluation_form_id'=> $evaluationFormsCreate->id,
                    'title'             => $questiongroup['groupname'],
                    'description'       => $questiongroup['groupdescription'],
                    'groupmode'         => $questiongroup['groupmode'],
                    'percentage'        => $questiongroup['percentage'],
                    'groupmode'         => $questiongroup['groupmode'],
                ];
                $questionGroupCreate = QuestionGroup::create($qGroupData);
                
                //Questions
                foreach($questiongroup['questions'] as $questionKey => $question)
                {
                    //Questions : Insert
                    $gquestion = [
                        'group_id'  => $questionGroupCreate->id,
                        'title'     => $question['questiontitle'],
                        'type'      => $question['questiontype'],
                        'mandatory' => (isset($question['mandatory'])) ? $question['mandatory'] : 0,
                        'values'    => json_encode($question['options']),
                        'score'     => json_encode($question['score']),
                        'rawvalues' => $question['rawvalues'],
                        'comment'   => $question['showcommentbox'],
                        'commentmandatory'  => $question['commentmandatory'],
                        'actual_score'      => $question['weightage'],
                    ];
                    $Question = Question::create($gquestion);
                    
                    //Sub-Groups : Insert
                    if(isset($question['subgroups'])){
                        $i = 0;
                        $subGroups = [];
                        foreach($question['subgroups'] as $skey => $subGroup){
                            $subGroups[$i++] = [
                                'group_id'          => $questionGroupCreate->id,
                                'parent_question_id'=> $Question->id,
                                'title'             => $subGroup['questiontitle'],
                                'mandatory'         => (isset($subGroup['mandatory'])) ? $subGroup['mandatory'] : 0,
                                'type'              => $subGroup['questiontype'],
                                'values'            => json_encode($subGroup['options']),
                                'score'             => json_encode($subGroup['score']),
                                'comment'           => $subGroup['showcommentbox'],
                                'commentmandatory'  => $subGroup['commentmandatory'],
                                'actual_score'      => $subGroup['weightage'],
                            ];
                        }
                        $createsubgroup = Question::insert($subGroups);
                    }
                }
            }
            if(isset($evaluationFormsCreate->id))
                $data = ['status'=>1, "form_id"=>$evaluationFormsCreate->id];
            else
                $data = ['status'=>0, "form_id"=>0]; 
            
            return response($data, 200);
        } catch (\Exception $e) {
            return response(["errors"=>"Server error"], 500);
        }
    }

}