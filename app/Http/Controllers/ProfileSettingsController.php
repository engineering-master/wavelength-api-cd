<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\NonEmployeeUser;
use App\UserProfileAudit;
use App\GroupUser;
use App\ModelHasRole;
use App\Http\Requests\ProfileSettingsRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Notifications\UserProfileNotification;
use Auth, DB;
use Carbon\Carbon;
use Exception;
use Notification;
use Log;

class ProfileSettingsController extends Controller
{
    /**
     * Update the profile from the request.
     *
     * @return array
     */
    public function profileSettings(ProfileSettingsRequest $request)
    {
        try {
            $userprofileRequest = $request->all();
            foreach ($userprofileRequest as $key => $value) {
                $user = User::where('user_id', $value['user_id'])->with('modelrole')->first(); //App User
                $userprofile = new UserProfileAudit; // Insert audit table for every user profile update request
                $userprofile->user_id = $user->user_id;
                if ($user->modelrole) {
                    $userprofile->prev_role_id = $user->modelrole->role_id;
                }
                if ($value['role_id']) {
                    $userprofile->change_role_id = isset($value['role_id']) ? $value['role_id'] : '';
                } //Change role
                $userprofile->prev_email = $user->email;
                $userprofile->change_email = isset($value['email']) ? $value['email'] : '';
                $userprofile->prev_unite = $user->unite;
                $userprofile->change_unite = isset($value['unite']) ? $value['unite'] : '';
                $userprofile->created_at = Carbon::Now();
                $userprofile->created_by = Auth::user()->user_id;
                $userprofile->save(); //Audit table insert
                $notifyTo = [];
                if ($user->suerpvisorid) {
                    $notifyTo = User::where('dim_user_id', $user->suerpvisorid)->first(); //App User
                }
                $authuserrole = Auth::user()->roles->pluck('name')[0]; //Get Auth user role name
                if ($authuserrole == 'Administrator' || $authuserrole == 'QA Manager' || $authuserrole == 'Supervisor') {
                    $profileStatus = new ProfileUpdateRequest();
                    $profileStatus->id = $userprofile->id;
                    $profileStatus->status = 'Accept';
                    $this->updateProfile($profileStatus);
                    $data['message'] = 'Profile updated successfully';
                } else {
                    Notification::send($notifyTo, new UserProfileNotification($user));
                    $data['message'] = 'Profile update request send successfully';
                }
            }
            return response($data, 200);
        } catch (Exception $e) {
            DB::rollback();
            $data['message'] = 'Profile not updated ' . $e->getMessage();
            return response($data, 422);
        }
    }
    /**
     * Approve the profile from the request.
     *
     * @return array
     */
    public function updateProfile(ProfileUpdateRequest $request)
    {
        DB::beginTransaction();
        try {
            $authuserrole = Auth::user()->roles->pluck('name')[0]; //Get Auth user role name //$authuserrole =='QA Specialist'||
            if ($authuserrole == 'Administrator' || $authuserrole == 'QA Manager' || $authuserrole == 'Supervisor') {
                $userprofile = UserProfileAudit::where('id', $request->id)->whereNull('status')->first();
                if ($userprofile) {
                    if ($request->status == 'Reject') {
                        $userprofile->status = $request->status; //Update status accept/reject                    
                        $userprofile->save();
                        DB::commit();
                        $data['message'] = 'Profile update request reject successfully';
                        return response($data, 200);
                    }
                    $user = User::where('user_id', $userprofile->user_id)->with('modelrole')->first(); //App User
                    //Role id change based on request
                    $role = (($userprofile->change_role_id == 1) ? 1 : (($userprofile->change_role_id == 2) ? 2 : (($userprofile->change_role_id == 6) ? 4 : (($userprofile->change_role_id == 7) ? 5 : 3))));
                    $NonEmployeeUser = NonEmployeeUser::where('id', $userprofile->user_id)->first(); // JWT user
                    $groupUser = GroupUser::where('user_id', $userprofile->user_id)->first();
                    if (($NonEmployeeUser) && ($groupUser)) {
                        if ($userprofile->change_role_id) {
                            ModelHasRole::where('model_id', $userprofile->user_id)->delete();
                            $groupUser->group_id = $role;
                            $groupUser->update();
                            $user->assignRole($userprofile->change_role_id);
                        }
                        $user->email = $userprofile->change_email ? $userprofile->change_email : $user->email;
                        $user->unite = $userprofile->change_unite ? $userprofile->change_unite : $user->unite;
                        $updateStatus = $user->save(); //App user email or unite update
                        if ($updateStatus) {
                            $NonEmployeeUser->email = $userprofile->change_email ? $userprofile->change_email : $NonEmployeeUser->email;
                            $NonEmployeeUser->updated = 1; //Set user updated  1
                            $NonEmployeeUser->save();
                            //Update audits table
                            $userprofile->status = $request->status; //Update status accept/reject
                            $userprofile->updated_by = Auth::user()->user_id;
                            $userprofile->updated_at = Carbon::Now();
                            $userprofile->save();
                            DB::commit();
                            $notifyTo = [];
                            if ($user->suerpvisorid) {
                                $notifyTo = User::where('dim_user_id', $user->suerpvisorid)->first(); //App User
                            }
                            Notification::send($notifyTo, new UserProfileNotification($user));
                            $data['message'] = 'Profile updated successfully';
                            return response($data, 200);
                        } else {
                            DB::rollback();
                            $data['message'] = 'Server error on updating profile.';
                            return response($data, 501);
                        }
                    } else {
                        $data['message'] = 'Profile not found';
                        return response($data, 422);
                    }
                } else {
                    $data['message'] = 'Profile not found';
                    return response($data, 422);
                }
            } else {
                $data['message'] = 'This action is unauthorized.';
                return response($data, 403);
            }
        } catch (Exception $e) {
            DB::rollback();
            $data['message'] = 'Profile not updated' . $e->getMessage();
            return response($data, 422);
        }
    }
}
