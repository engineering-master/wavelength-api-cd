<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserListRequest;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Cache;
use App\User;
use App\Employee;
use App\EvaluationForm;
use App\Disposition;
use Auth;

class UserController extends Controller
{
    /**
     * Display user information.
     *
     * @return Response
     */
    
    public function userBasicInformation(Request $request)
    {
        $cacheExpireTime = '60';
        $userInfo = Auth::user(); // Get Auth user information

        if( $userInfo )
        {
            $token = $request->bearerToken();
            $claims = JWTAuth::getJWTProvider()->decode($token);

            $user = array();
            $user['id'] = $claims['sub'];
            $user['dim_user_id'] = $userInfo->dim_user_id;
            $user['name'] = $userInfo->name;
            $user['role'] = $claims['groups'];

            $u = User::with('roles')->where('user_id', $user['id'])->first();
            if ($u) {
                if ($u->roles) {
                    $roles = $u->roles;
                    $user['role'] = $roles['0']['name'];
                }
                $allpermissions = collect($u->getPermissionsViaRoles())->groupBy('page')->toArray();
                $i = 0;
                foreach($allpermissions as $key => $value){
                        $user['pages'][$i]['page'] = $key;
                        $user['pages'][$i]['permissions'] = $value;
                        $i++;
                }
            }
            $users[0] = $user;
            return  json_encode($users);
        } else {
            return response()->json(['message' => 'Unauthorized action'], 401);
        }
    }

    /**
     * Display JWT claims and user information.
     *
     * @return Response
     */

    public function jwtParser(Request $request)
    {
        $user = Auth::user();
        $token = $request->bearerToken();
        $claims = JWTAuth::getJWTProvider()->decode($token);
        $userInfo['user_info'] = $user;
        $userInfo['claims'] = $claims;
        return  json_encode($userInfo);
    }

    /**
     * Display JWT claims and user information.
     *
     * @return Response
     */
    
    public function listUsers(Request $request)
    {
        if(count($request['roleids']) > 0){
            isset($request->sortordercolumn) ? $request->sortordercolumn : $request->sortordercolumn = 'name';
            isset($request->sortorder) ? $request->sortorder : 'ASC';
            $roles = Role::whereIn('id', $request['roleids'])->get();
            $users = User::role($roles);
            
            if($request->username != ""){
                $users->where('name', 'like', '%'.$request['username'].'%');
            }

            $users->orderBy($request->sortordercolumn, $request->sortorder);

            $one = $users->skip($request['skip'])->take($request['take'])->get();

            $all = [];
            foreach($one as $k => $user){
                $all[$k]['user_id'] = $user['user_id'];
                $all[$k]['name'] = $user['name'];
                $all[$k]['email'] = $user['email'];
                $all[$k]['role'] = ((count($user->roles)) > 0) ? $user->roles['0']['name'] : '';
            }
            $v['users'] = $all;
            $v['count'] = $this->getUsersCount($request, $roles);
        }else{
            $v['users'] = 0;
            $v['count'] = 0;
        }
        
        return $v;
    }

    public function getUsersCount($request, $roles){
        $users = User::role($roles);

        if($request->username != ""){
            $users->where('name', 'like', '%'.$request['username'].'%');
        }

        $count = $users->count();
        return $count;
    }


     /**
     * Get a list of QA specialist for reevaluation:-
     * This API will list all users
     * @return array
     */
    public function getusers(UserListRequest $request){
        try{
            
            $roleids = explode(',', $request->role_id);
            $users = User::with('modelrole')->whereHas('modelrole' , function($q)use($roleids){
                $q->whereIn('role_id',$roleids);
            })->get();
            $data = [];
            if ($users->count() > 0) {
                foreach ($users as $k => $user) {
                    $data[$k]['employee_name'] = $user['name'];
                    $data[$k]['eid'] = $user['user_id'];
                }
                return response($data, 200);
            }
            else{
                 $data['message'] = 'No users found'; 
                 return response($data, 200);
            }
        } catch (Exception $ex) { // Anything that went wrong
        return response()->json(['errors' => 'Something went wrong,try again later '.$ex->getMessage()], 422);
        }  
    }

    public function sendRocketMessage($request){
        $ch = curl_init('https://unite.fpsinc.com/api/v1/login');
        $data = array('username' => 'umesh', 'password' => 'Bdp5c8eT3ZqN');
        $payload = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $v = json_decode($result);
        $userid = $v->data->userId;
        $auth = $v->data->authToken;
        curl_close($ch);

        $chr = curl_init('https://unite.fpsinc.com/api/v1/im.create');
        $r1 = array('username' => $request['username']);
        $payloadr1 = json_encode($r1);
        curl_setopt($chr, CURLOPT_POSTFIELDS, $payloadr1);
        curl_setopt($chr, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'X-Auth-Token:'.$auth.'', 'X-User-Id:'.$userid));
        curl_setopt($chr, CURLOPT_RETURNTRANSFER, true);
        $roomid = curl_exec($chr);
        $rv = json_decode($roomid);
        if ($rv->success == true) {
            $rid = $rv->room->_id;
            curl_close($chr);
            
            $chs = curl_init('https://unite.fpsinc.com/api/v1/chat.postMessage');
            $chsdata = array('roomId' => $rid, 'text' => $request['text']);
            $payloadchs = json_encode($chsdata);
            curl_setopt($chs, CURLOPT_POSTFIELDS, $payloadchs);
            curl_setopt($chs, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'X-Auth-Token:'.$auth.'', 'X-User-Id:'.$userid));
            curl_setopt($chs, CURLOPT_RETURNTRANSFER, true);
            $sendmessage = curl_exec($chs);
            curl_close($chs);
            return "message Sent Successfully";
        } else {
            return;
        }
    }


}