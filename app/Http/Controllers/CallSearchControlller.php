<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use DataTables;
use App\User;
use App\Employee;
use App\SystemId;
use App\Call;
use Auth;
use DB;
use App\CallSearch\CallSearch;
use App\EvaluationForm;
use App\Question;
use App\QuestionGroup;
use App\Answer;
use App\Evaluation;
use App\Disposition;
use App\EvaluationStatusMaster;
use App\CampaignGroup;
use App\CampaignSkill;
use App\EvaluationFormCampaign;
use App\UserSystemMapping;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class CallSearchControlller extends Controller
{
    public function getCallform(Request $request)
    {
        $form_id = $request['evaluation_form_id'];
        $callid = $request['callid'];
        $val = Call::select('unique_id as callid', 'unique_id', 'lead_id', 'agent_id', 'segstart as callstartdatetime', 'segstop as callenddatetime', 'duration as callduaration', 'direction as calldirection', 'outbound_cid as outboundcid', 'calling_pty as callingpartycid', 'dialed_num as diallednumber', 'screenfileurl as screenfileurl', 'recordingfileurl as recordingfile', 'recordingfileurl as audiofileurl', 'client as client', 'dialed_num', 'extension')->with(
            ['employee' => function ($query) {
                $query->select('name as agentname', 'user_id', 'supervisor_user_id', 'user_id as eid');
            }, 'comments' => function ($query) {
                $query->select('commentable_id', 'commentable_type');
            },'evaluations' => function ($query) use ($form_id) {
                $query->select('id', 'evaluation_form_id', 'call_id', 'status_id', 'agent_id', 'created_by')->where('evaluation_form_id', $form_id);
            }]
        )->where('unique_id', $callid)->first();
        return $val;
    }

    public function getCallSearch(Request $request)
    {
        try {
            $logArray = [];
            $agentids = $temp_table = '';
            $start_getCallSearch = microtime(true);
            $form_id = $request->evaluation_form_id;
            if ($request->from_date != '' || $request->to_date != '') {
                $form_ids = $group_form_ids = $skill_form_ids = [];
                if (!empty($request->group_campaign)) {
                    $group_campaign = explode(',', $request->group_campaign);
                    $group_form_ids = EvaluationFormCampaign::where('type', 'group')
                        ->whereIN('type_id', $group_campaign)
                        ->pluck('form_id')->toArray();
                    if (!empty($request->skill_campaign)) {
                        $skill_campaign = explode(',', $request->skill_campaign);
                        $skill_form_ids = EvaluationFormCampaign::where('type', 'skill')
                            ->whereIN('type_id', $skill_campaign)
                            ->pluck('form_id')->toArray();
                        //ignore duplicate skills
                        $skills = CampaignSkill::select('id','name')->whereIN('id', $skill_campaign)->get()->toArray();
                        $skill_campaign = [];
                        foreach($skills as $skill)
                                $skill_campaign[$skill['name']] = $skill['id'];
                        $skill_campaign = array_values($skill_campaign);
                        $request->skill_campaign = implode(",",$skill_campaign);
                    }
                }
                $form_ids = array_merge($form_ids, $group_form_ids, $skill_form_ids);
                if (empty($form_ids) || !in_array($form_id, $form_ids)) {
                    $data['callcount'] = 0;
                    $data['callresults'] = [];
                    $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
                    $data['message'] = 'Form not maped to any campaign group or skill.';
                    return response($data, 422);
                }
                $from_date = $request['from_date'];
                $to_date = $request['to_date'];
                $duration = $request['minduration'];
                $dialed_num = $request['customer_num'];
                $lead_id = isset($request['lead_id']) ? $request['lead_id'] : "";
                $unique_id = isset($request['unique_id']) ? $request['unique_id'] : "";
                if (Auth::user()->hasRole('Agent')) {
                    $userSystemMapping = UserSystemMapping::where('user_id', Auth::user()->user_id)
                        ->pluck('system_user_id');
                    $request->agent_ids = implode('","', $userSystemMapping->toArray());
                }
                if ($request->agent_ids != '') {
                    $agent_ids = explode(',', $request->agent_ids);
                    if (Auth::user()->hasRole('Supervisor')) {
                        $supervisor_id = UserSystemMapping::where('user_id', Auth::user()->user_id)
                            ->pluck('system_user_id');
                        $agent_ids = User::whereIn('user_id', $agent_ids)
                            ->whereIn('suerpvisorid', $supervisor_id)->pluck('user_id');
                        if (empty(count($agent_ids))) {
                            $data['callcount'] = 0;
                            $data['callresults'] = [];
                            return $data;
                        }
                        $userSystemMapping = UserSystemMapping::whereIn('user_id', $agent_ids)
                            ->pluck('system_user_id');
                        $agentids = implode('","', $userSystemMapping->toArray());
                    } else if (Auth::user()->hasRole('Agent')) {
                        $agentids = $request->agent_ids;
                    } else {
                        $userSystemMapping = UserSystemMapping::whereIn('user_id', $agent_ids)
                            ->pluck('system_user_id');
                        $agentids = implode('","', $userSystemMapping->toArray());
                    }
                } else {
                    if ($request->site != '' || Auth::user()->hasRole('Supervisor')) {
                        $agents = new User;
                        if ($request->site != '') {
                            $sites = explode(',', $request->site);
                            $agents = $agents->whereIn('site', $sites);
                        }
                        if (Auth::user()->hasRole('Supervisor')) {
                            $userSystemMapping = UserSystemMapping::select('system_user_id')
                                ->where('user_id', Auth::user()->user_id)->get();
                            $agents = $agents->whereIn('suerpvisorid', $userSystemMapping);
                        }
                        $start = microtime(true);
                        $logArray['agents']['start'] = $start;
                        $agents = $agents->pluck('user_id');
                        $end = microtime(true);
                        $logArray['agents']['end'] = $end;
                        $logArray['agents']['timediff'] = $end-$start;
                        if (!empty($agents->count())) {
                            $userSystemMapping = UserSystemMapping::whereIn('user_id', $agents)
                                ->pluck('system_user_id');
                            $agentids = $userSystemMapping->toArray();
                            $agentids = implode('","', $agentids);
                        } else {
                            $data['callcount'] = 0;
                            $data['callresults'] = [];
                            return $data;
                        }
                    }
                }
                $table_name = "portal_user";
                if ($agentids) {
                    $agentids = explode('","', $agentids);
                    $employee = Employee::whereIn('user_id', $agentids)->pluck('user_id');
                    if (empty(count($employee))) {
                        $data['callcount'] = 0;
                        $data['callresults'] = [];
                        return $data;
                    }
                    $agentids = implode('","', $employee->toArray());
                    $table_name = 'temp_call_'. strtolower(Auth::user()->user_id);
                    $create_statment = 'CREATE TEMPORARY TABLE '. $table_name .' SELECT `id`, `user_id`, `name`, `vendor` FROM portal_user WHERE portal_user.user_id IN("' . $agentids . '")';
                    $start = microtime(true);
                    $logArray['create_statment']['start'] = $start;
                    DB::statement($create_statment);
                    $end = microtime(true);
                    $logArray['create_statment']['end'] = $end;
                    $logArray['create_statment']['timediff'] = $end-$start;
                    $start = microtime(true);
                    $logArray['insert_random']['start'] = $start;
                    $insert_random = "INSERT INTO " . $table_name . " SELECT 50000+rand()*50000, 'a', 'a', 'a' FROM portal_user LIMIT 1000";
                    DB::statement($insert_random);
                    $end = microtime(true);
                    $logArray['insert_random']['end'] = $end;
                    $logArray['insert_random']['timediff'] = $end-$start;
                    $index_statment = "CREATE INDEX IDX_Users_". strtolower(Auth::user()->user_id) ." ON ". $table_name ."(user_id)";
                    $start = microtime(true);
                    $logArray['index_statment']['start'] = $start;
                    DB::statement($index_statment);
                    $end = microtime(true);
                    $logArray['index_statment']['end'] = $end;
                    $logArray['index_statment']['timediff'] = $end-$start;
                    $temp_table = "Available";
                }

                $sql = "SELECT FCD.ucid as callid, FCD.ucid, FCD.unique_id as unique_id, FCD.lead_id, FCD.agent_id as agent_id, FCD.segstop as callenddatetime, FCD.duration as callduaration, FCD.direction as calldirection, FCD.outbound_cid as outboundcid, FCD.recordingfileurl as audiofileurl,FCD.client as client, FCD.disposition as calldisposition, FCD.customer_num as call_customer_num, FCD.skill_campaign as call_skill_campaign, DU.name AS employee_agentname, FCD.vendor AS employee_vendor, CS.id AS campaign_skill_id, CS.name AS campaign_skill_name, CS.group_id AS campaign_skill_group_id, CG.type AS campaign_group_type, CG.name AS campaign_group_name,FCD.usr_grp as usr_grp, FCD.state as state, $form_id AS form_id ";

                if ($form_id) {
                    $sql .= ", SR.status_id as evaluation_status, SR.id as evaluation_id, SR.assigned_to as assigned_to ";
                }
                $sql .= " FROM  fact_call_details AS FCD ";
                $sql .= " STRAIGHT_JOIN " . $table_name . " AS DU ON FCD.agent_id = DU.user_id";
                if ($form_id) {
                    /* 6 : Dispute Accepted */
                    $sql .= " LEFT JOIN evaluations AS SR ON FCD.ucid = SR.call_id AND status_id != 6 AND SR.evaluation_form_id = " . $form_id;
                }
                $sql .= " JOIN campaign_skills AS CS ON FCD.skill_campaign = CS.name".($request->skill_campaign ? " AND CS.id IN($request->skill_campaign)" : "");
                $sql .= " JOIN campaign_groups AS CG ON CS.group_id = CG.id";
                $sql .= " WHERE FCD.segstop BETWEEN '$from_date' AND '$to_date'";
                if (!empty($duration)) {
                    $sql .= "AND FCD.duration >= '$duration'";
                }
                if (!empty($dialed_num)) {
                    $sql .= " AND FCD.customer_num = '$dialed_num'";
                }
                if (!empty($lead_id)) {
                    $sql .= " AND FCD.lead_id = '$lead_id'";
                }
                if (!empty($unique_id)) {
                    $sql .= " AND FCD.unique_id = '$unique_id'";
                }
                if (!empty($request['disposition'])) {
                    $request['disposition'] = $request['disposition'] . ",junk1,junk2,junk3,junk4,junk11,junk2,junk31,junk121,junk151,junk41,junk42,junk33,junk74,junk411,junk22,junk321,junk221,junk451";
                    $disposition=$this->stringReplace($request['disposition']);
                    $sql .= " AND FCD.disposition IN ($disposition)";
                }
                if (empty($request['skill_campaign'])) {
                    if (!empty($request['group_campaign'])) {
                        $group_campaign = explode(',', $request->group_campaign);
                        $campaignSkill = CampaignSkill::whereIN('group_id', $group_campaign)->pluck('name');
                        if ($campaignSkill) {
                            $skill_campaigns = implode(",", $campaignSkill->toArray());
                            $skill_campaign = $this->stringReplace($skill_campaigns);
                            $sql .= " AND FCD.skill_campaign IN ($skill_campaign)";
                        }
                    }
                } else {
                    $skill_campaigns_id = explode(",", $request['skill_campaign']);
                    $campaignSkill = CampaignSkill::whereIn('id', $skill_campaigns_id)->pluck('name');
                    if ($campaignSkill) {
                        $skill_campaigns = implode(",", $campaignSkill->toArray());
                        $skill_campaign = $this->stringReplace($skill_campaigns);
                        $sql .= " AND FCD.skill_campaign IN ($skill_campaign)";
                    }
                }
                if ($request->sortordercolumn == 'employee.agentname') {
                    $sql .= " ORDER BY employee_agentname $request->sortorder";
                } elseif ($request->sortordercolumn == 'employee.vendor') {
                    $sql .= " ORDER BY employee_vendor $request->sortorder";
                } else {
                    $sql .= " ORDER BY $request->sortordercolumn $request->sortorder";
                }
                $sql .= " LIMIT $request->take OFFSET $request->skip";
                $start = microtime(true);
                $logArray['sql']['start'] = $start;
                $callresults = DB::select(DB::raw($sql));
                $end = microtime(true);
                $logArray['sql']['end'] = $end;
                $logArray['sql']['timediff'] = $end-$start;
                $callresultsArray = json_decode(json_encode($callresults), true);
                $calls['callresults'] =  $callresultsArray;
                $calls['is_count'] = 0;
                foreach ($calls['callresults'] as $key => $callresults) {
                    $calls['callresults'][$key]['employee']['agentname'] = $callresults['employee_agentname'];
                    $calls['callresults'][$key]['employee']['vendor'] = $callresults['employee_vendor'];
                    if (!empty($callresults['evaluation_id'])) {
                        $calls['callresults'][$key]['evaluations']['status'] = $callresults['evaluation_status'];
                        $statusMaster = EvaluationStatusMaster::select('id', 'status')
                            ->where('id', $callresults['evaluation_status'])->first();
                        if ($statusMaster) {
                            $calls['callresults'][$key]['evaluations']['status'] = $statusMaster;
                        }
                        $calls['callresults'][$key]['evaluations']['id'] = $callresults['evaluation_id'];
                        $calls['callresults'][$key]['evaluations']['assigned_to'] = $callresults['assigned_to'];
                    }
                    $audiofileurl = Call::multiAudioFile($callresults['audiofileurl'], $callresults['employee_agentname'], $callresults['usr_grp']);
                    $calls['callresults'][$key]['audiofileurl'] = $audiofileurl;
                }
                if ($calls['is_count']) {
                    $callcount = $this->getCallsCount($request, $table_name, $agentids);
                    $calls['callcount'] = $callcount['callcount'];
                    $logArray = array_merge($logArray, $callcount['log']);
                }
                if ($temp_table) {
                    $drop_statement = 'DROP TEMPORARY TABLE '. $table_name;
                    $start = microtime(true);
                    $logArray['drop_statement']['start'] = $start;
                    DB::statement($drop_statement);
                    $end = microtime(true);
                    $logArray['drop_statement']['end'] = $end;
                    $logArray['drop_statement']['timediff'] = $end-$start;
                }
    
                $logArray['function']['start'] = $start_getCallSearch;
                $end_getCallSearch = microtime(true);
                $logArray['function']['end'] = $end_getCallSearch;
                $logArray['function']['timediff'] = $end_getCallSearch-$start_getCallSearch;
                $this->putLog('getcallsearchlog', $logArray);
                $calls['log'] = $logArray;
                return $calls;
            } else {
                $data['callcount'] = 0;
                $data['callresults'] = [];
                $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
                $data['message'] = 'From date and to date missing.';
                return response($data, 200);
            }
        } catch (Exception $e) {
            $data['callcount'] = 0;
            $data['callresults'] = [];
            $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
            $data['message'] = 'Sorry, something went wrong.';
            return response($data, $e->getStatusCode());
        }
    }

    public function getCallsCount(Request $request, $table_name, $agentids)
    {
        try {
            $logArray = [];
            $start_getCallsCount = microtime(true);
            $form_id = $request->evaluation_form_id;
            if ($request->from_date != '' || $request->to_date != '') {
                $from_date = $request['from_date'];
                $to_date = $request['to_date'];
                $duration = $request['minduration'];
                $dialed_num = $request['customer_num'];
    
                $sql = "SELECT count(*) AS callcount FROM fact_call_details AS FCD ";
                $sql .= " JOIN " . $table_name . " AS DU ON FCD.agent_id = DU.user_id";
                $sql .= " WHERE segstart BETWEEN '$from_date' AND '$to_date'";
                if (!empty($duration)) {
                    $sql .= " AND duration >= '$duration'";
                }
                if (!empty($dialed_num)) {
                    $sql .= " AND customer_num = '$dialed_num'";
                }
                if (!empty($request['disposition'])) {
                    $request['disposition'] = $request['disposition'] . ",junk1,junk2,junk3,junk4,junk11,junk2,junk31,junk121,junk151,junk41,junk42,junk33,junk74,junk411,junk22,junk321,junk221,junk451";
                    $disposition = $this->stringReplace($request['disposition']);
                    $sql .= " AND disposition IN ($disposition)";
                }
                $callresults = DB::select(DB::raw($sql));
                $callresultsArray = json_decode(json_encode($callresults), true);

                $logArray['count']['start'] = $start_getCallsCount;
                $end_getCallsCount = microtime(true);
                $logArray['count']['end'] = $end_getCallsCount;
                $logArray['count']['timediff'] = $end_getCallsCount-$start_getCallsCount;
                $return['log'] = $logArray;
                $return['callcount'] = $callresultsArray[0]['callcount'];
                return $return;
            }
        } catch (Exception $e) {
            $return['callcount'] = 0;
            $return['log'] = [];
            $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria'; 
            $data['message'] = 'Sorry, something went wrong.';
            return response($data, $e->getStatusCode());
        }
    }

       

    public function getCall($callid)
    {
        $val = Call::select('unique_id as callid', 'unique_id', 'agent_id', 'segstart as callstartdatetime', 'segstop as callenddatetime', 'duration as callduaration', 'direction as calldirection', 'outbound_cid as outboundcid', 'calling_pty as callingpartycid', 'dialed_num as diallednumber', 'screenfileurl as screenfileurl', 'recordingfileurl as recordingfile', 'recordingfileurl as audiofileurl', 'client as client', 'dialed_num', 'extension')->with(
            ['employee' => function ($query) {
                $query->select('name as agentname', 'user_id', 'supervisor_user_id', 'user_id as eid');
            }, 'comments' => function ($query) {
                $query->select('commentable_id', 'commentable_type');
            }]
        )->where('unique_id', $callid)->first();
        return $val;
    }

    public function stringReplace($request_string)
    {
        $result = "'" . str_replace(",", "','", $request_string) . "'";
        return $result;
    }

    public function putLog($logfile, $logArray)
    {
        $config = "logging.channels." . $logfile . ".debug";
        if (config($config)) {
            Log::channel($logfile)->Info(print_r($logArray, true));
        }
        return;
    }
}
