<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EnumController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function index(Request $request)
    {
        try {
            $enums = config('enum');
            return $enums; 
        } catch (Exception $ex) {
            return response()->json(
                [ 'errors' => 'Something went wrong,try again later.' ], 
                $ex->getStatusCode()
            );
        }
    }
}
