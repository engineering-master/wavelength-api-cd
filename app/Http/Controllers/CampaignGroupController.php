<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CampaignGroupRequest;
use App\Http\Requests\CampaignMapGroupRequest;
use App\CampaignGroup;
use App\CampaignSkill;
use App\EvaluationFormCampaign;
use App\EvaluationForm;
use Auth;
class CampaignGroupController extends Controller
{
    /**
     * Display a list of all campaign groups.
     *
     * @return \Illuminate\Http\Response response for the input parameter
     */
    public function index(Request $request)
    {
        try {      
            $CampaignGroup=CampaignGroup::get();
            return response($CampaignGroup, 200);
        } catch (Exception $ex) { 
            return response()->json(['errors' => 'Something went wrong, try again later.'], 422);
        }
    }

    /**
     * Store a newly created campaign group in Storage.
     *
     * @param \app\Http\Requests $request input request parameter
     * 
     * @return \Illuminate\Http\Response  response for the input parameter
     */
    public function store(CampaignGroupRequest $request)
    {
        try{
            $campaignData = $request->only(['name','description']);
            $campaignData['name'] = strip_tags($campaignData['name']);
            $campaignData['created_by'] = Auth::user()->user_id;
            $campaignGroup = CampaignGroup::create($campaignData);          
            return response($campaignGroup, 200);
        } catch (Exception $ex) { 
            return response()->json(['errors' => 'Something went wrong, try again later.'], 422);
        }
    }
    /**
     * Update the specified campaign group in storage.
     * 
     * @param int                $id      campaign group id
     * @param \app\Http\Requests $request input request parameter
     * 
     * @return \Illuminate\Http\Response response for the input parameter
     */
    public function update($id,CampaignGroupRequest $request)
    {
        try {      
            $campaignData = $request->only(['name','description']);
            if ($request->has('name')) {
                $campaignData['name'] = strip_tags($campaignData['name']);
            }
            $campaignData['updated_by'] = Auth::user()->user_id;
            $campaignGroup = CampaignGroup::find($id);
            $campaignGroup->fill($campaignData);
            $campaignGroup->update();
            return response($campaignGroup, 200);
        } catch (Exception $ex) {   
            return response()->json(['errors' => 'Something went wrong,try again later.'], 422);
        }
    }

    /**
     * List the campaign skills based on the campaign group id.
     * group/{group_ids}/skills
     * 
     * @param int $group_ids multiple group ids
     *
     * @return \Illuminate\Http\Response
     */
    public function getCampaignSkill($group_ids)
    {
        try {
            $group_ids = explode(',', $group_ids);
            $campaignSkill = CampaignSkill::whereIn('group_id', $group_ids)->select('id', 'name')->get();
            return response()->json($campaignSkill, 200);
        } catch (Exception $ex) {
            return response()->json(
                ['errors' => 'Something went wrong, try again later.'], 
                $ex->getStatusCode()
            );
        }
    }

    /**
     * List the forms related to the campaign group and campaign skills.
     * {type}/{type_ids}/forms
     *
     * @param string                   $type     Contain group/skill
     * @param int                      $type_ids multiple type ids
     * @param \Illuminate\Http\Request $request  Query param
     * 
     * @return \Illuminate\Http\Response 
     */
    public function getCampaignForm($type, $type_ids, Request $request)
    {
        try {
            $evaluationFormCampaign = $campaignSkillFormIds = [];
            $ids = explode(",", $type_ids);
            $form_ids = EvaluationFormCampaign::where('type', $type)
                ->whereIN('type_id', $ids)
                ->pluck('form_id')->toArray();
            if ($type == 'group') {
                $campaignSkillIds = CampaignSkill::whereIn('group_id', $ids);
                $campaignSkillIds = $campaignSkillIds->pluck('id');
                $campaignSkillFormIds = EvaluationFormCampaign::where('type', 'skill')
                    ->whereIN('type_id', $campaignSkillIds)
                    ->pluck('form_id')->toArray();
                $form_ids = array_merge($form_ids, $campaignSkillFormIds);
            } else if ($type == 'skill') {
                $campaignGroupIds = CampaignSkill::whereIn('id', $ids);
                $campaignGroupIds = $campaignGroupIds->pluck('group_id');
                $campaignSkillFormIds = EvaluationFormCampaign::where('type', 'group')
                    ->whereIN('type_id', $campaignGroupIds)
                    ->pluck('form_id')->toArray();
                $form_ids = array_merge($form_ids, $campaignSkillFormIds);
            }
            if (empty(count($form_ids))) {
                $evaluationForm = [];
            } else {
                $evaluationForm = EvaluationForm::whereIn('id', $form_ids);
                if ($request->has('active') && ($request->active == 1 || $request->active == 0)) {
                    $evaluationForm = $evaluationForm->where('active', $request->active);
                }
                $evaluationForm = $evaluationForm->select('id', 'title')->get();
            }
            return response()->json($evaluationForm, 200);
        } catch (Exception $ex) {
            return response()->json(
                ['errors' => 'Something went wrong, try again later.'], 
                $ex->getStatusCode()
            );
        }
    }
    /**
     * It map form against campaign skills or campaign groups.
     * 
     * @param string             $type    campaign group/skill type
     * @param int                $type_id campaign group/skill id
     * @param int                $id      campaign group id
     * @param \app\Http\Requests $request input request parameter
     * 
     * @return \Illuminate\Http\Response  response for the input parameter
     */
    public function mapGroupOrSkills($type,$type_id,$id,CampaignMapGroupRequest $request)
    {
        try {   
            $mapping = EvaluationFormCampaign::where('type', $type)
                ->where('type_id', $type_id)
                ->where('form_id', $id)->first();
            if ($mapping) {
                return response($mapping, 200);
            } else {
                $mapGroupOrSkills = new EvaluationFormCampaign;
                $mapGroupOrSkills->type = $type;
                $mapGroupOrSkills->type_id = $type_id;
                $mapGroupOrSkills->form_id = $id;
                $mapGroupOrSkills->created_by = Auth::user()->user_id;
                $mapGroupOrSkills->save();
                return response($mapGroupOrSkills, 200);
            }
        } catch (Exception $ex) { 
            return response()->json(['errors' => 'Something went wrong,try again later '], 422);
        }
    }
}
