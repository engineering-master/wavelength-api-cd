<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Collection;
use App\User;
use Auth;
use App\Http\Requests\NotificationRequest;
use DB, Log;

class NotificationController extends Controller
{

    /**
     * @param
     * @return
     */
    var $user;

    /**
     * @param  $id
     * @return
     */
    public function markAsRead(NotificationRequest $request)
    {
        try {
            $this->user = Auth::user();
            $notification = $this->user->notifications()->where('id', $request->id)->first();
            $notification->markAsRead();
            
            if ($notification->read_at) {
                return Response::json(array('success' => true), 200);
            } else {
                return response()->json(['message' => 'Notification update failed'], 422);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Something Went Wrong'], 422);
        }
    }

    public function markAllAsRead()
    {
        try {
            $this->user = Auth::user();
            $this->user->unreadNotifications->markAsRead();
            $notification = $this->user->notifications()->whereNull('read_at')->count();

            if (!$notification) {
                return response()->json(['success' => true], 200);
            } else {
                return response()->json(['success' => "Partially Updated"], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Something Went Wrong'], 422);
        }
    }

    /**
     * @param
     * @return
     */
    public function markSelectedAsRead($ids)
    {
        try {
            $this->user = Auth::user();
            $notification_ids = explode(",", $ids);
        
            $this->user->notifications()->whereIn('id', $notification_ids)->get()->markAsRead();
            $notification = $this->user->notifications()->whereIn('id', $notification_ids)->whereNotNull('read_at')->pluck('id')->toArray();
            $notupdatedids=array_diff($notification_ids, $notification);
          
            if (count($notification)==count($notification_ids)) {
                return response()->json(['success' => true], 200);
            } else {
                Log::Info(print_r($notupdatedids, true));
                return response()->json(['success' => "Partially Updated"], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Something Went Wrong'], 422);
        }
    }

    public function getNotifications($sort_type)
    {
        $this->user = Auth::user(); // Get Auth user information
        if ($this->user) {
            $notificationData = $this->notificationData('', $sort_type);
            return response()->json($notificationData['message'], $notificationData['code']);
        } else {
            return response()->json(['message' => 'Unauthorized action'], 401);
        }
    }

    public function getNotification($id)
    {
        $this->user = Auth::user(); // Get Auth user information
        if ($this->user) {
            $notificationData = $this->notificationData($id, '');
            return response()->json($notificationData['message'], $notificationData['code']);
        } else {
            return response()->json(['message' => 'Unauthorized action'], 401);
        }
    }

    private function notificationData($id='',$sort_type='unread')
    {
        try {
            $notifications = $this->user->notifications()->select('id', 'type', 'notifiable_type', 'notifiable_id', 'data', 'read_at', 'created_at');
            if ($id != '') {
                $getData = $notifications->where('id', $id)->first();
                if ($getData) {
                    $return = [
                        'message' => [
                            'notification' => $getData
                        ],
                        'code' => '200'
                    ];
                } else {
                    $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
                    $data['message'] = 'Invalid notification id';
                    $return = [
                        'message' => $data,
                        'code' => '422'
                    ];
                }
                return $return;
            } else {
                $beforedate = date('Y-m-d', strtotime('-90 days'));
                $getsort = $notifications->whereDate('created_at', '>=', $beforedate)->get();
                $unreadcount = $this->user->notifications()->whereDate('created_at', '>=', $beforedate)->whereNull('read_at')->count();
                if ($sort_type=='latest') {
                    $getData = $getsort->sortByDesc('created_at')->toArray();
                } elseif ($sort_type=='oldest') {
                    $getData = $getsort->sortBy('created_at')->toArray(); 
                } elseif ($sort_type=='unread') {
                    $getData = $getsort->sort(
                        function ($a, $b) {
                            if ($a->read_at === $b->read_at) {
                                return $a->created_at < $b->created_at;
                            }
                            return $a->read_at > $b->read_at;
                        }
                    )->toArray();
                } elseif ($sort_type=='read') {
                    $getData = $getsort->sort(
                        function ($a, $b) {
                            if ($a->read_at === $b->read_at) {
                                return $a->created_at < $b->created_at;
                            }
                            return $a->read_at < $b->read_at;
                        }
                    )->toArray();
                } else {
                    $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
                    $data['message'] = 'Invalid sort option';
                    $return = [
                        'message' => $data,
                        'code' => '422'
                    ];
                    return $return;
                }
            }
            $return = [
                'message' => [
                    'unreadcount' => $unreadcount,
                    'notifications' => array_values($getData)
                ],
                'code' => '200'
            ];
            return $return;
        } catch (\Exception $e) {
            $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
            $data['message'] = 'Something Went Wrong' . $e->getMessage();
            $return = [
                'message' => $data,
                'code' => '422'
            ];
            return $return;
        }
    }
}
