<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\EvaluationForm;
use App\Employee;
use App\Answer;
use App\Call;
use App\User;
use Auth;
use App\Comment;
use App\Evaluation;
use App\Disposition;
use App\Question;
use DB, Log;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\EvaluationDispositions;
use App\EvaluationWorkflows;
use App\CampaignGroup;
use App\CampaignSkill;
use App\EvaluationFormCampaign;
use App\UserSystemMapping;
use App\Http\Requests\EvaluationsSearchRequest;
use App\Http\Requests\EvaluationsGetEvaluationsRequest;
use App\Http\Requests\EvaluationsGetCallsEvaluationsRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\Metrics;

class EvaluationSearchController extends Controller
{
    use Metrics;
    var $metricsArray = array();

    /**
     * This function used to get a single evaluation details with evaluation id
    **/
    public function getEvaluations(EvaluationsGetEvaluationsRequest $request){
        try {
            return $this->search($request);
        } catch (\Exception $e) {
            return array("errors"=>"Server error");
        }
    }


    /**
     * This function used to get a single call's evaluations
    **/
    public function getCallsEvaluations(EvaluationsGetCallsEvaluationsRequest $request){
        try {
            return $this->search($request);
        } catch (\Exception $e) {
            return array("errors"=>"Server error");
        }
    }

    /**
     * Get search method used to search evaluations with search parameters such as call_id, eval_id, from_date, to_date, status_id, disposition_code, evaluation_form_id, agent_ids, minscore, maxscore, sortordercolumn, sortorder, take & skip
    **/
    public function evaluationSearch(EvaluationsSearchRequest $request){
        try {
            $form_id = $request->evaluation_form_id;
            $form_ids = $group_form_ids = $skill_form_ids = [];
            if (!empty($request->group_campaign)) {
                $group_campaign = explode(',', $request->group_campaign);
                $group_form_ids = EvaluationFormCampaign::where('type', 'group')
                    ->whereIN('type_id', $group_campaign)
                    ->pluck('form_id')->toArray();
                if (!empty($request->skill_campaign_id)) {
                    $skill_campaign = explode(',', $request->skill_campaign_id);
                    $skill_form_ids = EvaluationFormCampaign::where('type', 'skill')
                        ->whereIN('type_id', $skill_campaign)
                        ->pluck('form_id')->toArray();
                }
            }
            $form_ids = array_merge($form_ids, $group_form_ids, $skill_form_ids);
            if (empty($form_ids) || !in_array($form_id, $form_ids)) {
                $data['count'] = 0;
                $data['evaluationcallresults'] = [];
                $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria';
                $data['message'] = 'Form not maped to any campaign group or skill.';
                return response($data, 422);
            }
            return $this->search($request);
        } catch (\Exception $e) {
            return array("errors"=>"Server error");
        }
    }

    /**
     * This evaluation search(private) function used to search evaluations with or without input fields.
     *
     * @return array()
     */
    private function search($request){
        $select_fields = array('evaluations.id as evaluation_id','evaluations.evaluation_form_id', 'evaluations.predecessor_id', 'evaluations.agent_id', 'evaluations.agent_name', 'evaluations.call_id', 'evaluations.disposition','evaluations.phone_number', 'evaluations.lead_id', 'evaluations.unique_id', 'evaluations.created_at', 'evaluations.updated_at', 'evaluations.status_id', 'evaluation_status_masters.status as status_name', 'evaluations.assigned_to', 'evaluations.final_score', 'evaluations.created_by','evaluations.skill_campaign_id','evaluations.state','evaluation_dispositions.severity_id','evaluation_disposition_definitions.description as severity_name','evaluation_dispositions.evaluation_disposition_definitions_id','evaluation_disposition_definitions.name as evaluation_disposition_name','portal_user.vendor','portal_user.user_id');

        //Get more details iff eval_id/call_id filter fields present
        if($request->eval_id || $request->call_id)
        {
            $select_fields[] = "evaluations.headers";
            $select_fields[] = "evaluations.group_score";
            $select_fields[] = "evaluations.ratings";
            $select_fields[] = "evaluations.allow_dispute";
        }
        $evaluations = Evaluation::select($select_fields)
            ->join('portal_user', 'portal_user.user_id', '=', 'agent_id')
            ->join('evaluation_status_masters', 'evaluation_status_masters.id', '=', 'evaluations.status_id')
            ->with(['evaluationcall', 
                'evaluation_form' => function ($query) {
                    $query->select('id', 'title','type');
                },
                'createduser' => function ($query) {
                    $query->select('user_id', 'name');
                },
                'campaignSkill' => function ($query) {
                    $query->select('id', 'name', 'group_id')
                        ->with(['campaignGroup' => function ($query) {
                            $query->select('id', 'name');
                        }]);
                }
            ]);

        //Role
        $roles_id = Auth::user()->roles[0]->id;
        if(($request->eval_id || $request->call_id) && strpos($request->access_from, "reevaluation")>-1) //This condition is required when prev evaluation is done by other user/role
        {}
        /*else if(Auth::user()->hasRole('Supervisor')) //Supervisor / TM
            $evaluations->where('supervisor_user_id', Auth::user()->dim_user_id);*/
        else if(Auth::user()->hasRole('QA Specialist')) //QA Specialist
            $evaluations->where('evaluations.assigned_to', '=', Auth::user()->user_id);
        /*else if(Auth::user()->hasRole('QA Manager')) //QA Manager
        {}*/
         else if (Auth::user()->hasRole('Agent')) {
            $userSystemMapping = UserSystemMapping::where('user_id', Auth::user()->user_id)
            ->pluck('system_user_id');
            $agentids = implode(',', $userSystemMapping->toArray());
        }
        /*else if(Auth::user()->hasRole('Administrator')) //Administrator
        {}*/

        $agentids = [];
        if (!$request->eval_id && !$request->call_id) {
            if (Auth::user()->hasRole('Agent')) {
                $userSystemMapping = UserSystemMapping::where('user_id', Auth::user()->user_id)
                    ->pluck('system_user_id');
                $request->agent_ids = implode(',', $userSystemMapping->toArray());
            }
            if ($request->agent_ids != '') {
                $agent_ids = explode(',', $request->agent_ids);
                if (Auth::user()->hasRole('Supervisor')) {
                    $supervisor_id = UserSystemMapping::where('user_id', Auth::user()->user_id)
                        ->pluck('system_user_id');
                    $agent_ids = User::whereIn('user_id', $agent_ids)
                        ->whereIn('suerpvisorid', $supervisor_id)->pluck('user_id');
                    if (empty(count($agent_ids))) {
                        $data['count'] = 0;
                        $data['evaluationcallresults'] = [];
                        return $data;
                    }
                    $userSystemMapping = UserSystemMapping::whereIn('user_id', $agent_ids)
                        ->pluck('system_user_id');
                    $agentids = $userSystemMapping->toArray();
                } else if (Auth::user()->hasRole('Agent')) {
                    $agentids = $agent_ids;
                } else {
                    $userSystemMapping = UserSystemMapping::whereIn('user_id', $agent_ids)
                        ->pluck('system_user_id');
                    $agentids = $userSystemMapping->toArray();
                }
            } else {
                if ($request->site != '' || Auth::user()->hasRole('Supervisor')) {
                    $agents = new User;
                    if ($request->site != '') {
                        $sites = explode(',', $request->site);
                        $agents = $agents->whereIn('site', $sites);
                    }
                    if (Auth::user()->hasRole('Supervisor')) {
                        $userSystemMapping = UserSystemMapping::select('system_user_id')
                            ->where('user_id', Auth::user()->user_id)->get();
                        $agents = $agents->whereIn('suerpvisorid', $userSystemMapping);
                    }
                    $agents = $agents->pluck('user_id');
                    if (!empty($agents->count())) {
                        $userSystemMapping = UserSystemMapping::whereIn('user_id', $agents)
                            ->pluck('system_user_id');
                        $agentids = $userSystemMapping->toArray();
                    } else {
                        $data['count'] = 0;
                        $data['evaluationcallresults'] = [];
                        return $data;
                    }
                }
            }
        }

        if (!empty(count($agentids))) {
            $evaluations->whereIn('evaluations.agent_id', $agentids);
        }
        
        if (!$request->eval_id && !$request->call_id) {
            /* Skill Campaign */
            if ($request->has('skill_campaign_id') && $request->skill_campaign_id) {
                $skill_campaign_ids = explode(',', $request->skill_campaign_id);
                $evaluations->whereIn('skill_campaign_id', $skill_campaign_ids);
            } else {
                if (!empty($request['group_campaign'])) {
                    $group_campaign = explode(',', $request->group_campaign);
                    $campaignSkill = CampaignSkill::whereIN('group_id', $group_campaign)->pluck('id');
                    $evaluations->whereIn('skill_campaign_id', $campaignSkill);
                }
            }
        }

        /* Disposition */
        if ($request->disposition != '') {
            $dispostion = explode(',', $request->disposition);
            $evaluations->whereIn('disposition', $dispostion);
        }

        //Phone number search
        if ($request->customer_number) {
            $customer_number = $request->customer_number;
            if (substr($customer_number, 0, 2) === '+1') {
                $customer_number = substr($customer_number, 2);
            } else if (substr($customer_number, 0, 3) === '(1)') { 
                $customer_number = substr($customer_number, 3);
            } else if (substr($customer_number, 0, 4) === '001-') { 
                $customer_number = substr($customer_number, 4);
            }
            $customer_number = preg_replace("/[^0-9]/", "", $customer_number);
            $evaluations->where('phone_number', $customer_number);
        }
        //lead id search
        if(isset($request['lead_id']) && $request['lead_id'])
        {
            $evaluations->where('lead_id', $request['lead_id']);
        }
        //uniqueid search
        if(isset($request['unique_id']) && $request['unique_id'])
        {
            $evaluations->where('unique_id', $request['unique_id']);
        }

        //Severity search
        $severity_id = $request->severity_id;
        $evaluation_disposition_id = $request->evaluation_disposition_id;
        if (!empty($severity_id) && empty($request->evaluation_disposition_id)) {
            $evaluations->join('evaluation_dispositions', function($join) use($severity_id) {
                $join->on('evaluation_dispositions.evaluations_id', '=', 'evaluations.id')
                ->where('evaluation_dispositions.severity_id', $severity_id);
            })->join('evaluation_disposition_definitions', function($join) {
                $join->on('evaluation_disposition_definitions.id', '=', 'evaluation_dispositions.evaluation_disposition_definitions_id');
            });
        }
        //Severity &  Evaluation search
        elseif (!empty($severity_id) && !empty($evaluation_disposition_id)) {
            $evaluations->join('evaluation_dispositions', function($join) use($evaluation_disposition_id, $severity_id) {
                $join->on('evaluation_dispositions.evaluations_id', '=', 'evaluations.id')
                    ->where('evaluation_dispositions.severity_id', $severity_id)
                    ->where('evaluation_dispositions.evaluation_disposition_definitions_id', $evaluation_disposition_id);
            })->join('evaluation_disposition_definitions', function($join) {
                $join->on('evaluation_disposition_definitions.id', '=', 'evaluation_dispositions.evaluation_disposition_definitions_id');
            });
        }
        //No filter search
        else{
            $evaluations->leftJoin('evaluation_dispositions', function($join)
            {
                $join->on('evaluation_dispositions.evaluations_id', '=', 'evaluations.id');
            })->leftJoin('evaluation_disposition_definitions', function($join)
            {
                $join->on('evaluation_disposition_definitions.id', '=', 'evaluation_dispositions.evaluation_disposition_definitions_id');
            });
        }

        //Other filter conditions
        if(!$request->eval_id && !$request->call_id) //This condition applies only for search to return evaluations without 'Dispute Accepted'
            $evaluations->where('evaluations.status_id', '!=', 6);
        if($request->eval_id)
            $evaluations->where('evaluations.id', $request->eval_id);
        if($request->call_id)
            $evaluations->where('evaluations.call_id', $request->call_id);
        if($request->assigned_to)
            $evaluations->where('evaluations.assigned_to', $request->assigned_to);
        if($request->disposition_code)
            $evaluations->where('evaluations.disposition', $request->disposition_code);
        if($request->status_id)
            $evaluations->whereIn('evaluations.status_id', explode(",", $request->status_id));

        if($request->from_date && $request->to_date)
            $evaluations->whereBetween('evaluations.created_at', [$request->from_date, $request->to_date." 23:59:59"]);
        else if($request->from_date)
            $evaluations->where('evaluations.created_at', '>=', $request->from_date);
        else if($request->to_date)
            $evaluations->where('evaluations.created_at', '<=', $request->to_date." 23:59:59"); 

        if($request->sortordercolumn == 'createduser.name')
            $evaluations->orderBy('created_by', $request->sortorder);
        else if($request->sortordercolumn == 'severity_name')
            $evaluations->orderBy('evaluation_dispositions.severity_id', $request->sortorder);
        else if($request->sortordercolumn == 'evaluation_disposition_name')
            $evaluations->orderBy('evaluation_dispositions.evaluation_disposition_definitions_id', $request->sortorder);
        else if($request->sortordercolumn == 'vendor')
            $evaluations->orderBy('portal_user.vendor', $request->sortorder);
        else if($request->sortordercolumn && $request->sortorder)
            $evaluations->orderBy($request->sortordercolumn, $request->sortorder);

        if($request->minscore != '')
            $evaluations->where('final_score', '>=', $request->minscore);
        if($request->maxscore != '')
            $evaluations->where('final_score', '<=', $request->maxscore);
        if($request->evaluation_form_id)
            $evaluations->where('evaluation_form_id', '=', $request->evaluation_form_id);
        if($request->skip!="")
            $evaluations->skip($request->skip);

        $evaluations->take($request->take ? $request->take : 25);
        $evaluations_result = $evaluations->get();
        //Headers, answers, next allowed transition, calldetails & other data
        if($request->eval_id || $request->call_id)
        {
            foreach ($evaluations_result as $key => $evaluation) {
                $evaluations_result[$key]['headers'] = json_decode($evaluation['headers']);
                $evaluations_result[$key]['answers'] = $this->getAnswersFormAndValues($evaluation['evaluation_id']);
                $evaluations_result[$key]['allowed_transition'] = $evaluation['status_id']!=6 ? $this->allowedTransitions($evaluation['evaluation_form_id'], $evaluation['status_id']) : [];
                $evaluations_result[$key]['group_score'] = json_decode($evaluation['group_score']);
            }
        }
        //Get campaign group types
        $campaign_group_type = CampaignGroup::whereIN('id', explode(",", $request->group_campaign))->groupBy('type')->pluck('type')->implode(',');

        //Format the output result
        foreach ($evaluations_result as $key => $evaluation) {
            $evaluations_result[$key]['status'] = array(
                'id' => $evaluation['status_id'],
                'status' => $evaluation['status_name']
            );
            unset($evaluations_result[$key]['status_id']);
            unset($evaluations_result[$key]['status_name']);

            if (isset($evaluation['evaluationcall']['recordingfileurl'])) {
                $recordingfileurl = $evaluation['evaluationcall']['recordingfileurl'];
                $usr_grp = $evaluation['evaluationcall']['usr_grp'];
                $audiofileurl = Call::multiAudioFile($recordingfileurl, $evaluation['agent_name'],$usr_grp);
                $evaluation['evaluationcall']['recordingfileurl'] = $audiofileurl;
            }
            $evaluations_result[$key]['call_details'] = array(
                "usr_grp" => $evaluation['evaluationcall']['usr_grp'],
                "call_date" => $evaluation['evaluationcall']['call_date'],
                "audiofileurl" => $evaluation['evaluationcall']['recordingfileurl'] ? $evaluation['evaluationcall']['recordingfileurl'] : [[]],
                "campaign_group_type" => $campaign_group_type
            );
            unset($evaluations_result[$key]['evaluationcall']);
        }

        $this->metricsArray[] = array('MetricName'=>"evaluation/search");

        if(!count($evaluations_result) && ($request->eval_id || $request->call_id) && Auth::user()->hasRole(["Agent", "QA Specialist", "Supervisor"]))
        {
            $data = ['errors'=>['evaluation'=>["You're not allowed to access this page"]]];
            return response($data, 401);
        } else {
            $data = [
                'count' => $evaluations->skip(0)->count(), 
                'evaluationcallresults' => $evaluations_result
            ];
            return response($data, 200);
        }
    }

    /**
     * This function used to get evaluation's answer form
     *
     * @return array()
     */
    public function getAnswersFormAndValues($evaluation_id){
        $evaluations = Evaluation::where('id', $evaluation_id)->with(['answers', 'acknowledgeduser', 'user', 'createduser', 'evaluationcall' => function($query){
            $query->select('ucid as callid', 'ucid', 'unique_id','agent_id','segstart as callstartdatetime','segstop as callenddatetime','duration as callduaration','direction as calldirection','outbound_cid as outboundcid','calling_pty as callingpartycid','dialed_num as diallednumber','screenfileurl as screenfileurl','recordingfileurl as recordingfile','recordingfileurl as audiofileurl','client as client', 'dialed_num', 'extension', 'customer_num','usr_grp');
        }])->first();
       
        $evaluation_form = EvaluationForm::where('id', $evaluations['evaluation_form_id'])->first();

        $evaluationdisposition = EvaluationDispositions::where('evaluations_id', $evaluation_id)->with(['evaluationdispositiondefinition' => function($q){
            $q->select('id', 'name', 'severity', 'description');
        }])->get();
        if($evaluationdisposition->isNotEmpty()){
            $evaluation['evaluation']['evaluationdisposition'] = $evaluationdisposition[0]['evaluationdispositiondefinition'];
        }
        else
        {
            $evaluation['evaluation']['evaluationdisposition'] = [];
        }
        $evaluation['evaluation']['description'] = $evaluation_form['title'];
        $evaluation['evaluation']['evaluatorname'] = $evaluations['createduser']['name'];
        $evaluation['evaluation']['evaluatorid'] = $evaluations['created_by'];
        $evaluation['evaluation']['ratings'] = json_decode($evaluation_form['ratings'], true);
        $evaluation['evaluation']['finalratings'] = $evaluations['ratings'];
        $evaluation['evaluation']['acknowledgedusername'] = ($evaluations['acknowledgeduser'] != null) ? $evaluations['acknowledgeduser']['name'] : '';
        $evaluation['evaluation']['commentcreateddatetime'] = $evaluations['comment_created_at'];
        $evaluation['evaluation']['commentbody'] = $evaluations['comment'];
        $evaluation['evaluation']['acknowledgeduserid'] = $evaluations['acknowledged_by'];
        $evaluation['evaluation']['isacknowledged'] = ($evaluations['status'] == "pending") ? false:true;
        $v = [];
        foreach($evaluations['answers'] as $akey => $avalue){
            if($avalue['question']['group_id'] != null){
                $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['groupname'] = $avalue['question']['questiongroup']['title'];
                $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['groupdescription'] = $avalue['question']['questiongroup']['description'];
                $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['groupid'] = $avalue['question']['group_id'];
                $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['percentage'] = $avalue['question']['questiongroup']['percentage'];
                $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['groupmode'] = $avalue['question']['questiongroup']['groupmode'];
            }
            if($avalue['question'] != null){
                $v[$akey]['options'] = json_decode($avalue['question']['values'], true);
                $v[$akey]['score'] = json_decode($avalue['score'], true);
                $v[$akey]['mandatory'] = $avalue['question']['mandatory'];
                $v[$akey]['weightage'] = $avalue['question']['actual_score'];
                $v[$akey]['showcommentbox'] = $avalue['question']['comment'];
                $v[$akey]['commentmandatory'] = $avalue['question']['commentmandatory'];
                $v[$akey]['groupid'] = $avalue['question']['group_id'];
                $v[$akey]['questionid'] = $avalue['question']['id'];
                $v[$akey]['questiontype'] = $avalue['question']['type'];
                $v[$akey]['questiontitle'] = $avalue['question']['title'];
                $v[$akey]['value'] = $avalue['answer_text'];
                $v[$akey]['rawvalues'] = $avalue['question']['rawvalues'];
                $comment = collect($avalue['comments'])->first(function($item, $key){
                    return $item->body;
                });
                $v[$akey]['comment'] = (isset($comment->body)) ? $comment->body : '';
                $v[$akey]['disabled'] = false;
                if($avalue['question']['type'] == 'lineitems'){
                    $subgroups = Question::select('id', 'id as questionid', 'type as questiontype', 'title as questiontitle', 'parent_question_id','score','values as options', 'comment as showcommentbox', 'commentmandatory', 'mandatory', 'actual_score')->where('parent_question_id', $avalue['question']['id'])->with(['answers' => function($query) use ($evaluation_id){
                        $query->select('answer_text', 'evaluation_id', 'question_id', 'id')->where('evaluation_id', $evaluation_id);
                    }])->get();
                    $subgroup = [];
                    foreach($subgroups as $skey => $svalue){
                        $subgroup[$skey]['questionid'] = $svalue['questionid'];
                        $subgroup[$skey]['score'] = json_decode($svalue['score'], true);
                        $subgroup[$skey]['options'] = json_decode($svalue['options'], true);
                        $subgroup[$skey]['questiontitle'] = $svalue['questiontitle'];
                        $subgroup[$skey]['questiontype'] = $svalue['questiontype'];
                        // $subgroup[$skey]['options'] = json_decode($svalue['question']['values'], true);
                        $subgroup[$skey]['mandatory'] = $svalue['mandatory'];
                        $subgroup[$skey]['showcommentbox'] = $svalue['showcommentbox'];
                        $subgroup[$skey]['commentmandatory'] = $svalue['commentmandatory'];
                        $subgroup[$skey]['weightage'] = $svalue['actual_score'];
                        foreach($svalue['answers'] as $sanswer){
                            $subgroup[$skey]['value'] = $sanswer['answer_text'];
                            if(count($sanswer['comments']) > 0){
                                foreach($sanswer['comments'] as $comments){
                                    $subgroup[$skey]['comment'] = $comments['body'];
                                }
                            }else{
                                $subgroup[$skey]['comment'] = '';
                            }
                        }
                    }

                    $v[$akey]['subgroups'] = $subgroup;
                }else{
                    $v[$akey]['subgroups'] = '';
                }
            }
        }
        if(isset($evaluation['evaluation']['questiongroups']))
        {
            foreach($evaluation['evaluation']['questiongroups'] as $keyresponse => $responseval){
                if(isset($responseval['groupid'])){
                    if ($responseval['groupname'] == 'Header Details') {
                        continue;
                    }
                    $res[$keyresponse]['groupname'] = $responseval['groupname']; 
                    $res[$keyresponse]['groupdescription'] = $responseval['groupdescription'];
                    $res[$keyresponse]['groupid'] = $responseval['groupid'];
                    $res[$keyresponse]['percentage'] = $responseval['percentage'];
                    $res[$keyresponse]['groupmode'] = $responseval['groupmode'];
                    $res[$keyresponse]['questions'] = collect($v)->where('groupid', $responseval['groupid'])->values()->all();
                }
            }
            $evaluation['evaluation']['questiongroups'] = collect($res)->values()->all();
        }
        else
            $evaluation['evaluation']['questiongroups'] = array();
        $va = collect($evaluation)->values()->all();
        return $va['0'];
    }

    /**
     * This function used to get next allowed transition for the given/current status_id
     *
     * @return array()
     */
    public function allowedTransitions($evaluation_form_id=0, $status_id=0, $action="")
    {
        $evaluationForm          = EvaluationForm::find($evaluation_form_id);
        $allowed_transitions     = EvaluationWorkflows::workflow($evaluationForm->workflow_id);
        $next_allowed_transition = array();
        if($allowed_transitions)
        {
            foreach($allowed_transitions as $allowed_transition)
            {
                if($allowed_transition['PickUpStatus']==$status_id)
                {
                    if(strpos($action, "internalCommand")>-1 || in_array(Auth::user()->roles->pluck('id')[0], $allowed_transition['Roles']))
                        $next_allowed_transition = $allowed_transition;
                    break;
                }
            }
        }
        return $next_allowed_transition;
    }

    public function __destruct() {
        //Log the captured information
        if (count($this->metricsArray)) {
            try{
                //Log to CloudWatch
                $cw_response = $this->putMetricData($this->metricsArray);
            } catch (\Exception $e) {
                //print_r($e);
            }
        }
    }
    
}
