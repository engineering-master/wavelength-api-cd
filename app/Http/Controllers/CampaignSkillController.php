<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CampaignSkillRequest;
use App\CampaignSkill;
use App\Disposition;
use Auth;
use App\Evaluation;
class CampaignSkillController extends Controller
{
    /**
     * Display a list of all campaign skills.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {      
             $CampaignSkill=CampaignSkill::get();
             return response($CampaignSkill, 200);
        } catch (Exception $ex) { 
            return response()->json(['errors' => 'Something went wrong,try again later '], 422);
        }
    }
    /**
     * Store a newly created campaign skill in Storage.
     *
     * @param int                $id      campaign skill id
     * @param \app\Http\Requests $request input request parameter
     * 
     * @return \Illuminate\Http\Response        response for the given input
     */
    public function store($id,CampaignSkillRequest $request)
    {
        try {
            $campaignData = $request->only(['name','description','group_id']);
            $campaignData['name'] = strip_tags($campaignData['name']);
            $campaignData['created_by'] = Auth::user()->user_id;
            $campaignSkill = CampaignSkill::create($campaignData);
            return response($campaignSkill, 200);     
        } catch (Exception $ex) { 
            return response()->json(['errors' => 'Something went wrong,try again later '], 422);
        }
    }
    /**
     *  Update the specified campaign skill in storage.
     *
     * @param int                $id      campaign skill id
     * @param \app\Http\Requests $request input request parameter
     * 
     * @return \Illuminate\Http\Response  response for the input parameter
     */
    public function update($id,CampaignSkillRequest $request)
    {
        try {
            $eval = Evaluation::where('skill_campaign_id',$id)->first();
            if(!$eval){
            $campaignData = $request->only(['name','description']);
            if ($request->has('name')) {
            $campaignData['name'] = strip_tags($campaignData['name']);
            }
            $campaignData['updated_by'] = Auth::user()->user_id;
            $campaignSkill = CampaignSkill::find($id);
            $campaignSkill->fill($campaignData);
            $campaignSkill->update();
            return response($campaignSkill, 200);
            }
            else{
            return response()->json(['errors' => 'Unable to update, skill already used in evaluations '], 422);
            }
            
        } catch (Exception $ex) { 
            return response()->json(['errors' => 'Something went wrong,try again later '], 422);
        }
    }

    /**
     * List the disposition codes based on the campaign skills.
     * If campaign group ids have been sent, 
     * The system will take the campaign skills belongs to the campaign group 
     * 
     * {type}/{type_ids}/disposition
     *
     * @param string $type     contain group/skill
     * @param int    $type_ids contain ids of type
     * 
     * @return \Illuminate\Http\Response 
     */
    public function getSkillDisposition($type, $type_ids)
    {
        try {
            $dispositions = [];
            $ids = explode(",", $type_ids);
            $campaignSkill = new CampaignSkill;
            if ($type == 'group') {
                $campaignSkill = $campaignSkill->whereIn('group_id', $ids);
            } elseif ($type == 'skill') {
                $campaignSkill = $campaignSkill->whereIn('id', $ids);
            }
            $skill_names = $campaignSkill->pluck('name');
            if (!empty($skill_names->count())) {
                $dispositions = Disposition::whereIn('skill_campaign', $skill_names)
                    ->distinct()->orderBy('disposition', 'asc')
                    ->pluck('disposition');
            }
            return response()->json($dispositions, 200);
        } catch (Exception $ex) {
            return response()->json(
                ['errors' => 'Something went wrong, try again later.'], 
                $ex->getStatusCode()
            );
        }
    }
}
