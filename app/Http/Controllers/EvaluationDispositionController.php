<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EvaluationDispositionDefinitions;
use Auth;

class EvaluationDispositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function index()
    {
        $userInfo = Auth::user(); // Get Auth user information
        if( $userInfo )
        {
            try {
                $evaluationdisposition = EvaluationDispositionDefinitions::select('id', 'name', 'severity', 'description')->get();
                if($evaluationdisposition->isEmpty())
                {
                    return response()->json(['message' => 'No Data Available'], 402);
                }
            } 
            catch (\Exception $e) {
                return response()->json(['message' => 'Something Went Wrong'], 400);
            }
            return  json_encode($evaluationdisposition);
        } 
        else 
        {
            return response()->json(['message' => 'Unauthorized action'], 401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
