<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerCallSearchControlller extends Controller
{
    /**
         *
         *
         * @SWG\Post(
         *      path="/calls",
         *      tags={"Calls"},
         *      operationId="searchCalls",
         *      consumes={"application/x-www-form-urlencoded"},     
         *      summary="Search Calls",
         *      security={{"Bearer":{}}},
         * @SWG\Parameter(
         *          name="agent_ids",
         *          in="query",
         *          type="integer",
         *          required=false,
         *          description="Agent ID",
         *      ),
         * @SWG\Parameter(
         *          name="sprint_extension",
         *          in="query",
         *          type="string",
         *          required=false,
         *          description="Extension ID",
         *      ),
         * @SWG\Parameter(
         *          name="from_date",
         *          in="query",
         *          type="string",
         *          required=true,
         *          description="Start Date",
         *      ),
         * @SWG\Parameter(
         *          name="to_date",
         *          in="query",
         *          type="string",
         *          required=true,
         *          description="Stop Date",
         *      ),
         * @SWG\Parameter(
         *          name="evaluation_form_id",
         *          in="query",
         *          type="string",
         *          required=true,
         *          description="Form Id",
         *      ),
         * @SWG\Parameter(
         *          name="sortordercolumn",
         *          in="formData",
         *          type="string",
         *          required=true,
         *          description="Sort Order Column",
         *      ),
         * @SWG\Parameter(
         *          name="sortorder",
         *          in="formData",
         *          type="string",
         *          required=false,
         *          description="Sort Order",
         *      ),
         * @SWG\Parameter(
         *          name="take",
         *          in="formData",
         *          type="string",
         *          required=true,
         *          description="Take",
         *      ),
         * @SWG\Parameter(
         *          name="skip",
         *          in="formData",
         *          type="string",
         *          required=false,
         *          description="Skip",
         *      ),
         * @SWG\Response(
         *     response=200,
         *     description="Success Response"
         *   ),
         * @SWG\Response(
         *     response=400,
         *     description="Not a Valid Request"
         *   ),
         * @SWG\Response(
         *     response="default",
         *     description="an ""unexpected"" error"
         *   ),
     
         *  )
         **/

          /**
         *
         *
         * @SWG\Get(
         *      path="/call/{callid}",
         *      tags={"Calls"},
         *      operationId="singlecall",
         *      consumes={"application/x-www-form-urlencoded"},     
         *      summary="Get all evaluation form questions",
         *      security={{"Bearer":{}}},
         * @SWG\Parameter(
         *          name="callid",
         *          in="path",
         *          type="string",
         *          required=true,
         *          description="Call ID",
         *      ),
         * @SWG\Response(
         *     response=200,
         *     description="Success Response"
         *   ),
         * @SWG\Response(
         *     response=400,
         *     description="Not a Valid Request"
         *   ),
         * @SWG\Response(
         *     response="default",
         *     description="an ""unexpected"" error"
         *   ),
     
         *  )
         **/
}
