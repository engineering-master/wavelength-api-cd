<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerCommentController extends Controller
{
   /**
     *
     * @SWG\Get(
     *      path="/comments/{type}/{id}",
     *      tags={"Comments"},
     *      operationId="comments",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get all comments",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="type",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Comment type",
     *      ),
     *      @SWG\Parameter(
     *          name="eval_id",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Evalauation id",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

     /**
     *
     *
     * @SWG\Post(
     *      path="/{type}/{id}/comment",
     *      tags={"Comments"},
     *      operationId="eval_comment",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Comment",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="type",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Comment type",
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="integer",
     *          required=true,
     *          description="Comment id",
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Comment body",
     *      ),
     *      
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/
}