<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerEnumController extends Controller
{
    /**
     *
     *
     * @SWG\Get(
     *      path="/enums",
     *      tags={"Enums"},
     *      operationId="getEnums",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get all enums",
     *      security={{"Bearer":{}}},
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/
}