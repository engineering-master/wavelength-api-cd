<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerIndex extends Controller
{
    /**
 * @SWG\Swagger(
 *   schemes={"https","http"},
 *   host=L5_SWAGGER_CONST_HOST,
 *   basePath="/api/v1.1",
 * @SWG\Info(
 *     title="Wavelength",
 *     version="1.0.0"
 *   )
 * )
 */
/**
 * @SWG\SecurityScheme(
 *   securityDefinition="Bearer",
 *   type="apiKey",
 *   in="header",
 *   name="Authorization",
 *   description="Enter your bearer token in the format *Bearer &lt;token>*"
 * )
 */
}