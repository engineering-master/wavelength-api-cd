<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerCampaignGroupController extends Controller
{
    /**
    * @SWG\Get(
    *      path="/campaign/groups",
    *      tags={"campaigns"},
    *      operationId="campaigngrouplist",
    *      consumes={"application/x-www-form-urlencoded"},
    *      summary="Get campaign group",
    *      security={{"Bearer":{}}},
    * @SWG\Response(
    *      response=200,
    *      description="Success Response"
    *   ),
    * @SWG\Response(
    *      response=400,
    *      description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *      response="default",
    *      description="an ""unexpected"" error"
    *   ),
    *  )
   **/
    /**
    * @SWG\Post(
    *      path="/campaign/group",
    *      tags={"campaigns"},
    *      operationId="storecampaigngroup",
    *      consumes={"application/x-www-form-urlencoded"},
    *      summary="Create new campaign group",
    *      security={{"Bearer":{}}},
    * @SWG\Parameter(
    *      name="name",
    *      in="body",
    *      type="string",
    *      required=true,
    *      description="campaign group details",
    *      format="application/json",
    * @SWG\Schema(
    *      type="object",
    *      @SWG\Property(property="name",        type="string", example="groupname"),
    *      @SWG\Property(property="description", type="string", example="group description")
    *          )
    *      ),
    * @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    * @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
    /**
    * @SWG\Put(
    *      path="/campaign/group/{id}",
    *      tags={"campaigns"},
    *      operationId="updatecampaigngroup",
    *      consumes={"application/x-www-form-urlencoded"},
    *      summary="Update existing campaign group",
    *      security={{"Bearer":{}}},
    * @SWG\Parameter(
    *      name="name",
    *      in="body",
    *      type="string",
    *      required=true,
    *      description="campaign group name",
    *      format="application/json",
    * @SWG\Schema(
    *      type="object",
    *      @SWG\Property(property="name",        type="string", example="groupname"),
    *      @SWG\Property(property="description", type="string", example="group description")
    *          )
    *      ),
    * @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    * @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
    /**
    * @SWG\Post(
    *      path="/campaign/{type}/{type_id}/form/{id}",
    *      tags={"campaigns"},
    *      operationId="mapcampaignGroup/skills",
    *      consumes={"application/x-www-form-urlencoded"},
    *      summary=" to map form against campaign skills or campaign groups",
    *      security={{"Bearer":{}}},
    * @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    * @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
    /**
    * @SWG\Get(
    *     path="/campaign/group/{group_id}/skills",
    *     tags={"campaigns"},
    *     operationId="getCampaignSkill",
    *     consumes={"application/x-www-form-urlencoded"},
    *     summary="This will list the campaign skills based on the campaign group id ",
    *     security={{"Bearer":{}}},
    * @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    * @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
    /**
    * @SWG\Get(
    *     path="/campaign/{type}/{type_id}/forms",
    *     tags={"campaigns"},
    *     operationId="getCampaignForm",
    *     consumes={"application/x-www-form-urlencoded"},
    *     summary="This will list the forms related to the campaign group and campaign skills",
    *     security={{"Bearer":{}}},
    * @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    * @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
}
