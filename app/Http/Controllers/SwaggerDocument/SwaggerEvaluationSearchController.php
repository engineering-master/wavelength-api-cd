<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerEvaluationSearchController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/call/{call_id}/evaluations",
     *      tags={"Evaluation"},
     *      operationId="GetCallsEvaluations",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get Call's Evaluations",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="call_id",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Call ID",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
     *
     * 
    **/

     /**
     * @SWG\Get(
     *      path="/evaluations/{eval_id}",
     *      tags={"Evaluation"},
     *      operationId="GetEvaluationByEvaluationId",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get all evaluations with evaluation id",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="eval_id",
     *          in="path",
     *          type="integer",
     *          required=true,
     *          description="Evaluation ID",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
     *
     * 
    **/

    /**
     * @SWG\Get(
     *      path="/evaluations/search",
     *      tags={"Evaluation"},
     *      operationId="searchEvaluations",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Search Evaluations",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="eval_id",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Evaluation ID",
     *      ),
     *      @SWG\Parameter(
     *          name="call_id",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="Call Id",
     *      ),
     *      @SWG\Parameter(
     *          name="assigned_to",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Assigned to",
     *      ),
     *      @SWG\Parameter(
     *          name="agent_ids",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="Agent Ids",
     *      ),
     *      @SWG\Parameter(
     *          name="from_date",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="From Date",
     *      ),
     *      @SWG\Parameter(
     *          name="to_date",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="To Date",
     *      ),
     *      @SWG\Parameter(
     *          name="status_id",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Status Id",
     *      ),
     *      @SWG\Parameter(
     *          name="severity_id",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Severity Id",
     *      ),
     *      @SWG\Parameter(
     *          name="evaluation_disposition_id",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Evaluation disposition id",
     *      ),
     *      @SWG\Parameter(
     *          name="customer_number",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="Customer Number",
     *      ),
     *      @SWG\Parameter(
     *          name="dispositon_code",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="Disposition code",
     *      ),
     *      @SWG\Parameter(
     *          name="minscore",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Minscore",
     *      ),
     *      @SWG\Parameter(
     *          name="maxscore",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Maxscore",
     *      ),
     *      @SWG\Parameter(
     *          name="sortordercolumn",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="Sort column",
     *      ),
     *      @SWG\Parameter(
     *          name="sortorder",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="Sort order",
     *      ),
     *      @SWG\Parameter(
     *          name="take",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Take",
     *      ),
     *      @SWG\Parameter(
     *          name="skip",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Skip",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
     *
     * 
    **/
}