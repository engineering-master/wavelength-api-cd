<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerRolePermissionController extends Controller
{
   /**
     *
     *
     * @SWG\Get(
     *      path="/permissions",
     *      tags={"Roles and Permissions"},
     *      operationId="getpermissions",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get all permissions",
     *      security={{"Bearer":{}}},
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

    /**
     *
     *
     * @SWG\Get(
     *      path="/roles/{role_id}",
     *      tags={"Roles and Permissions"},
     *      operationId="roles",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get all roles with permissions or single role with permissions",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="role_id",
     *          in="path",
     *          type="integer",
     *          required=false,
     *          description="Role ID",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

     /**
     *
     *
     * @SWG\Post(
     *      path="/role/permissions",
     *      tags={"Roles and Permissions"},
     *      operationId="postRolePermissions",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Save Role Permissions",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="rolename",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="Role name",
     *      ),
     *      @SWG\Parameter(
     *          name="roleid",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Role Id",
     *      ),
     *      @SWG\Parameter(
     *          name="permissions[0]",
     *          in="formData",
     *          type="integer",
     *          required=true,
     *          description="Permission Id",
     *      ),
     *      @SWG\Parameter(
     *          name="permissions[1]",
     *          in="formData",
     *          type="integer",
     *          required=true,
     *          description="Permission Id",
     *      ),
     *      @SWG\Parameter(
     *          name="permissions[2]",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Permission Id",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

        /**
     *
     *
     * @SWG\Post(
     *      path="/user/role",
     *      tags={"Roles and Permissions"},
     *      operationId="assignrole",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Assign Role",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="role_id",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Role Id",
     *      ),
     *      @SWG\Parameter(
     *          name="user_id",
     *          in="formData",
     *          type="integer",
     *          required=true,
     *          description="User Id",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/
}