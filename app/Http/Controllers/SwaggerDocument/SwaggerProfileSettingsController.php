<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerProfileSettingsController extends Controller
{
    /**
     *
     *
     * @SWG\Post(
     *      path="/user/profile",
     *      tags={"Users"},
     *      operationId="userUpdateInformation",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Request update profile",
     *      security={{"Bearer":{}}},
     * @SWG\Parameter(
     *          name="userprofile",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="User profile information",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

    /**
     *
     *
     * @SWG\Post(
     *      path="/user/profile/{id}",
     *      tags={"Users"},
     *      operationId="userUpdate",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Update profile",
     *      security={{"Bearer":{}}},
     * @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="User profile id(Profile change request history table id)",
     *      ),
     * @SWG\Parameter(
     *          name="status",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="User profile request update Accept/Reject",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/
}