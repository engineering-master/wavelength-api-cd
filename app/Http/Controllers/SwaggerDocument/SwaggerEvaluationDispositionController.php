<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerEvaluationDispositionController extends Controller
{
    /**
     *
     *
     * @SWG\Get(
     *      path="/evaluation/disposition",
     *      tags={"Evaluation"},
     *      operationId="disposition",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get all evaluation disposition codes",
     *      security={{"Bearer":{}}},
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/
}