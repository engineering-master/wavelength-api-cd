<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerAuthController extends Controller
{
    
    /**
     *
     *
     * @SWG\Post(
     *      path="/auth/token",
     *      tags={"Auth"},
     *      operationId="Get Authorization Token",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get token",
     *      @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Email",
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Password",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/
}
