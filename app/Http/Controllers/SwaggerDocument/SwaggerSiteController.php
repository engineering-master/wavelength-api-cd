<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerSiteController extends Controller
{
    /**
     *
     *
     * @SWG\Get(
     *      path="/sites",
     *      tags={"Site"},
     *      operationId="site",
     *      consumes={"application/x-www-form-urlencoded"},
     *      summary="Get list of sites",
     *      security={{"Bearer":{}}},
     * @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     * @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     * @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *
     *  )
     **/

     /**
     *
     *
     * @SWG\Post(
     *      path="/sites/agent",
     *      tags={"Site"},
     *      operationId="get_site_agent",
     *      consumes={"application/x-www-form-urlencoded"},
     *      summary="Get agents based on sites",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="site",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Site names",
     *      ),
     *      @SWG\Parameter(
     *          name="flag",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="Action (ex:evaluation)",
     *      ),
     * @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     * @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     * @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *
     *  )
     **/
}