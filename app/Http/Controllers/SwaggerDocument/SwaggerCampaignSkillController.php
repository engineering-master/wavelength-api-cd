<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerCampaignSkillController extends Controller
{
    /**
    *
    * @SWG\Get(
    *     path="/campaign/skills",
    *     tags={"campaigns"},
    *     operationId="campaignskillslist",
    *     consumes={"application/x-www-form-urlencoded"},
    *     summary="Get campaign skills",
    *     security={{"Bearer":{}}},     
    * @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    * @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
    /**
    * @SWG\Post(
    *     path="/campaign/group/{group_id}/skill",
    *     tags={"campaigns"},
    *     operationId="storecampaignskills",
    *     consumes={"application/x-www-form-urlencoded"},
    *     summary="Create new campaign skill",
    *     security={{"Bearer":{}}},
    * @SWG\Parameter(
    *     name="name",
    *     in="body",
    *     type="string",
    *     required=true,
    *     description="campaign skill details",
    *     format="application/json",
    * @SWG\Schema(
    *     type="object",
    *     @SWG\Property(property="name",        type="string", example="groupname"),
    *     @SWG\Property(property="description", type="string", example="group description")
    *          )
    *      ),
    * @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    * @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
    /**
    * @SWG\Put(
    *     path="/campaign/skill/{id}",
    *     tags={"campaigns"},
    *     operationId="updatecampaignskill",
    *     consumes={"application/x-www-form-urlencoded"},
    *     summary="Update existing campaign skill",
    *     security={{"Bearer":{}}},
    * @SWG\Parameter(
    *     name="name",
    *     in="body",
    *     type="string",
    *     required=true,
    *     description="campaign skill details",
    *     format="application/json",
    * @SWG\Schema(
    *     type="object",
    *     @SWG\Property(property="name",        type="string", example="groupname"),
    *     @SWG\Property(property="description", type="string", example="group description")
    *          )
    *      ),
    * @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    * @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
    /**
    *
    *
    * @SWG\Get(
    *     path="/campaign/{type}/{type_id}/disposition",
    *     tags={"campaigns"},
    *     operationId="getSkillDisposition",
    *     consumes={"application/x-www-form-urlencoded"},
    *     summary="This will list the disposition codes based on the campaign skills",
    *     security={{"Bearer":{}}},
    * @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    * @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    * @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
}
