<?php

namespace App\Http\Controllers\SwaggerDocument;

class SwaggerCallDispositionController extends Controller
{

    /**
      *
      *     @SWG\Get(
      *      path="/call/disposition",
      *      tags={"Calls"},
      *      operationId="calldisposition",
      *      consumes={"application/x-www-form-urlencoded"},     
      *      summary="Get all call disposition based on the campaign",
      *     @SWG\Parameter(
      *          name="Authorization",
      *          in="header",
      *          type="string",
      *          required=true,
      *          description="JWT Token",
      *      ),
      *     @SWG\Response(
      *         response=200,
      *         description="Success Response"
      *     ),
      *     @SWG\Response(
      *         response=400,
      *         description="Not a Valid Request"
      *     ),
      *     @SWG\Response(
      *         response="default",
      *         description="an ""unexpected"" error"
      *     ),
      *  )
      **/
}
