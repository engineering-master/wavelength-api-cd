<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerEvaluationFormController extends Controller
{
    /**
    *
    * @SWG\Get(
    *      path="/evaluationforms",
    *      tags={"evaluationforms"},
    *      operationId="evaluationformsget",
    *      consumes={"application/x-www-form-urlencoded"},
    *      summary="Get evaluation forms list",
    *      security={{"Bearer":{}}},
    * @SWG\Parameter(
    *          name="Authorization",
    *          in="header",
    *          type="string",
    *          required=true,
    *          description="JWT Token",
    *      ),
    * @SWG\Parameter(
    *          name="form_id",
    *          in="formData",
    *          type="string",
    *          required=false,
    *          description="evaluation form id(s)",
    *      ),
    *      @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    *      @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    *   @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/

    /**
    *
    * @SWG\Get(
    *      path="/evaluationform/{form_id}",
    *      tags={"evaluationforms"},
    *      operationId="evaluationformget",
    *      consumes={"application/x-www-form-urlencoded"},
    *      summary="Get evaluation form detail",
    *      security={{"Bearer":{}}},
    * @SWG\Parameter(
    *          name="Authorization",
    *          in="header",
    *          type="string",
    *          required=true,
    *          description="JWT Token",
    *      ),
    * @SWG\Parameter(
    *          name="form_id",
    *          in="path",
    *          type="integer",
    *          required=true,
    *          description="evaluation form id",
    *      ),
    *      @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    *      @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    *   @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
    
    /**
    *
    * @SWG\Post(
    *      path="/evaluationform",
    *      tags={"evaluationforms"},
    *      operationId="evaluationformpost",
    *      consumes={"application/x-www-form-urlencoded"},
    *      summary="Create an evaluation form",
    *      security={{"Bearer":{}}},
    * @SWG\Parameter(
    *          name="Authorization",
    *          in="header",
    *          type="string",
    *          required=true,
    *          description="JWT Token",
    *      ),
    * @SWG\Parameter(
    *          name="title",
    *          in="formData",
    *          type="string",
    *          required=true,
    *          description="title",
    *      ),
    * @SWG\Parameter(
    *          name="description",
    *          in="formData",
    *          type="string",
    *          required=true,
    *          description="description",
    *      ),
    * @SWG\Parameter(
    *          name="headers",
    *          in="formData",
    *          type="string",
    *          required=true,
    *          description="headers",
    *      ),
    * @SWG\Parameter(
    *          name="ratings",
    *          in="formData",
    *          type="string",
    *          required=true,
    *          description="ratings",
    *      ),
    * @SWG\Parameter(
    *          name="version",
    *          in="formData",
    *          type="string",
    *          required=true,
    *          description="version",
    *      ),
    * @SWG\Parameter(
    *          name="questiongroups",
    *          in="formData",
    *          type="string",
    *          required=true,
    *          description="Question groups",
    *          format="application/json",
    *          @SWG\Schema(
    *              type="object",
    *              @SWG\Property(property="groupname", type="string", example="groupname"),
    *              @SWG\Property(property="groupdescription", type="string", example="groupdescription"),
    *              @SWG\Property(property="groupmode", type="string", example="groupmode"),
    *              @SWG\Property(property="ratings", type="string", example="ratings"),
    *              @SWG\Property(property="percentage", type="string", example="percentage"),
    *              @SWG\Property(property="questions", type="string", 
    *                    @SWG\Schema(
    *                       type="object",
    *                       @SWG\Property(property="questiontitle", type="string", example="questiontitle"),
    *                       @SWG\Property(property="questiontype", type="string", example="questiontype"),
    *                       @SWG\Property(property="rawvalues", type="string", example="rawvalues"),
    *                       @SWG\Property(property="showcommentbox", type="string", example="showcommentbox"),
    *                       @SWG\Property(property="commentmandatory", type="string", example="commentmandatory"),
    *                       @SWG\Property(property="weightage", type="string", example="weightage"),
    *                       @SWG\Property(property="score", type="string", example="score"),
    *                       @SWG\Property(property="options", type="string", example="options"),
    *                    )
    *                ),
    *          )
    *      ),
    *      @SWG\Response(
    *     response=200,
    *     description="Success Response"
    *   ),
    *      @SWG\Response(
    *     response=400,
    *     description="Not a Valid Request"
    *   ),
    *      @SWG\Response(
    *     response=401,
    *     description="Unauthorized access"
    *   ),
    *   @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   ),
    *  )
   **/
}
