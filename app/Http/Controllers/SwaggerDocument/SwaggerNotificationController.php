<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerNotificationController extends Controller
{
    /**
     *
     *
     * @SWG\Get(
     *      path="/notifications/{sort_type}",
     *      tags={"Notification"},
     *      operationId="notification",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get list of notifications",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="sort_type",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Sort type can be 'unread','read','latest','oldest'",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

    /**
     *
     *
     * @SWG\Get(
     *      path="/notification/{id}",
     *      tags={"Notification"},
     *      operationId="singlenotification",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Get notification based on id",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Notification ID",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

    /**
     *
     *
     * @SWG\Put(
     *      path="/notification/{id}/markasread",
     *      tags={"Notification"},
     *      operationId="notificationmarkasread",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Mark as read for a single notification",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Notification ID",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

    /**
     *
     *
     * @SWG\Put(
     *      path="/notifications/{ids}/markallasread",
     *      tags={"Notification"},
     *      operationId="notificationmarkallasread",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Mark all as read for a given notifications",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Notification ID",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/
}