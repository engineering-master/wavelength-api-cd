<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerWorkflowController extends Controller
{
   /**
   *
   * @SWG\Get(
   *      path="/workflow",
   *      tags={"Workflow"},
   *      operationId="workflowlist",
   *      consumes={"application/x-www-form-urlencoded"},
   *      summary="Get workflows",
   *      security={{"Bearer":{}}},
   *      @SWG\Parameter(
   *          name="status",
   *          in="path",
   *          type="integer",
   *          required=true,
   *          description="Workflow status",
   *      ),
   *      @SWG\Response(
   *     response=200,
   *     description="Success Response"
   *   ),
   *      @SWG\Response(
   *     response=400,
   *     description="Not a Valid Request"
   *   ),
   *   @SWG\Response(
   *     response="default",
   *     description="an ""unexpected"" error"
   *   ),
   *  )
   *
   *
  **/
  
  /**
   *
   *
   * @SWG\Post(
   *      path="/workflow",
   *      tags={"Workflow"},
   *      operationId="storeworkflow",
   *      consumes={"application/x-www-form-urlencoded"},
   *      summary="Create new workflow",
   *      security={{"Bearer":{}}},
   *      @SWG\Parameter(
   *          name="description",
   *          in="formData",
   *          type="string",
   *          required=true,
   *          description="Workflow description",
   *      ),
   *     @SWG\Parameter(
   *          name="start_date",
   *          in="formData",
   *          type="string",
   *          required=true,
   *          description="Workflow start date",
   *      ),
   *     @SWG\Parameter(
   *          name="wf_details",
   *          in="formData",
   *          type="string",
   *          required=true,
   *          description="Workflow details(json)",
   *      ),
   *      @SWG\Response(
   *     response=200,
   *     description="Success Response"
   *   ),
   *      @SWG\Response(
   *     response=400,
   *     description="Not a Valid Request"
   *   ),
   *   @SWG\Response(
   *     response="default",
   *     description="an ""unexpected"" error"
   *   ),
   *  )
   *
   *
  **/
}