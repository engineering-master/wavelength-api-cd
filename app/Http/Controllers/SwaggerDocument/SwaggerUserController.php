<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerUserController extends Controller
{
    
    /**
     *
     *
     * @SWG\Get(
     *      path="/me",
     *      tags={"Users"},
     *      operationId="userBasicInformation",
     *      consumes={"application/x-www-form-urlencoded"},
     *      summary="Get User Information",
     *      security={{"Bearer":{}}},
     *      @SWG\Response(
     *     response=200,
     *     description="Response with Users"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
     *
     *
     *
     */

    /**
     *
     *
     * @SWG\Get(
     *      path="/jwtparser",
     *      tags={"Users"},
     *      operationId="jwtParser",
     *      consumes={"application/x-www-form-urlencoded"},
     *      summary="Get JWT claims and user information",
     *      security={{"Bearer":{}}},
     *      @SWG\Response(
     *     response=200,
     *     description="Response with Users"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
     *
     *
     *
     */

     /**
     *
     *
     * @SWG\Get(
     *      path="/users",
     *      tags={"Users"},
     *      operationId="user_list",
     *      consumes={"application/x-www-form-urlencoded"},
     *      summary="Get all user list based on role id",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="role_id",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Role ID's",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Response with Users"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
     *
     *
     *
     */

      /**
     *
     *
     * @SWG\Post(
     *      path="/sendRocketMessage",
     *      tags={"Users"},
     *      operationId="sendRocketMessage",
     *      consumes={"application/x-www-form-urlencoded"},
     *      summary="Send rocket message to user",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="username",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Rocket chat username",
     *      ),
     *      @SWG\Parameter(
     *          name="text",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Rocket chat message",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Response with Users"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
     *
     *
     *
     */
    /**
     *
     *
     * @SWG\Post(
     *      path="/listusers",
     *      tags={"Users"},
     *      operationId="userlist",
     *      consumes={"application/x-www-form-urlencoded"},
     *      summary="Get user's list",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="roleids",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Role Id's",
     *      ),
     * @SWG\Parameter(
     *          name="sortordercolumn",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Sort order column",
     *      ),
     * @SWG\Parameter(
     *          name="sortorder",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Sort order",
     *      ),
     * @SWG\Parameter(
     *          name="username",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="User name",
     *      ),
     * @SWG\Parameter(
     *          name="skip",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Skip",
     *      ),
     * @SWG\Parameter(
     *          name="take",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Take",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Response with Users"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
     *
     *
     *
     */
}
