<?php

namespace App\Http\Controllers\SwaggerDocument;

use Illuminate\Http\Request;

class SwaggerEvaluationController extends Controller
{
    /**
     *
     *
     * @SWG\Post(
     *      path="/call/{call_id}/evaluation",
     *      tags={"Evaluation"},
     *      operationId="initiateEvaluation",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Initiate Evaluation",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="call_id",
     *          in="path",
     *          type="string",
     *          required=true,
     *          description="Call Id",
     *      ),
     *      @SWG\Parameter(
     *          name="evaluation_form_id",
     *          in="formData",
     *          type="integer",
     *          required=true,
     *          description="Evaluation Form ID",
     *      ),
     *      @SWG\Parameter(
     *          name="assigned_to",
     *          in="formData",
     *          type="integer",
     *          required=true,
     *          description="Assigned To",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

    /**
     *
     *
     * @SWG\Put(
     *      path="/evaluation/{eval_id}/allowdispute",
     *      tags={"Evaluation"},
     *      operationId="evaluationAllowdispute",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Admin will make an evaluation to allow dispute",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="eval_id",
     *          in="path",
     *          type="integer",
     *          required=true,
     *          description="Evaluation ID",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

    /**
     *
     *
     * @SWG\Put(
     *      path="/evaluation/{eval_id}/score",
     *      tags={"Evaluation"},
     *      operationId="evaluation_score",
     *      consumes={"application/x-www-form-urlencoded"},
     *      summary="Store evaluation score",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="eval_id",
     *          in="path",
     *          type="integer",
     *          required=true,
     *          description="Evaluation ID",
     *      ),
     *      @SWG\Parameter(
     *          name="group_score",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Evaluation Group score json",
     *      ),
     *      @SWG\Parameter(
     *          name="final_score",
     *          in="formData",
     *          type="number",
     *          required=true,
     *          description="Evaluation final score",
     *      ),
     *      @SWG\Parameter(
     *          name="ratings",
     *          in="formData",
     *          type="string",
     *          required=false,
     *          description="Evaluation ratings",
     *      ),
     *      @SWG\Parameter(
     *          name="answers",
     *          in="formData",
     *          type="string",
     *          required=true,
     *          description="Evaluation questions answers Json",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
    **/

    /**
     *
     *
     * @SWG\Post(
     *      path="/evaluation/{eval_id}/transition/{id}",
     *      tags={"Evaluation"},
     *      operationId="evaluation_transition",
     *      consumes={"application/x-www-form-urlencoded"},
     *      summary="Store evaluation transition",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="eval_id",
     *          in="path",
     *          type="integer",
     *          required=true,
     *          description="Evaluation ID",
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          in="path",
     *          type="integer",
     *          required=true,
     *          description="Status Transition Id",
     *      ),
     *      @SWG\Parameter(
     *          name="status_id",
     *          in="formData",
     *          type="integer",
     *          required=true,
     *          description="Status Id",
     *      ),
     *      @SWG\Parameter(
     *          name="comment_id",
     *          in="formData",
     *          type="integer",
     *          required=false,
     *          description="Comment Id",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     *  )
     *
    **/

    /**
     *
     *
     * @SWG\Post(
     *      path="/evaluation/{eval_id}/disposition",
     *      tags={"Evaluation"},
     *      operationId="evaluation_disposition",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Store evaluation disposition",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="eval_id",
     *          in="path",
     *          type="integer",
     *          required=true,
     *          description="Evaluation ID",
     *      ),
     *      @SWG\Parameter(
     *          name="severityid",
     *          in="formData",
     *          type="integer",
     *          required=true,
     *          description="Severity Id",
     *      ),
     *      @SWG\Parameter(
     *          name="evaluationdispositionid",
     *          in="formData",
     *          type="integer",
     *          required=true,
     *          description="Evaluation Disposition Id",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

    /**
     *
     *
     * @SWG\Post(
     *      path="/evaluation/{eval_id}/assign",
     *      tags={"Evaluation"},
     *      operationId="assign_QA_specialist",
     *      consumes={"application/x-www-form-urlencoded"},     
     *      summary="Assign QA specialist for a reevaluation",
     *      security={{"Bearer":{}}},
     *      @SWG\Parameter(
     *          name="eval_id",
     *          in="path",
     *          type="integer",
     *          required=true,
     *          description="Evaluation ID",
     *      ),
     *      @SWG\Parameter(
     *          name="user_id",
     *          in="formData",
     *          type="integer",
     *          required=true,
     *          description="User Id",
     *      ),
     *      @SWG\Response(
     *     response=200,
     *     description="Success Response"
     *   ),
     *      @SWG\Response(
     *     response=400,
     *     description="Not a Valid Request"
     *   ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   ),
     
     *  )
     *
     * 
    **/

    /**
     *
     *
     *  @SWG\Definition(
     *      definition="EvaluationScore",
     *      @SWG\Property(property="group_score", type="array", @SWG\Items(ref="#/definitions/GroupScore"), example="[{'groupid':25,'groupname':'COMPLIANCE', 'obscore':5}]"),
     *      @SWG\Property(property="final_score", type="string", example="10"),
     *      @SWG\Property(property="ratings", type="string", example="10"),
     *      @SWG\Property(property="answers", type="array", @SWG\Items(ref="#/definitions/Answers"), example="[{'questionid':200, 'answertext': ' ', 'body': ' ', 'score':0}]")
     *     )
     *  @SWG\Definition(
     *      definition="GroupScore",
     *      example={
     *         "group_score":25,
     *         "final_score":"COMPLIANCE",
     *         "ratings" : 10
     *      },
     *      @SWG\Property(property="groupid", type="string", example=25),
     *      @SWG\Property(property="groupname", type="string", example="COMPLIANCE"),
     *      @SWG\Property(property="obscore", type="string", example=10)
     *     )
     *  @SWG\Definition(
     *      definition="Answers",
     *      example={
     *         "questionid": 200,
     *         "answertext":" ",
     *         "body" : " ",
     *         "score" : 0
     *      },
     *      @SWG\Property(property="questionid", type="int", example=200),
     *      @SWG\Property(property="answertext", type="string", example=" "),
     *      @SWG\Property(property="body", type="string", example=" "),
     *      @SWG\Property(property="score", type="int", example=0)
     *     )
     *
     * 
    **/
}