<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Call;
use App\Campaign;
use App\CampaignCall;
use App\CampaignSearch;
use Auth;
use DataTables;

class CampaignController extends Controller
{
   
    public function validateCampaign(Request $request){
        $calls = Call::select('unique_id', 'agent_id', 'duration', 'client', 'segstart', 'segstop', 'direction');
        foreach($request->filters as $key => $value){
            if($value['key'] == 'equal'){
                $v = explode('|', $value['value']);             
                $calls->whereIn($value['name'], $v);
            }else if($value['key'] == 'notequal'){
                $v = explode('|', $value['value']);             
                $calls->whereNotIn($value['name'], $v);
            }else if($value['key'] == 'like'){
                $calls->where($value['name'], 'like', '%'.$value['value'].'%');
            }else{
                if($value['name'] == "maxduration"){
                    $calls->where('duration', $value['key'], $value['value']);
                }else if($value['name'] == "minduration"){
                    $calls->where('duration', $value['key'], $value['value']);
                }else{
                    $calls->where($value['name'], $value['key'], $value['value']);
                }
            }
        }
        $allcalls = $calls->get();
        $count = count($allcalls);
        if($request->counttype == 'All'){
            if($count == 0){
                $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria'; 
                $data['message'] = 'Validation Failed'; 
                return response($data, 422);
            }else{
                $data['message'] = 'Validation Successful'; 
                return response($data, 200);
            }
        }
        if($request->counttype == 'Number'){
            if($count >= $request->countvalue){
                $data['message'] = 'Validation Successful'; 
                return response($data, 200);
            }else if($count > 0 && $count < $request->countvalue){
                $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria'; 
                $data['message'] = 'Validation Failed'; 
                return response($data, 422);
            }else{
                $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria'; 
                $data['message'] = 'Validation Failed'; 
                return response($data, 422);
            }
        }
        if($request->type == 'Percentage'){
            $percentage = ($request->countvalue / 100 ) * $count;
            if($percentage < 1 && $percentage > 0){
                $percentage = 1;
            }
            $percentage = round($percentage);
            if($percentage <= $count && $percentage != 0){
                $data['message'] = 'Validation Successful'; 
                return response($data, 200);
            }else{
                $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria'; 
                $data['message'] = 'Validation Failed'; 
                return response($data, 422);
            }
        }
    }

    public function validateCamp($request){
        $calls = Call::select('unique_id', 'agent_id', 'duration', 'client', 'segstart', 'segstop', 'direction');
        foreach($request->filters as $key => $value){
            if($value['key'] == 'equal'){
                $v = explode('|', $value['value']);             
                $calls->whereIn($value['name'], $v);
            }else if($value['key'] == 'notequal'){
                $v = explode('|', $value['value']);             
                $calls->whereNotIn($value['name'], $v);
            }else if($value['key'] == 'like'){
                $calls->where($value['name'], 'like', '%'.$value['value'].'%');
            }else{
                if($value['name'] == "maxduration"){
                    $calls->where('duration', $value['key'], $value['value']);
                }else if($value['name'] == "minduration"){
                    $calls->where('duration', $value['key'], $value['value']);
                }else{
                    $calls->where($value['name'], $value['key'], $value['value']);
                }
            }
        }
        $checkcalls = Campaign::where('survey_id', $request->evaluationid)->with('campaigncall')->get();
        $cid = [];
        $i = 0;
        foreach($checkcalls as $k => $v){
            foreach($v->campaigncall as $val){
                $cid[$i] = $val->call_id;
                $i++;
            }
        }
        $cid = array_unique($cid);
        if(count($cid) > 0){
            $calls->whereNotIn('unique_id', $cid);
        }
        $allcalls = $calls->get();
        $count = count($allcalls);
        if($request->counttype == 'All'){
            if($count == 0){
                return false;
            }else{
                return true;
            }
        }
        if($request->counttype == 'Number'){
            if($count >= $request->countvalue){
                return true;
            }else if($count > 0 && $count < $request->countvalue){
                return false;
            }else{
                return false;
            }
        }
        if($request->type == 'Percentage'){
            $percentage = ($request->countvalue / 100 ) * $count;
            if($percentage < 1 && $percentage > 0){
                $percentage = 1;
            }
            $percentage = round($percentage);
            if($percentage <= $count && $percentage != 0){
                return true;
            }else{
                return false;
            }
        }
    }
    public function addCampaign(Request $request){
        $validate = $this->validateCamp($request);
        if($validate == true){
            $campaign['name'] = $request->name;
            $campaign['version'] = 'W1';
            $campaign['survey_id'] = $request->evaluationid;
            $campaign['description'] = $request->description;
            $campaign['end_date'] = $request->duedate;
            $campaign['frequency_type'] = 'one';
            $campaign['frequency_val'] = 'one';
            $campaign['type'] = $request->counttype;
            $campaign['type_val'] = $request->countvalue;
            $campaign['created_by'] = Auth::user()->user_id;
            $campaign['status'] = $request->status;
            $addcampaign = Campaign::create($campaign);
            $campaignSearch = [];
            foreach($request->filters as $key => $value){
                $campaignSearch[$key]['column_name'] = $value['name'];
                $campaignSearch[$key]['column_val'] = $value['value'];
                $campaignSearch[$key]['column_conditions'] = $value['key'];
                $campaignSearch[$key]['campaign_id'] = $addcampaign->id;
                $campaignSearch[$key]['created_at'] = date('Y-m-d H:i:s');
            }
            $addsearch = CampaignSearch::insert($campaignSearch);
            $request['campaignid'] = $addcampaign->id;
            $request['surveyid'] = $request->evaluationid;
            $cc = $this->addCampaignCalls($request);
            $data['message'] = 'Campaign Added Successfully'; 
            return response($data, 200); 
        }else{
            $data['errors']['Invalid Search']['0'] = 'Please Modify Your Search Criteria'; 
            $data['message'] = 'Validation Failed';
            return response($data, 422); 
        }
    }

    public function addCampaignCalls($request){
        $calls = Call::select('unique_id', 'agent_id', 'duration', 'client', 'segstart', 'segstop', 'direction');
        foreach($request->filters as $key => $value){
            if($value['key'] == 'equal'){
                $v = explode('|', $value['value']);             
                $calls->whereIn($value['name'], $v);
            }else if($value['key'] == 'notequal'){
                $v = explode('|', $value['value']);             
                $calls->whereNotIn($value['name'], $v);
            }else if($value['key'] == 'like'){
                $calls->where($value['name'], 'like', '%'.$value['value'].'%');
            }else{
                if($value['name'] == "maxduration"){
                    $calls->where('duration', $value['key'], $value['value']);
                }else if($value['name'] == "minduration"){
                    $calls->where('duration', $value['key'], $value['value']);
                }else{
                    $calls->where($value['name'], $value['key'], $value['value']);
                }
            }
        }
        $checkcalls = Campaign::where('survey_id', $request->evaluationid)->with('campaigncall')->get();
        $cid = [];
        $i = 0;
        foreach($checkcalls as $k => $v){
            foreach($v->campaigncall as $val){
                $cid[$i] = $val->call_id;
                $i++;
            }
        }
        $cid = array_unique($cid);
        if(count($cid) > 0){
            $calls->whereNotIn('unique_id', $cid);
        }
        $allcalls = $calls->get();
        $cc = collect($allcalls)->pluck('unique_id');
        $count = count($allcalls);
        $camp = [];
        if($request->counttype == 'All'){
            foreach($cc as $k => $v){
                $camp[$k]['call_id'] = $v;
                $camp[$k]['campaign_id'] = $request->campaignid;
                $camp[$k]['status'] = 'todo';
                $camp[$k]['created_by'] = Auth::user()->user_id;
                $camp[$k]['created_at'] = date('Y-m-d H:i:s');
            }
            $campaigncalls = CampaignCall::insert($camp);
            return;
        }
        if($request->counttype == 'Number'){
            $request->countvalue;
            for($i=0;$i<$request->countvalue;$i++){
                $camp[$i]['call_id'] = $cc[$i];
                $camp[$i]['campaign_id'] = $request->campaignid;
                $camp[$i]['status'] = 'todo';
                $camp[$i]['created_by'] = Auth::user()->user_id;
                $camp[$i]['created_at'] = date('Y-m-d H:i:s');
            }
            $campaigncalls = CampaignCall::insert($camp);
            return;
        }
        if($request->counttype == 'Percentage'){
            $percentage = ($request->countvalue / 100 ) * $count;
            if($percentage < 1 && $percentage > 0){
                $percentage = 1;
            }
            $percentage = round($percentage);
            for($i=0;$i<$percentage;$i++){
                $camp[$i]['call_id'] = $cc[$i];
                $camp[$i]['campaign_id'] = $request->campaignid;
                $camp[$i]['status'] = 'todo';
                $camp[$i]['created_by'] = Auth::user()->user_id;
                $camp[$i]['created_at'] = date('Y-m-d H:i:s');
            }
            $campaigncalls = CampaignCall::insert($camp);
            return;
        }
    }
    
    public function getCampaigns(Request $request){
        $campaigns = Campaign::select('id','name', 'status', 'start_date as startdate', 'end_date as duedate')->where('status', $request->status)->get();
        return $campaigns;
    }

    public function listCampaigns(Request $request){
        if($request->status == 'active'){
            $campaigns = Campaign::select('id','name', 'survey_id', 'start_date as startdate', 'end_date as enddate', 'frequency_type as frequency', 'created_by','status')->with(['evaluation_form' => function($query){
                $query->select('id','title', 'client_id');
            }, 'createduser' => function($query){
                $query->select('user_id','name');
            }])->where('status', '!=','completed')->where('status', '!=', 'inactive')->get();
        }else if($request->status == 'completed'){
            $campaigns = Campaign::select('id','name', 'survey_id', 'start_date as startdate', 'end_date as enddate', 'frequency_type as frequency', 'created_by','status')->with(['evaluation_form' => function($query){
                $query->select('id','title', 'client_id');
            }, 'createduser' => function($query){
                $query->select('user_id','name');
            }])->where('status', 'completed')->get();
        }else{
            $campaigns = Campaign::select('id','name', 'survey_id', 'start_date as startdate', 'end_date as enddate', 'frequency_type as frequency', 'created_by','status')->with(['evaluation_form' => function($query){
                $query->select('id','title', 'client_id');
            }, 'createduser' => function($query){
                $query->select('user_id','name');
            }])->get();
        }
        return DataTables::of($campaigns)->addColumn('completed', function(Campaign $campaign) {
                    $total = $campaign->campaigncall->count();
                    if($total > 0){
                        $completed = $campaign->campaigncall->where('status', 'completed')->count();
                        $completedpercent = ($completed / $total) * 100;
                        return round($completedpercent);
                    }else{
                        return $total;
                    }
                })->addColumn('inprogress', function(Campaign $campaign) {
                    $total = $campaign->campaigncall->count();
                    if($total > 0){
                        $inprogress = $campaign->campaigncall->where('status', 'inprogress')->count();
                        $inprogresspercent = ($inprogress / $total) * 100;
                        return round($inprogresspercent);
                    }else{
                        return $total;
                    }
                })->toJson();
    }

    public function updateCampaign(Request $request){
        $check = Campaign::where('id', $request->id)->count();
        $current = date('Y-m-d H:i:s');
        if($check == 1){
            if($request->currentstatus == 'new' && $request->status == 'active'){
                $campaign = Campaign::where('id', $request->id)->update(['status' => $request->status, 'start_date' => $current]);
            }else{
                $campaign = Campaign::where('id', $request->id)->update(['status' => $request->status]);
            }
            $data['message'] = 'Campaign Updated Successfully'; 
            return response($data, 200);
        }else{
            $data['message'] = 'Invalid Campaign'; 
            return response($data, 422);
        }
    }
}
