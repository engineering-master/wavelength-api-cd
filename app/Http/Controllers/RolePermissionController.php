<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AssignRoleRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use GuzzleHttp\Client;
use DataTables;
use App\User;
Use App\GroupUser;
use App\ModelHasRole;
use DB;

class RolePermissionController extends Controller
{
	
    public function getPermissions(Request $request){
        $permissions = Permission::select('id', 'name', 'mandatory', 'page')->get();
        $collection = $permissions;
        $grouped = collect($collection)->groupBy('page');
        $grouped->toArray();
        $val = [];
        $i = 0;
        foreach($grouped as $key => $value){
            $val[$i]['page'] = $key;
            $val[$i]['permissions'] = $value;
            $i++;
        }
    	return $val;
    }

    
    public function getRoles($id = null){
    	if($id != null){
    		$role = Role::where('id', $id)->with(['permissions' => function($query){
                $query->select('name', 'page', 'mandatory', 'id');
            }])->first();
            $allroles = [];
            $allroles['id'] = $role['id'];
            $allroles['name'] = $role['name'];
            $collection = collect($role['permissions'])->groupby('page');
            $collection->toArray();
            $i = 0;
            if(count($role['permissions']) == 0){
                $allroles['pages'] = [];
            }else{
                foreach($collection as $key => $value){
                    $allroles['pages'][$i]['page'] = $key;
                    $allroles['pages'][$i]['permissions'] = $value; 
                    $i++;
                }
            }
            return $allroles;
    	}else{
    		$roles = Role::with(['permissions' => function($query){
                $query->select('name', 'page', 'mandatory', 'id');
            }])->get();
    		$allroles = [];
            foreach($roles as $k => $role){
                $allroles[$k]['id'] = $role['id'];
                $allroles[$k]['name'] = $role['name'];
                $collection = collect($role['permissions'])->groupby('page');
                $collection->toArray();
                $i = 0;
                if(count($role['permissions']) == 0){
                    $allroles[$k]['pages'] = [];
                }else{
                    foreach($collection as $key => $value){
                        $allroles[$k]['pages'][$i]['page'] = $key;
                        $allroles[$k]['pages'][$i]['permissions'] = $value; 
                        $i++;
                    }
                }
            }
            return $allroles;
    	}
    }

       
    public function assignPermissions(Request $request){
        $role = Role::where('id', $request->role_id)->first();
        $v = $role->syncPermissions($request->permissions);
        $data['message'] = 'Roles Updated Successfully';
        $data['permissions'] = $this->getRoles();
        return response($data, 200);
    }

    public function assignRole(AssignRoleRequest $request){
        try{            
            DB::beginTransaction();
            try {//Auth portal role updated 
                $client = new Client();
                $authConfig = config('auth.authentication');
                $host = $authConfig['portal']['url'].'/api/v1.1/user/role';
                $result = $client->request('POST',$host, [
                    'form_params' => ['user_id' => $request->user_id, 'role_name' => $request->role_name,'site_id'=>1]
                ]);
            }catch (\GuzzleHttp\Exception\RequestException $e) { // exception in auth portal
                $data['message'] = 'Roles Not Updated in auth portal.' . $e->getMessage();
                return response($data, 500);
            }
            if ($result->getStatusCode() == 200) {
                    ModelHasRole::where('model_id', $request['user_id'])->delete();                    
                    $user = User::where('user_id', $request['user_id'])->first();
                    $role = Role::where('name',$request->role_name)->first();
                    if ($user->assignRole($role->id)) {                        
                        DB::commit();
                        $data['message'] = 'Roles Updated Successfully';
                        return response($data, 200);
                    } else {
                        DB::rollback();
                        $data['message'] = 'Roles Not Updated on assignRole';
                        return response($data, 422);
                    }             
            } else {
                return response($result->getBody(), 422);
            }
        } catch (Exception $e) {
            $data['message'] = 'Roles Not Updated ' . $e->getMessage();
            return response($data, 500);
        }
    }
}