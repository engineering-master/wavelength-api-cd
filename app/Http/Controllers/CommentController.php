<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Http\Requests\CommentsViewRequest;
use App\Comment;
use App\Call;
use App\Evaluation;
use App\EvaluationForm;
use App\EvaluationWorkflows;
use App\Role;
use Auth;
use App\Http\Requests\CommentsRequest;
use Exception;
use App\EvaluationStatusMaster;
use App\Http\Traits\Metrics;
use Log;
use App\UserSystemMapping;

class CommentController extends Controller
{
    use Metrics;
    var $metricsArray = array();
    
    public function store(CommentRequest $request)
    {
        $comment = new Comment(['body' => $request->body,'created_by' => $request->employeeid]);
        $call = Call::find($request->callid);
        $comment = $call->comments()->save($comment);
        if($comment->id){
            $data['message'] = 'Comment Saved Successfully'; 
            return response($data, 200);
        } else{
            return response('Comment Not Saved', 500);
        }
    }

    
    /**
     * View Comment for entity 
     * Method: GET
     * 
     * Endpoint: /comments/{type}/{id}  ex:type=>evaluation,answer
     * Required Parameters: NA
     * 
     */
    public function show($type,$id,CommentsViewRequest $request)
    {
        $comment_view_start = microtime(true);
        try {
            $comment_find_start = microtime(true);
            $commentData=$collectComments=array();
            $entity = '';
            $entityClass = "App\\".studly_case($type);
            if (class_exists($entityClass)) {
                $entity = $entityClass::where('id',$id);          
                if (Auth::user()->roles->pluck('id')[0]==2) {//Get Auth user role id)
                    $userSystemMapping = UserSystemMapping::where('user_id', Auth::user()->user_id)
                    ->pluck('system_user_id');
                    $entity = $entity->whereIn('agent_id',$userSystemMapping);
                }
                $entity = $entity->first();
            }

            if ($entity) {
                //Get Evaluation results
                $loadEntity =  $this->getComments($entityClass,$id);
                $this->metricsArray[] = array('MetricName'=>"comment:Evaluation", "Timestamp"=>$this->get_elapsed_time($comment_find_start));
         
                foreach($loadEntity[0]->comments as $key=>$comment){
                    $collectComments[]=$comment;
                }

                //Get Re-Evaluation results
                if( $entity->predecessor_id){ 
                    $loadEntity =  $this->getComments($entityClass,$entity->predecessor_id);
                    $this->metricsArray[] = array('MetricName'=>"comment:Re-evaluation", "Timestamp"=>$this->get_elapsed_time($comment_find_start));
                    foreach($loadEntity[0]->comments as $key=>$comment){
                        $collectComments[]=$comment;
                    }
                }
                //Create Response json
                foreach($collectComments as $key=>$comment){                    
                    $commentData[$key]['status_id']=isset($comment->status->status_id)?$comment->status->status_id:'';
                    $commentData[$key]['status']=isset($comment->status->status->status)?$comment->status->status->status:'';
                    $commentData[$key]['comment']=$comment->body;
                    $commentData[$key]['commented_by']=$comment->user->name;
                    $commentData[$key]['date']=$comment->date;
                }
                $rolesPending = [];
                if ($type == "evaluation") {
                    $transition_status_id = $entity->status_id;
                    if ($transition_status_id == 6) { /* 6 : Dispute Accepted */
                        $newEvaluation = Evaluation::where('predecessor_id', $entity->id)->first();
                        if ($newEvaluation) {
                            $transition_status_id = $newEvaluation->status_id;
                        }
                    }
                    $evaluationForm = EvaluationForm::find($entity->evaluation_form_id);
                    $allowed_transitions = EvaluationWorkflows::workflow($evaluationForm->workflow_id);
                    $next_allowed_transition = array();
                    if ($allowed_transitions) {
                        foreach ($allowed_transitions as $allowed_transition) {
                            if ($allowed_transition['PickUpStatus'] == $transition_status_id) {
                                $roles = $allowed_transition['Roles'];
                                if (!empty(count($roles))) {
                                    $rolesPending = Role::where('id', $roles[0])->pluck('name');
                                }
                            }
                        }
                    }
                }
                $this->metricsArray[] = array('MetricName'=>"comment", "Timestamp"=>$this->get_elapsed_time($comment_view_start));
                if(empty($commentData)){
                    $data['message'] = 'No comments found for this evaluation';
                    $data['pending'] = $rolesPending;
                    return response($data, 200);
                }
                $returnData = [];
                $returnData['comments'] = $commentData;
                $returnData['pending'] = $rolesPending;
                return response($returnData, 200);
            } else { //Request id not found
                $this->metricsArray[] = array('MetricName'=>"comment", "Timestamp"=>$this->get_elapsed_time($comment_view_start));
                $data['message'] = 'Entity resource not found';  
                return response($data, 422);
            }

        } catch (Exception $ex) { // Anything that went wrong
            $this->metricsArray[] = array('MetricName'=>"comment", "Timestamp"=>$this->get_elapsed_time($comment_view_start));
            return response()->json(['errors' => 'Something went wrong,try again later '.$ex->getMessage()], 422);
        }
    }


    private function getComments($entityClass,$id){
        $evaluationcomments='';   
            $evaluationcomments = $entityClass::select('id')
            ->where('id', $id)
            ->with(['comments' => function($q){
                $q->select('id','comments.commentable_id','comments.body','comments.created_by','comments.created_at as date')
                ->with(['user'=> function($q){
                    $q->select('user_id','name');
                }])
                ->with(['status'=> function($q){
                    $q->select('comment_id','status_id');
                }]);
            }])->get();
        return $evaluationcomments;

    }
    /**
     * Comment for evaluation 
     * Method: POST
     * 
     * Endpoint: {type}/{id}/comment  ex:type=>evaluation 
     * Required Parameters: body
     * 
     */
    public function comment($type,$id,CommentsRequest $request){
        $comment_start = microtime(true);
        try{
            $comment_find_start = microtime(true);
            $statuslist =config('enum.allow_entity_to_process.allow_comment'); 
            $evaluationStatusMaster = EvaluationStatusMaster::whereIn('status', $statuslist[0])->pluck('id');
            $evaluation = Evaluation::where('id', $id)
                                    ->whereIN('status_id',$evaluationStatusMaster)
                                    ->first();
             $this->metricsArray[] = array('MetricName'=>"comment:createEvaluationComment", "Timestamp"=>$this->get_elapsed_time($comment_find_start));
            if($evaluation){
                $commentbody = json_decode($request->body,true);
                foreach($commentbody as $body){
                    $comment = new Comment;
                    $comment->body = $body['comment'];
                    $comment->created_by = Auth::user()->user_id;
                    $evaluation->comments()->save($comment);
                }
                $this->metricsArray[] = array('MetricName'=>"comment", "Timestamp"=>$this->get_elapsed_time($comment_start));
                $data['message'] = 'Comment Saved Successfully'; 
                return response($comment, 200);
            }
            else{ 
                $this->metricsArray[] = array('MetricName'=>"comment", "Timestamp"=>$this->get_elapsed_time($comment_start));
                $data['message'] = 'Evaluation resource not found';  
                return response($data, 422);
            }
        } catch (Exception $ex) { // Anything that went wrong
            $this->metricsArray[] = array('MetricName'=>"comment", "Timestamp"=>$this->get_elapsed_time($comment_start));
            return response()->json(['errors' => 'Something went wrong,try again later'], 422);
        }
    }
    
    public function __destruct() {
        //Log the captured information
        if (count($this->metricsArray)) {
            try{
                //Log to CloudWatch
                $cw_response = $this->putMetricData($this->metricsArray);

            } catch (\Exception $e) {
                //print_r($e);
            }
        }
    }
}
