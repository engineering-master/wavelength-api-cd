<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\WorkflowRequest;
use App\Http\Requests\WorkflowListRequest;
use App\EvaluationWorkflows;
use Auth, Cache;
use App\Http\Traits\Metrics;

class WorkflowController extends Controller
{
    use Metrics;
    var $metricsArray = array();
    /**
     * Display a listing of the resource.
     * Workflow list 
     * method:GET
     *
     * @return \Illuminate\Http\Response
     */
    public function index(WorkflowListRequest $request)
    {
        $WorkflowList_start = microtime(true);
        try{
            $WorkflowList_find_start = microtime(true);
            $wokflows=EvaluationWorkflows::where('status',$request->status)->get(); 
            return response($wokflows,200);
        } catch (Exception $ex) { // Anything that went wrong
            $this->metricsArray[] = array('MetricName'=>"Workflow", "Timestamp"=>$this->get_elapsed_time($WorkflowList_start));
            return response()->json(['errors' => 'Something went wrong,try again later'], 422);
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(WorkflowRequest $request)
    {
        try{
            EvaluationWorkflows::create([
                'description' =>  $request['description'],
                'status'=>0,
                'start_date'=>$request['start_date'],
                'wf_details'=>$request['wf_details'],
                'created_by' => Auth::user()->user_id]);
            $data['message'] = 'Workflow Saved Successfully ';
            return response($data, 200);
        } catch (Exception $ex) { // Anything that went wrong
            return response()->json(['errors' => 'Something went wrong,try again later'], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }    
    public function __destruct() {
        //Log the captured information
        if (count($this->metricsArray)) {
            try{
                //Log to CloudWatch
                $cw_response = $this->putMetricData($this->metricsArray);

                //Log to a file
                $logfile = "evaluations";
                Log::channel($logfile)->Info( json_encode($this->metricsArray, true) );
            } catch (\Exception $e) {
                //print_r($e);
            }
        }
    }
}
