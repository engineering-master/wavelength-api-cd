<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Requests\EvaluationScoreStoreRequest;
use App\Http\Requests\EvaluationTransitionStoreRequest;
use App\Http\Requests\EvaluationsAllowDisputeRequest;
use App\Jobs\ProcessEvaluation;
use App\EvaluationForm;
use App\Employee;
use App\Answer;
use App\Call;
use App\User;
use Auth;
use App\Comment;
use App\Evaluation;
use App\Disposition;
use App\Question;
use App\QuestionGroup;
use App\Campaign;
use App\CampaignCall;
use App\CampaignSearch;
use App\Worklist;
use App\CampaignSkill;
use App\WavClient;
use DB, Log;
use DataTables;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\EvaluationDispositions;
use App\EvaluationStatusMaster;
use App\EvaluationWorkflows;
use App\EvaluationStatusAudit;
use App\Http\Requests\CallEvaluationPostRequest;
use App\Http\Requests\EvaluationDispositionPostRequest;
use App\Http\Requests\AssignQASpecialistPostRequest;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\Metrics;

class EvaluationController extends Controller
{
    use Metrics;
    var $metricsArray = array();
    
    public function store(Request $request)
    {
        if($request->evaluation_id == ''){
            $surchecksubmitted = Evaluation::where('call_id', $request->callid)->where('evaluation_form_id', $request->evaluation_form_id)->where('status', 'submitted')->count();

            $surcheckinprogress = Evaluation::where('call_id', $request->callid)->where('evaluation_form_id', $request->evaluation_form_id)->where('status', 'inprogress')->count();
            $surcheck = $surcheckinprogress + $surchecksubmitted;
        }else{
            $surcheck = Evaluation::where('call_id', $request->callid)->where('evaluation_form_id', $request->evaluation_form_id)->where('status', 'submitted')->count();    
        }
        

        if($surcheck > 0){
            $data['errors']['Invalid Search']['0'] = 'Evaluation Already Completed'; 
            $data['message'] = 'Validation Failed';
            return response($data, 422);
        }elseif($request->evaluationdispositionid == '' && $request->status=="submitted"){
            $data['errors']['Invalid Search']['0'] = 'Evaluation Disposition Missing'; 
            $data['message'] = 'Validation Failed';
            return response($data, 422);
        }else{
            $postdatas = $request->all();
            //for update $request->evaluation_id;
            $evaluation['evaluation_form_id'] = $request->evaluation_form_id;
            $evaluation['employee_id'] = $request->employeeid;
            $evaluation['call_id'] = $request->callid;
            $evaluation['final_score'] = $request->finalscore;
            $evaluation['ratings'] = $request->rating;
            $evaluation['status'] = $request->status;
            $evaluation['created_by'] = Auth::user()->user_id;
            $evaluation['group_score'] = collect($request->questiongroup)->toJson();
            foreach($request->headervalues as $header) {
                if ($header['name'] == 'Phone Number') {
                    $evaluation['phone_number'] = $header['value'];
                }
                if ($header['name'] == 'Disposition') {
                    $evaluation['disposition'] = $header['value'];
                }
                if ($header['name'] == 'Agent Name') {
                    $evaluation['agent_name'] = $header['value'];
                }
            }
            $headers = collect($request->headervalues)->toJson();
            $evaluation['headers'] = $headers;

            if($request->evaluation_id != ''){
                $sursave = Evaluation::where('id', $request->evaluation_id)->update($evaluation);
                $storedata['evaluation_id'] = $request->evaluation_id;
                Answer::with('comments')->where('evaluation_id', $request->evaluation_id)->delete();
            }else{
                $sursave = Evaluation::Create($evaluation);
                $storedata['evaluation_id'] = $sursave->id;
            }
            if(isset($request->evaluationdispositionid) && $request->evaluationdispositionid!='' && isset($storedata['evaluation_id']) && $storedata['evaluation_id']!='')
            {
                $evaluationdispositions['evaluations_id'] = $storedata['evaluation_id'];
                $evaluationdispositions['evaluation_disposition_definitions_id'] = $request->evaluationdispositionid;
                $evaluationdispositions['severity_id'] = $request->severityid;
                $evaluationdispositions['created_by'] = Auth::user()->user_id;
                $evaluationdispositionsave = EvaluationDispositions::Create($evaluationdispositions);
            }
            foreach($postdatas['questions'] as $key => $postdata){
                 $storedata['question_id'] = $postdata['questionid'];
                 $storedata['answer_text'] = $postdata['answertext'];
                 $storedata['value'] = $postdata['score'];
                 $save = Answer::Create($storedata);
                 if(isset($postdata['body'])){
                    $comment = new Comment(['body' => $postdata['body'],'created_by' => Auth::user()->user_id]);
                    $call = Answer::find($save->id);
                    $comment = $call->comments()->save($comment);
                 }
            }

            if(isset($sursave->id)){
                $data['message'] = 'Evaluation Saved Successfully';
                $sur = Evaluation::select('id','evaluation_form_id', 'call_id', 'status', 'employee_id', 'created_by')->where('id', $sursave->id)->first();
                $data['evaluations'] = $sur;
                return response($data, 200);
            }elseif($request->evaluation_id != ''){
                $data['message'] = 'Evaluation Updated Successfully';
                $sur = Evaluation::select('id','evaluation_form_id', 'call_id', 'status', 'employee_id', 'created_by')->where('id', $request->evaluation_id)->first();
                $data['evaluations'] = $sur;
                return response($data, 200);
            }{
                $data['errors']['Invalid Search']['0'] = 'Evaluation Not Saved Successfully'; 
                $data['message'] = 'Validation Failed';
                return response($data, 422);
            }
        }          
    }

    public function updateWorklist($request, $id){
        $checkworklist = Worklist::where('call_id', $request->callid)->where('created_by', Auth::user()->user_id)->with(['campaign' => function($query)use($request){
            $query->where('survey_id', $request->surveyid);
        }])->get();
        $cwc = count($checkworklist);
        if($cwc > 0){
            foreach($checkworklist as $cwk => $cwval){
                if($cwval['campaign'] != null){
                    $cid = $cwval['campaign']['id'];
                    $wid = $cwval['id'];
                }
            }
            $checkcampaign = CampaignCall::where('call_id', $request->callid)->where('campaign_id', $cid)->count();
            $wc = Worklist::where('call_id', $request->callid)->where('campaign_id', $cid)->count();
            if($checkcampaign == 1 && $wc == 0){
                $worklist['call_id'] = $request->callid;
                $worklist['campaign_id'] = $cid;
                $worklist['created_by'] = Auth::user()->user_id;
                $worklist['status'] = ($request->status == 'Submitted') ? 'completed' : 'inprogress';
                $worklist['evaluation_id'] = $id;
                Worklist::create($worklist);
                $campaign['status'] = ($request->status == 'Submitted') ? 'completed' : 'inprogress';
                CampaignCall::where('call_id', $request->callid)->where('campaign_id', $cid)->update($campaign);
                return;
            }
            if($wc == 1){
                $worklist['status'] = ($request->status == 'Submitted') ? 'completed' : 'inprogress';
                $worklist['evaluation_id'] = $id;
                $wl = Worklist::where('id', $wid)->update($worklist);
                $campaign['status'] = ($request->status == 'Submitted') ? 'completed' : 'inprogress';
                CampaignCall::where('call_id', $request->callid)->where('campaign_id', $cid)->update($campaign);
                return;
            }
        }
        return;
    }

    /*public function getEvaluations($callid){
        $user = Auth::user();
        $role = $user->roles->pluck('name');
        if($role['0'] == 'Administrator' || $role['0'] == 'QA Manager' || $role['0'] == 'QA Specialist'){
            $evaluations = Evaluation::select('id as responseid', 'evaluation_form_id', 'employee_id', 'call_id', 'final_score as finalscore', 'created_at', 'updated_at', 'status', 'created_by')->where('call_id', $callid)->with(['evaluation_form' => function($query){
            $query->select('id', 'title');
            }, 'createduser' => function($query){
                $query->select('user_id', 'name');
            }, 'user' => function($query){
                $query->select('user_id', 'name as employee_name', 'user_id as eid');
            }])->get();
            $evaluations = $evaluations->filter(function($item, $key)use($user, $evaluations){
                if($item->status == 'In progress'){    
                    if($item->created_by != $user->user_id){
                        $evaluations->forget($key);
                    }    
                }
            });
            $evaluation = $evaluations;
        }else{
            $evaluations = Evaluation::select('id as responseid', 'evaluation_form_id', 'employee_id', 'call_id', 'final_score as finalscore', 'created_at', 'updated_at', 'status', 'created_by')->where('call_id', $callid)->where('status', 'submitted')->with(['evaluation_form' => function($query){
            $query->select('id', 'title');
            }, 'createduser' => function($query){
                $query->select('user_id', 'name');
            }, 'user' => function($query){
                $query->select('user_id', 'name as employee_name', 'user_id as eid');
            }])->get();
            $evaluation = collect($evaluations);
        }
        return DataTables::of($evaluation)->make(true);
    }*/

    public function showEvaluation($id){
        $evaluations = Evaluation::where('id', $id)->with(['answers', 'acknowledgeduser', 'user', 'createduser', 'evaluationcall' => function($query){
            $query->select('ucid as callid', 'ucid', 'unique_id','agent_id','segstart as callstartdatetime','segstop as callenddatetime','duration as callduaration','direction as calldirection','outbound_cid as outboundcid','calling_pty as callingpartycid','dialed_num as diallednumber','screenfileurl as screenfileurl','recordingfileurl as recordingfile','recordingfileurl as audiofileurl','client as client', 'dialed_num', 'extension', 'customer_num','usr_grp')->with(['employee' => function($q){
                    $q->select('name','name as agentname', 'user_id', 'supervisor_user_id');
                }]);
        }])->first();

        $evaluation_form = EvaluationForm::where('id', $evaluations['evaluation_form_id'])->first();

        $evaluationdisposition = EvaluationDispositions::where('evaluations_id', $id)->with(['evaluationdispositiondefinition' => function($q){
            $q->select('id', 'name', 'severity', 'description');
        }])->get();
        if($evaluationdisposition->isNotEmpty()){
            $evaluation['evaluation']['evaluationdisposition'] = $evaluationdisposition[0]['evaluationdispositiondefinition'];
        }
        else
        {
            $evaluation['evaluation']['evaluationdisposition'] = [];
        }
        if (!empty(count($evaluations['evaluationcall'])) && isset($evaluations['evaluationcall']['audiofileurl'])) {
            $audiofileurl = Call::multiAudioFile($evaluations['evaluationcall']['audiofileurl'], $evaluations['user']['name'],$evaluations['evaluationcall']['usr_grp']);
            $evaluations['evaluationcall']['audiofileurl'] = $audiofileurl;
        }
        $evaluation['evaluation']['calldetails'] = $evaluations['evaluationcall'];
        $evaluation['evaluation']['description'] = $evaluation_form['title'];
        $evaluation['evaluation']['evaluatorname'] = $evaluations['createduser']['name'];
        $evaluation['evaluation']['evaluatorid'] = $evaluations['created_by'];
        $evaluation['evaluation']['responseid'] = $evaluations['id'];
        $evaluation['evaluation']['callid'] = $evaluations['call_id'];
        $evaluation['evaluation']['evaluationformid'] = $evaluations['evaluation_form_id'];
        $evaluation['evaluation']['agentid'] = $evaluations['employee_id'];
        $evaluation['evaluation']['agentname'] = $evaluations['user']['name'];
        $evaluation['evaluation']['headers'] = json_decode($evaluations['headers'], true);
        $evaluation['evaluation']['group_score'] = json_decode($evaluations['group_score'], true);
        $evaluation['evaluation']['ratings'] = json_decode($evaluation_form['ratings'], true);
        $evaluation['evaluation']['finalratings'] = $evaluations['ratings'];
        $evaluation['evaluation']['finalscore'] = $evaluations['final_score'];
        $evaluation['evaluation']['status'] = $evaluations['status'];
        $evaluation['evaluation']['acknowledgedusername'] = ($evaluations['acknowledgeduser'] != null) ? $evaluations['acknowledgeduser']['name'] : '';
        $evaluation['evaluation']['commentcreateddatetime'] = $evaluations['comment_created_at'];
        $evaluation['evaluation']['commentbody'] = $evaluations['comment'];
        $evaluation['evaluation']['acknowledgeduserid'] = $evaluations['acknowledged_by'];
        $evaluation['evaluation']['isacknowledged'] = ($evaluations['status'] == "pending") ? false:true;
        $v = [];
            foreach($evaluations['answers'] as $akey => $avalue){
                if($avalue['question']['group_id'] != null){
                    $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['groupname'] = $avalue['question']['questiongroup']['title'];
                    $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['groupdescription'] = $avalue['question']['questiongroup']['description'];
                    $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['groupid'] = $avalue['question']['group_id'];
                    $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['percentage'] = $avalue['question']['questiongroup']['percentage'];
                    $evaluation['evaluation']['questiongroups'][$avalue['question']['group_id']]['groupmode'] = $avalue['question']['questiongroup']['groupmode'];
                }
                if($avalue['question'] != null){
                    $v[$akey]['options'] = json_decode($avalue['question']['values'], true);
                    $v[$akey]['score'] = json_decode($avalue['score'], true);
                    $v[$akey]['mandatory'] = $avalue['question']['mandatory'];
                    $v[$akey]['weightage'] = $avalue['question']['actual_score'];
                    $v[$akey]['showcommentbox'] = $avalue['question']['comment'];
                    $v[$akey]['commentmandatory'] = $avalue['question']['commentmandatory'];
                    $v[$akey]['groupid'] = $avalue['question']['group_id'];
                    $v[$akey]['questionid'] = $avalue['question']['id'];
                    $v[$akey]['questiontype'] = $avalue['question']['type'];
                    $v[$akey]['questiontitle'] = $avalue['question']['title'];
                    $v[$akey]['value'] = $avalue['answer_text'];
                    $v[$akey]['rawvalues'] = $avalue['question']['rawvalues'];
                    $comment = collect($avalue['comments'])->first(function($item, $key){
                        return $item->body;
                    });
                    $v[$akey]['comment'] = (isset($comment->body)) ? $comment->body : '';
                    $v[$akey]['disabled'] = false;
                    if($avalue['question']['type'] == 'lineitems'){
                        $subgroups = Question::select('id', 'id as questionid', 'type as questiontype', 'title as questiontitle', 'parent_question_id','score','values as options', 'comment as showcommentbox', 'commentmandatory', 'mandatory', 'actual_score')->where('parent_question_id', $avalue['question']['id'])->with(['answers' => function($query) use ($id){
                            $query->select('answer_text', 'evaluation_id', 'question_id', 'id')->where('evaluation_id', $id);
                        }])->get();
                        $subgroup = [];
                        foreach($subgroups as $skey => $svalue){
                            $subgroup[$skey]['questionid'] = $svalue['questionid'];
                            $subgroup[$skey]['score'] = json_decode($svalue['score'], true);
                            $subgroup[$skey]['options'] = json_decode($svalue['options'], true);
                            $subgroup[$skey]['questiontitle'] = $svalue['questiontitle'];
                            $subgroup[$skey]['questiontype'] = $svalue['questiontype'];
                            // $subgroup[$skey]['options'] = json_decode($svalue['question']['values'], true);
                            $subgroup[$skey]['mandatory'] = $svalue['mandatory'];
                            $subgroup[$skey]['showcommentbox'] = $svalue['showcommentbox'];
                            $subgroup[$skey]['commentmandatory'] = $svalue['commentmandatory'];
                            $subgroup[$skey]['weightage'] = $svalue['actual_score'];
                            foreach($svalue['answers'] as $sanswer){
                                $subgroup[$skey]['value'] = $sanswer['answer_text'];
                                if(count($sanswer['comments']) > 0){
                                    foreach($sanswer['comments'] as $comments){
                                        $subgroup[$skey]['comment'] = $comments['body'];
                                    }
                                }else{
                                    $subgroup[$skey]['comment'] = '';
                                }
                            }
                        }

                        $v[$akey]['subgroups'] = $subgroup;
                    }else{
                        $v[$akey]['subgroups'] = '';
                    }
                }
            }
        foreach($evaluation['evaluation']['questiongroups'] as $keyresponse => $responseval){
            if(isset($responseval['groupid'])){
                if ($responseval['groupname'] == 'Header Details') {
                    continue;
                }
                $res[$keyresponse]['groupname'] = $responseval['groupname']; 
                $res[$keyresponse]['groupdescription'] = $responseval['groupdescription'];
                $res[$keyresponse]['groupid'] = $responseval['groupid'];
                $res[$keyresponse]['percentage'] = $responseval['percentage'];
                $res[$keyresponse]['groupmode'] = $responseval['groupmode'];
                $res[$keyresponse]['questions'] = collect($v)->where('groupid', $responseval['groupid'])->values()->all();
            }
        }

        $evaluation['evaluation']['questiongroups'] = collect($res)->values()->all();
        $va = collect($evaluation)->values()->all();
        return $va['0'];
    }

    /**
     * This function used to enable dispute option
     *
     * @return array()
     */
    public function allowDispute(EvaluationsAllowDisputeRequest $request)
    {
        if(Auth::user()->roles->pluck('id')[0] == '1')
        {
            $updateResponse = Evaluation::where('id', $request->eval_id)->where('allow_dispute', 'False')->where('status_id', 3)->update(['allow_dispute'=>'True']);
            $this->metricsArray[] = array('MetricName'=>"evaluation/allowdispute");
            if($updateResponse)
            {
                $data['message'] = "Dispute option enabled successfully";
                return response($data, 200);
            }
            else
            {
                $data['errors'] = ["allow_dispute"=>["Enabling dispute failed / invalid evaluation"]];
                return response($data, 422);
            }
        }
        else
            return response(array("message"=>"This action is unauthorized"), 403);
    }

    public function updateAcknowledgement(Request $request){
        if($request->type == 'acknowledge'){
            $ack = 'Acknowledged';
        }else{
            $ack = 'Disputed';
        }
        $updateResponse = Evaluation::where('id', $request->id)->update(['status' => $ack, 'acknowledged_by' => Auth::user()->dim_user_id, 'comment' => $request->comment, 'comment_created' => date('Y-m-d H:i:s')]);
        $data['message'] = 'Evaluation Acknowledged'; 
        return response($data, 200);
    }

    public function surveyres(Request $request){
        $evaluations = Evaluation::select('id as responseid', 'evaluation_form_id', 'employee_id', 'call_id', 'final_score as finalscore', 'created_at', 'updated_at', 'status', 'acknowledged_by', 'status', 'created_by', 'agent_name', 'disposition', 'phone_number')->with(['evaluation_form' => function($query){
            $query->select('id', 'title');
            }, 'createduser' => function($query){
                $query->select('user_id', 'name');
             }, 'acknowledgeduser' => function($query){
                $query->select('user_id', 'name');
            }, 'user' => function($query){
                $query->select('user_id', 'name');
            }]);
        $evaluations->where('status', '=', $request->status);
        $user = Auth::user();
        $role = $user->roles->pluck('name');
        if($role['0'] == 'Agent'){
            $evaluations->where('employee_id', '=', $request->agent_id);    
        }
        $data['evaluations'] = $evaluations->get();
        $data['count'] = $this->getSurveyrescount($request);
        return $data;
    }

    public function getSurveyrescount($request){
        $evaluations = Evaluation::where('status', '=', $request->status);
        $user = Auth::user();
        $role = $user->roles->pluck('name');
        if($role['0'] == 'Agent'){
            $evaluations->where('employee_id', '=', $request->agent_id);    
        }
        return $evaluations->count();
    }
    //Get Agent List
    public function agentslist(){    
        $employees = Employee::select('name as employee_name', 'user_id as eid');
                    if (Auth::user()->hasRole('Supervisor')) {
                        $employees = $employees->where('supervisor_user_id', Auth::user()->dim_user_id)
                                                ->join('evaluations', 'evaluations.employee_id', '=', 'portal_user.user_id');
                    }
                    else if(Auth::user()->hasRole('QA Specialist')) {
                        $employees = $employees->join('evaluations', function($join)
                        {
                            $join->on( 'evaluations.employee_id', '=', 'portal_user.user_id'); //
                            $join->where('evaluations.created_by', '=', Auth::user()->user_id);
                            
                        });
                    }else{
                        $employees = $employees->join('evaluations', 'evaluations.employee_id', '=', 'portal_user.user_id');
                    }
                    $employees = $employees->distinct()->orderBy('name', 'ASC')->get();
        return  $employees;
    }

    /**
     * This function save evaluation score.
     * The evaluation status must be in 'Accepted' and 'In Progress' status.
    **/
    public function evaluationScore($eval_id, EvaluationScoreStoreRequest $request)
    {
        $evaluation_score_start = microtime(true);
        try {
            $evaluation_find_start = microtime(true);
            $allow_score = config("enum.allow_entity_to_process.allow_score");
            $evaluationStatusMaster = EvaluationStatusMaster::whereIn('status', $allow_score[0])->pluck('id');
            Log::Info(print_r($evaluationStatusMaster, true));
            $evaluation = Evaluation::where('id', $eval_id)
                ->whereIn('status_id', $evaluationStatusMaster)
                ->first();
            $this->metricsArray[] = array(
                'MetricName'=>"evaluation/score:checkEvaluation",
                "Timestamp"=>$this->get_elapsed_time($evaluation_find_start)
            );
            if ($evaluation) {
                $evaluation_data['final_score'] = $request->final_score;
                $evaluation_data['ratings'] = $request->ratings;
                $evaluation_data['group_score'] = $request->group_score;
                if ($evaluation->update($evaluation_data)) {
                    $delete_answers_start = microtime(true);
                    Answer::with('comments')->where('evaluation_id', $eval_id)->delete();
                    $this->metricsArray[] = array(
                        'MetricName'=>"evaluation/score:deleteAnswers",
                        "Timestamp"=>$this->get_elapsed_time($delete_answers_start)
                    );
                    $answers = json_decode($request->answers, true);
                    foreach ($answers as $answer) {
                        $storedata['evaluation_id'] = $eval_id;
                        $storedata['question_id'] = $answer['questionid'];
                        $storedata['answer_text'] = $answer['answertext'];
                        $storedata['value'] = $answer['score'];
                        $save = Answer::Create($storedata);
                        if (isset($answer['body']) && $save->id) {
                            $comment = new Comment(['body' => $answer['body'],'created_by' => Auth::user()->user_id]);
                            $answer = Answer::find($save->id);
                            $comment = $answer->comments()->save($comment);
                        }
                    }
                    $this->metricsArray[] = array(
                        'MetricName'=>"evaluation/score",
                        "Timestamp"=>$this->get_elapsed_time($evaluation_score_start)
                    );
                    $data['message'] = 'Evaluation updated successfully';
                    return response($data, 200);
                } else {
                    $this->metricsArray[] = array(
                        'MetricName'=>"evaluation/score",
                        "Timestamp"=>$this->get_elapsed_time($evaluation_score_start)
                    );
                    $data['message'] = 'Evaluation update failed';
                    return response($data, 422);
                }
            } else {
                $this->metricsArray[] = array(
                    'MetricName'=>"evaluation/score",
                    "Timestamp"=>$this->get_elapsed_time($evaluation_score_start)
                );
                $data['message'] = 'Unprocessable entity';
                return response($data, 422);
            }
        } catch (Exception $e) {
            $this->metricsArray[] = array('MetricName'=>"evaluation/score", "Timestamp"=>time(), "Value"=>$this->get_elapsed_time($evaluation_score_start));
            $data['message'] = 'Evaluation update failed' . $e->getMessage(); 
            return response($data, 422);
        }
    }
    
    /**
     * This API gets invoked when the QA accepts to start the evaluation for a call.
     * This endpoint insert evaluation details irrespective of call id.
     * Return basic details of call evaluation
     */
    public function acceptEvaluation(CallEvaluationPostRequest $request) 
    {
        $request->evaluation_form_id = !empty($request->evaluation_form_id) ? $request->evaluation_form_id : 0;
        $request->dependent_fields = $request->dependent_fields ?? [];
        try {
            $evaluationrecord = Evaluation::select('id')->where('call_id', $request->call_id)->where('evaluation_form_id', $request->evaluation_form_id)->first();
            if (!$evaluationrecord) {
                $evaluated_by = User::select('name')->where('user_id', $request->assigned_to)->first();
                $call = Call::with('employee')
                    ->select('extension', 'segstop', 'customer_num', 'disposition', 'agent_id', 'lead_id', 'unique_id', 'customer_num', 'skill_campaign','state')
                    ->where('ucid', $request->call_id)->first();
                //Get skill details: joins used to avoid wrong skill id
                $sql = "SELECT campaign_skills.id, campaign_groups.type AS campaign_group_type FROM campaign_skills
                JOIN campaign_groups ON campaign_groups.id=campaign_skills.group_id
                JOIN evaluation_form_campaign_mappings FMapping ON ((FMapping.type='group' AND FMapping.type_id=campaign_groups.id) OR (FMapping.type='skill' AND FMapping.type_id=campaign_skills.id))
                WHERE campaign_skills.name='$call->skill_campaign'
                ORDER BY FIELD(FMapping.form_id, $request->evaluation_form_id) DESC
                LIMIT 1";
                $campaignSkill = DB::select(DB::raw($sql));
                $campaignSkill = $campaignSkill[0];
                //headers
                $headers = [
                    "Supervisor" => ["name" => "Supervisor", "type" => "text", "mandatory" => true, "disabled" => true, "value" => isset($call['employee']['supervisor']['manager_name'])?$call['employee']['supervisor']['manager_name']:''],
                    "Agent Name" => ["name" => "Agent Name", "type" => "text", "mandatory" => true, "disabled" => true, "value" => $call['employee']['name']],
                    "Call Date"  => ["name" => "Call Date", "type"=> "text", "disabled" => true, "value" => $call['segstop']]
                ];
                if(in_array("customer_num", $request->dependent_fields))
                {
                    $headers["Phone Ext"]   = ["name" => "Phone Ext", "type" => "text", "mandatory" => true, "disabled" => true, "value" => $call['extension']];
                    $headers["Phone Number"]= ["name" => "Phone Number", "type" => "text", "disabled" => true, "value" => $call['customer_num']];
                }
                if(in_array("lead_id", $request->dependent_fields))
                    $headers["Case Number"] = ["name" => "Case Number", "type" => "text", "disabled" => true, "value" => $call['lead_id']];
                if(in_array("unique_id", $request->dependent_fields))
                    $headers["MRR Number"] = ["name" => "MRR Number", "type" => "text", "disabled" => true, "value" => $call['unique_id']];
		if(in_array("state", $request->dependent_fields))
                    $headers["State"] = ["name" => "State", "type" => "text", "disabled" => true, "value" => $call['state']];
                $headers["Site"]          = ["name" => "Site", "type" => "text", "disabled" => true, "value" => $call['employee']['vendor']];
                $headers["Disposition"]   = ["name" => "Disposition", "type" => "text", "disabled" => true, "value" => $call['disposition']];
                $headers["Evaluated By"]  = ["name" => "Evaluated By", "type" => "text", "disabled" => true, "value" => $evaluated_by['name']];
                $headers["Skill Campaign"]= ["name" => "Skill Campaign", "type" => "text", "disabled" => true, "value" => $call['skill_campaign']];
                $headers_json = json_encode(array_values($headers));
                
                DB::beginTransaction();
                $storedata['agent_id'] = $call['agent_id'];
                $storedata['lead_id'] = $call['lead_id'];
                $storedata['unique_id'] = $call['unique_id'];
                $storedata['call_id'] = $request->call_id;
                $storedata['assigned_to'] = $request->assigned_to;
                $storedata['headers'] = $headers_json;
                $storedata['status_id'] = 1;
                $storedata['created_by'] = $request->assigned_to;
                $storedata['evaluation_form_id'] = $request->evaluation_form_id;
                $storedata['allow_dispute'] = "True";
                $storedata['agent_name'] = $call['employee']['name'];
                $storedata['disposition'] = $call['disposition'];
                $storedata['phone_number'] = $call['customer_num'];
		$storedata['state'] = $call['state'];
                if ($campaignSkill) {
                    $storedata['skill_campaign_id'] = $campaignSkill->id;
                }
                $save = Evaluation::Create($storedata);
               
                if ($save->id) {
                    $evaluationform = EvaluationForm::select('id', 'title','type')->where('id', $save->evaluation_form_id)->first();
                    $save->setAttribute('evaluation_form', $evaluationform);
                    $data['evaluation'] = $save->setAttribute('allowed_transition', app(EvaluationSearchController::class)->allowedTransitions($save->evaluation_form_id, $save->status_id));
                    DB::commit();
                    return response($data, 200);
                } else {
                    DB::rollback();
                    return response()->json(['message' => 'Unable to create evaluation'], 422);
                }
            } else {
                return response()->json(['message' => 'Evaluation already exist'], 422);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Something Went Wrong. '.$e->getMessage()], 400);
        }
    }

    /**
     * This function save evaluation status and actions.
     * The evaluation status verified based on the Transition.
    **/
    public function evaluationTransition($eval_id, $transition_id, EvaluationTransitionStoreRequest $request)
    {
        try {
            $dataStore = $request->input();
            $statuslist = config('enum.allow_entity_to_process.allow_comment');
            $statusMaster = EvaluationStatusMaster::whereIn('status', $statuslist[0])->pluck('id');
            $evaluation = Evaluation::where('id', $eval_id)->first();
            $evaluationForm = EvaluationForm::find($evaluation->evaluation_form_id);
            $workflow = EvaluationWorkflows::workflow($evaluationForm->workflow_id);
            $transition = $workflow[$transition_id];
            if (!in_array($transition['PickUpStatus'], $statusMaster->toArray())) {
                unset($dataStore['comment_id']);
            }
            /*Evaluation Status Audits */
            $statusAudit = EvaluationStatusAudit::addStatusAudit($eval_id, $dataStore, $statusMaster->toArray());
            if ($statusAudit) {
                /* Update Status in evaluation */
                $evaluation->status_id = $dataStore['status_id'];
                $evaluation->save();
                $data['eval_id'] = $eval_id;
                $data['action'] = 'evaluation';
                if ($dataStore['status_id'] == 6) { /* 6 :	Dispute Accepted */
                    /* Reevaluation : Create new reevaluation record */
                    $evaluationArray = $evaluation->toArray();
                    $evaluationArray['predecessor_id'] = $evaluationArray['id'];
                    unset($evaluationArray['id']);
                    unset($evaluationArray['created_at']);
                    unset($evaluationArray['updated_at']);
                    unset($evaluationArray['deleted_at']);
                    $evaluationArray['status_id'] = 8;
                    $reevaluation = Evaluation::create($evaluationArray);
                    $old_answers = Answer::where('evaluation_id', $evaluationArray['predecessor_id'])->get();
                    foreach ($old_answers as $old_answer) {
                        $storedata = [];
                        $storedata['evaluation_id'] = $reevaluation['id'];
                        $storedata['question_id'] = $old_answer->question_id;
                        $storedata['answer_text'] = $old_answer->answer_text;
                        $storedata['value'] = $old_answer->value;
                        $old_comment = Comment::where('commentable_type', 'App\Answer')->where('commentable_id', $old_answer->id)->first();
                        $save = Answer::Create($storedata);
                        if ($save->id && $old_comment && isset($old_comment->body)) {
                            $comment = new Comment(['body' => $old_comment->body,'created_by' => $old_comment->created_by]);
                            $answer = Answer::find($save->id);
                            $comment = $answer->comments()->save($comment);
                        }
                    }
                    /*
                    $old_evaluationStatusAudits = EvaluationStatusAudit::where('evaluation_id', $evaluationArray['predecessor_id'])->get();
                    foreach ($old_evaluationStatusAudits as $old_evaluationStatusAudit) {
                        $evaluationStatusAudit = [];
                        $evaluationStatusAudit = $old_evaluationStatusAudit->toArray();
                        $evaluationStatusAudit['evaluation_id'] = $reevaluation['id'];
                        EvaluationStatusAudit::create($evaluationStatusAudit);
                    }
                    */
                    //$reevaluation['headers'] = json_decode($reevaluation['headers'], true);
                    //$reevaluation['group_score'] = json_decode($reevaluation['group_score'], true);
                    //$reevaluation['ratings'] = json_decode($reevaluation['ratings'], true);
                    $data['eval_id'] = $reevaluation['id'];
                    $data['action'] = 'reevaluation';
                }
                $data['message'] = 'Evaluation updated successfully';
                return response($data, 200);
            }
            $data['message'] = 'Evaluation update failed ';
            return response($data, 422);
        } catch (Ecxception $e) {
            $data['message'] = 'Evaluation update failed ' . $e->getMessage();
            return response($data, 422);
        }
    }
    /**
     * This API stores the severity and disposition code for the evaluation.
     */
    public function evaluationDisposition(EvaluationDispositionPostRequest $request) {
        try 
        {
            $allow_evaluation_disposition = config("enum.allow_entity_to_process.allow_evaluation_disposition");
            $evaluationStatusMaster = EvaluationStatusMaster::whereIn('status', $allow_evaluation_disposition[0])->pluck('id');
            $evaluation = Evaluation::where('id', $request->eval_id)->whereIn('status_id', $evaluationStatusMaster)->first();
            if($evaluation)
            {
                $evaluationdispostionrecord = EvaluationDispositions::select('id')->where('evaluations_id', $request->eval_id)->first();
                if(!$evaluationdispostionrecord)
                {
                    DB::beginTransaction();
                    $storedata['evaluations_id'] = $request->eval_id;
                    $storedata['severity_id'] = $request->severityid;
                    $storedata['evaluation_disposition_definitions_id'] = $request->evaluationdispositionid;
                    $storedata['created_by'] = Auth::user()->user_id;
                    $storedata['created_at'] = date('Y-m-d H:i:s');     
                    $save = EvaluationDispositions::Create($storedata);
                    
                    if($save->id)
                    {
                        $data['message'] = 'Dispostion details added successfully'; 
                        DB::commit();
                        return response($data, 200); 
                    }
                    else
                    {
                        DB::rollback();
                        return response()->json(['message' => 'Unable to add evaluation dispostion'], 422);
                    }
                }
                else
                {
                    return response()->json(['message' => 'Evaluation dispostion already exist'], 422);
                }
            }
            else
            {
                return response()->json(['message' => 'Create disposition is not allowed in this state'], 422);
            }
        } 
        catch (\Exception $e) 
        {
            DB::rollback();
            return response()->json(['message' => 'Something Went Wrong. '.$e->getMessage()], 400);
        }
    }
     /**
     * This API will provide an ability to assign QA specialist for a reevaluation.
     * QA Manager will have the capability to assign the user.
     */
    public function assignQASpecialist(AssignQASpecialistPostRequest $request) {
        try 
        {
            $user = User::select('user_id')->where('user_id', $request->user_id)->first();
            if ($user) 
            {
                if ($user->hasRole(['QA Specialist'])) 
                {
                    $allow_revaluation_assignment = config("enum.allow_entity_to_process.allow_revaluation_assignment");
                    $evaluationStatusMaster = EvaluationStatusMaster::whereIn('status', $allow_revaluation_assignment[0])->pluck('id');
                    $evaluation = Evaluation::where('id', $request->eval_id)->whereIn('status_id', $evaluationStatusMaster)->first();
                    if($evaluation)
                    {
                            DB::beginTransaction();
                            $evaluation->assigned_to = $request->user_id;
                            if($save = $evaluation->save())
                            {
                                $data['message'] = 'QA Specialist assigned successfully'; 
                                DB::commit();
                                return response($data, 200); 
                            }
                            else
                            {
                                DB::rollback();
                                return response()->json(['message' => 'QA Specialist assignment failed'], 422);
                            }
                    }
                    else
                    {
                        return response()->json(['message' => 'Revaluation assignment is not allowed in this state'], 200);
                    }
                }
                else
                {
                    return response()->json(['message' => 'Invalid QA Specialist Id'], 422);
                }
            }
            else
            {
                return response()->json(['message' => 'Unprocessable entity'], 422);
            }
            
        } 
        catch (\Exception $e) 
        {
            DB::rollback();
            return response()->json(['message' => 'Something Went Wrong. '.$e->getMessage()], 400);
        }
    }
    public function __destruct() {
        //Log the captured information
        if (count($this->metricsArray)) {
            try{
                //Log to CloudWatch
                $cw_response = $this->putMetricData($this->metricsArray);
            } catch (\Exception $e) {
                //print_r($e);
            }
        }
    }

}
