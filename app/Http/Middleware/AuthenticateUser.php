<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Extensions\JWTGuard;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use JWTAuth;
use App\User;
use App\Employee;
use App\NonEmployeeUser;
use App\Client;
use Cookie;
use Symfony\Component\HttpFoundation\IpUtils;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Exception;
use DB;
use App\UserSystemMapping;
use Cache;

class AuthenticateUser
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guard = 'api')
    {

        $token = $request->bearerToken();

        if ($token) {
            try {

                JWTAuth::parseToken()->checkOrFail(); // check Token expired or not

                $user = Auth::guard($guard)->attempt($token, false);

                if (!$user) {
                    return response()->json(['error' => 'Something went wrong trying to set the user instance'], 401);
                } else {
                    $userInfo = json_decode($user);

                    $authedUser = Auth::user(); // Get Auth user information

                    if (!$authedUser) {
                        $this->createOrUpdateUser($userInfo, $token); // Create or Update User based on authed info
                    }
                }
            } catch (TokenExpiredException $e) {
                return response()->json(['error' => 'Token expired'], 498);
            } catch (JWTException $e) {
                return response()->json(['error' => 'Permisson denied'], 401);
            }
        } else {

            if (request()->headers->get('referer')) {
                $url = parse_url(request()->headers->get('referer'));

                $port = isset($url['port']) ? (':' . $url['port']) : '';

                $destinationUrl = $url['scheme'] . '://' . $url['host'] . $port . '/login';
            } else {
                $destinationUrl = $request->fullUrl();
            }

            /*$destinationUrl = request()->headers->get('referer')?(
                strpos(request()->headers->get('referer'),'login')?request()->headers->get('referer'):request()->headers->get('referer').'login'):$request->fullUrl();
            */
            $destinationUrl = explode('?', $destinationUrl);

            //$clientIp = $request->ip();
            $clientIp =  $this->getIp();

            $ipvalidate = $this->ipRange($clientIp);

            // Need to setup ENUM for internal IP addresses and redirect appropirately
            $authConfig = config('auth.authentication');
            return redirect()->away($authConfig['portal']['url'] . '/authorize?redirect_uri=' . $destinationUrl[0] . '&client_id=' . $authConfig['portal']['clientid']);
        }

        return $next($request);
    }

    /**
     * Validate client ip
     * Validate IP Range using symfony HTTP foundation
     * @param client ip from $request->ip()
     * @return boolean
     **/

    public function ipRange($clientIp)
    {
        $clientIp = ip2long($clientIp);
        $ipRanges = explode(",", config("auth.ip_wl"));
        foreach ($ipRanges as $ipRange) {
            $ranges = explode("-", $ipRange);
            if (count($ranges) == 1) {
                $ranges[1] = $ranges[0];
            }
            $startIp = ip2long($ranges[0]);
            $endIp = ip2long($ranges[1]);
            if ($clientIp >= $startIp && $clientIp <= $endIp)
                return true;
        }
        return false;
    }

    /**
     * Create or Update user
     * Build user model
     * @param $userinfo - Object
     * @param $token - JWT token
     * @return
     **/
    public function createOrUpdateUser($userInfo, $token)
    {
        try {

            if (isset($userInfo->eid)) { // Create Employee Model
                $userid = $userInfo->eid;
                $username = $userInfo->employee_name;
                $email = $userInfo->email;
                $dim_user_id = $userInfo->dim_user_id;
                $site = $userInfo->site;
                $userType = 'employee';
                $vendor = 'FPS';
            } else { // Create Client Model
                $claims = JWTAuth::getJWTProvider()->decode($token);
                $userid = $userInfo->id;
                $username = $userInfo->first_name . ' ' . $userInfo->last_name;
                $email = $userInfo->email;
                $dim_user_id = $claims['dim_user_id'][0];
                $site = $claims['site_id'];
                $suerpvisorid = isset($claims['manager_id'][0]) ? $claims['manager_id'][0] : "";
                $userType = 'client';
                //$vendor = Client::where('id',$userInfo->site_id)->first()->client_name;
            }

            if (User::find($userid)) {
                $user = User::with('roles')->find($userid);
                if($user->site != $site){//Site cache key remove     
                    $cache_key = 'site_'.$user->user_id;
                    Cache::forget($cache_key);
                }
            } else {
                $validGroup = JWTAuth::getJWTProvider()->decode($token);

                if (!$validGroup['groups']) {
                    return response()->json(['message' => 'Unauthorized action'], 401);
                }
                $user = new User();
            }

            //$dim = DB::table('dw.dim_user')->where('user_id', $dim_user_id)->first();
            $user->user_id = $userid;
            $user->name = $username;
            $user->email = $email;
            $user->dim_user_id = $dim_user_id;
            $user->site = $site;
            $user->suerpvisorid = $suerpvisorid;

            /*
            if (isset($dim->usr_grp) && !empty($dim->usr_grp)) {
                $user->usr_grp = $dim->usr_grp;
            } else {
                Log::emergency('dim user table , usr_grp is empty');
            }
            */

            /*if (!empty($site)) {
                $client = Client::where('client_name', $site)->first();
                $data = [];
                if (!$client) {
                    $client = new Client;
                    $client->client_name = $dim->vendor;
                    $client->site_id = $site;
                    $client->save();
                }
            }*/
            //$user->user_type = $userType;
            //$user->last_login = Carbon::now();
            //$user->vendor = $vendor;    
             
            $user->save();
            $checkuser['user_id'] = $userid;
            $this->setSystemMapping($token, $checkuser);
            $this->setRolesbyClaims($token, $checkuser); // Set roles

            $user = User::with('roles')->find($userid);

            Auth::setUser($user); // Set Authed user

        } catch (\Exception $e) {
            // abort(401, 'Unauthorized action.');
            Log::emergency($e);
            return response()->json(['message' => 'Unauthorized action'], 401);
        }
    }

    function setSystemMapping($token, $user)
    {
        $claims = JWTAuth::getJWTProvider()->decode($token);
        if (isset($claims['dim_user_id']) && is_array($claims['dim_user_id'])) {
            UserSystemMapping::where('user_id', $user['user_id'])->delete();
            foreach ($claims['dim_user_id'] as $systemUserId) {
                $systemMapping = new UserSystemMapping();
                $systemMapping->user_id = $user['user_id'];
                $systemMapping->system_user_id = $systemUserId;
                $systemMapping->save();
            }
        }
    }

    /**
     * Assign Role to user based on Groups in claims
     *
     * @return
     **/
    private function setRolesbyClaims($token, $user)
    {
        $claims = JWTAuth::getJWTProvider()->decode($token);
        if ($claims['groups']) {
            $role = $claims['groups'];
            $checkuser = User::where('user_id', $user['user_id'])->first();
            if ($checkuser && !$checkuser->hasRole($role)) {
                $roleval = Role::where('name', $role)->first();
                if ($roleval) { //Existing role
                    $roles = DB::table('model_has_roles')->where('model_id', $user['user_id'])->get();
                    if (empty($roles->count())) {
                        $checkuser->assignRole($roleval->id);
                    } else {
                        if (DB::table('model_has_roles')->where('model_id', $user['user_id'])->delete()) {
                            Log::Info("Deleted model_has_roles");
                            if ($checkuser->assignRole($roleval->id)) {
                                Log::Info("Successfully assigned to Role");
                            } else {
                                Log::Info("Failed to assign Role");
                            }
                        } else {
                            Log::Info("Not Deleted model_has_roles");
                        }
                    }
                }
                else{ //Create new role
                    $newrole = new Role();
                    $newrole->name = $claims['groups'];
                    $newrole->guard_name ='web';
                    $newrole->created_at = date('Y-m-d H:i:s');
                    $newrole->updated_at = date('Y-m-d H:i:s');
                    if($newrole->save()){
                        DB::table('model_has_roles')->where('model_id', $user['user_id'])->delete();//Delete model has role 
                        $checkuser->assignRole($newrole->id);
                        //Assign default calsearch permission
                        $role = Role::where('id', $newrole->id)->first();                        
                        $permission = Permission::where('page', 'callsearch')->first();
                        $role->syncPermissions($permission);
                    }
                }
            }
        }else {
            return response()->json(['message' => 'Unauthorized action'], 401);
        }
    }

    /**
     * Get real IP
     *
     * @return string
     **/

    public function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }
    }
}
