<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SurveyRequest extends FormRequest
{
    protected $rules = [
        'surveyid' => 'integer',
        'employeeid' => 'integer',
        'callid' => 'string',
        'obscore' => 'string',
        'finalscore' => 'string'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->rules;
        if($this->isMethod('POST')){
            $rules['surveyid'] = 'required |'.$rules['surveyid'];
            $rules['employeeid'] = 'required |'.$rules['employeeid'];
            $rules['callid'] = 'required |'.$rules['callid'];
            $rules['obscore'] = 'required |'.$rules['obscore'];
            $rules['finalscore'] = 'required |'.$rules['finalscore'];
        }
    }

    public function messages(){
        return [
            'surveyid.required' => 'A surveyid is required',
            'surveyid.integer' => 'A surveyid is not a integer',
            'employeeid.required' => 'A employeeid is required',
            'employeeid.integer' => 'A employeeid is not a integer',
            'callid.required' => 'A callid is required',
            'callid.string' => 'A callid is not a string',
            'obscore.required' => 'A obscore is required',
            'obscore.string' => 'A obscore is not a string',
            'finalscore.required' => 'A finalscore is required',
            'finalscore.string' => 'A finalscore is not a string'
        ];
    }
}
