<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Auth;

class ProfileSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $data = parent::all();
        if (isset($data['userprofile']) && is_array($data['userprofile'])) {
            $data['userprofile'] = collect($data['userprofile'])->toJson();
        }
        $data = json_decode($data['userprofile'], true);
        $authuserrole = Auth::user()->roles->pluck('name')[0]; //Get Auth user role name
        $authuserroleId = Auth::user()->roles->pluck('id')[0]; //Get Auth user role name
        // dd($authuserroleId);
        $authuserid = Auth::user()->user_id; //Get Auth user id
        if (($authuserrole == 'Agent')) {
            if (count($data) > 1) {
                return false;
            } else {
                foreach ($data as $key => $value) { 
                    if ((isset($value['role_id'])) && ($value['role_id'] != $authuserroleId)) {
                        return false;
                    } else if (isset($value['user_id']) && ($value['user_id'] != $authuserid)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Get the request input that apply to the requst.
     *
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all();
        if (isset($data['userprofile']) && is_array($data['userprofile'])) {
            $data['userprofile'] = collect($data['userprofile'])->toJson();
        }
        $data = json_decode($data['userprofile'], true);
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = parent::all();
        if (isset($data['userprofile']) && is_array($data['userprofile'])) {
            $data['userprofile'] = collect($data['userprofile'])->toJson();
        }
        $data = json_decode($data['userprofile'], true);
        $rules = [];
        foreach ($data as $key => $value) {
            $rules[$key . ".user_id"] = 'required|exists:users,user_id';
            $rules[$key . ".role_id"] = 'required|exists:roles,id|integer';
            $rules[$key . ".email"] = 'sometimes|required|email|max:255|unique:mysql.users,email,' . $value['user_id'] . ',user_id|unique:mysql_nonemployee.users,email,' . $value['user_id'] . ',id';
            $rules[$key . ".unite"] = 'sometimes|required|unique:users,unite,' . $value['user_id'] . ',user_id';
        }
        return $rules;
    }
    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }
}
