<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;

class EvaluationDispositionPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function all($keys = null)
    {
        $data = parent::all();
        $data['eval_id'] = $this->route('eval_id');
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            "eval_id"                 => "required|exists:evaluations,id",
            "severityid"              => "required|exists:evaluation_disposition_definitions,severity",
            "evaluationdispositionid" => "required|exists:evaluation_disposition_definitions,id",
        ];
    }

    public function messages(){
        return [
            "eval_id.required" => "Evaluation Id is required",
            "eval_id.exists"    => "Invalid Evaluation Id",
            "severityid.required"    => "Severity Id is required",
            "severityid.exists"    => "Invalid Severity Id",
            "evaluationdispositionid.required"    => "Evaluation Disposition Id is required",
            "evaluationdispositionid.exists"    => "Invalid Evaluation Disposition Id",
        ];
    }

    /**
    * Handle a failed validation attempt.
    *
    * @param \Illuminate\Contracts\Validation\Validator $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }

}