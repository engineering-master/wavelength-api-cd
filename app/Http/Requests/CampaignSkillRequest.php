<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Auth;
use Log;
class CampaignSkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
     /**
      * Get the request input that apply to the requst.
      *
      * @return array
      */
    public function all($keys = null)
    {
        $data = parent::all();
        if($this->isMethod('POST')) {
                $data['group_id'] = $this->route('group_id');
        }
        elseif($this->isMethod('PUT')) {
                $data['id'] = $this->route('id');
        }
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'name' => 'required|string|max:50|unique:campaign_skills,name',
            'description' => 'max:100',
        ];
        if($this->isMethod('POST')) {
            $rule['group_id'] = 'required|exists:campaign_groups,id';
        }
        else{
            $rule['id'] = 'required|exists:campaign_skills';
            $rule['name'] = 'sometimes|required|string|max:50|unique:campaign_skills,name,'.$this->route('id');
        }
        return $rule;
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'description.string' => 'A description is required'
        ];
    }
    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }
}
