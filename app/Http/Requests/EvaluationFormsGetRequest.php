<?php
namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;

class EvaluationFormsGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "form_id"   => "numeric|not_in:0",
            "callid"    => "string|not_in:0|regex:/^[\w-]*$/", //alpha numeric
        ];
    }

    public function messages(){
        return [
            "form_id.string"    => "The form id is invalid",
            "form_id.not_in"    => "The form id is invalid",
            "callid.string"     => "The call id is invalid",
            "callid.not_in"     => "The call id is invalid",
            "callid.regex"      => "The call id is invalid",
        ];
    }
    
    /**
    * Handle a failed validation attempt.
    *
    * @param \Illuminate\Contracts\Validation\Validator $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }

}