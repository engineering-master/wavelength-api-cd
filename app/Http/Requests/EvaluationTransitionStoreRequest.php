<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use App\EvaluationWorkflows;
use App\EvaluationStatusMaster;
use App\EvaluationStatusAudit;
use App\Evaluation;
use App\Comment;
use App\EvaluationForm;
use Spatie\Permission\Models\Role;
use Log;
use Auth;

class EvaluationTransitionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $data = parent::all();
        $data['eval_id'] = $this->route('eval_id');
        $data['transition_id'] = $this->route('id');
        if (isset($data['transition_id']) && !empty($data['transition_id'])) {
            Log::Info("Transition ID");
            $evaluation = Evaluation::find($data['eval_id']);
            if ($evaluation) {
                $evaluationForm = EvaluationForm::find($evaluation->evaluation_form_id);
                $workflow = EvaluationWorkflows::workflow($evaluationForm->workflow_id);
                if (isset($workflow[$data['transition_id']])) {
                    Log::Info("Workflow Transition ID");
                    $transition = $workflow[$data['transition_id']];
                    if (in_array($data['status_id'], $transition['DropStatus'])) {
                        Log::Info("Workflow Transition Status ID");
                        if ($evaluation->status_id == $transition['PickUpStatus']) {
                            Log::Info("Evaluation Workflow Transition Status ID");
                            $roles = Role::whereIn('id', $transition['Roles'])->pluck('name');
                            foreach ($roles as $role) {
                                if (Auth::user()->hasRole($role)) {
                                    Log::Info("Role Evaluation Workflow Transition Status ID");
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        /* Delete Comment */
        if (isset($data['comment_id'])) {
            $comment = Comment::where('id', $data['comment_id'])->where('commentable_id', $data['eval_id'])
                ->where('commentable_type', 'App\Evaluation')
                ->where('created_by', Auth::user()->user_id)->first();
            if ($comment) {
                $evaluationStatusAudit = EvaluationStatusAudit::where('evaluation_id', $data['eval_id'])
                    ->where('comment_id', $data['comment_id'])->first();
                if (!$evaluationStatusAudit) {
                    Comment::where('id', $data['comment_id'])->delete();
                }
            }
        }
        return false;
    }

    /**
     * Get the request input that apply to the requst.
     *
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all();
        $data['eval_id'] = $this->route('eval_id');
        $data['transition_id'] = $this->route('id');
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data = parent::all();
        $data['transition_id'] = $this->route('id');
        $statuslist = config('enum.allow_entity_to_process.allow_comment');
        $statusMaster = EvaluationStatusMaster::whereIn('status', $statuslist[0])->pluck('id');
        $rules = [
            'status_id' => 'required|integer|exists:evaluation_status_masters,id'
        ];
        $evaluation = Evaluation::find($this->route('eval_id'));
        $evaluationForm = EvaluationForm::find($evaluation->evaluation_form_id);
        $workflow = EvaluationWorkflows::workflow($evaluationForm->workflow_id);
        $transition = $workflow[$data['transition_id']];
        if ($transition['PickUpStatus'] != $data['status_id'] && in_array($transition['PickUpStatus'], $statusMaster->toArray())) {
            $rules['comment_id'] = 'required|integer|exists:comments,id';
        }
        return $rules;
    }

    /**
     * Get the validation messages that apply to the validation.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'status_id.required' => 'Status ID is required!',
            'status_id.integer' => 'Invalid Status ID.',
            'status_id.exists' => 'Invalid Status ID.',
            'comment_id.required' => 'Comment ID is required!',
            'comment_id.integer' => 'Invalid Comment ID.',
            'comment_id.exists' => 'Invalid Comment ID.',
        ];
    }
    
    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }
}
