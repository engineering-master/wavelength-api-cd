<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Auth;

class CampaignMapGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the request input that apply to the requst.
     *
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all();
        $data['type'] = $this->route('type');
        $data['type_id'] = $this->route('type_id');
        $data['id'] = $this->route('id');
        return $data;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule =[
            'id' => 'required|exists:evaluation_forms,id',
            'type' => 'in:group,skill',
            'type_id' => 'required'
        ];
        if($this->route('type') == 'skill'){
            $rule['type_id'] = 'exists:campaign_skills,id';
        }
        elseif($this->route('type') == 'group'){
            $rule['type_id'] = 'exists:campaign_groups,id';
        }
        return $rule;
    }

    public function messages()
    {
        $message = [
            'id.required' => 'Enter a valid ID',
            'type.string' => 'Enter a valid type'
        ];
        if($this->route('type') == 'skill'){
            $message['type_id.exists'] = 'please enter valid skill id';
        } 
        elseif ($this->route('type') == 'group') {
                $message['type_id.exists'] = 'please enter valid group id';
             }  
        return $message;
    }
    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }
}
