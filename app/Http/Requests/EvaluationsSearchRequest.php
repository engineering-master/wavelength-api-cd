<?php
namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;

class EvaluationsSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            "eval_id"        => "integer",
            "call_id"        => "string|regex:/^[a-zA-Z0-9]+$/|max:50",
            "assigned_to"    => "integer",
            "agent_ids"      => "string",
            "from_date"      => "required_with:to_date|date_format:Y-m-d",
            "to_date"        => "required_with:from_date|date_format:Y-m-d|after_or_equal:from_date",
            "status_id"      => "string|regex:/^[0-9,]+$/|max:25",
            "evaluation_form_id"=> "integer",
            "severity_id"    => "integer",
            "evaluation_disposition_id"=>"integer",
            "customer_number"=>"numeric|digits_between:0,13",
            "disposition_code"=> "string",
            "minscore"       => "integer",
            "maxscore"       => "integer",
            "sortordercolumn"=> "required_with:sortorder|string",
            "sortorder"      => "required_with:sortordercolumn|string",
            "skip"           => "integer",
            "take"           => "integer"
        ];
    }

    public function messages(){
        return [
            "eval_id.integer"       => "Given evaluation id is invalid",
            "call_id.string"        => "Given call id is invalid",
            "call_id.regex"         => "Given call id is invalid",
            "assigned_to.integer"   => "Given assigned to is invalid",
            "agent_ids.string"      => "Given Agent Id(s) is invalid",
            "from_date.date_format" => "Given from-date is not in 'Y-m-d' format",
            "to_date.date_format"   => "Given to-date is not 'Y-m-d' format",
            "status_id.string"      => "Given status id(s) is invalid",
            "status_id.regex"       => "Given status id(s) is invalid",
            "evaluation_form_id.integer" => "Given evaluation form id is invalid",
            "severity_id.integer"   => "Given severity id is invalid",
            "evaluation_disposition_id.integer"=> "Given evaluation disposition id is invalid",
            "customer_number.numeric"=> "Given customer number is invalid",
            "customer_number.digits_between"  => "Given customer number must be within 13 digits",
            "disposition_code.string"=> "Given disposition(s) is invalid",
            "minscore.integer"      => "Given minscore is invalid",
            "maxscore.integer"      => "Given maxscore is invalid",
            "sortordercolumn.string"=> "Given sort order column is invalid",
            "sortorder.string"      => "Given sort order is invalid",
            "skip.integer"          => "Given skip value is invalid",
            "take.integer"          => "Given take value is invalid"
        ];
    }

    /**
    * Handle a failed validation attempt.
    *
    * @param \Illuminate\Contracts\Validation\Validator $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }

}