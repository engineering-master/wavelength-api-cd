<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Auth;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $data = parent::all();
        $authuserrole=Auth::user()->roles->pluck('name')[0];//Get Auth user role name
        if(($authuserrole == 'Agent') && ($data['role_id'])){
            return false;
        }
        return true;
    }
  /**
     */
     // Setup the validation of route parameters
     public function all($keys = null)
     {
         $data = parent::all(); 
         $data['id'] = $this->route('id');
         return $data;
     }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'  => 'required|integer', //  url parameter
            'status'=>'required|String|in:Accept,Reject',
        ];
    }
      /**
     * Get the validation messages that apply to the validation.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'unite.unique' => 'Unite id has already been taken.',
        ];
    }
       /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }
}
