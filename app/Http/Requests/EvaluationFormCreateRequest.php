<?php
namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;

class EvaluationFormCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            //form
            "title"        => "required|string|min:2|max:100",
            "description"  => "string",
            "headers"      => "required|string",
            "ratings"      => "required|string",
            "version"      => "required|string|max:10",
            //groups
            "questiongroups"=> "required",
            "questiongroups.*.groupname"        => "required|string|min:2|max:50",
            "questiongroups.*.groupdescription" => "required|string|max:500",
            "questiongroups.*.groupmode"        => "required|string|max:500",
            "questiongroups.*.percentage"       => "required|numeric|min:-1|max:100",
            //questions
            "questiongroups.*.questions"                    => "required",
            "questiongroups.*.questions.*.questiontitle"    => "required|string|min:2|max:50",
            "questiongroups.*.questions.*.questiontype"     => "required|string|min:2|max:50",
            "questiongroups.*.questions.*.rawvalues"        => "max:500",
            "questiongroups.*.questions.*.showcommentbox"   => "required|numeric|in:0,1",
            "questiongroups.*.questions.*.commentmandatory" => "required|numeric|in:0,1",
            "questiongroups.*.questions.*.weightage"        => "required|numeric|between:0,999999.9999",
            "questiongroups.*.questions.*.score"            => "required",
            "questiongroups.*.questions.*.options"          => "required",
            //subgroups
            //"questiongroups.*.questions.*.subgroups"                => "required",
            "questiongroups.*.questions.*.subgroups.*.questiontitle"=> "required|string|min:2|max:50",
            "questiongroups.*.questions.*.subgroups.*.questiontype" => "required|string|min:2|max:50",
            "questiongroups.*.questions.*.subgroups.*.showcommentbox"=> "required|numeric|in:0,1",
            "questiongroups.*.questions.*.subgroups.*.commentmandatory"=> "required|numeric|in:0,1",
            "questiongroups.*.questions.*.subgroups.*.weightage"=> "required|numeric|between:0,999999.9999",
            "questiongroups.*.questions.*.subgroups.*.score"=> "required",
            "questiongroups.*.questions.*.subgroups.*.options"=> "required",
        ];
    }

    public function messages(){
        return [
            //form
            "title.required"    => "Title is required",
            "title.string"      => "The title is not valid string",
            "title.min"         => "The title must be >=2 characters in length",
            "title.max"         => "The title must be <=100 characters in length",
            "description.string"=> "The description is not valid string",
            "headers.string"    => "The headers is not valid string",
            "ratings.required"  => "Ratings is required",
            "ratings.string"    => "The ratings is not valid string",
            "version.required"  => "Version  is required",
            "version.string"    => "The version is not valid string",
            "version.max"       => "The version must be <=10 characters in length",
            //groups
            "questiongroups.required"             => "Question groups are required",
            "questiongroups.*.groupname.required" => "Group name is required",
            "questiongroups.*.groupname.string"   => "The Group name is not valid string",
            "questiongroups.*.groupname.min"      => "The Group name must be >=2 characters in length",
            "questiongroups.*.groupname.max"      => "The Group name must be <=50 characters in length",
            "questiongroups.*.groupdescription.required"=> "Group description is required",
            "questiongroups.*.groupdescription.string"  => "The Group description is not valid string",
            "questiongroups.*.groupdescription.max"     => "The Group description must be <=500 characters in length",
            "questiongroups.*.groupmode.required"       => "Group mode is required",
            "questiongroups.*.groupmode.string"         => "The Group mode is not valid string",
            "questiongroups.*.groupmode.max"            => "The Group mode must be <=500 characters in length",
            "questiongroups.*.percentage.required"      => "Percentage is required",
            "questiongroups.*.percentage.numeric"       => "The Percentage is not valid number",
            "questiongroups.*.percentage.min"           => "The Percentage must be >=0",
            "questiongroups.*.percentage.max"           => "The Percentage must be <=100",
            //questions
            "questiongroups.*.questions.required"                 => "Questions are required",
            "questiongroups.*.questions.*.questiontitle.required" => "Question title is required",
            "questiongroups.*.questions.*.questiontitle.string" => "The Question title is not valid string",
            "questiongroups.*.questions.*.questiontitle.min"    => "The Question title must be >=2 characters in length",
            "questiongroups.*.questions.*.questiontitle.max"    => "The Question title must be <=50 characters in length",
            "questiongroups.*.questions.*.questiontype.required"=> "Question type is required",
            "questiongroups.*.questions.*.questiontype.string"  => "The Question type is not valid string",
            "questiongroups.*.questions.*.questiontype.min"     => "The Question type must be >=2 characters in length",
            "questiongroups.*.questions.*.questiontype.max"     => "The Question type must be <=50 characters in length",
            "questiongroups.*.questions.*.rawvalues.max"        => "The Raw values must be <=500 characters in length",
            "questiongroups.*.questions.*.showcommentbox.required" => "Show comment box is required",
            "questiongroups.*.questions.*.showcommentbox.numeric"  => "The Show comment box is not a valid number",
            "questiongroups.*.questions.*.showcommentbox.in"       => "The Show comment box value must be 0 or 1",
            "questiongroups.*.questions.*.weightage.required"      => "Weightage is required",
            "questiongroups.*.questions.*.weightage.numeric"       => "Weightage is not a valid number",
            "questiongroups.*.questions.*.weightage.between"       => "Weightage value must be between 0 and 999999.9999",
            "questiongroups.*.questions.*.score.required"          => "Score are required",
            "questiongroups.*.questions.*.options.required"        => "The Options are required",
        ];
    }
    
    /**
    * Handle a failed validation attempt.
    *
    * @param \Illuminate\Contracts\Validation\Validator $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }

}