<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;

class CommentsViewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     // Setup the validation of route parameters
    public function all($keys = null)
    {
        $data = parent::all(); 
        $data['id'] = $this->route('id');
        $data['type'] = $this->route('type');
        return $data;
    }
  public function rules()
    {
        $list =implode( ",",config('enum.comment_type'));
        return  [
            'id'  => 'integer', //  url parameter
            'type'=>'in:'.$list //  url parameter
        ];
    }
    public function messages(){
        return [
            'id.integer' => 'The id must be an integer'

        ];
    }
   
    /**
    * Handle a failed validation attempt.
    *
    * @param \Illuminate\Contracts\Validation\Validator $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    protected function failedValidation(Validator $validator)
    {
    $errors = (new ValidationException($validator))->errors();
    throw new HttpResponseException(
    response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
    );
    }
}
