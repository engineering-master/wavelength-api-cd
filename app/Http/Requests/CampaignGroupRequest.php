<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Auth;

class CampaignGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the request input that apply to the requst.
     *
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all();
        if($this->isMethod('PUT')) {
                $data['id'] = $this->route('id');
        }
        return $data;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule =[
            'name' => 'required|string|max:50|unique:campaign_groups,name',
            'description' => 'max:100'
           
        ];
        if($this->isMethod('PUT')) {
            $rule['name'] = 'sometimes|required|string|max:50|unique:campaign_groups,name,'.$this->route('id');
            $rule['id'] = 'required|exists:campaign_groups,id';
        }
        return $rule;
    }

    public function messages()
    {
        return [
            'name.required' => 'A name is required',
            'description.max' => 'description should be max of 100',
            'id.exists' => 'Invalid Group Id'
        ];
    }
    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }
}
