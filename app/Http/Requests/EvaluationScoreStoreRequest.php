<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use App\Evaluation;
use Auth;

class EvaluationScoreStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $evaluation = Evaluation::find($this->route('eval_id'));
        if ($evaluation && (Auth::user()->user_id == $evaluation->assigned_to || Auth::user()->hasRole('Administrator'))) {
            return true;
        }
        return false;
    }

    /**
     * Get the request input that apply to the requst.
     *
     * @return array
     */
    public function all($keys = null)
    {
        $data = parent::all();
        if (isset($data['group_score']) && is_array($data['group_score'])) {
            $data['group_score'] = collect($data['group_score'])->toJson();
        }
        if (isset($data['answers']) && is_array($data['answers'])) {
            $data['answers'] = collect($data['answers'])->toJson();
        }
        $data['eval_id'] = $this->route('eval_id');
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'eval_id' => 'required|integer',
            'group_score' => 'required|json',
            'final_score' => 'required|numeric|between:0,100',
            'answers' => 'required|json',
        ];
    }

    /**
     * Get the validation messages that apply to the validation.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'eval_id.required' => 'Evaluation ID is required!',
            'eval_id.integer' => 'Invalid evaluation ID.',
            'group_score.required' => 'Group score is required!',
            'group_score.json' => 'Group score is must be a valid JSON!',
            'final_score.required' => 'Final score is required!',
            'final_score.integer' => 'The final score must be an integer.',
            'answers.required' => 'Answers is required!',
            'answers.json' => 'Answers is must be a valid JSON!',
        ];
    }
        
    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
