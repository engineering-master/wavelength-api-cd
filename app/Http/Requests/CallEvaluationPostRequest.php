<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Auth;

class CallEvaluationPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $data = parent::all();
        if (Auth::user()->user_id == $data['assigned_to'] && Auth::user()->hasRole(['QA Manager','QA Specialist','Administrator'])) {
            return true;
        }
        return false;
    }

    public function all($keys = null)
    {
        $data = parent::all();
        $data['call_id'] = $this->route('call_id');
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            "call_id"            => "required|exists:fact_call_details,ucid",
            "evaluation_form_id" => "required|exists:evaluation_forms,id",
            "assigned_to"        => "required|exists:users,user_id",
        ];
    }

    public function messages(){
        return [
            "call_id.required" => "Call Id is required",
            "call_id.exists"    => "Invalid call Id",
            "evaluation_form_id.required"    => "Evaluation Form Id is required",
            "evaluation_form_id.exists"    => "Invalid evaluation form Id",
            "assigned_to.required"    => "Assigned To is required",
            "assigned_to.exists"    => "Invalid assigned user",
        ];
    }

    /**
    * Handle a failed validation attempt.
    *
    * @param \Illuminate\Contracts\Validation\Validator $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }
}