<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;

class UserListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

  public function rules()
    {
        return  [
            'role_id'  => 'required|string', //  url parameter
        ];
    }
    public function messages(){
        return [
            'role_id.required' => 'The role id is required'

        ];
    }
   
    /**
    * Handle a failed validation attempt.
    *
    * @param \Illuminate\Contracts\Validation\Validator $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    protected function failedValidation(Validator $validator)
    {
    $errors = (new ValidationException($validator))->errors();
    throw new HttpResponseException(
    response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
    );
    }
}
