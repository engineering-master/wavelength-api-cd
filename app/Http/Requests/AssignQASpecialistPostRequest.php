<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;
use Auth;

class AssignQASpecialistPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->hasRole(['QA Manager','Administrator'])) {
            return true;
        }
        return false;
    }

    public function all($keys = null)
    {
        $data = parent::all();
        $data['eval_id'] = $this->route('eval_id');
        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return  [
            "eval_id"              => "required|exists:evaluations,id",
            "user_id"              => "required|integer",
        ];
    }

    public function messages(){
        return [
            "eval_id.required" => "Evaluation Id is required",
            "eval_id.exists"    => "Invalid Evaluation Id",
            "user_id.required"    => "QA Specialist Id is required",
            "user_id.integer"    => "QA Specialist Id must be an integer",
        ];
    }

    /**
    * Handle a failed validation attempt.
    *
    * @param \Illuminate\Contracts\Validation\Validator $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    protected function failedValidation(Validator $validator)
    {
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
        );
    }

}