<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;

class WorkflowRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|string',//Form Data
            'start_date'=>'required|date',
            'wf_details'=>'required|json'

        ];
    } 
    public function all($keys = null)
    {
        $data = parent::all(); 
        if (isset($data['wf_details']) && is_array($data['wf_details'])) {
        $data['wf_details'] = collect($data['wf_details'])->toJson();
        }
        return $data;
    }
    public function messages(){
        return [
            'description.required' => 'Description is required',
            'wf_details.json' => 'Workflow details is must be an json',
            'wf_details.required' => 'Workflow details is required',

        ];
    }
   
    /**
    * Handle a failed validation attempt.
    *
    * @param \Illuminate\Contracts\Validation\Validator $validator
    * @return void
    *
    * @throws \Illuminate\Validation\ValidationException
    */
    protected function failedValidation(Validator $validator)
    {
    $errors = (new ValidationException($validator))->errors();
    throw new HttpResponseException(
    response()->json(['errors' => $errors], JsonResponse::HTTP_BAD_REQUEST)
    );
    }
}