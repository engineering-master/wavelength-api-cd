<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    protected $rules = [
        'body' => 'string',
        'callid' => 'string'
    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = $this->rules;
        if($this->isMethod('POST')){
            $rules['body'] = 'required |'.$rules['body'];
            $rules['callid'] = 'required |'.$rules['callid'];
        }
        return $rules;
    }

    public function messages(){
        return [
            'body.required' => 'A body is required',
            'body.string' => 'A body is not a string',
            'callid.required' => 'A callid is required',
            'callid.string' => 'A callid is not a string'
        ];
    }
}
