<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use HasRoles;
    
    protected $primaryKey = 'user_id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','unite','site'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Comments(){
        return $this->hasMany('Comment');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return ['user' => ['id' => $this->id]];
    }
    public function modelrole()
    {
    return $this->belongsTo('App\ModelHasRole','user_id','model_id');
    }
    public function updateProfile(){
        return $this->belongsTo('App\UserProfileAudit','user_id','user_id')->select('id','user_id','prev_role_id','change_role_id','prev_email','change_email','prev_unite','change_unite','created_at','status')->whereNull('status');
    }
}
