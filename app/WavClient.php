<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WavClient extends Model
{
  protected $table = 'wav_clients';
}
