<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use Log;

class EvaluationStatusAudit extends Model
{
    public $timestamps = false;

    protected $fillable = [
       'evaluation_id', 'status_id', 'comment_id', 'created_by', 'created_at'
    ];

    public static function addStatusAudit($evaluation_id, $statusAudit, $allow_status)
    {
        if (isset($statusAudit['comment_id'])) {
            $comment = Comment::where('id', $statusAudit['comment_id'])->where('commentable_id', $evaluation_id)
                ->where('commentable_type', 'App\Evaluation')->where('created_by', Auth::user()->user_id)->first();
            if (!$comment) {
                return false;
            }
        }

        $evaluationStatusAudit = new EvaluationStatusAudit;
        $evaluationStatusAudit->evaluation_id = $evaluation_id;
        $evaluationStatusAudit->status_id = $statusAudit['status_id'];
        $evaluationStatusAudit->created_at = now();
        if (isset($statusAudit['comment_id'])) {
            $evaluationStatusAudit->comment_id = $statusAudit['comment_id'];
        }
        $evaluationStatusAudit->created_by = Auth::user()->user_id;
        if ($evaluationStatusAudit->save()) {
            return $evaluationStatusAudit->id;
        }
        return false;
    }
    public function status()
    {
        return $this->belongsTo('App\EvaluationStatusMaster', 'status_id', 'id');
    }
}
