<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // 'App\Console\Commands\SampleCron',
        'App\Console\Commands\UpdateCampaignStatus',
        'App\Console\Commands\DispositionCommand',
        'App\Console\Commands\UpdateEvaluationsStatus',
        'App\Console\Commands\EvaluationNotificationCommand',
        'App\Console\Commands\UpdateEvaluationCampaignSkill',
        'App\Console\Commands\AddDimUsersIntoUsers',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        //$schedule->command('generate:dispositions')
        //    ->daily();
        $schedule->command('Evaluations:AutoUpdateDisputeStatus')
            ->everyThirtyMinutes();
        $schedule->command('evaluations:notification')
            ->daily();
        $schedule->command('generate:users')
            ->daily();
        $schedule->command('users:update_wavelength_hierarchies')
            ->everyTenMinutes()->unlessBetween('23:00', '23:59')->unlessBetween('00:00', '1:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
