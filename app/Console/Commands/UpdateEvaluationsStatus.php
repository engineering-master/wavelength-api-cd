<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\Evaluation;
use App\EvaluationForm;
use App\Http\Controllers\EvaluationSearchController;

class UpdateEvaluationsStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Evaluations:AutoUpdateDisputeStatus';

    /**
     * The console command description.
     *
     * @var highlight_string(str)
     */
    protected $description = 'Update evaluations status based on the workflow cycle';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * This function used to update evaluations status
     * @return mixed
     */
    public function handle()
    {
        $status_id = 3; //Yet to be acknowledge

        $EvaluationSearchController = app()->make('App\Http\Controllers\EvaluationSearchController');
        $evaluation_forms = EvaluationForm::where('active', "=", "1")->get();
        foreach($evaluation_forms as $evaluation_form){
            $evaluation_form_id = $evaluation_form['id'];
            $transition = app()->call([$EvaluationSearchController, 'allowedTransitions'], ["evaluation_form_id"=>$evaluation_form_id, "status_id"=>$status_id, "action"=>"internalCommand"]);
            if(isset($transition['EscalationPolicy']['Threshold']))
            {
                //Get business threshold date wihtout H:i:s
                $thresholdDateTime = $this->getBusinessDay(date("Y-m-d"), "-", $transition['EscalationPolicy']['Threshold']);
               
                $disabledDisputeResponse = Evaluation::where('evaluation_form_id', $evaluation_form_id)
                ->where('status_id', $status_id)
                ->where('updated_at', '<', $thresholdDateTime)
                ->where('allow_dispute', '=', 'True')
                ->update(['allow_dispute'=>'False']);
            }
        }
        return;
    }

    function getBusinessDay($startDate, $skipToNextOrPrevious="-", $skipBusinessDays=0, $skipHoliday='')
    {
        //Using -/+ weekdays excludes weekends
        $newDate = date('Y-m-d', strtotime("{$startDate} $skipToNextOrPrevious{$skipBusinessDays} weekdays"));
        $startDateSplitted = explode(" ", $startDate);
        $newDate = isset($startDateSplitted[1]) ? $newDate." ".$startDateSplitted[1] : $newDate;
        $holidayTS = strtotime($skipHoliday);

        //If holiday falls between start date and new date, then account for it
        if($holidayTS>=strtotime($startDate) && $holidayTS<=strtotime($newDate)) {
            //Check if the holiday falls on a working day
            $holiday = date('w', $holidayTS);
            //Holiday falls on a working day, add/sub an extra working day
            if($holiday != 0 && $holiday != 6)
                $newDate = date('Y-m-d H:i:s', strtotime("{$newDate} $skipToNextOrPrevious 1 weekdays"));
        }

        return $newDate;
    }

}