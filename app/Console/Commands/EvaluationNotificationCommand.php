<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\ProcessRocketNotification;
use Notification;
use App\Notifications\EvaluationEscalationNotification;
use DateTime;
use Log;
use App\User;
use App\Role;
use App\Evaluation;
use App\EvaluationForm;
use App\EvaluationWorkflows;
use App\EvaluationStatusMaster;
use App\Http\Controllers\EvaluationSearchController;

class EvaluationNotificationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'evaluations:notification';

    /**
     * The console command description.
     *
     * @var highlight_string(str)
     */
    protected $description = 'Notify evaluations on before threshold end.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * This function used to update evaluations status
     * @return mixed
     */
    public function handle()
    {
        $evaluationForms = EvaluationForm::where('active', "1")->get();
        foreach ($evaluationForms as $evaluationForm) {
            $evaluationWorkflows = EvaluationWorkflows::find($evaluationForm->workflow_id);
            $transitions = json_decode($evaluationWorkflows->wf_details, true);
            foreach ($transitions as $transition) {
                if (!empty(isset($transition['EscalationPolicy']['Threshold'])) && is_numeric($transition['EscalationPolicy']['Threshold'])) {
                    $threshold = $transition['EscalationPolicy']['Threshold'] - 1;
                    //Get business threshold date wihtout H:i:s
                    $thresholdDateTime = $this->getBusinessDay(date("Y-m-d"), "-", $threshold);
                    $from_date = $thresholdDateTime . " 00:00:00"; 
                    $to_date = $thresholdDateTime . " 23:59:59";
                    $notifyEvaluations = Evaluation::where('evaluation_form_id', $evaluationForm->id)
                        ->where('status_id', $transition['PickUpStatus'])
                        //->where('updated_at', "<", $thresholdDateTime)
                        ->whereBetween('updated_at', [$from_date, $to_date])
                        ->get();
                    foreach ($notifyEvaluations as $notifyEvaluation) {
                        //Log::Info(print_r($notifyEvaluation, true));
                        $agent = User::where('dim_user_id', $notifyEvaluation->agent_id)->first();
                        $notifies = [];
                        //$notifyEmail[] = 'arul.manickam@fpsinc.com';
                        //$notifyRocket[] = 'arul.manickam';
                        if ($transition['EscalationPolicy']['NotifyTo']) {
                            $notifyTo = $transition['EscalationPolicy']['NotifyTo'];
                            $rolesNotifyTo = Role::whereIn('id', $notifyTo)->pluck('name');
                            foreach ($rolesNotifyTo as $role) {
                                if ($role == 'Administrator') {
                                    $administrator = User::role('Administrator')->first();
                                    if ($administrator) {
                                        Log::Info("administrator");
                                        //Log::Info(print_r($administrator->toArray(), true));
                                        $notifies[] = $administrator;
                                    }
                                } else if ($role == 'Agent') {
                                    if ($agent) {
                                        Log::Info("agent");
                                        $notifies[] = $agent;
                                    }
                                } else if ($role == 'QA Manager') {
                                    $specialist = User::where('user_id', $notifyEvaluation->assigned_to)->first();
                                    if ($specialist && $specialist->suerpvisorid) {
                                        $manager = User::where('dim_user_id', $specialist->suerpvisorid)->first();
                                        if ($manager) {
                                            Log::Info("manager");
                                            //Log::Info(print_r($manager->toArray(), true));
                                            $notifies[] = $manager;
                                        }
                                    } else {
                                        $manager = User::role('QA Manager')->first();
                                        if ($manager) {
                                            Log::Info("manager");
                                            //Log::Info(print_r($manager->toArray(), true));
                                            $notifies[] = $manager;
                                        }
                                    }
                                } else if ($role == 'QA Specialist') {
                                    $specialist = User::where('user_id', $notifyEvaluation->assigned_to)->first();
                                    if ($specialist) {
                                        Log::Info("specialist");
                                        $notifies[] = $specialist;
                                    }
                                } else if ($role == 'Supervisor') {
                                    if ($agent && $agent->suerpvisorid) {
                                        $supervisor = User::where('dim_user_id', $agent->suerpvisorid)->first();
                                        if ($supervisor) {
                                            Log::Info("supervisor");
                                            //Log::Info(print_r($supervisor->toArray(), true));
                                            $notifies[] = $supervisor;
                                        }
                                    }
                                }
                            }
                        }
                        
                        $evaluationStatusMaster = EvaluationStatusMaster::find($notifyEvaluation->status_id);
                        $request['agent_name'] = $notifyEvaluation->agent_name;
                        $request['target_status'] = $evaluationStatusMaster->status;
                        $request['date'] = $notifyEvaluation->updated_at;
                        $request['eval_id'] = $notifyEvaluation->id;
                        $headers = json_decode($notifyEvaluation->headers, true);
                        $request['supervisor'] = "";
                        foreach ($headers as $header) {
                            if ($header['name'] == "Supervisor") {
                                $request['supervisor'] = $header["value"];
                            }
                        }
                        $request['eval_url'] = "Please login to <a href=". config('app.front_end_url') ." style='color: #3C8DBC'>Wavelength</a> and Click <a href=". config('app.front_end_url') . "/viewevaluation/" .  $notifyEvaluation->id ." style='color: #3C8DBC'>here</a> to view the evaluation.";
                        $request['type'] = "EvaluationEscalation";
                        if (!empty(count($notifies))) {
                            //Log::Info(print_r($notifies, true));
                            foreach ($notifies as $notify) {
                                if ($notify != null) {
                                    $enable_rocket_notification = config('queue.enable_rocket_notification');
                                    if ($enable_rocket_notification && isset($notify->unite)) {
                                        $request['username'] = $notify->unite;
                                        ProcessRocketNotification::dispatch($request);
                                    }
                                    Notification::send($notify, new EvaluationEscalationNotification($request));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    function getBusinessDay($startDate, $skipToNextOrPrevious="-", $skipBusinessDays=0, $skipHoliday='')
    {
        //Using -/+ weekdays excludes weekends
        $newDate = date('Y-m-d', strtotime("{$startDate} $skipToNextOrPrevious{$skipBusinessDays} weekdays"));
        $startDateSplitted = explode(" ", $startDate);
        $newDate = isset($startDateSplitted[1]) ? $newDate." ".$startDateSplitted[1] : $newDate;
        $holidayTS = strtotime($skipHoliday);

        //If holiday falls between start date and new date, then account for it
        if ($holidayTS>=strtotime($startDate) && $holidayTS<=strtotime($newDate)) {
            //Check if the holiday falls on a working day
            $holiday = date('w', $holidayTS);
            //Holiday falls on a working day, add/sub an extra working day
            if ($holiday != 0 && $holiday != 6) {
                $newDate = date('Y-m-d H:i:s', strtotime("{$newDate} $skipToNextOrPrevious 1 weekdays"));
            }
        }
        return $newDate;
    }

}