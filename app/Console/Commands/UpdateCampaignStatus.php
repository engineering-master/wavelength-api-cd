<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\Campaign;

class UpdateCampaignStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:status';

    /**
     * The console command description.
     *
     * @var highlight_string(str)
     */
    protected $description = 'Update campaign status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Get today date
        $dateToday = date('Y-m-d');
        $today = new DateTime($dateToday);
        //finding yesterday date
        $yesterdayDate = $today->modify('-1 day');
        $yesterday = $yesterdayDate->format('Y-m-d');
        //update status as completed with yestrday date as end date
        $status = Campaign::whereDate('end_date', $yesterday)->update(['status' => 'completed']);
        return;
    }
}
