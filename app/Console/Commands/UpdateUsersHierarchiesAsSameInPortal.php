<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Log;
use Artisan;
use DateTime;
use App\User;
use Carbon\Carbon;

class UpdateUsersHierarchiesAsSameInPortal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:update_wavelength_hierarchies';

    /**
     * The console command description.
     *
     * @var highlight_string(str)
     */
    protected $description = 'Update Users Hierarchies As Same In Portal.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * This function used to update evaluations status
     * @return mixed
     */
    public function handle()
    {
        //Portal DB
        $PO_DB = DB::connection('mysql_nonemployee')->getDatabaseName();

        $wp_users = User::select("users.user_id AS wl_user_id", "users.dim_user_id AS wl_dim_user_id", "po_users.id AS po_id", "po_users.username AS po_username", "po_users.manager_id AS po_manager_id", "users.name AS wl_name", "po_users.first_name AS po_first_name", "po_users.last_name AS po_last_name", "users.site AS wl_site", "po_clients.client_name AS po_site", "users.suerpvisorid AS wl_supervisorid", "po_sys_map.system_user_id as po_system_user_id")
        ->join("$PO_DB.users as po_users", "po_users.username","=","users.dim_user_id")
        ->join("$PO_DB.clients as po_clients", "po_clients.id","=","po_users.client_id")
        ->leftJoin("$PO_DB.user_system_mapping AS po_sys_map", "po_sys_map.user_id","=","po_users.manager_id")
        ->where("po_users.updated_at", ">", Carbon::now()->subDays(1))
        ->where(function($query) {
            $query->whereNotNull('po_users.manager_id')->orWhereNotNull('users.suerpvisorid');
        })
        ->get();
        
        $updated_users_count = 0;
        foreach ($wp_users as $wp_user) {
            $wp_user->po_name = ($wp_user->po_first_name ?? "")." ".($wp_user->po_last_name ?? "");
            if ($wp_user->wl_supervisorid!=$wp_user->po_system_user_id || $wp_user->wl_site!=$wp_user->po_site || $wp_user->wl_name!=$wp_user->po_name) {
                $wp_user->po_system_user_id = $wp_user->po_system_user_id ?? null;
                User::where("user_id", "=", "$wp_user->wl_user_id")
                ->update(['suerpvisorid'=>"$wp_user->po_system_user_id", 'site'=>"$wp_user->po_site", 'name'=>"$wp_user->po_name"]);
                Log::Info("Updated wl.user:".json_encode($wp_user));
                $updated_users_count++;
            }
        }
        if($updated_users_count)
            Artisan::call('cache:clear');
    }

}