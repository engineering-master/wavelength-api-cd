<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\User;
use App\Client;
use App\Employee;
use App\NonEmployeeUser;
use App\NonEmployeeUserSystemMapping;
use App\UserSystemMapping;
use DB;

class AddDimUsersIntoUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:users';

    /**
     * The console command description.
     *
     * @var highlight_string(str)
     */
    protected $description = 'Add users from portal_user(instead of dim_user) view';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $create_statment = 'CREATE TEMPORARY TABLE temp_portal_user SELECT * FROM portal_user';
        DB::statement($create_statment);
        $delete_statment = 'DELETE FROM temp_portal_user WHERE user_id IN (select system_user_id FROM user_system_mapping)';
        DB::statement($delete_statment);
        $select_statment = 'SELECT * FROM temp_portal_user';
        $portal_users = DB::select(DB::raw($select_statment));
        foreach ($portal_users as $portal_user) {
            $client_id = Client::where('client_name', $portal_user->vendor)->first();
            $nonEmployeeUser = NonEmployeeUser::where('username', $portal_user->user_id)->first();
            if (!$nonEmployeeUser) {
                $nonEmployeeUser = new NonEmployeeUser();
                if (!$client_id){
                    $client = new Client();
                    $client->client_name = $portal_user->vendor;
                    $client->site_id= 1;
                    $client->active = 1;
                    if($client->save()){
                        $client_id = Client::where('client_name', $portal_user->vendor)->first();
                    }
                }                
                $nonEmployeeUser->client_id = $client_id->id;
                $nonEmployeeUser->username = $portal_user->user_id;
                $nonEmployeeUser->first_name =$portal_user->name;
                $nonEmployeeUser->email = $portal_user->email;
                $nonEmployeeUser->vendor = $portal_user->vendor;
                $nonEmployeeUser->manager_id = $portal_user->supervisor_user_id;
                $nonEmployeeUser->site_id = 1;
                $nonEmployeeUser = $nonEmployeeUser->save();
                $nonEmployeeUser = NonEmployeeUser::where('username', $portal_user->user_id)->first();
            }
            $user_id = $nonEmployeeUser->id;
            
            if ($nonEmployeeUser) {
                $systemMapping = new NonEmployeeUserSystemMapping();
                $systemMapping->user_id = $user_id;
                $systemMapping->system_id = 1;
                $systemMapping->system_user_id = $portal_user->user_id;
                $systemMapping->save();
                $user = User::where('dim_user_id', $portal_user->user_id)->first();
                if (!$user) {
                    $user = new User();
                    $user->user_id = $user_id;
                    $user->name = $portal_user->name;
                    $user->email = $portal_user->email;
                    $user->dim_user_id = $portal_user->user_id;
                    $user->site = $portal_user->vendor;
                    $user->suerpvisorid = $portal_user->supervisor_user_id;
                    $user->usr_grp = $portal_user->usr_grp;
                    $user = $user->save();
                    $user = User::where('dim_user_id', $portal_user->user_id)->first();
                }
                if ($user) {
                    $systemMapping = new UserSystemMapping();
                    $systemMapping->user_id = $user_id;
                    $systemMapping->system_user_id = $portal_user->user_id;
                    $systemMapping->save();
                }
            }
        }
        $drop_statement = 'DROP TEMPORARY TABLE temp_portal_user';
        DB::statement($drop_statement);
    }
}
