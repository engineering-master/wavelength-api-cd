<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateTime;
use App\Evaluation;
use App\Call;
use App\CampaignSkill;

class UpdateEvaluationCampaignSkill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'evaluations:skill {limit} {skip}';

    /**
     * The console command description.
     *
     * @var highlight_string(str)
     */
    protected $description = 'Update campaign skill id in evaluations tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = $this->argument('limit');
        $skip = $this->argument('skip');
        $evaluations = Evaluation::whereNull('skill_campaign_id')
            ->with(
                ['call' => function ($query) {
                    $query->select('ucid', 'skill_campaign')->with(
                        ['campaignSkill' => function ($query) {
                            $query->select('id', 'name');
                        }]
                    );
                }]
            )
            ->select('id', 'call_id', 'skill_campaign_id', 'headers')
            ->skip($skip)->take($limit)->get();

        foreach ($evaluations as $evaluation) {
            $evaluation_input = [];
            $evaluation_input['id'] = $evaluation->id;
            $headers = json_decode($evaluation->headers);
            $is_skill_campaign = "Not Found";
            $skill_campaign = $campaign = [];
            if (!empty(count($headers))) {
                foreach ($headers as $header) {
                    if ($header->name == 'Skill Campaign') {
                        $is_skill_campaign = "Found";
                    }
                }
            }
            if ($is_skill_campaign == "Not Found") {
                $skill_campaign = [
                    'name' => "Skill Campaign",
                    'value' => '',
                    'type' => "text",
                    'disabled' => true
                ];
                if ($evaluation->call) {
                    $skill_campaign['value'] = $evaluation->call->skill_campaign;
                }
                $headers = json_decode(json_encode($headers), true);
                $campaign[] = $skill_campaign;
                $headers = array_merge($headers, $campaign);
            }
            $headers = json_decode(json_encode($headers), true);
            $headers = array_values($headers);
            $headers = json_encode($headers);
            $evaluation_input['headers'] = $headers;
            if ($evaluation->call && $evaluation->call->campaignSkill) {
                $evaluation_input['skill_campaign_id'] = $evaluation->call->campaignSkill->id;
            }
            Evaluation::where('id', $evaluation->id)->update($evaluation_input);
        }
        print_r($evaluations->toArray());
    }
}
