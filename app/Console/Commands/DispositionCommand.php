<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Call;
use App\Disposition;
use DB;

class DispositionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:dispositions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'generate dispositions from fact_call_details view';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Disposition::truncate();
            $statement = 'insert into dispositions select distinct `disposition`, `skill_campaign` from `fact_call_details` where `disposition` is not null and skill_campaign is not null and `disposition` != "(null)"';
            DB::statement($statement);
        } catch (Exception $e) {
            Log::Info("**************** Disposition not inserted start ****************");
            Log::Info($e->getMessage());
            Log::Info("**************** Disposition not inserted end ****************");
        }
    }
}
