<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EvaluationNotification extends Notification
{
    use Queueable;
    private $_details;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->_details = $details;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $evaluation = config('services.notification.evaluation');
        return $evaluation;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown('mail.evaluation.status', [
                'eval_id' => $this->_details['eval_id'],
                'agent_name' => $this->_details['agent_name'],
                'target_status' => $this->_details['target_status'],
                'name' => $this->_details['name'],
                'role' => $this->_details['role'],
                'date' => $this->_details['date'],
                'comment' => $this->_details['comment'],
                'qa_specialist_name' => $this->_details['qa_specialist_name']
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'eval_id' => $this->_details['eval_id'],
            'agent_name' => $this->_details['agent_name'],
            'target_status' => $this->_details['target_status'],
            'name' => $this->_details['name'],
            'role' => $this->_details['role'],
            'date' => $this->_details['date'],
            'comment' => $this->_details['comment'],
            'eval_url' => "Please login to <a href=". config('app.front_end_url') ." style='color: #3C8DBC'>Wavelength</a> and Click <a href=". config('app.front_end_url') . "/viewevaluation/" .  $this->_details['eval_id'] ." style='color: #3C8DBC'>here</a> to view the evaluation.",
            'qa_specialist_name' => $this->_details['qa_specialist_name']
        ];
    }
}
