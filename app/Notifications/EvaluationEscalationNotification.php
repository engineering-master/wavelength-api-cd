<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EvaluationEscalationNotification extends Notification
{
    use Queueable;
    private $_details;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->_details = $details;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        $evaluation = config('services.notification.evaluation');
        return $evaluation;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->markdown('mail.evaluation.escalation', [
                'eval_id' => $this->_details['eval_id'],
                'agent_name' => $this->_details['agent_name'],
                'supervisor' => $this->_details['supervisor'],
                'type' => $this->_details['type'],
                'target_status' => $this->_details['target_status'],
                'date' => $this->_details['date'],
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'eval_id' => $this->_details['eval_id'],
            'agent_name' => $this->_details['agent_name'],
            'supervisor' => $this->_details['supervisor'],
            'type' => $this->_details['type'],
            'target_status' => $this->_details['target_status'],
            'date' => $this->_details['date'],
            'eval_url' => $this->_details['eval_url'],
        ];
    }
}
