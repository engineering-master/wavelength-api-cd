<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuestionGroup extends Model
{
    protected $connection = 'mysql';
    use SoftDeletes;

    protected $fillable = [
        'evaluation_form_id',
        'slug',
        'title',
        'description',
        'groupmode',
        'percentage',
        'order',
    ];

    protected $dates = ['deleted_at'];

    public function evaluation_form()
    {
    	return $this->belongsTo('App\EvaluationForm');
    }

    public function questions()
    {
        return $this->hasMany('App\Question','group_id')->whereNull('parent_question_id')->orderBy('order', 'asc');
    }

    public function scopeFromSurvey($query, EvaluationForm $evaluation_form)
    {
        return $query->where('evaluation_form_id', $evaluation_form->id);
    }
}
