<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Http\Controllers\UserController;
use Log;

class ProcessRocketNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->request['username']) {
            $message = "";
            if (isset($this->request['type']) && $this->request['type'] == "EvaluationEscalation") {
                $message = $this->request['supervisor'];
                $message .= " has 1 more day left to dispute the evaluation of ";
                $message .= $this->request['agent_name'];
                $message .= ". Please login to Wavelength ". config('app.front_end_url');
                $message .= "and click here to view evaluation.";
                $message .= config('app.front_end_url') . "/viewevaluation/" . $this->request['eval_id'];
            } else {
                $message = "The evaluation for ".$this->request['agent_name'];
                $message .= " has been moved to ".$this->request['target_status'];
                $message .= " by ".$this->request['name'].", ".$this->request['role'];
                if (isset($this->request['qa_specialist_name'])) {
                    $message .= " and assigned to QA Specialist ".$this->request['qa_specialist_name'];
                }
                $message .= " on ".date('jS M, Y', strtotime($this->request['date']));
                $message .= ". Please login to Wavelength ". config('app.front_end_url');
                $message .= "and click here to view evaluation.";
                $message .= config('app.front_end_url') . "/viewevaluation/" . $this->request['eval_id'];
                Log::Info($message);
            }
            $request['username'] = $this->request['username'];
            $request['text'] = $message;
            $userController = new UserController;
            $userController->sendRocketMessage($request);
        }
    }
}
