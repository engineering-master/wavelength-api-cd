<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\ProcessRocketNotification;
use Notification;
use App\Notifications\EvaluationNotification;
use App\Evaluation;
use App\Employee;
use App\User;
use App\EvaluationStatusMaster;
use App\EvaluationStatusAudit;
use App\Role;
use App\EvaluationForm;
use App\Comment;
use App\EvaluationWorkflows;
use Mail;
use Log;
use App\Http\Controllers\UserController;

class ProcessEvaluation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $evaluation;
    protected $user_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Evaluation $evaluation, $user_id)
    {
        $this->evaluation = $evaluation;
        $this->user_id = $user_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notifyTo = [];
        $trigger_notification = config('enum.trigger_notification');
        if (isset($trigger_notification[$this->evaluation->status_id])) {
            $notifyTo = $trigger_notification[$this->evaluation->status_id];
        }
        if (!empty(isset($notifyTo))) {
            $agent = User::where('dim_user_id', $this->evaluation->agent_id)->first();
            $rolesNotifyTo = Role::whereIn('id', $notifyTo)->pluck('name');
            $notifies = [];
            //$notifyEmail[] = 'arul.manickam@fpsinc.com';
            //$notifyRocket[] = 'arul.manickam';
            foreach ($rolesNotifyTo as $role) {
                if ($role == 'Administrator') {
                    $administrator = User::role('Administrator')->first();
                    if ($administrator) {
                        Log::Info("administrator");
                        //Log::Info(print_r($administrator->toArray(), true));
                        $notifies[] = $administrator;
                    }
                } else if ($role == 'Agent') {
                    if ($agent) {
                        Log::Info("agent");
                        $notifies[] = $agent;
                    }
                } else if ($role == 'QA Manager') {
                    $specialist = User::where('user_id', $this->evaluation->assigned_to)->first();
                    if ($specialist && $specialist->suerpvisorid) {
                        $manager = User::where('dim_user_id', $specialist->suerpvisorid)->first();
                        if ($manager) {
                            Log::Info("manager");
                            //Log::Info(print_r($manager->toArray(), true));
                            $notifies[] = $manager;
                        }
                    } else {
                        $manager = User::role('QA Manager')->first();
                        if ($manager) {
                            Log::Info("manager");
                            //Log::Info(print_r($manager->toArray(), true));
                            $notifies[] = $manager;
                        }
                    }
                } else if ($role == 'QA Specialist') {
                    $specialist = User::where('user_id', $this->evaluation->assigned_to)->first();
                    if ($specialist) {
                        Log::Info("specialist");
                        $notifies[] = $specialist;
                    }
                } else if ($role == 'Supervisor') {
                    if ($agent && $agent->suerpvisorid) {
                        $supervisor = User::where('dim_user_id', $agent->suerpvisorid)->first();
                        if ($supervisor) {
                            Log::Info("supervisor");
                            //Log::Info(print_r($supervisor->toArray(), true));
                            $notifies[] = $supervisor;
                        }
                    }
                }
            }
            $request['qa_specialist_name'] = '';
            if ($this->evaluation->status_id == 8 ) {
                $oldEvaluation = Evaluation::find($this->evaluation->predecessor_id);
                $specialist = User::where('user_id', $oldEvaluation->assigned_to)->first();
                if ($specialist) {
                    Log::Info("Old specialist");
                    $notifies[] = $specialist;
                }
                $assigned_to = User::where('user_id', $this->evaluation->assigned_to)->first();
                $request['qa_specialist_name'] = $assigned_to->name;
            }
            $evaluationStatusMaster = EvaluationStatusMaster::find($this->evaluation->status_id);
            $evaluationStatusAudit = EvaluationStatusAudit::select('created_by', 'comment_id')
                ->where('evaluation_id', $this->evaluation->id)
                ->where('status_id', $this->evaluation->status_id)->first();
            $request['comment'] = $request['name'] = $request['role'] = '';
            if ($evaluationStatusAudit && $evaluationStatusAudit->comment_id) {
                $comment = Comment::select('body')->where('id', $evaluationStatusAudit->comment_id)->first();
                $request['comment'] = $comment->body;
            }
            $user = User::where('user_id', $this->user_id)->first();
            if ($user) {
                $userRoles = $user->getRoleNames();
                $request['name'] = $user->name;
                $request['role'] = $userRoles[0];
            }
            $request['agent_name'] = $this->evaluation->agent_name;
            $request['target_status'] = $evaluationStatusMaster->status;
            $request['date'] = $this->evaluation->updated_at;
            $request['eval_id'] = $this->evaluation->id;
    
            if (!empty(count($notifies))) {
                foreach ($notifies as $notify) {
                    if ($notify != null) {
                        $enable_rocket_notification = config('queue.enable_rocket_notification');
                        if ($enable_rocket_notification && isset($notify->unite)) {
                            $request['username'] = $notify->unite;
                            ProcessRocketNotification::dispatch($request);
                        }
                        Notification::send($notify, new EvaluationNotification($request));
                    }
                }
            }
        }
    }
}
