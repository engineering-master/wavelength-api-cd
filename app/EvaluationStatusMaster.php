<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluationStatusMaster extends Model
{
    protected $fillable = [
        'status',
        'description'
    ];
}
