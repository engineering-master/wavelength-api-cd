<?php

namespace App\Listeners;

use App\Events\EvaluationEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\Jobs\ProcessEvaluation;
use Auth;

class EvaluationEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EvaluationEvent  $event
     * @return void
     */
    public function handle(EvaluationEvent $event)
    {
        if (Auth::check()) {
            $notifyTo = [];
            $trigger_notification = config('enum.trigger_notification');
            if (isset($trigger_notification[$event->_evaluation->status_id])) {
                $notifyTo = $trigger_notification[$event->_evaluation->status_id];
            }
            if (isset($notifyTo) && !empty(count($notifyTo))) {
                ProcessEvaluation::dispatch($event->_evaluation, Auth::user()->user_id);
            }
        }
    }
}
