<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignSkill extends Model
{
    protected $fillable=['name','description','group_id','created_by','updated_by'];
    /**
     * Get campaign Group
     * 
     * @return Object
     */
    public function campaignGroup()
    {
        return $this->belongsTo('App\CampaignGroup', 'group_id');
    }
}
