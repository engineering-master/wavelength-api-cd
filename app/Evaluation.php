<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $connection = 'mysql';
    protected $fillable = [
        'evaluation_form_id',
        'agent_id',
        'lead_id',
        'unique_id',
        'call_id',
        'ob_score',
        'final_score',
        'ratings',
        'headers',
        'status_id',
        'created_by',
        'acknowledged_by',
        'group_score',
        'comment',
        'comment_created_at',
        'agent_name',
        'disposition',
        'phone_number',
        'agent_id',
        'assigned_to',
        'status_id',
        'allow_dispute',
        'predecessor_id',
        'skill_campaign_id',
	'state'
    ];

    public function answers()
    {
        return $this->hasMany('App\Answer')->with('question', 'comments');
    }

    public function evaluation_form()
    {
        return $this->belongsTo('App\EvaluationForm');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee','eid');
    }

    /**
     * Get call detail with campaign skill
     * 
     * @return Object
     */
    public function call()
    {
        return $this->belongsTo('App\Call', 'call_id', 'ucid');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable')->orderBy('created_at', 'asc' );
    }

    public function user(){
        return $this->hasOne('App\Employee', 'user_id', 'agent_id');
    }
   
    public function createduser(){
        return $this->hasOne('App\User', 'user_id', 'created_by');
    }

    public function acknowledgeduser(){
        return $this->hasOne('App\User', 'dim_user_id', 'acknowledged_by');
    }

    public function evaluationcall(){
        return $this->hasOne('App\Call', 'ucid', 'call_id');
    }

    public function evaluationdisposition()
    {
        return $this->hasOne('App\EvaluationDispositions', 'evaluations_id');
    }

    public function status()
    {
        return $this->belongsTo('App\EvaluationStatusMaster','status_id');
    }

    /**
     * Get campaign skill
     * 
     * @return Object
     */
    public function campaignSkill()
    {
        return $this->belongsTo('App\CampaignSkill', 'skill_campaign_id');
    }
}
