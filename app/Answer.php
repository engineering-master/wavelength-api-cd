<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $connection = 'mysql';
	protected $fillable = [
        'question_id',
        'value',
        'answer_text',
        'evaluation_id'
    ];

    public function question()
    {
        return $this->belongsTo('App\Question')->whereNull('parent_question_id')->with('questiongroup');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable')->orderBy('created_at', 'desc' );
    }
}
