<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignSearch extends Model
{
    protected $table = 'campaign_search';
    protected $fillable = ['column_name', 'column_val', 'column_conditions', 'campaign_id'];
    public $timestamps = true;
}
