<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class CanvassingQAFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Canvassing Quality Form", "description"=>"Canvassing Quality Form", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"7", "usr_grp"=>"CG"]);
        $form_details = [
            //GAINING CLAIMANT'S INTEREST
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"100.00", "groupmode"=>"default", "title"=>"CALL HANDLING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"8.00", "title"=>"CALL INTRODUCTION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"8.00", "title"=>"Did the agent open the call confidently, introduce himself, provide the purpose of the call, and the patient's information accurately to confirm their identity?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100, "No":0, "NA":100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"84.00", "title"=>"DATA GATHERING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q1 - Dates of service. Did the agent ask for the dates when the patient was most recently seen, first seen, and any other dates on file?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q2 - Medical records department contact info. Did the agent ask for the mailing address, email address, and fax number of the medical records department?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"9.00", "title"=>"Q3 - Centralized records. Did the agent ask if the facility's records are centralized?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"9.00", "title"=>"Q4 - Medical retainage. Did the agent ask how long the facility retains records?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q5 - HIPAA release form. Did the agent ask if the facility accepts a generic Hipaa release or a specific one that we'll need to use?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"9.00", "title"=>"Q6 - Facility contact information. Did the agent verify the contact information of the facility? (Provider Name, Provider Phone Number, and Provider Address)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"9.00", "title"=>"Q7 - Facility Representative information. Did the agent ask for the name and department of the representative? "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"9.00", "title"=>"Q8 - Physician's NPI *When a pharmacy identifies that a patient filled a presciption. Did the rep ask for the name and unique identification number of the physician? "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"9.00", "title"=>"Q9 - Medical visits. Did the agent ask if the patient had an ER, outpatient, inpatient visit or other?"],
                               
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100, "No":0, "NA":100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.00", "title"=>"CLAIMANT HANDLING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Active listening. Did the agent actively listen during the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Confident Delivery. Was the agent confident and clear in his/her delivery? (Sounding  Pleasant, Clear delivery, Call Control)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Phone Etiquette. Did the agent follow proper hold, mute when necessary, courtesy close, and avoid interruption?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Responding appropriately. Did the agent address all of the concerns and objections during the call?"],
                               
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100, "No":0, "NA":100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"8.00", "title"=>"AFTER CALL WORK"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"8.00", "title"=>"Notes. Did the agent notes match what transpired in the call?"],
                               
                            ],
                        ],
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"AUTO FAILURE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR - RECORDING VERBIAGE"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Call recording verbiage. Did the agent fail to mention the call recording verbiage?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR - IMPROPER DISPOSITION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Improper disposition. Did the agent fail to dispose of the call appropriately in both Vici and Salesforce?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR - DATA ACCURACY"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Data accuracy. Is the information inaccurate?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR - SKIPPING QUESTIONS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Skipping Questions. Did the agent skip any of the questions?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"ZTP - INAPPROPRIATE CONDUCT"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Inappropriate Conduct. Did the agent fail to display professional behavior during the call?"],
                            ]
                        ],
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}