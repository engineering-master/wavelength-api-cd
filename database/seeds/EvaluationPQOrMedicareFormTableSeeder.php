<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationPQOrMedicareFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Pre-Qualification Quality Form - V6", "description"=>"Pre-Qualification Quality Form - V6", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"5", "usr_grp"=>"CG"]);
        $form_details = [
            //AUTO-FAILURE
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CALL COMPLIANCE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERRORS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep willfully transfer an unqualified claimant to the next step of the qualification process?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep provide misleading information or have possible intent for fraud?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to display professional behavior in the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep dispose the call inappropriately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep disconnect the call outside of the defined policies?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to follow the call avoidance policy?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to follow the call recording verbiage completely?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep speak in vernacular with or without the claimant on the line?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to follow the DNC policy?"],
                            ]
                        ]
                    ]
            ],
            //CLAIMANT HANDLING
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"60.00", "groupmode"=>"default", "title"=>"CLAIMANT HANDLING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"GREETING AND CALL INTRODUCTION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"15.00", "title"=>"Did the rep sound energetic and confident when opening the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep avoid all opportunities for voluntary disqualification?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep set the claimant's expectations properly?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CLAIMANT HANDLING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep address questions/concerns/clarifications on the call accurately and promptly?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep listen actively throughout the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep avoid verbal collision/interrupting the claimant?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep deliver the message clearly?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep sound pleasant and use appropriate tone, pace, volume?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep empathize when necessary?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Was the rep able to convert the call to a valid transfer?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No-lack of rebuttal", "No-Incorrect rebuttal", "No-did not attempt to rebut twice", "NA"]', "score"=>'{"Yes":100,"No-lack of rebuttal":0,"No-Incorrect rebuttal":0,"No-did not attempt to rebut twice":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"REBUTTAL"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of rebuttal", "No-Incorrect rebuttal", "No-did not attempt to rebut twice", "NA"]', "score"=>'{"Yes":100,"No-lack of rebuttal":0,"No-Incorrect rebuttal":0,"No-did not attempt to rebut twice":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"25.00", "title"=>"Did the rep accurately provide rebuttals and tried at least 2x?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CLOSING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep discuss the downsell spiel and/or ended the call with a positive tone?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep follow the hand-off and transfer guidelines?"],
                            ]
                        ],
                    ]
            ],
            //Prequalification / Medicare
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"40.00", "groupmode"=>"default", "title"=>"Prequalification / Medicare"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"PREQUALIFICATION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q1 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (SOCIAL SECURITY BENEFITS)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q2 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (MILITARY)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q3 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (AGE)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q4 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (LONGEVITY)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q5 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (LAST TIME WORKED)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q6 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (WORK TIME)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q7 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (LONGEVITY 2)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q8 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (MEDICAL HISTORY)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q9 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (ALREADY REPRESENTED)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q10 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (US CITIZEN)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q11 - Did the rep ask questions and receive a clear and affirmative answer (main and follow-up questions) before moving to the next question? (SS TAXES)"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"MEDICARE"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No-lack of probing", "No-incorrect probing", "NA"]', "score"=>'{"Yes":100,"No-lack of probing":0,"No-incorrect probing":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"100.00", "title"=>"Did the rep ask the Medicare items completely before transferring to the LIA?"],
                            ]
                        ],
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data)
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}