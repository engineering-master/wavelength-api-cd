<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class CSLegalReferralFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Legal Referral QA Form", "description"=>"Legal Referral QA Form", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"6", "usr_grp"=>"CG"]);
        $form_details = [
            //GAINING CLAIMANT'S INTEREST
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"100.00", "groupmode"=>"default", "title"=>"CALL HANDLING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"40.00", "title"=>"Opening and Purpose of Call"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Did the agent use the correct call opening greeting or verbiage and obtain the name of the person speaking with?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Did the rep mention the purpose of the call?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"40.00", "title"=>"Data Gathering"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Did the rep confirm the claimant's eligibility?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Did the rep check if an attorney is helping the claimant?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"20.00", "title"=>"Closing"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Did the rep end the call properly?"],
                            ],
                        ],
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"0.00", "groupmode"=>"no-score", "title"=>"CRITICAL ERROR"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.00", "title"=>"Critical Errors"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep fail to mention the call recording verbiage?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep fail to follow the TCPA guidelines?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep fail to read the disclaimer in verbatim?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep fail to follow the DNC Policy?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep fail to dispose the call accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep provide misleading information?"],
                            ]
                        ],
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}