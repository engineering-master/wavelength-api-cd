<?php
use Illuminate\Database\Seeder;

use App\User;
use App\Client;
use App\Employee;
use App\NonEmployeeUser;

class DimUsersUpdateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees = Employee::all();
        foreach ($employees as $employee) {
            $user = User::where('dim_user_id', $employee->user_id)->first();
            if ($user) {
                $user->dim_user_id = $employee->user_id;
                $user->site = $employee->vendor;
                $user->suerpvisorid = $employee->supervisor_user_id;
                $user->save();
                $nonEmployeeUser = NonEmployeeUser::find($user->user_id);
                if ($nonEmployeeUser) {
                    $client = Client::where('client_name', $employee->vendor)->first();
                    if ($client) {
                        $nonEmployeeUser->client_id = $client->id;
                    }
                    $nonEmployeeUser->vendor = $employee->vendor;
                    $nonEmployeeUser->manager_id = $employee->supervisor_user_id;
                    $nonEmployeeUser = $nonEmployeeUser->save();
                }
            }
        }
    }
}
