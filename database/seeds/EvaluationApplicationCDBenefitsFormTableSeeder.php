<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationApplicationCDBenefitsFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluationform = EvaluationForm::create(['title' => 'Application CD Benefits Form','description' => 'Application CD Benefits Form','headers' => '[]','ratings' => '[]','version' => '1.0','workflow_id' => '1','active' => '1','created_by' => '1000','type' => '5','usr_grp' => 'CG']);

        $questiongroup1 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'CALL COMPLIANCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup2 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'AUTO-FAILURE','percentage' => '-100.00','groupmode' => 'auto-failure']);
        
        //Question Group 1
        $questionlineitem1 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Call the Customer (Introduction)','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Introduce name and Citizen\'s Disability','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Provide Call Purpose','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Set Proper Call Expectations','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Advise Call Recording Disclosure','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Provide Rebuttal Attempts During Intro (at least 2 times)','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2','comment' => '1','commentmandatory' => '0']);


        $questionlineitem2 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Lead to Qualify','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Ask Q1-Q5",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Correctly Identify Claimant's Program Eligibility",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10','comment' => '1','commentmandatory' => '0']);


        $questionlineitem3 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Acknowledge Concerns and Address Objections','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Handle Hesitations, concerns, and Objections appropriately','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Ask Effective Probing Questions','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Listen attentively and respond clearly','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5','comment' => '1','commentmandatory' => '0']);


        $questionlineitem4 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Interview to Identify Benefits and Health Issues Experience','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Ask and properly document all interview questions','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10','comment' => '1','commentmandatory' => '0']);


        $questionlineitem5 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Make an Offer','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Pitch Pharmacy Verbatim','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Pitch Medicare Verbatim','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Effective Pitch','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '15','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Handoff Process','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '6','comment' => '1','commentmandatory' => '0']);


        $questionlineitem6 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Secure Good Call Experience','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Use appropriate tone, pace, and message delivery','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Show Empathy','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Escalate/De-escalate the call appropriately','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5','comment' => '1','commentmandatory' => '0']);

        //Question Group 2: AUTO-FAILURE  
        $questionlineitem7 = Question::create(['group_id' => $questiongroup2->id,'title' => 'CRITICAL ERRORS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup2->id,'title' => "Disconnect or avoid the call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup2->id,'title' => "Fail to discuss Call Recording Disclosure | TCPA | DNC Spiel / Disposition",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup2->id,'title' => "Falsify claimant's information to submit a sale - Fraud",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup2->id,'title' => "Misrepresentation / Misleading",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup2->id,'title' => "Display unprofessional behavior in the call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup2->id,'title' => "Fail to dispose the call properly",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
    }

}