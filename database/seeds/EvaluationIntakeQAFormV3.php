<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationIntakeQAFormV3 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Intake Quality Form V3", "description"=>"Intake Quality Form V3", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"7", "usr_grp"=>"CG"]);
        $form_details = [
            //GAINING CLAIMANT'S INTEREST
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"100.00", "groupmode"=>"default", "title"=>"CALL HANDLING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"5.00", "title"=>"Opening"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep open the call properly?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"10.00", "title"=>"Nest Steps"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep discuss the CFA and SSN Disclaimer"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"70.00", "title"=>"PQ and Data Gathering"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Did the rep ask for the name of the claimant as it appears on SS card, Confirm the spelling of Mother's maiden Name and Father's full name phonetically?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep confirm the cl's phone number as shown on the dialer and the Alt Contact number?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep ask for an Alternate Contact info?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep Update Work History and Section 6 accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep Update Medications accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep Update Conditions accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep Update Medical Treatment / Testings and Medications accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the rep Update Spouse Info accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep Verify other Personal Information?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"15.00", "title"=>"Closing"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep set proper expecatation on the next steps?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep follow the VA guide when needed?"],
                            ],
                        ],
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"AUTO FAILURE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to discuss the call recording verbiage?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail  to List Doctor’s Name for TSS?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep capture inaccurate information on Esig and input information on the SSA Website?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep purposely not use the PQ guide?"],
                            ]
                        ],
                    ]
            ],
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"4Cs CE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"Conduct"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"ZT-Did the rep provide false information/ Manipulate Stat or  sign up unqualified claimant?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"ZT-Did the rep show unprofessional behavior during the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"ZT-Did the rep fail to follow the DNC Policy?"],
                            ]
                        ],
                    ]
            ],

            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"0.00", "groupmode"=>"no-score", "title"=>"4Cs NON-CE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.00", "title"=>"Connection"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Did the rep  Gain Claimant's Attention"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Did the rep Acknowledge Cl's emotion"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Was the rep's tone of  voice engaging?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Was the rep Casual and Conversational?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.00", "title"=>"Compatibility"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Did the rep provide tailored value prop to Cl's circumstances?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Did the rep showcase FAB?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Was the rep's delivery effective?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Did the rep show genuine interest to help the claimant?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.00", "title"=>"Control"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Did the rep listen actively?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Did the rep Lead the Conversation?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Was the agent easily understood?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement", "NA"]', "score"=>'{"Yes": 0,"No":0, "Needs Improvement": 0, "NA": 0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Did the rep educate the claimant on the next step?"],
                            ],
                        ],
                    ]
            ],


        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}