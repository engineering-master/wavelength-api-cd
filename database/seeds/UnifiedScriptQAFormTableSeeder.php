<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class UnifiedScriptQAFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title" => "Unified Quality Form", "description" => "Unified Quality Form", "headers" => "[]", "ratings" => "[]", "version" => "1.0", "workflow_id" => "1", "active" => "1", "created_by" => "1000", "type" => "3", "usr_grp" => "CG"]);
        $form_details = [
            //GAINING CLAIMANT'S INTEREST
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "100.00", "groupmode" => "default", "title" => "CALL HANDLING"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes": 100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "10.00", "title" => "GAINING CLAIMANT'S INTEREST"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes": 100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10.00", "title" => "Opening - Did the rep open the call promptly and completely (including call introduction)?"],
                        ],
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "65.00", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5.00", "title" => "Q1 SSDI BENEFITS - Are you currently receiving any benefits from Social Security Disability? This includes Social Security Disability or Supplemental Security Income benefits."],

                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "PQ1 - Are you between 53 and 64 Years Old?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "4.00", "title" => "PQ2 - Are you currently out of work due to your health condition?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "4.00", "title" => "PQ3 - Have you worked 5 out of the last 10 years?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "4.00", "title" => "PQ4 - Are you currently represented by a lawyer or advocate for your claim?"],

                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5.00", "title" => "Q2 MILITARY - Were you every in the Military?"],

                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "VA1 - Have you ever gotten a decision from the VA regarding your VA disability status or rating?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "VA2 - Are you currently working? "],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "VA3 - Are you receiving any money from the VA for any service-connected disabilities?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "VA4 - What is your VA rating? (Follow-up questions must be asked when necessary)."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "VA5 - Are you currently being represented by an attorney or an accredited agent for your VA benefits?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "VA6 - Do any of the following apply to you?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "VA7 - Would you like to speak to Coveney Law, LLC about securing additional VA disability compensation or obtaining a higher VA disability rating?"],

                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5.00", "title" => "Q3 MEDICARE - What do you have for health insurance?"],

                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "M1 - Have you changed your medicare advantage plan in the last 3 months?"],

                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5.00", "title" => "Q4 - PHARMACY - How many prescriptions do you have?"],

                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "PH1 - What do you pay for prescriptions?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.00", "title" => "PH2 - Where do you fill your prescriptions?"],
                        ],
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.00", "title" => "EFFECTIVE PRESENTATION"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Active Listening - Did the rep listen actively throughout the call?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Addressing Concern - Did the rep address the claimant's concern/question?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Confident Delivery - Was the agent confident in his/her delivery? (Deliveringthe Message clearly, appropriate tone, pace , volume)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Empathy and Responding Appropriately - Did the rep respond appropriately and/or empathize when necessary?"],
                        ],
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "15.00", "title" => "HANDLING OBJECTIONS"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Acknowledged Reason for Objection - Did the rep acknowledge the reason for objection?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Uncovering underlying reason for Objection - Did the agent uncover the underlying reason for objection (when needed)?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Tailored Value Rebuttal - Did the agent provide a tailored value rebuttal?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Assumptive Transition - Was the agent assumptive in his transition to continue the conversation/sale?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "15.00", "title" => "Objection Handling Process - Did the rep attempt to address the claimant's objection? (Allowed rebuttal State)"],
                        ],
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "10.00", "title" => "CLOSING"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5.00", "title" => "Closing - Did the agent close the call properly?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 100,"No":0, "NA": 100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5.00", "title" => "SSDI Handoff - Did the agent follow the intake handoff correctly?"],
                        ],
                    ],
                ]
            ],
            //CRITICAL ERRORS
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "-100.00", "groupmode" => "auto-failure", "title" => "AUTO FAILURE"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "-100.00", "title" => "CRITICAL ERROR - OPENING"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Opening - Did the rep fail to utilize the Opening spiel in verbatim?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Call Recording Verbiage - Did the rep provide the call recording spiel?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "-100.00", "title" => "CRITICAL ERROR - EFFECTIVE PRESENTATION"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Misleading - Did the rep provide misleading statements that influenced the claimant's decision making?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "-100.00", "title" => "CRITICAL ERROR - HANDLING OBJECTIONS"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "No Rebuttal State Compliance - Did the rep fail comply with the No Rebuttal State regulation?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "-100.00", "title" => "CRITICAL ERROR - IDENTIFYING ELIGIBILITY"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Identifying Eligibility - Did the agent offer the appropriate option?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Invalid Sale - Did the agent transferred an unqualified claimant?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Skipping of questions - Did the agent skip any of the questions?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "-100.00", "title" => "CRITICAL ERROR - IMPROPER DISPOSITION"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Improper Disposition - Did the rep dispose of the call inappropriately (except for DNC)?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "-100.00", "title" => "ZTP"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "VA TCPA - Did the agent follow the VA TCPA verbatim?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Medicare TCPA - Did the agent follow the Medicare TCPA verbatim?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Pharmacy TCPA - Did the agent follow the Pharma TCPA verbatim?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Unprofessional Behavior - The rep fail to display professional behavior?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "DNC Policy - Did the rep fail to comply to DNC policy?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Fraud / Stat Manipulation - Did the rep take deliberate action to improve his scorecard?"],
                        ]
                    ],
                ]
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "default", "title" => "NCE CHECK"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "NCE CHECK"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Is this NCE?"],
                        ]
                    ],

                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Valid", "Invalid", "NA"]', "score" => '{"Valid":100,"Invalid":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "NCE Driver"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Valid", "Invalid", "NA"]', "score" => '{"Valid":100,"Invalid":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Is this agent driven?"],
                        ]
                    ],

                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Social security benifits","Age","Longevity","Worked credits","Already Represented","US Citizen","NA"]', "score" => '{"Social security benifits":0,"Age":0,"Longevity":0,"Worked credits":0,"Already Represented":0,"US Citizen":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Eliminate Reason"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Social security benifits","Age","Longevity","Worked credits","Already Represented","US Citizen","NA"]', "score" => '{"Social security benifits":0,"Age":0,"Longevity":0,"Worked credits":0,"Already Represented":0,"US Citizen":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "What is the eliminate reason?"],
                        ]
                    ],

                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Lack of probing","Incorrect probing","Force transfer","Skipping of questions","Active listening","Verbal collision","Incorrectly asking PQ questions","Interested and answered all questions","NA"]', "score" => '{"Lack of probing":0,"Incorrect probing":0,"Force transfer":0,"Skipping of questions":0,"Active listening":0,"Verbal collision":0,"Incorrectly asking PQ questions":0,"Interested and answered all questions":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Drilldown Reason"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Lack of probing","Incorrect probing","Force transfer","Skipping of questions","Active listening","Verbal collision","Incorrectly asking PQ questions","Interested and answered all questions","NA"]', "score" => '{"Lack of probing":0,"Incorrect probing":0,"Force transfer":0,"Skipping of questions":0,"Active listening":0,"Verbal collision":0,"Incorrectly asking PQ questions":0,"Interested and answered all questions":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "What is the drilldown reason?"],
                        ]
                    ],
                ],
            ],
        ];

        //Do not change this
        foreach ($form_details as $form_detail) {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach ($form_detail["group_data"] as $group_data) {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach ($group_data["questions"] as $question) {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }
    }
}
