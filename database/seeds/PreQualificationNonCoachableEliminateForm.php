<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class PreQualificationNonCoachableEliminateForm extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"PQ Non Coachable Eliminate Form", "description"=>"PreQualification Non Coachable Eliminate Form", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"6", "usr_grp"=>"CG"]);
        $form_details = [
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"0.00", "groupmode"=>"default", "title"=>"COMPLIANCE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'', "score"=>'', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"Intake Eliminate"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'', "score"=>'', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"What was the Eliiminate reason selected by Intake?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"NCE Check"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Was this valid transfer?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Valid", "Invalid"]', "score"=>'{"Valid": 0,"Invalid":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"NCE Driver"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Is this agent driven?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Age","Longevity","Longevity 2","Last time worked","Work Time","Work Credits","Social Security Benifits","Medical History","Post Hearing","Already Represented","Citizen","Income","SS Taxes","CD Client","NA"]', "score"=>'{"Age":0,"Longevity":0,"Longevity 2":0,"Last time worked":0,"Work Time":0,"Work Credits":0,"Social Security Benifits":0,"Medical History":0,"Post Hearing":0,"Already Represented":0,"Citizen":0,"Income":0,"SS Taxes":0,"CD Client":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"Eliminate Reason"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Age","Longevity","Longevity 2","Last time worked","Work Time","Work Credits","Social Security Benifits","Medical History","Post Hearing","Already Represented","Citizen","Income","SS Taxes","CD Client","NA"]', "score"=>'{"Age":0,"Longevity":0,"Longevity 2":0,"Last time worked":0,"Work Time":0,"Work Credits":0,"Social Security Benifits":0,"Medical History":0,"Post Hearing":0,"Already Represented":0,"Citizen":0,"Income":0,"SS Taxes":0,"CD Client":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"What is the eliminate reason?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Lack of Probing","Force Transfer","Incorrect Probing","Incorrectly asking PQ Questions","Skipping PQ Question","Active Listening","Verbal Collision","NA"]', "score"=>'{"Lack of Probing":0, "Force Transfer":0, "Incorrect Probing":0, "Incorrectly asking PQ Questions":0, "Skipping PQ Question":0, "Active Listening":0, "Verbal Collision":0, "NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"Drilldown Reason"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Lack of Probing","Force Transfer","Incorrect Probing","Incorrectly asking PQ Questions","Skipping PQ Question","Active Listening","Verbal Collision","NA"]', "score"=>'{"Lack of Probing":0, "Force Transfer":0, "Incorrect Probing":0, "Incorrectly asking PQ Questions":0, "Skipping PQ Question":0, "Active Listening":0, "Verbal Collision":0, "NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"What is the drilldown reason"],
                            ]
                        ],
                        
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}