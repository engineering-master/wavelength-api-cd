<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RoleHasPermissionTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(EvaluationWorkflowsTableSeeder::class);
        $this->call(EvaluationDispositionDefinitionsTableSeeder::class);
        $this->call(EvaluationStatusMastersTableSeeder::class);
        $this->call(EvaluationFormTableSeeder::class);
        $this->call(CampaignsTableSeeder::class);
        $this->call(UsersDimUserIdTableSeeder::class);
        $this->call(EvaluationPQFormTableSeeder::class);
    }
}
