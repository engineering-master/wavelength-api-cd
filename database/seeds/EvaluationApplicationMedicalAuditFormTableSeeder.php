<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationApplicationMedicalAuditFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluationform = EvaluationForm::create(['title' => 'Application Medical Audit (MACC) Quality Form','description' => 'Application Medical Audit (MACC) Quality Form','headers' => '[]','ratings' => '[]','version' => '1.0','workflow_id' => '1','active' => '1','created_by' => '1000','type' => '2','usr_grp' => 'CG']);

        $questiongroup1 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'COMPLIANCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup2 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'CUSTOMER EXPERIENCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup3 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'AUTO-FAILURE','percentage' => '-50.00','groupmode' => 'auto-failure']);
        //Question group 1
        $questionlineitem1 = Question::create(['group_id' => $questiongroup1->id,'title' => 'TECHNICAL PROCEDURE','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Did Agent use correct guide?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Did Agent review the Med Audit case before making call?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);

        $questionlineitem2 = Question::create(['group_id' => $questiongroup1->id,'title' => 'PURPOSE OF CALL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Review any previous notes if any from prior attempt to reach client",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);

        $questionlineitem3 = Question::create(['group_id' => $questiongroup1->id,'title' => 'OPENING','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Confirm the Client is speaking on the phone (Or AUTHORIZED contact)','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'State clearly reason for call','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'If you were unable to reach the client or Authorized contact, did you leave a message with the Alt Contacts?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        /*NEXT STEPS*/
        $questionlineitem4 = Question::create(['group_id' => $questiongroup1->id,'title' => 'NEXT STEPS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent confirm all of the Client's contact info is up to date?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent confirm that the client will be able to attend the hearing (or by phone)?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Did Agent ask if the client has worked since AOD?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'If Client has worked, was rep able to obtain as much info from the client as they could?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent ask if the client had a prior Workers Comp case?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent confirm if the client had a primary pharmacy?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent review all of the ERE MRRs and the client's last treatment there?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent ask if there was any additional treating locations not listed in the case?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent review all of the CSMUs for missing providers?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        

        $questionlineitem5 = Question::create(['group_id' => $questiongroup1->id,'title' => '10 Questions section. Did the Agent request information from the client for the following:','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
                ','score' => '{"Yes":100,"No":0,"NA":0}
                ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Does the client have a PCP?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Has the client been to an Emergency room recently?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Did the client have any recent surgeries?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Has the client had any other testing done?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Has the client had any Physical Therapy?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Does the client see anyone specific for Pain Management?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Does the client treat with anyone for their mental health?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Does the client require an assistive device (and if so, was it prescribed)?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Does the client have an handicapped driving placard?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Does the client receive any in home care?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Does the client have access to their records via an online portal?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Since the Pandemic, has the client received any phone or online consultations?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Does the client receive VA benefits?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);

          /*WRAPPING UP THE CALL*/
        $questionlineitem6 = Question::create(['group_id' => $questiongroup1->id,'title' => 'WRAPPING UP THE CALL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent offer up contact info for CS to the client?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent explain to the client about us paying the cost of records up front?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent offer to send the client a list of Free and Low Cost Clinics in their area (if client had no treatment)?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);

        /*ADDITIONAL ACTIONS*/
        $questionlineitem7 = Question::create(['group_id' => $questiongroup1->id,'title' => 'ADDITIONAL ACTIONS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent review the case after speaking with the client to ensure there were no duplicate requests?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent include all pertitent information in their Case Note following call?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup1->id,'title' => 'Did the client ask any questions?  Was the agent able to repond appropriately and with correct information?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup1->id,'title' => 'Did agent ask for assistant on how to create additional requests outside of the guide?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup1->id,'title' => 'Did the agent review their notes and create any requests needed?  (ie: Client gives name of PCP; is there a request to the doctor?  Client states Dr that would complete RFC - did an RFC get sent to that DR?)','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup1->id,'title' => 'Did the agent notify TL or Manager if encountered guide error?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.70','comment' => '1','commentmandatory' => '1']);


        /*CALL CONTROL*/    
        $questionlineitem8 = Question::create(['group_id' => $questiongroup2->id,'title' => 'CALL CONTROL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Understand content of conversation and respond correctly','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Handle questions or objections appropriately, Responds appropriately','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Does the rep allow the client to speak without talking over them?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => "Does the rep have the information needed in order to answer the client's questions without delaying the call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Does not repeat questions or ask for repeat answers excessively','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Listen attentively (ask for names to be spelled or numbers clarified. Repeat back information) Active Listening.','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Ask effective probing questions','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Respond clearly and concisely  Offer to spell names (if needed)','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Sound friendly & confident (Be aware of delay in voice transmission due to internet connection speeds)','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Avoid excessive dead air','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Empathize and/or apologize appropriately','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup2->id,'title' => 'Escalate the call appropriately','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.33','comment' => '1','commentmandatory' => '1']);

        /*AUTO-FAILURE*/  
        $questionlineitem9 = Question::create(['group_id' => $questiongroup3->id,'title' => 'CRITICAL ERRORS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup3->id,'title' => "Disconnect or avoid the call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup3->id,'title' => 'Display unprofessional behavior in the call; ignore client questions or requests for additional information','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup3->id,'title' => 'Disclosing HIPAA protected information to unauthorized individuals','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup3->id,'title' => 'Not being prepared for call - no review of previous notes','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup3->id,'title' => "Failure to update client's contact info if info is provided",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup3->id,'title' => 'Misrepresent the company','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);        
        $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup3->id,'title' => 'Falsify information in notes, about actions being taken, or client info','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);

    }
}
