<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationApplicationFollowUpFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluationform = EvaluationForm::create(['title' => 'Application Follow Up Quality Form','description' => 'Application Follow Up Quality Form','headers' => '[]','ratings' => '[]','version' => '1.0','workflow_id' => '1','active' => '1','created_by' => '1000','type' => '4','usr_grp' => 'CG']);

        $questiongroup1 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'COMPLIANCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup2 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'CUSTOMER EXPERIENCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup3 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'AUTO-FAILURE','percentage' => '-50.00','groupmode' => 'auto-failure']);
        //Question group 1
        $questionlineitem1 = Question::create(['group_id' => $questiongroup1->id,'title' => 'PURPOSE OF CALL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Review previous notes determine reason why you are calling','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);

        $questionlineitem2 = Question::create(['group_id' => $questiongroup1->id,'title' => 'OPENING','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Correct use of call tree/options when offered",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Use correct call opening greeting/verbiage  Obtain name of person speaking with",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Confirm the Providers Name if not specified on the phone's answering recording",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);

        $questionlineitem3 = Question::create(['group_id' => $questiongroup1->id,'title' => 'SET THE CALL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Confirm correct dept reached (ie med records) (or ask to be transferred)','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Redo intro if you have been transferred','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        

        $questionlineitem4 = Question::create(['group_id' => $questiongroup1->id,'title' => 'NEXT STEPS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'State clearly reason for call','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => "Provide correct request type in opening (I'm calling about X request)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);

        /*Asked Follow Up Questions*/
        $questionlineitem5 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Asked Follow Up Questions','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Give provider correct CL name/info - including AKA if needed','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Confirm with provider if they process records in house or use a copy service','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'If copy service: Confirm copy service details and update SF','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'If provider mentions another MRR#, confirm if the records are centralized','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);


        /*Has request been received? Yes - Request Received*/
        $questionlineitem6 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Has request been received? - Yes - Request Received','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Confirm MRR#','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Offer that request number starts with MRR and where number is located on request','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Confirm if the request is still in process or completed','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Confirm TAT if still in process','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'Confirm if provider has CD callback number/fax/email for sending records','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);

        /*If records have been sent*/
        $questionlineitem6 = Question::create(['group_id' => $questiongroup1->id,'title' => 'If records have been sent','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => 'How were they sent (Mail, Paper, CD, USB, Fax or Email) Confirmed Mailing Address, Fax number sent to, number of pages sent, Fax number sent From','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => "Confirm provider's mailing address, fax number, email address being used to send records from.",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => "Date they were sent",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup1->id,'title' => "If it has been over 2 weeks ask if the records can be resent.  If under 2 weeks advise provider we will look for them.",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);

        /*If records have not been sent*/
        $questionlineitem8 = Question::create(['group_id' => $questiongroup1->id,'title' => 'If records have not been sent','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup1->id,'title' => "When will they send them?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup1->id,'title' => "How will they be sent?  Mail, Fax, Email, CD or USB? (Email preferred method)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup1->id,'title' => "Confirm provider's mailing address, fax number, email address being used to send records from",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem8->id,'group_id' => $questiongroup1->id,'title' => "Will there be an invoice with the records? Invoices over $20 require preapproval before sending records",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);

         /*No - Request Not Received*/
         $questionlineitem9 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Has request been received? - No - Request Not Received','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
         $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup1->id,'title' => "Ask if the request can be faxed",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup1->id,'title' => "Ask if the request can be emailed (Agent needs to confirm email address as well)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup1->id,'title' => "Yes - can be faxed - Confirm fax number to send to",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup1->id,'title' => "Update fax number in guide and fax request",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup1->id,'title' => "Provider does not accept fax - state that it will be mailed, confirm mailing address and addressed to",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup1->id,'title' => "Update mailing address in guide if needed",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         $question = Question::create(['parent_question_id' => $questionlineitem9->id,'group_id' => $questiongroup1->id,'title' => "If the request has been sent multiple times to the same address, confirm if there is an alternate method/address to send the request.",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         
         /*If provider mentions invoice:*/
         $questionlineitem10 = Question::create(['group_id' => $questiongroup1->id,'title' => 'If provider mentions invoice:','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
         $question = Question::create(['parent_question_id' => $questionlineitem10->id,'group_id' => $questiongroup1->id,'title' => "Check SF if inv has been received, ask for resend if not rec'd and confirm how inv previously sent as well as date previously sent",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         $question = Question::create(['parent_question_id' => $questionlineitem10->id,'group_id' => $questiongroup1->id,'title' => "If payment has been made, provide payment info",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         $question = Question::create(['parent_question_id' => $questionlineitem10->id,'group_id' => $questiongroup1->id,'title' => "If inv has been disputed, provide dispute info",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
         $question = Question::create(['parent_question_id' => $questionlineitem10->id,'group_id' => $questiongroup1->id,'title' => "If provider states they have not received the dispute, confirm where to resend it",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
         ','score' => '{"Yes":100,"No":0,"NA":0}
         ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);


          /*WRAPPING UP THE CALL*/
        $questionlineitem11 = Question::create(['group_id' => $questiongroup1->id,'title' => 'WRAPPING UP THE CALL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem11->id,'group_id' => $questiongroup1->id,'title' => 'Confirm who you are speaking with and a good callback number if needed','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem11->id,'group_id' => $questiongroup1->id,'title' => 'If you need to leave a voicemail, all appropriate details are left for the provider.','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem11->id,'group_id' => $questiongroup1->id,'title' => 'Thank the respondent for their time','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem11->id,'group_id' => $questiongroup1->id,'title' => 'Select correct dispostion in the guide','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem11->id,'group_id' => $questiongroup1->id,'title' => 'Make detailed notes summarizing call','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem11->id,'group_id' => $questiongroup1->id,'title' => '**Was conversation is appropriate for TSS','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem11->id,'group_id' => $questiongroup1->id,'title' => '**Was conversation is appropriate for Medical Records','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.33','comment' => '1','commentmandatory' => '1']);

        /*CALL CONTROL*/    
        $questionlineitem12 = Question::create(['group_id' => $questiongroup2->id,'title' => 'CALL CONTROL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Understand content of conversation and respond correctly','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Handle questions or objections appropriately, Responds appropriately','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Does the rep allow the provider to speak without talking over them?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => "Does the rep have the information needed in order to answer the provider's questions without delaying the call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Did the agent avoid repeating answers and questions excessively?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Listen attentively (ask for names to be spelled or numbers clarified. Repeat back information) Active Listening.','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Ask effective probing questions','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Respond clearly and concisely  Offer to spell names (if needed)','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Sound friendly & confident (Be aware of delay in voice transmission due to internet connection speeds)','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Avoid excessive dead air','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Empathize and/or apologize appropriately','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem12->id,'group_id' => $questiongroup2->id,'title' => 'Escalate the call appropriately','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '8.34','comment' => '1','commentmandatory' => '1']);

        /*AUTO-FAILURE*/  
        $questionlineitem13 = Question::create(['group_id' => $questiongroup3->id,'title' => 'CRITICAL ERRORS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem13->id,'group_id' => $questiongroup3->id,'title' => "Disconnect or avoid the call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem13->id,'group_id' => $questiongroup3->id,'title' => 'Display unprofessional behavior in the call','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem13->id,'group_id' => $questiongroup3->id,'title' => 'Disclosing HIPAA protected information to wrong provider','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem13->id,'group_id' => $questiongroup3->id,'title' => 'Not being prepared for call - no review of previous notes','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem13->id,'group_id' => $questiongroup3->id,'title' => "Failure to update Fax, Phone, Provider or Copy Service fields if info is provided",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem13->id,'group_id' => $questiongroup3->id,'title' => 'Misrepresent the company','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);        
        $question = Question::create(['parent_question_id' => $questionlineitem13->id,'group_id' => $questiongroup3->id,'title' => 'Falsify information in notes, about actions being taken, or client info','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);

    }
}
