<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class IntakeDBDQualityFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title" => "Intake DBE Quality Form", "description" => "Intake DBE Quality Form", "headers" => "[]", "ratings" => "[]", "version" => "1.0", "workflow_id" => "1", "active" => "1", "created_by" => "1000", "type" => "6", "usr_grp" => "CG"]);
        $form_details = [
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Intake DBE Quality Form"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Empathy"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep actively listen to the clmt's without interrupting?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep use verbal cues/language that expressed understanding? (Oh, no!, Wow, I See..)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep offer a helpful next step or acknowledgement statement that validated their current emotional state?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Knowledge"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep provide accurate info on CD and it's process?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep sound confident in the delivery?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep match the claimant's profile, CD process orientation, and communication style? "],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep offer resources/info to help the clmt make an informed decision? (Website, EV)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Were the rep's responses relevant to the clmt's statement/concern? "],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Was the rep quick to respond to unexpected questions or statements? "],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Call Control"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep clearly explain the purpose of the call?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Was the rep able to easily get back to the eval/app after answering questions?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Were the reps responses short, simple, and on point for efficient handling time?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep use a fluctuation in their tone of voice to drive the sale/emphasize points?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep verify there were no additional questions before providing the best next step/ending the call? "],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 0,"No":0,"NA":0 }', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Professionalism"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep have a friendly tone of voice, avoided sounding monotone, even when faced with challenges?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Was the rep polite throughout the call?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep explain the reason to any mutes or holds?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep use professional language (limiting mhmm's, ya, gotcha...)?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Was the rep respectful of the clmt's time/prior arrangements to getting a phone call?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep address the claimant appropriately throughout the call? (Mostly using their name, no overuse of \"sir\"/\"ma'am\")"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 0,"No":0,"NA":0 }', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Value Proposition"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep acknowledge the opportunity that the claimant raised? "],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep compare our services to other methods of applying in a positive manner?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep tailor-value the value prop to the specific opportunity? "],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Did the rep leave room for the claimant to ask any questions they may have?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes": 0,"No":0,"NA":0 }', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Integrity (Auto Fail)"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Misleading"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Unprofessional Behavior"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Listing False information"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":0,"No":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Non-Permission App"],
                        ]
                    ],
                ],
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Comment"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '', "score" => '', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Comment"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '', "score" => '', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "General Feedback"],
                        ],


                    ]
                ],
            ],
        ];

        //Do not change this
        foreach ($form_details as $form_detail) {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach ($form_detail["group_data"] as $group_data) {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach ($group_data["questions"] as $question) {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }
    }
}
