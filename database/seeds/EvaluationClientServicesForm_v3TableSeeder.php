<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationClientServicesForm_v3TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Client Services Quality Form v3", "description"=>"Client Services Quality Form v3", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"3", "usr_grp"=>"CG"]);
        $form_details = [

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CRITICAL ERRORS"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERRORS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to verify who they are speaking to and the last 4 digits of SSN?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep failed to verify updated Claimant contact information (address, phone number, Authorized Contact)?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to mention the call recording verbiage?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to document the case notes accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to offer the survey?"],
                            ]
                        ]
                    ]
            ],

            //4CS - CONDUCT
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"4CS - CONDUCT"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"4CS - CONDUCT"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to provide accurate resolution?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep provide misleading information?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Was the rep unprofessional during the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep do anything to boost the metrics?"],
                            ]
                        ]
                    ]
            ],

            //4CS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"100.00", "groupmode"=>"default", "title"=>"4CS - CONNECTION, COMPATIBILITY & CONTROL"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"4CS - CONNECTION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Did the rep show genuine willingness to help?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Did the rep acknowledge the claimants emotion and circumstance?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Was the rep's tone voice engaging?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Was the rep casual and conversational?"],
                            ]
                        ], 

                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"4CS - COMPATIBILITY"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep understand the client's concern?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Did the rep use knowledge/utilize resources in resolving the client's issue?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Did the rep proactively Educates the client on the next steps?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Did the rep showcase the benefits of being represented by CD?"],
                            ]
                        ], 

                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"4CS - CONTROL"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Did the rep listen attentively and responds appropriately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Did the rep lead the conversation?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Was the rep easily understood?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "Needs Improvement"]', "score"=>'{"Yes":100,"No":0,"Needs Improvement":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Takes Ownership"],
                            ]
                        ], 

                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"OPENING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent introduce himself and brand the company?"],
                            ]
                        ], 

                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"MEDICAL RECORDS DATA GATHERING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Did the agent confirm that the client will be able to attend the hearing (or by phone)?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Did the agent ask  and gather if there are changes on the cl's Medical provider, conditions and treatment, pharmacy?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Did the agent follow the Med Update provider info format?"],
                            ]
                        ], 

                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CLAIMS CONFIRM DATA GATHERING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Did the rep provide accurate information to SSA to locate the claim?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Did the rep ask all necessary probing questions to identify the status of the claim?"],
                            ]
                        ], 

                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"WRAP UP"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent proactively mentioned the name of the account owner?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100, "No": 0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"9.00", "title"=>"Did the agent complete all follow up work needed for the call?"],
                            ]
                        ], 
                    ],
            ],

        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}