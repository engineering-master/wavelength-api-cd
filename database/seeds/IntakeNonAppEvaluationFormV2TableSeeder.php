<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class IntakeNonAppEvaluationFormV2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Non App Evaluation Form v2", "description"=>"Non App Evaluation Form v2", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"6", "usr_grp"=>"CG"]);
        $form_details = [
            //AUTO-FAILURE
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"0.00", "groupmode"=>"auto-failure", "title"=>"AUTO-FAILURE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"Non CE call out"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Was this an Agent driven eliminate?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep set proper expectation?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep handle the objection effectively?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep ask effective probing question?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did the rep listen actively?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Was the agent confident in his/her delivery? (Delivering the Message clearly, appropriate tone, pace, volume)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Was the call correctly tag as Eliminate"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERROR"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Call Avoidance-Low : Did the rep fail to answer the call within 30 seconds?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Call Avoidance-High : Did the rep stay silent resulting to claimant disconnectiing the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Failure to Discuss Call Recording Verbiage"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Failure to Input Information on the SSA website"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Inaccurate information on Esig"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Did not attempt to overcome an objection"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Failure to List Doctor’s Name for TSS"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"No attempt to callback"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Purposely not using the PQ guide - Low"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Purposely not using the PQ guide - High"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"ZERO TOLERANCE"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Zero Tolerance - Misleading"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Zero Tolerance - Fraud"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Zero Tolerance - Unprofessional Behavior "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.0000", "title"=>"Zero Tolerance - DNC Policy"],
                            ]
                        ],
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}