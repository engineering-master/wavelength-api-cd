<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class CS4CQualityFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title" => "CS 4Cs Quality Form", "description" => "CS 4Cs Quality Form", "headers" => "[]", "ratings" => "[]", "version" => "1.0", "workflow_id" => "1", "active" => "1", "created_by" => "1000", "type" => "6", "usr_grp" => "CG"]);
        $form_details = [
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Connection"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Shows genuine willingness to help"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Acknowledge the claimants emotion and circumstance"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Has engaging tone of voice"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Casual and conversational"],
                        ]
                    ],
                ],
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Compatibility"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Understand the client's concern"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Uses knowledge/utilize resources in resolving the client's issue"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Proactively Educates the client on the next steps"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Showcases benefits of being represented by CD"],
                        ]
                    ],
                ],
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Control"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Listens attentively and responds appropriately"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Lead the conversation"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Easily understood"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Takes Ownership"],
                        ]
                    ],
                ],
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Conduct"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Does not demonstrate unprofessional behaviors"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Does not mislead"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Always provides accurate information"]
                        ]
                    ],
                ],
            ]
        ];

        //Do not change this
        foreach ($form_details as $form_detail) {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach ($form_detail["group_data"] as $group_data) {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach ($group_data["questions"] as $question) {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }
    }
}
?>
