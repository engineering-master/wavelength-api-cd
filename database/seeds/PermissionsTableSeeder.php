<?php
use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Permission::create(['name' => 'Create Survey','guard_name' => 'web']);
      Permission::create(['name' => 'View Survey','guard_name' => 'web']);
      Permission::create(['name' => 'Edit Survey','guard_name' => 'web']);
      Permission::create(['name' => 'Create Survey Response','guard_name' => 'web']);
      Permission::create(['name' => 'View Survey Response','guard_name' => 'web']);
      Permission::create(['name' => 'Edit Survey Response','guard_name' => 'web']);
      Permission::create(['name' => 'Add Comments','guard_name' => 'web']);
      Permission::create(['name' => 'View Comments','guard_name' => 'web']);
      Permission::create(['name' => 'Edit Comments','guard_name' => 'web']);
      Permission::create(['name' => 'View Customer calls','guard_name' => 'web']);
      Permission::create(['name' => 'View Agent calls','guard_name' => 'web']);
    }
}
