<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationApplicationMasterQAUploadsFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluationform = EvaluationForm::create(['title' => 'Application QA Uploads Form','description' => 'Application QA Uploads Form','headers' => '[]','ratings' => '[]','version' => '1.0','workflow_id' => '1','active' => '1','created_by' => '1000','type' => '5','usr_grp' => 'CG']);

        $questiongroup1 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'COMPLIANCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup2 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'AUTO-FAILURE','percentage' => '-100.00','groupmode' => 'auto-failure']);
        
        //Question Group 1
        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'Technical Application','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent select correct guide?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);


        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'Procedural Application','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Is the Document attached to the correct MRR and did the Agent confirm this?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent access the correct Client ERE file on the SSA website (using the most recent barcode attachment)?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent confirm all of the pages are records for the client in question?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Are all the pages in the correct order and right side up?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);
        

        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'Detail Application','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent upload the document to the correct client's file?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Is the ERE Confirmation Number recorded?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent enter appropriate DOS for the document?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent escalate documents when needed?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent report an issues that they may have experienced while using the guide? (not just marking it on the assignment sheet)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.00','comment' => '1','commentmandatory' => '0']);
        


        //Question Group 2: AUTO-FAILURE  
        $questionlineitem = Question::create(['group_id' => $questiongroup2->id,'title' => 'CRITICAL ERRORS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent fail to confirm the records are for the right client?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent upload the file to the wrong client file?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent fail to confirm all the pages are in the correct order?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent upload incorrect documents (invoices/Rejection notices) as records?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Failure to report guide errors or issues.",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
    }

}