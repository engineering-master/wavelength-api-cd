<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class FollowUpQAFromTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Follow Up QA Form", "description"=>"Follow Up QA Form", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"3", "usr_grp"=>"CG"]);
        $form_details = [
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"100.00", "groupmode"=>"default", "title"=>"COMPLIANCE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"6.00", "title"=>"OPENING AND PURPOSE OF CALL"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent use the correct call opening greeting or verbiage and obtain the name of the person speaking with?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent confirm the Providers Name if not specified on the phone's answering recording?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"3.0", "title"=>"SET THE CALL"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent mention the correct purpose of the call, provide the correct request type or ask to be transferred?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"9.0", "title"=>"ASKED FOLLOW UP QUESTIONS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent give provider correct CL name/info - including AKA if needed"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent confirm if the provider process records in house or if they use a copy service? If Copy service, confirm copy service details and update SF"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"If provider mentions another MRR#, confirm if the records are centralized"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"21.0", "title"=>"Has request been received? - Yes - Request Received"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent confirm the MRR# and offer that the request number starts with MRR and where it is located?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent confirm if the request is in process or completed, confirm TAT if it is still in process and confirm if the provider has the CD's details for sending the records?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did  the agent state the type of form they were calling for?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent state the name of the physician meant to complete the form?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent check Med Audit to confirm if there is another form?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent confirm if any other physician is able to complete the form If facility doesn't have the physician RFC was addressed to?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent ask if an an appointment will be needed If rep says form will not be completed?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"9.0", "title"=>"Has request been received? - If Records/RFC have been sent"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent ask how the records were sent and confirm the providers address being used to send the records from? (mailing address, fax number sent to and from, number of pages sent)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent confirm the Date the records were sent?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent provide alternative means of sending records if records were sent but not received?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"9.0", "title"=>"Has request been received? - If Records/RFC have not been sent"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent ask when will they send the records?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent ask how the RFC will be sent and confirm the provider's mailing address/fax number being used to send records from?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent ask if  there will be an invoice with the records? Invoices over $20 require preapproval before sending records?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"11.0", "title"=>"Has request been received? - No - Request Not Received"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent ask if the request can be faxed and update the fax number to send to?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent ask if the request can be emailed (Agent needs to confirm email address as well)?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"Did the agent state that it will be mailed and confirm the mailing address if the provider does not accept fax?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the rep  confirm if there is an alternate method/address to send the request If the request has been sent multiple times to the same address?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"9.0", "title"=>"If provider mentions invoice:"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"Did the rep check SF if inv has been received, ask for resend if not rec'd and confirm how inv previously sent as well as date previously sent?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"If payment has been made, provide payment info"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"If inv has been disputed, provide dispute info"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"If provider states they have not received the dispute, confirm where to resend it"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"14.0", "title"=>"WRAPPING UP THE CALL"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent confirm who they are speaking with and a good callback number, if needed?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did Agent confirm provider's email?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"If provided with a different MRR# did Agent follow up on the status of the provided request?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"Did the Agent ask for no records/no patient correspondence to be sent if applicable?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Was agents notes detailed summarizing the call and was the conversation appropriate for the request type?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"9.0", "title"=>"CALL HANDLING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"1.00", "title"=>"Did the agent addressed all questions/ concerns correctly?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"1.00", "title"=>"Did the agent addressed all objections appropriately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"Was the agent confident in his/her delivery? (Delivering the Message clearly, appropriate tone, pace , volume)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"Did the agent listen actively?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"1.00", "title"=>"Did the agent empathize and Respond Appropriately at all times?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"Did the agent observe proper phone etiquette?"],
                            ],
                        ],
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"AUTO FAILURE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"Critical Errors"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to follow the correct call flow?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent disconnect or avoid the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent Display unprofessional behavior in the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent Disclose HIPAA protected information to wrong provider?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent fail to update Fax, Phone, Provider or Copy Service fields if info is provided?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent misrepresent the company?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent falsify information in notes, about actions being taken, or client info?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did agent dispose the call incorrectly?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to state that the call is recorded?"],
                            ]
                        ],
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}