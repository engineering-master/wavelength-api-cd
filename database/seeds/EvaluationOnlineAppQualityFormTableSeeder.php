<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationOnlineAppQualityFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title" => "Online Application Quality Form", "description" => "Online Application Quality Form", "headers" => "[]", "ratings" => "[]", "version" => "3.0", "workflow_id" => "1", "active" => "1", "created_by" => "1000", "type" => "6", "usr_grp" => "CG"]);
        $form_details = [
            //CALL HANDLING
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "100.00", "groupmode" => "default", "title" => "CALL HANDLING"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Document Attaching - Online Application Documents"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Did the agent attach the online error document and label it correctly?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Did the agent attach the re-entry# document and label it correctly?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Did the agent attach the completed 16bk document and label it correctly?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Did the agent attach the review and summary document and label it correctly?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Did the agent attach the receipt document and label it correctly?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Application - 16BK"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Other Social Security Numbers and Names. If there are any listed is this on the application?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Does the cmnt want to receive reduced retirement benefits (this question will appear based on the cmnt age) Answer should always be no and then a note in remarks should be added in remarks informing SSA to contact the cmnt directly about early retirement benefits."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Currently Married. If there is marriage info on file it should be listed on the app"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Prior Marriages: If there is prior marriage info it should be listed on the app too"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Dependent Children: If there are children on file then it should be listed on the app."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Employer Details (Worked for an employer last year) - Should be yes if there is info or No if there isn't"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Employer Details (Worked for an employer this year)- Should be yes if there is info or No if there isn't"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Employer Details (Name, address and phone number) - Should be listed if there was work. There should be Employer Name, Address, date work began and date work ended. Work ended should be blank ONLY if the cmnt is still working"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Self Employment Details: Earnings and amounts should be listed if the cmnt was self employed"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Agree with earnings history as shown on SS statement. - This should always be Not sure or (cmnt name) does not have a statement"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Total Earnings: Earnings and amounts should be listed if the cmnt worked this year or last"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Intend to apply for Supplemental Security Income benefits: Should be listed as Yes if its a concurrent claim (99% are)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Ability to work now - Should be listed as No unless the cmnt is currently working"],
                        
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Application - 3368 (Review and Summary)"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Conditions should match was is listed on the file (if blind or low vision is listed this should not be counted against them)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Other Contact - Name, relationship and phone number should be listed if we have one"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Hospital / Clinic - Name should be listed in both the name and name of healthcare professional unless we have both"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Hospital / Clinic - There should be an address, phone number and visit dates. Also medical conditions treated and treatment received"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Hospital / Clinic - Each Doctor/clinic/hospital should be only logged one time."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Tests- This section should have the type of test, date of tests and sent for by the particular Dr."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Medication - This section should have one medication listed at a time with the prescribing doctor (note most won't have a reason for the medication. It should be left blank)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Work Status - Should be listed as No has stopped working unless the cmnt is currently working"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Work Activity - Date stopped working should be the onset date and reason should be because of conditions"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Job History - This should match the number of jobs we have on file for the cmnt. If there is only one then the additional job details will need to be listed as well. If there is more than 1 job but the cmnt has the same job type the additional job details need to be added to remarks"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Job History -If the cmnt had more than 5 jobs. The additional job(s) that didnt fit needs to be added to the remarks section."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Job History - Each job should have a job title, type of business, start date, end date, hours per day, days per week, pay amount and pay frequency."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Education - Highest Grade Completed and date completed should be filled in"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Education - School name should be unknown. City should be unknown and there should state should be filled in"],
                            

                        ]
                    ],
                    [
                        //ADDITIONAL INFO
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Application - Remarks"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Remarks section of the 16BK should at least contain the following: Per POMS SI 00601.060, please contact the claimant to conduct the required SSA Supplemental Security Income (SSI) Interview. This disability application includes any application for SSI/Title XVI benefits claimant maybe entitled to."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Remarks section of the 16BK should also contain any additional information we didn't have in the app and they need to contact the cmnt (for example we used a fake marriage date, the early retirement question appeared etc)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Remarks section of the 3368 (review and summary) should at least contain the following: Per POMS SI 00601.060, please contact the claimant to conduct the required SSA Supplemental Security Income (SSI) Interview. This disability application includes any application for SSI/Title XVI benefits claimant maybe entitled to."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3.7", "title" => "Remarks section of the 3368 should also contain any additional information we didn't have in the app and they need to contact the cmnt (for example there should also be a sentence of contacting the claimant for more information regarding their education)"],
                        ]
                    ],
                    
                ]
            ],
            //CRITICAL ERRORS
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "-100.00", "groupmode" => "auto-failure", "title" => "CRITICAL ERRORS"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "AUTO-FAILURE"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Was an app error attached that wasn't the normal error?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Was an app error attached after the completed application was previously submitted?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Was an app filed for the wrong cmnt?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Was an app filed after the client file was closed?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Was any of the online app docs not attached?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Is the Preparer's Name not listed as Andrew Youngman?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Is the Preparer's Relationship to Applicant not listed as Non-Attorney Representative?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Is the Preparer's Organization Name not listed as Citizens Disability?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Is the Preparer's Address not listed as PO Box 15023 Worcester, Massachusetts 01615"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Is the Preparer's phone number not listed as 877-207-5256?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "If the cmnt had additional job details and had more than 1 job. Were the details not added to the remarks section?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "If the question appeared in the 16bk section of the app asking if the cmnt would like to file for reduced retirement. Did the rep add the appropite info to the remarks section? (there needs to be a remark stating to please contact the claimant directly about filing for early retirement)"],
                        ]
                    ]
                ]
            ],
        ];

        //Do not change this
        foreach ($form_details as $form_detail) {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach ($form_detail["group_data"] as $group_data) {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach ($group_data["questions"] as $question) {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }
    }
}
