<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationApplicationMedicalAuditFormV4TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title" => "Application Medical Audit (MACC) Quality Form - V4", "description" => "Application Follow Up Quality Form v4", "headers" => "[]", "ratings" => "[]", "version" => "3.0", "workflow_id" => "1", "active" => "1", "created_by" => "1000", "type" => "6", "usr_grp" => "CG"]);
        $form_details = [
            //CALL HANDLING
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "100.00", "groupmode" => "default", "title" => "COMPLIANCE"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "10", "title" => "Opening"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10", "title" => "Did the rep review the notes before making a call and opened the call correctly?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "40", "title" => "Data Gathering"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10", "title" => "Did the agent confirm that the client will be able to attend the hearing (or by phone)?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10", "title" => "Did the agent ask all necessary questions related to compensation?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10", "title" => "Did the agent ask all necessary medical questions?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10", "title" => "Did the agent ask necessary questions related to Medical Treatment?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "20", "title" => "Call Handling"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10", "title" => "Did the agent addressed all questions/ concerns correctly?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5", "title" => "Did the agent addressed all objections appropriately?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "4", "title" => "Was the agent confident in his/her delivery? (Delivering the message clearly, appropriate tone, pace, volume)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3", "title" => "Did the agent listen actively?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "3", "title" => "Did the agent empathize and respond appropriately at all times?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5", "title" => "Did the agent observe proper phone etiquette?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "10", "title" => "Closing"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5", "title" => "Did the agent ask if there was anything else they could assist with?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5", "title" => "Did the agent set proper expectation on the next steps and informed the Cl about the survey?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "10", "title" => "After Call Work"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5", "title" => "Did the agent's notes match their call? "],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "5", "title" => "Did the agent complete all follow up work needed for the call?"],
                        ]
                    ],
                ]
            ],
            //CRITICAL ERRORS
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "-100.00", "groupmode" => "auto-failure", "title" => "CRITICAL ERRORS"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Critical Errors"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Failure to verify client using last 4 of SSN, phone number and address"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Failure to inform client they are on a recorded line"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Disconnect or avoid the call"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Display unprofessional behavior on the call"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Disclosing PHI with non authorized contact (Protect Health Information)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Failure to update clients contact info if info is provided"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Misrepresent the company"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Falsify information in notes, about actions being taken, or client info"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Did the agent provide accurate information?"],
                            
                        ]
                    ]
                ]
            ],
        ];

        //Do not change this
        foreach ($form_details as $form_detail) {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach ($form_detail["group_data"] as $group_data) {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach ($group_data["questions"] as $question) {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }
    }
}