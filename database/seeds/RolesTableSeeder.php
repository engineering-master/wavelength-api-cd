<?php
use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(
            [
                'name' => 'Administrator',
                'guard_name' => 'web',
            ]
        );
		Role::create(
            [
                'name' => 'Agent',
                'guard_name' => 'web',
            ]
        );
		Role::create(
            [
                'name' => 'Client',
                'guard_name' => 'web',
            ]
        );
		Role::create(
            [
                'name' => 'Comments',
                'guard_name' => 'web',
            ]
        );
		Role::create(
            [
                'name' => 'Operations Manager',
                'guard_name' => 'web',
            ]
        );
        Role::create(
            [
                'name' => 'QA Manager',
                'guard_name' => 'web',
            ]
        );
        Role::create(
            [
                'name' => 'QA Specialist',
                'guard_name' => 'web',
            ]
        );
    }
}
