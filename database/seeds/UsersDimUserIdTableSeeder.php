<?php

use App\User;
use App\UserSystemMapping;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class UsersDimUserIdTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::WhereNotNull('dim_user_id')->get();
        foreach ($users as $user) {
            $data = [];
            if ($user->dim_user_id) {
                $data = [
                    'user_id' => $user->user_id,
                    'system_user_id' => $user->dim_user_id
                ];
                UserSystemMapping::create($data);
            }
        }
    }

}