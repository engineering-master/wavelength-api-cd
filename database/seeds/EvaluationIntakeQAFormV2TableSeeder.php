<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationIntakeQAFormV2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Intake Compliance Form Version 2", "description"=>"Intake Compliance Form Version 2", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"6", "usr_grp"=>"CG"]);
        $form_details = [
            //COMPLIANCE
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"100.00", "groupmode"=>"default", "title"=>"COMPLIANCE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"APPLICATION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep ask for the name of the claimant as it appears on SS card. Confirm spelling of Mother's maiden Name and Father's full name phonetically?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep ask  for the phone number that the claimant is using, as shown on the dialer?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep ask  for an alternative number?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the rep ask  for an alternate contact info?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep Update Work History accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Did the rep Update Medications accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Did the rep Update Conditions accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep Update Medical Treatment / Testings  accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep Verify other Personal Information?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"9.00", "title"=>"Did the rep Update Section 6 accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Did the rep Update Spouse Info accurately?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the rep Dispose the call properly in Five9 (status)?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Any opportunity found which is not listed on the form"],
                            ]
                        ]
                    ]
            ],
            
            //AUTO-FAILURE
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"AUTO-FAILURE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERRORS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Call Avoidance- Low : Did the rep fail to answer the call within 30 seconds?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Call Avoidance- high : Did the rep stay silent resulting to claimant disconnectiing the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to discuss the call recording verbiage?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to Input Information on the SSA website?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep capture inaccurate information on Esig?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to overcome an objection?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail  to List Doctor’s Name for TSS?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"No attempt to callback"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to offer Esig?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep purposely not using the PQ guide- low"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep purposely not using the PQ guide- High"],
                            ]
                        ]
                    ]
            ],
            //AUTO-FAILURE
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"ZERO TOLERANCE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERRORS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Zero Tolerance- Misleading"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Zero Tolerance- Fraud"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Zero Tolerance- Unprofessional Behavior "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Zero Tolerance- DNC Policy"],
                                
                            ]
                        ]
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data)
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}