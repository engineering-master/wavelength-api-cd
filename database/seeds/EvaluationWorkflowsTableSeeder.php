<?php

use App\EvaluationWorkflows;
use Illuminate\Database\Seeder;

class EvaluationWorkflowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EvaluationWorkflows::create(
            [
                'description'  => "Evaluation",
                'status'       => 1,
                'start_date'   => '2019-01-01',
                'wf_details'   => '{"1":{"TransitionId":"1","PickUpStatus":"1","DropStatus":["2","3"],"ButtonLabel":["In Progress","Submit"],"Roles":["7","6","1"]},"2":{"TransitionId":"2","PickUpStatus":"2","DropStatus":["2","3"],"ButtonLabel":["In Progress","Submit"],"Roles":["7","6","1"]},"3":{"TransitionId":"3","PickUpStatus":"3","DropStatus":["4","5"],"ButtonLabel":["Acknowledge","Dispute"],"Roles":["8","1"],"EscalationPolicy":{"Threshold":"5","NotifyTo":["2","1"]}},"4":{"TransitionId":"4","PickUpStatus":"4","DropStatus":["10"],"ButtonLabel":["Acknowledge"],"Roles":["2","1"]},"5":{"TransitionId":"5","PickUpStatus":"5","DropStatus":["6","7"],"ButtonLabel":["Accept","Reject"],"Roles":["6","1"]},"6":{"TransitionId":"6","PickUpStatus":"6","DropStatus":["8"],"ButtonLabel":["Submit Reevaluation"],"Roles":["6","7","1"]},"7":{"TransitionId":"7","PickUpStatus":"7","DropStatus":["10"],"ButtonLabel":["Acknowledge"],"Roles":["2","1"]},"8":{"TransitionId":"8","PickUpStatus":"8","DropStatus":["8","9"],"ButtonLabel":["Save Reevaluation","Submit Reevaluation"],"Roles":["7","6","1"]},"9":{"TransitionId":"9","PickUpStatus":"9","DropStatus":["4"],"ButtonLabel":["Acknowledge"],"Roles":["8","1"]}}}',
                'created_by'   => 1000,
            ]
        );
    }

}