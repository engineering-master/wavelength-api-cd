<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationCDHealthAndWellnessQAFormV5CTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"CD Health and Wellness - QA Form - V5", "description"=>"CD Health and Wellness - QA Form - V5", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"6", "usr_grp"=>"CG"]);
        $form_details = [
            //CALL HANDLING
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"10.00", "groupmode"=>"default", "title"=>"CALL HANDLING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"GAINING CLAIMANT INTEREST"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"50.00", "title"=>"Did the rep open the call promptly and completely (including call introduction)?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"50.00", "title"=>"Call Recording Verbiage"],
                            ]
                        ], 
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CALL HANDLING - Critical Error"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"GAINING CLAIMANT INTEREST"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep open the call promptly and completely (including call introduction)? - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Call Recording Verbiage - Critical Error"],
                            ]
                        ]
                    ]
            ],

            


            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"50.00", "groupmode"=>"default", "title"=>"QUALIFYING THE CLAIMANT"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"QUALIFYING THE CLAIMANT"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q1 - Did the rep ask the question (main and follow up questions) before moving to the next question? ((DAY BENEFITS RECEIVED)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q2 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?  (HEALTH INSURANCE)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q3 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?  (HEALTH INSURANCE) Who do you currently have for your health insurance"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q4 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?  (CHANGE IN PLAN WITHIN THE LAST 3 MONTHS) Have you changed your insurance plans in the last 3 months?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q5 - Did the rep ask questions (main and follow up questions) before moving to the next question? (DATE STARTED DISABILITY)Roughly when did you start collecting disability benefits? "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q6 - Did the rep ask questions (main and follow up questions) before moving to the next question? (CHILDREN BELOW 18 ) Do you have any children under the age of 18 when you were awarded disability benefits? "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q7- Did the rep ask questions (main and follow up questions) before moving to the next question? (SPOUSE ) Did you have a spouse who passed away within 7 years of you being found disabled? "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q8 - Did the rep ask questions (main and follow up questions) before moving to the next question? (MILITARY) Were you ever in the military? "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q9 - Did the rep ask questions (main and follow up questions) before moving to the next question? (PRESCRIPTION PAYMENT) What do you pay for prescriptions?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q10 - Did the rep ask questions (main and follow up questions) before moving to the next question?  (PRESCRIPTION COUNT)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q11- Did the rep ask questions  (main and follow up questions) before moving to the next question?  (PRESCRIPTION FILL)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "NA","No - Agent did not probe","No - Agent Incorrect Probing","No - Agent Inaccurate Probing","No - Agent asked questions in order","No - Agent Skipped Questions"]', "score"=>'{"Yes":100, "NA":0,"No - Agent did not probe":0,"No - Agent Incorrect Probing":0,"No - Agent Inaccurate Probing":0,"No - Agent asked questions in order":0,"No - Agent Skipped Questions":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the rep correctly offer the down sell?"],
                            ]
                        ],
                    ]
            ],


            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"QUALIFYING THE CLAIMANT - Critical Error"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"QUALIFYING THE CLAIMANT"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q1 - Did the rep ask the question (main and follow up questions) before moving to the next question? (TYPE OF BENEFITS RECEIVED) - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q2 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?  (HEALTH INSURANCE) What do you have for health insurance? - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q3 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?  (HEALTH INSURANCE) Who do you currently have for your health insurance?  - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q4 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?  (CHANGE IN PLAN WITHIN THE LAST 3 MONTHS) Have you changed your insurance plans in the last 3 months? - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q5- Did the rep ask questions (main and follow up questions) before moving to the next question? (DATE STARTED DISABILITY) Roughly when did you start collecting disability benefits? - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q6 - Did the rep ask questions (main and follow up questions) before moving to the next question? (CHILDREN BELOW 18 )  Do you have any children under the age of 18 when you were awarded disability benefits? - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q7- Did the rep ask questions (main and follow up questions) before moving to the next question? (SPOUSE )  Did you have a spouse who passed away within 7 years of you being found disabled?            - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q8 - Did the rep ask questions (main and follow up questions) before moving to the next question? (MILITARY) Were you ever in the military? - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q9 - Did the rep ask questions (main and follow up questions) before moving to the next question? (PRESCRIPTION PAYMENT) What do you pay for prescriptions? - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q10 - Did the rep ask questions (main and follow up questions) before moving to the next question?  (PRESCRIPTION COUNT) - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Q11- Did the rep ask questions  (main and follow up questions) before moving to the next question?  (PRESCRIPTION FILL) - Critical Error"],
                            ]
                        ]
                    ]
            ],

            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"20.00", "groupmode"=>"default", "title"=>"EFFECTIVE PRESENTATION"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"EFFECTIVE PRESENTATION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Did the rep listen actively throughout the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Addressing Concern"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Confident Delivery (Delivering the Message clearly, appropiate tone,pace , volume"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Empathy and Responding Appropriately"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0,"No - Crit Error":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"20.00", "title"=>"Product Mastery & Accuracy"],
                            ]
                        ],
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"EFFECTIVE PRESENTATION - Critical Error"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"EFFECTIVE PRESENTATION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Product Mastery & Accuracy - Critical Error"],
                            ]
                        ]
                    ]
            ],



            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"10.00", "groupmode"=>"default", "title"=>"HANDLING OBJECTIONS"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"HANDLING OBJECTIONS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"25.00", "title"=>"Acknowledged Reason for Objection"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":0,"No":0,"NA":100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"25.00", "title"=>"Uncovering underlying reason for Objection (when needed)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"25.00", "title"=>"Tailored Valued Objection"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"25.00", "title"=>"Assumptive Transition"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA", "No - Crit Error"]', "score"=>'{"Yes":0,"No":0,"NA":0,"No - Crit Error":-100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"No Rebuttal - No rebuttal state only"],
                            ]
                        ],
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"HANDLING OBJECTIONS - Critical Error"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"HANDLING OBJECTIONS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"No Rebuttal - No rebuttal state only - Critical Error"],
                            ]
                        ]
                    ]
            ],



            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"10.00", "groupmode"=>"default", "title"=>"CLOSE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No - Crit Error"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CLOSE"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"40.00", "title"=>"Invalid Sale"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"30.00", "title"=>"TRANSFER SPIEL (TCPA/TPMO)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"30.00", "title"=>"Improper Disposition"],
                               
                            ]
                        ],
                    ] 
            ],
            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CLOSE - Critical Error"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CLOSE"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Invalid Sale - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"TRANSFER SPIEL (TCPA/TPMO) - Critical Error"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Improper Disposition - Critical Error"],
                            ]
                        ]
                    ]
            ],


            
            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"ZTP"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No - Crit Error"]', "score"=>'{"No - Crit Error":100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"ZTP"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No - Crit Error"]', "score"=>'{"Yes":100,"No - Crit Error":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Unprofessional Behavior HIGH"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No - Crit Error"]', "score"=>'{"Yes":100,"No - Crit Error":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"DNC Policy"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No - Crit Error"]', "score"=>'{"Yes":100,"No - Crit Error":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Fraud / Stat Manipulation"],
                            ]
                        ]
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}