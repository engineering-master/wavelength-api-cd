<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationPQFormV2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluationform = EvaluationForm::create(['title' => 'Pre-Qualification Quality Form - V4','description' => 'Pre-Qualification Quality Form(ver 29062020)','headers' => '[]','ratings' => '[]','version' => '1.3','workflow_id' => '1','active' => '1','created_by' => '1000','type' => '2','usr_grp' => 'CG']);

        $questiongroup1 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'CALL COMPLIANCE','percentage' => '30.00','groupmode' => 'default']);
        $questiongroup2 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'COMMUNICATION AND COURTESY','percentage' => '70.00','groupmode' => 'default']);
        $questiongroup3 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'AUTO-FAILURE','percentage' => '-50.00','groupmode' => 'auto-failure']);
        //Question group 1
        $questionlineitem1 = Question::create(['group_id' => $questiongroup1->id,'title' => 'CALL GREETING','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep introduced name, National Disability, provide the call reason?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.0000','comment' => '1','commentmandatory' => '1']);

        $questionlineitem2 = Question::create(['group_id' => $questiongroup1->id,'title' => 'CALL INTRODUCTION','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Did the rep set the claimant's expectation properly?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '20.0000','comment' => '1','commentmandatory' => '1']);
        
        $questionlineitem3 = Question::create(['group_id' => $questiongroup1->id,'title' => 'PRE-QUALIFICATION','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep ask the pre-qualification questions on the script in the ORDER they appear?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '25.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep ask the necessary follow-up question(s) to get the needed information?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep discuss the disqualification verbiage as needed?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.0000','comment' => '1','commentmandatory' => '1']);

        $questionlineitem4 = Question::create(['group_id' => $questiongroup1->id,'title' => 'HANDOFF & TRANSFER','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep follow the hand-off guidelines and script?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '25.0000','comment' => '1','commentmandatory' => '1']);
        //Question group 2

        $questionlineitem5 = Question::create(['group_id' => $questiongroup2->id,'title' => 'COMMUNICATION AND COURTESY','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep listen to the claimant and provide quality responses?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '20.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep avoid verbal collision?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '20.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep address concerns appropriately?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '20.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep deliver the message clearly?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '15.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep sound pleasant and engaging?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep empathize when necessary?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '15.0000','comment' => '1','commentmandatory' => '1']);



        $questionlineitem6 = Question::create(['group_id' => $questiongroup3->id,'title' => 'CRITICAL ERRORS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup3->id,'title' => "Did the rep willfully transfer an unqualified claimant to the next step of the qualification process?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep provide misleading information or have possible intent for fraud?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep fail to display professional behavior in the call?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep dispose the call inappropriately?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup3->id,'title' => "Did the rep disconnect the call without sufficient reason / caller's consent?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep fail to follow the call avoidance policy?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);        
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup3->id,'title' => 'Did the  rep fail to follow the call recording verbiage completely?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);        
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep fail to follow the DNC policy?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
    }
}
