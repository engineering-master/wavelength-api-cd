<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationPQFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluationform = EvaluationForm::create(['title' => 'Pre-Qualification Quality Form','description' => 'Pre-Qualification Quality Form(ver 18122019)','headers' => '[]','ratings' => '[]','version' => '1.2','workflow_id' => '1','active' => '1','created_by' => '1000','usr_grp' => 'CG']);

        $questiongroup1 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'COMPLIANCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup2 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'CUSTOMER EXPERIENCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup3 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'AUTO-FAILURE','percentage' => '-50.00','groupmode' => 'auto-failure']);

        $questionlineitem1 = Question::create(['group_id' => $questiongroup1->id,'title' => 'OPENING','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep open the call correctly?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '15.0000','comment' => '1','commentmandatory' => '1']);

        $questionlineitem2 = Question::create(['group_id' => $questiongroup1->id,'title' => 'SET THE CALL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep set expectations properly?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '10.0000','comment' => '1','commentmandatory' => '1']);
        
        $questionlineitem3 = Question::create(['group_id' => $questiongroup1->id,'title' => 'PRE-QUALIFICATION','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep ask pre-qualification questions on the script in the order they appear?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '15.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep discuss the disqualification verbiage as needed?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '15.0000','comment' => '1','commentmandatory' => '1']);

        $questionlineitem4 = Question::create(['group_id' => $questiongroup1->id,'title' => 'CLOSING /  NEXT STEPS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep follow the transfer guidelines?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '15.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep thank the claimant for their time?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '10.0000','comment' => '1','commentmandatory' => '1']);

        $questionlineitem5 = Question::create(['group_id' => $questiongroup1->id,'title' => 'PROCEDURAL REQUIREMENTS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep update the information accurately?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem5->id,'group_id' => $questiongroup1->id,'title' => 'Did the rep follow the DNC Policy?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '10.0000','comment' => '1','commentmandatory' => '1']);

        $questionlineitem6 = Question::create(['group_id' => $questiongroup2->id,'title' => 'COMMUNICATION & COURTESY','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep listen to the claimant?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '20.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep avoid interrupting / talking over the customer?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '20.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep address concerns appropriately?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '20.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep speak clearly and concisely?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '15.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep sound pleasant and friendly?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '15.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem6->id,'group_id' => $questiongroup2->id,'title' => 'Did the rep empathize  when necessary?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '10.0000','comment' => '1','commentmandatory' => '1']);

        $questionlineitem7 = Question::create(['group_id' => $questiongroup3->id,'title' => 'CRITICAL ERRORS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup3->id,'title' => "Did the rep disconnect the call without sufficient reason / caller's consent?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep fail to follow the call avoidance policy?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep fail to dispose the call appropriately?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep fail to display professional behavior in the call?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep willfully transfer an unqualified claimant to the next step of the qualification process?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem7->id,'group_id' => $questiongroup3->id,'title' => 'Did the rep fail to adhere to the Call Recording Verbiage policy?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":100}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
    }
}
