<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationPQ5QFormV2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title" => "PQ 5Q QA Form v2", "description" => "PQ 5Q QA Form v2", "headers" => "[]", "ratings" => "[]", "version" => "1.0", "workflow_id" => "1", "active" => "1", "created_by" => "1000", "type" => "6", "usr_grp" => "CG"]);
        $form_details = [

            //CRITICAL ERRORS
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "-100.00", "groupmode" => "auto-failure", "title" => "COMPLIANCE"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "CRITICAL ERRORS-OPENING"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Opening - Critical Error - Did the agent fail to follow the prescribed opening spiel?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Call Recording Verbiage - Did the  agent fail to follow the call recording verbiage completely?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "CRITICAL ERRORS-QUALIFYING QUESTIONS"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Skipping of questions - Did the agent skip any of the prequalifying questions?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "CRITICAL ERROR- PRODUCT MASTERY"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Product Mastery & Accuracy - Did the agent provide inaccurate information?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "CRITICAL ERROR-OBJECTION HANDLING"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Rebuttal policy - Did the agent fail to comply with the rebuttal state regulation?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "CRITICAL ERROR-CLOSING"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Invalid or Unauthorized Transfer - Was the transfer invalid and unauthorized by the claimant?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "TCPA/TPMO (Low severity) - Did the agent fail to follow the TCPA/TPMO?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "TCPA/TPMO (High severity) - Did the agent fail to follow the TCPA/TPMO?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Improper Disposition - Did the agent fail to  dispose of the call appropriately?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "ZERO TOLERANCE"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Unprofessional Behavior HIGH"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "DNC Policy"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Fraud / Stat Manipulation"],
                        ]
                    ],
                ]
            ],

            //CALL HANDLING
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "100.00", "groupmode" => "default", "title" => "CLAIMANT HANDLING"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "10.0000", "title" => "GREETING AND CALL INTRODUCTION"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10.00", "title" => "Opening - Did the agent open the call promptly and completely (including call introduction)?"],
                        ]
                    ],

                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "EFFECTIVE PRESENTATION"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Active Listening - Did the agent listen actively throughout the call?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Addressing Concern - Did the agent address all of the claimant's concerns/questions?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Confident Delivery - Was the agent confident in his/her delivery? (Delivering the message clearly, appropriate tone, pace , volume)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Empathy and Responding Appropriately - Did the agent empathize and respond appropriately at all times?"],
                        ]
                    ],

                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":100}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "15.0000", "title" => "OBJECTION HANDLING"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Acknowledged Reason for Objection - Did the agent acknowledge the reason for objection?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Uncovering Underlying Reason for Objection (when needed) - Did the agent uncover the underlying reason for objection (when needed)?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Tailored-value Rebuttal - Did the agent provide a tailored value rebuttal?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Assumptive Transition - Was the agent assumptive in his transition to continue the conversation/sale?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "15.00", "title" => "Objection handling - Did the agent attempt to address the claimant's objection?"],
                        ]
                    ],

                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "10.0000", "title" => "CLOSING"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10.00", "title" => "Closing - Did the agent use the prescribed Intake handoff and transfer spiel or the prescribed DQ spiel? Was the lead not wasted by the agent?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "50.0000", "title" => "PREQUALIFICATION"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10.00", "title" => "Q1 - Did the agent ask questions and receive a clear and affirmative answer before moving to the next question? (SOCIAL SECURITY BENEFITS)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10.00", "title" => "Q2 - Did the agent ask questions and receive a clear and affirmative answer before moving to the next question? (AGE)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10.00", "title" => "Q3 - Did the agent ask questions and receive a clear and affirmative answer before moving to the next question? (LONGEVITY)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10.00", "title" => "Q4 - Did the agent ask questions and receive a clear and affirmative answer before moving to the next question? (WORK CREDITS)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "10.00", "title" => "Q5 - Did the agent ask questions and receive a clear and affirmative answer before moving to the next question?  (ALREADY REPRESENTED)"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "15.0000", "title" => "MEDICARE"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No"]', "score" => '{"Yes":100,"No":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "15.00", "title" => "Did the agent ask the Medicare items completely before transferring to the LIA?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No","NA"]', "score" => '{"Yes":100,"No":0,"NA":100}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "NCE Check"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No","NA"]', "score" => '{"Yes":100,"No":0,"NA":100}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Is this NCE?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Agent driven", "Claimant driven","Process driven","NA"]', "score" => '{"Agent driven":0,"Claimant driven":0,"Process driven":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "NCE Driver"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" =>'["Agent driven", "Claimant driven","Process driven","NA"]', "score" => '{"Agent driven":0,"Claimant driven":0,"Process driven":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "Is this agent driven?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Social Security Benefits","Age","Longevity","Work Credits","Already Represented","US Citizen","NA"]', "score" => '{"Social Security Benefits":0, "Age":0, "Longevity":0, "Work Credits":0, "Already Represented":0, "US Citizen":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Eliminate Reason"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" =>'["Social Security Benefits","Age","Longevity","Work Credits","Already Represented","US Citizen","NA"]', "score" => '{"Social Security Benefits":0, "Age":0, "Longevity":0, "Work Credits":0, "Already Represented":0, "US Citizen":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "What is the eliminate reason?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Lack of probing", "Incorrect probing", "Force transfer", "Skipping of questions", "Active listening", "Verbal collision", "Incorrectly asking PQ questions", "Interested and answered all questions", "NA"]', "score" => '{"Lack of probing":0, "Incorrect probing":0, "Force transfer":0, "Skipping of questions":0, "Active listening":0, "Verbal collision":0, "Incorrectly asking PQ questions":0, "Interested and answered all questions":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "Drilldown Reason"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" =>'["Lack of probing", "Incorrect probing", "Force transfer", "Skipping of questions", "Active listening", "Verbal collision", "Incorrectly asking PQ questions", "Interested and answered all questions", "NA"]', "score" => '{"Lack of probing":0, "Incorrect probing":0, "Force transfer":0, "Skipping of questions":0, "Active listening":0, "Verbal collision":0, "Incorrectly asking PQ questions":0, "Interested and answered all questions":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.00", "title" => "What is the drilldown reason?"],
                        ]
                    ],



                ],



            ],

        ];

        //Do not change this
        foreach ($form_details as $form_detail) {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach ($form_detail["group_data"] as $group_data) {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach ($group_data["questions"] as $question) {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }
    }
}
