<?php

use App\CampaignGroup;
use App\CampaignSkill;
use Illuminate\Database\Seeder;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campaign_datas = [
            'Agent Direct' => [
                'AGENTDIRECT'
            ],
            'Prequal' => [
                'CDinbound',
                'FPSPQ',
                'OUTSOURC'
            ],
            'Client Services' => [
                'CS',
                'CS_NOREC',
                'CS_REC'
            ],
            'Gold' => [
                'GOLD'
            ],
            'Government' => [
                'GOV_CALLS',
                'GOV_CONFIRM'
            ],
            'Intake' => [
                'INTAKE_DIALER'
            ],
            'Last Client Engagement' => [
                'LCE_1'
            ],
            'Medical Provider' => [
                'MED_PRO_CALLBACKS'
            ],
            'Wet Signature' => [
                'WETSIG'
            ]
        ];
        foreach ($campaign_datas as $key => $campaign_data) {
            $CampaignGroup = CampaignGroup::where('name', $key)->first();
            if (!$CampaignGroup) {
                $campaignData = [];
                $campaignData['name'] = strip_tags($key);
                $campaignData['description'] = strip_tags($key);
                $campaignGroup = CampaignGroup::create($campaignData);
            }
            if ($CampaignGroup) {
                foreach ($campaign_data as $campaign_skill) {
                    $CampaignSkill = CampaignSkill::where('name', $campaign_skill)->first();
                    if (!$CampaignSkill) {
                        $campaignSkillData = [];
                        $campaignSkillData['name'] = strip_tags($campaign_skill);
                        $campaignSkillData['description'] = strip_tags($campaign_skill);
                        $campaignSkillData['group_id'] = $CampaignGroup->id;
                        CampaignSkill::create($campaignSkillData);
                    }
                }
            }
        }
    }

}