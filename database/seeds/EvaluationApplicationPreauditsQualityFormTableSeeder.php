<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationApplicationPreauditsQualityFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluationform = EvaluationForm::create(['title' => 'Application Preaudits Quality Form','description' => 'Application Preaudits Quality Form','headers' => '[]','ratings' => '[]','version' => '1.0','workflow_id' => '1','active' => '1','created_by' => '1000','type' => '5','usr_grp' => 'CG']);

        $questiongroup1 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'COMPLIANCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup2 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'AUTO-FAILURE','percentage' => '-50.00','groupmode' => 'auto-failure']);
        //Question group 1
        $questionlineitem1 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Technical Application','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem1->id,'group_id' => $questiongroup1->id,'title' => 'Did the agent use the correct guide?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);

        $questionlineitem2 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Process Application','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Did the agent open the correct case and use the correct ERE file?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent input or update the correct AOD/DLI/App Date from the ERE Table of Contents page?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent accurately input the client's earnings information?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent request for a new Hire report if the earnings are out of date?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent input any alternate contact info for client or alternate contacts?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Has the Agent reviewed the Disability Report Adult and create a MRR for all providers? (there may be more than 1)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem2->id,'group_id' => $questiongroup1->id,'title' => "Do all the Provider Names and Phone Numbers point to the same facility for each MRR?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);



        $questionlineitem3 = Question::create(['group_id' => $questiongroup1->id,'title' => 'Analytical Application','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent create and ERE Record for all providers listed in the ERE under sections E and F?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent review "Section 9 Other Medical Info" for any Workers Comp info?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent review all Disability Report Adults and Appeals?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Are all ERE MRRs formatted correctly as instructed in training?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent obtain all provider fax numbers?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent create any "unique" ERE MRRs with overlapping contact info for another MRR?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent input all the correct DOS for the associated Provider?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent input all CE providers in case comments without a ERE MRR?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent create a new MRR for any providers listed in the F section without being mentioned in the E section?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent set all of the duplicate CSMUs as Duplicate?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent research &  update all unqiue CSMU to be all caps?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent review the case at the end of the pre audit to confirm no duplicate requests exist?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent review the medical audit case for any providers  with request type Client Supplied Records & create ERE RECORD if needed?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem3->id,'group_id' => $questiongroup1->id,'title' => 'Did the Agent review all created MRRs to see if a provider listed as a CE was created in error?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '4.55','comment' => '1','commentmandatory' => '1']);
       
        /*AUTO-FAILURE*/  
        $questionlineitem4 = Question::create(['group_id' => $questiongroup2->id,'title' => 'CRITICAL ERRORS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup2->id,'title' => "Not Researching the client's alleged providers",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);        
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup2->id,'title' => "Using the wrong ERE file and not alerting management right away",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup2->id,'title' => "Agent not correctly listing the DOS for the ERE MRRs",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup2->id,'title' => "Agent missing Client Supplied Records",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);
        $question = Question::create(['parent_question_id' => $questionlineitem4->id,'group_id' => $questiongroup2->id,'title' => "Agent not updating CSMUs",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '1']);

    }
}
