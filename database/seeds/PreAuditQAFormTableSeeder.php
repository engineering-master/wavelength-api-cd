<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class PreAuditQAFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Pre Audit QA Form", "description"=>"Application Preaudits Quality Form", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"7", "usr_grp"=>"CG"]);
        $form_details = [
            //GAINING CLAIMANT'S INTEREST
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"100.00", "groupmode"=>"default", "title"=>"CALL HANDLING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"6.00", "title"=>"Technical Application"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.00", "title"=>"Did the agent use the correct guide, open the right case, and access the appropriate ERE file?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"30.0", "title"=>"Process Application"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.00", "title"=>"Did the Agent input or update the correct AOD/DLI/App Date from the ERE Table of Contents page?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.0", "title"=>"Did the Agent accurately input the client's earnings information?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.0", "title"=>"Did the Agent request for a new Hire report  and exhibit case docs if the earnings are out of date and ERE is not exhibited?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.0", "title"=>"Did the Agent input any AKAs or alternate contact info for client or alternate contacts?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.00", "title"=>"Has the Agent reviewed the Disability Report Adult and appeals and create a MRR for all providers? (there may be more than 1)"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"64", "title"=>"Analytical Application"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.00", "title"=>"Did the agent create an ERE MRR for all providers listed in the ERE under sections E and F?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.0", "title"=>"Did the agent capture 'Disability Report Adult - Section 9 other medical info' for any workers comp info?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Are all ERE MRRs formatted correctly?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.0", "title"=>"Did the Agent obtain all provider fax numbers where available?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.00", "title"=>"Did the Agent create any 'unique' ERE MRRs with overlapping contact info for another MRR?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.0", "title"=>"Did the Agent input the correct DOS for the associated Provider?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.0", "title"=>"Did the Agent log all applicable F section case comments?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.00", "title"=>"Did the agent ensure there are no duplicate CSMU and are formatted correctly?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.0", "title"=>"Did the agent ensure no duplicate ERE MRR exists?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes": 100,"No":0, "NA": 100}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"6.0", "title"=>"Were the RFC generated at Intake set to no longer needed?"],
                            ],
                        ],
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"AUTO FAILURE"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"Critical Errors"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent fail to research the client's alleged providers?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent use the wrong ERE file and fail  to alert the management right away?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent incorrectly list the DOS for the ERE MRRs?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the agent fail to update the CSMUs?"],
                            ]
                        ],
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}