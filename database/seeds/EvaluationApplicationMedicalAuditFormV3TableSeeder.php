<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationApplicationMedicalAuditFormV3TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title" => "Application Medical Audit (MACC) Quality Form - V3", "description" => "Application Follow Up Quality Form v3", "headers" => "[]", "ratings" => "[]", "version" => "3.0", "workflow_id" => "1", "active" => "1", "created_by" => "1000", "type" => "6", "usr_grp" => "CG"]);
        $form_details = [
            //CALL HANDLING
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "50.00", "groupmode" => "default", "title" => "COMPLIANCE"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "PURPOSE OF CALL"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did Agent review the Med Audit case before making the call?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent review any previous notes if any from prior attempt to reach the client?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "OPENING"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent use the correct opening spiel/verbiage?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent confirm the Client is speaking on the phone (Or AUTHORIZED contact)?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent state clearly reason for call?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "If Agent was unable to reach client or autorized contact, did they leave a message(with alt contact or VM box)?"],
                        ]
                    ],
                    [
                        //CONFIRMING INFO
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "NEXT STEPS - CONFIRMING INFO"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did agent confirm security questions?  (Last 4 SSN and Date of Birth before continuing)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent confirm that the client will be able to attend the hearing (or by phone)?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask if the client had worked since Application date?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "If the client has worked was rep able to obtain as much info from the client as they could including when they started, how many hours they work and pay per hour?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask if the client was receiving workers comp or VA benefits?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent confirm if the client had a primary pharmacy"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent review all the facilities in clients SSA file?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent review all the CSMU facilities?	"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent offer to send wellness package?(in the case where client has little to no treatment )"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did agent offer to send paper audit? (in cases where client seems to have difficulty hearing/understanding/or does not want to talk)"],

                        ]
                    ],
                    [
                        //ADDITIONAL INFO
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "NEXT STEPS - ADDITIONAL INFO"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask if the client had a PCP that had not been mentioned yet?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask if the client had an ER visit that had not been mentioned yet?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask if the client had a physical therapy location that had not been mentioned yet?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask if the client had a surgery at a hospital that had not been mentioned yet?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask if the client had any testing(MRI,CT scan) that had not been mentioned yet?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask the client if they had treatment at any pain management facility that had not been mentioned yet?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask if the client had received any mental health treatment from a provider not mentioned?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "DId the agent ask the client if they had in-home or hospice care?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask the client if they had been prescribed a cane/crutches or other medical device or if they felt they needed one?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask the client if they had a handicapped parking placard?(if client had did agent ask for a copy or the application the doctor complete?)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask if the client had access to medical records online using a portal?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "If the client provided additional facilities did agent ensure they used accurate facility info(address/phone number/correct spelling)?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent go over the list of treating providers to be sent treating source statements(TSS)?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "If a TSS provider works at multiple locations did the agent make sure to send only one form instead of sending multiple forms to each location?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent send correct RFC type to correct provider?(Psych RFC to psych providers and PCP only)"],
                        ]
                    ],
                    [
                        //CLIENT REACHED
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "NEXT STEPS - CLIENT REACHED"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "If agent did not reach the client did they leave an appropraite VM?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "If the agent reached an alternate contact or the client could not speak did they confirm when would be a better time to call back?"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "WRAPPING UP"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ask the client if there was anything else they would like to add?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent offer the client CS contact if they had any additional or new treatment?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent inform the client they would be sending out requests and the costs would be covered and the client will be responsible for reimbursement ONLY if they win their claim?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent inform the client their case will be assigned to an advocate and that advocate would contact them typically within 2 weeks?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent thank the client for their time and ask again if they could help with anything?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent review the case after speaking to the client to ensure there was no duplicate request?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent include all pertinent information in their case notes following the call?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "If the client asked any questions did the agent respond appropriately and with correct information?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "When creating MRRs did agent ensure there were no typos for facilities created/provider names and ensure notes had no typos?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "1.62", "title" => "Did the agent ensure correct credentials were used for all providers created for?	"],
                        ]
                    ],
                ]
            ],
            //CUSTOMER EXPERIENCE
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "50.00", "groupmode" => "default", "title" => "CUSTOMER EXPERIENCE"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "CALL CONTROL"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Understand content of conversation and respond correctly"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Handle questions or objections appropriately, Responds appropriately"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Did the rep allow the client to speak without talking over them?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Did the rep have the information needed in order to answer the client's questions without delaying the call"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Listen attentively (ask for names to be spelled or numbers clarified. Repeat back information) Active Listening."],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Did the agent take charge of the conversation?"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Ask effective probing questions"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Respond clearly and concisely  Offer to spell names (if needed)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Sound friendly & confident  Be aware of delay in voice transmission due to internet connection speeds)"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Avoid excessive moments of silence"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Empathize and/or apologize appropriately"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "7.69", "title" => "Escalate the call appropriately"],
                        ]
                    ],
                ]
            ],
            //CRITICAL ERRORS
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "-100.00", "groupmode" => "auto-failure", "title" => "CRITICAL ERRORS"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => "AUTO-FAILURE"],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Failure to verify client using last 4 of SSN and Date of Birth"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Failure to inform client they are on a recorded line		"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Disconnect or avoid the call"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Display unprofessional behavior on the call"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Disclosing PHI with non authorized contact\"(Protect Health Information)\""],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Not being prepared for call - no review of previous notes"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Failure to update clients contact info if info is provided"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Misrepresent the company"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "NA"]', "score" => '{"Yes":100,"No":0,"NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "-10.0000", "title" => "Falsify information in notes, about actions being taken, or client info"],
                        ]
                    ]
                ]
            ],
        ];

        //Do not change this
        foreach ($form_details as $form_detail) {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach ($form_detail["group_data"] as $group_data) {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach ($group_data["questions"] as $question) {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }
    }
}
