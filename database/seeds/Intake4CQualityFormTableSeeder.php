<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class Intake4CQualityFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title" => "Intake 4Cs Quality Form", "description" => "Intake 4Cs Quality Form", "headers" => "[]", "ratings" => "[]", "version" => "1.0", "workflow_id" => "1", "active" => "1", "created_by" => "1000", "type" => "6", "usr_grp" => "CG"]);
        $form_details = [
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Connection"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Gain the claimant's attention"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Acknowledge the claimant's emotion"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Engaging Tone of voice"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Casual & Conversational"],
                        ]
                    ],
                ],
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Compatibility"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Provide a value prop tailored to the claimant's circumstance"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Showcase the features, advantages and benefits of being represented by CD"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Effective delivery"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Show genuine interest to understand the claimants situation and offer CD’s help"],
                        ]
                    ],
                ],
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Control"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Listens attentively and responds appropriately"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Lead the conversation"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Easily understood"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Be brief and concise"],
                        ]
                    ],
                ],
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Conduct"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Does not demonstrate unprofessional behaviors"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Does not mislead"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Always provides accurate information"],
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Yes", "No", "Needs Improvement" , "NA"]', "score" => '{"Yes":0,"No":0,"Needs Improvement":0, "NA":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Always use the Salesforce Script/PQ Guide"],
                        ]
                    ],
                ],
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Not Interested"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Agent Driven", "Claimant did not agree/wants to think about it", "Unknown reason for disconnecting/Tech issues"]', "score" => '{"Agent Driven":0,"Claimant did not agree/wants to think about it":0}', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Agent Driven", "Claimant did not agree/wants to think about it","Unknown reason for disconnecting/Tech issues"]', "score" => '{"Agent Driven":0,"Claimant did not agree/wants to think about it":0, "Unknown reason for disconnecting/Tech issues":0}', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Claimant's Reason for Saying No (L1)"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["(L1.1) Failed to handle objection/concern","(L1.2) Ineffective response to objection/concern","(L1.3) Unprofessional Behavior","(L1.4) Poor Call handling","(L1.5) Poor Communication Skills","(L1.6) Poor explanation of the purpose of the call","(L1.7) Poor introduction about CD","(L1.8) NA"
                            ]', "score" => '{"(L1.1) Failed to handle objection/concern":0,(L1.2) Ineffective response to objection/concern":0,"(L1.3) Unprofessional Behavior":0,"(L1.4) Poor Call handling":0,"(L1.5) Poor Communication Skills":0,"(L1.6) Poor explanation of the purpose of the call":0,"(L1.7) Poor introduction about CD":0,"(L1.8) NA":0}', "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["(L1.1) Failed to handle objection/concern","(L1.2) Ineffective response to objection/concern","(L1.3) Unprofessional Behavior","(L1.4) Poor Call handling","(L1.5) Poor Communication Skills","(L1.6) Poor explanation of the purpose of the call","(L1.7) Poor introduction about CD","(L1.8) NA"
                            ]', "score" => '{"(L1.1) Failed to handle objection/concern":0,(L1.2) Ineffective response to objection/concern":0,"(L1.3) Unprofessional Behavior":0,"(L1.4) Poor Call handling":0,"(L1.5) Poor Communication Skills":0,"(L1.6) Poor explanation of the purpose of the call":0,"(L1.7) Poor introduction about CD":0,"(L1.8) NA":0}', "actual_score" => "0.0000", "title" => "Agent Driven Reason L1"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["SSN","Fee","Timeline","Skeptic about CD","Wants to apply on their own","Other","NA"
                        ]', "score" => '{"SSN":0,"Fee":0,"Timeline":0,"Skeptic about CD":0,"Wants to apply on their own":0,"Other":0,"NA":0}', "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["SSN","Fee","Timeline","Skeptic about CD","Wants to apply on their own","Other","NA"
                            ]', "score" => '{"SSN":0,"Fee":0,"Timeline":0,"Skeptic about CD":0,"Wants to apply on their own":0,"Other":0,"NA":0}', "actual_score" => "0.0000", "title" => "Agent Driven Reason L2 (L1.1 & L1.2)"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Did not actively listen","Did not empathize","Verbal collision","NA"
                        ]', "score" => '{"Did not actively listen":0,"Did not empathize":0,"Verbal collision":0,"NA":0}', "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Did not actively listen","Did not empathize","Verbal collision","NA"
                            ]', "score" => '{"Did not actively listen":0,"Did not empathize":0,"Verbal collision":0,"NA":0}', "actual_score" => "0.0000", "title" => "Agent Driven Reason L2 (L1.4)"],
                        ]
                    ],
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '["Grammar","Pronunciation","Pacing","Delivery","NA"
                        ]', "score" => '{"Grammar":0,"Pronunciation":0,"Pacing":0,"Delivery":0,"NA":0}', "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '["Grammar","Pronunciation","Pacing","Delivery","NA"
                            ]', "score" => '{"Grammar":0,"Pronunciation":0,"Pacing":0,"Delivery":0,"NA":0}', "actual_score" => "0.0000", "title" => "Agent Driven Reason L2 (L1.5)"],
                        ]
                    ],
                ],
            ],
            [
                "group" => ["evaluation_form_id" => "#evaluation_form_id#", "percentage" => "0.00", "groupmode" => "no-score", "title" => "Call Summary"],
                "group_data" =>
                [
                    [
                        "lineitem" => ["group_id" => "#group_id#", "type" => "lineitems", "mandatory" => "0", "values" => '', "score" => '', "comment" => "0", "commentmandatory" => "0", "actual_score" => "0.0000", "title" => ""],
                        "questions" =>
                        [
                            ["group_id" => "#group_id#", "parent_question_id" => "#parent_question_id#", "type" => "radio", "mandatory" => "0", "values" => '', "score" => '', "comment" => "1", "commentmandatory" => "1", "actual_score" => "0.0000", "title" => "Call Summary"],
                        ],


                    ]
                ],
            ],
        ];

        //Do not change this
        foreach ($form_details as $form_detail) {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach ($form_detail["group_data"] as $group_data) {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach ($group_data["questions"] as $question) {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }
    }
}
