<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationPQFormV8_5TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"PQ  Quality Form  v8.5", "description"=>"PQ  Quality Form  v8.5", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"6", "usr_grp"=>"CG"]);
        $form_details = [

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CRITICAL ERRORS-OPENING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERRORS-OPENING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Opening- Critical Error Did the rep fail to follow the prescribed opening spiel?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Call Recording Verbiage - Did the  rep fail to follow the call recording verbiage completely?"],
                            ]
                        ]
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CRITICAL ERRORS-QUALIFYING QUESTIONS"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERRORS-QUALIFYING QUESTIONS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Skipping Question/s"],
                            ]
                        ]
                    ]
            ],
            
            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CRITICAL ERROR- PRODUCT MASTERY"],
                "group_data"=>

                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERROR- PRODUCT MASTERY"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Product Mastery & Accuracy - Did the rep provide inaccurate information?"],
                            ]
                        ]
                    ]
            ],
            
            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CRITICAL ERROR-OBJECTION HANDLING"],
                "group_data"=>

                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERROR-OBJECTION HANDLING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"No Rebuttal - No rebuttal state only - Did the rep fail to comply with the No Rebuttal State regulation?"],
                            ]
                        ]
                    ]
                
            ],
            
            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CRITICAL ERROR-CLOSING"],
                "group_data"=>

                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CRITICAL ERROR-CLOSING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Invalid or Unauthorized Transfer - Was the transfer vinalid and unauthorized by the claimant?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Transfer Spiel (TCPA/TPMO) - Did the agent fail to use the prescribed transfer spiel?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Improper Disposition - Did the rep fail to  dispose of the call inappropriately?"],
                            ]
                        ]
                    ]
                    
            ],
            
            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"ZERO TOLERANCE"],
                "group_data"=>

                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"ZERO TOLERANCE"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Unprofessional Behavior HIGH"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"DNC Policy"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Fraud / Stat Manipulation"],
                            ]
                        ]
                    ]
            ],



            //CALL HANDLING
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"50.00", "groupmode"=>"default", "title"=>"CLAIMANT HANDLING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"GREETING AND CALL INTRODUCTION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Opening - Did the rep open the call promptly and completely (including call introduction)?"],
                            ]
                        ], 

                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"EFFECTIVE PRESENTATION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Active Listening - Did the rep listen actively throughout the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Addressing Concern - Did the rep address all of the claimant's concerns/questions?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Confident Delivery - Was the agent confident in his/her delivery? (Deliveringthe Message clearly, appropriate tone, pace , volume)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Empathy and Responding Appropriately - Did the agent empathize and Respond Appropriately at all times?"],
                            ]
                        ], 

                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"OBJECTION HANDLING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"12.00", "title"=>"Acknowledged Reason for Objection - Did the agent acknowledge the reason for objection?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"8.00", "title"=>"Uncovering underlying reason for Objection (when needed) - Did the agent uncover the underlying reason for objection (when needed)?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"12.00", "title"=>"Tailored Valued Objection - Did the agent provide a tailored value rebuttal?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"8.00", "title"=>"Assumptive Transition - Was the agent assumptive in his transition to continue the conversation/sale?"],
                            ]
                        ], 
                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"CLOSING"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Handoff and Transfer Spiel- Intake Transfers - Did the agent use the prescribed Intake  handoff and transfer spiel?"],
                            ]
                        ], 

                        
                    ],



            ],


            //QUALIFYING THE CLAIMANT
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"50.00", "groupmode"=>"default", "title"=>"CLAIMANT HANDLING"],
                "group_data"=>
                    [
                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"PREQUALIFICATION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q1 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question? (SOCIAL SECURITY BENEFITS)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.50", "title"=>"Q2 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question? (MILITARY)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.50", "title"=>"Q3 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question? (AGE) "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q4 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question? (LONGEVITY)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.50", "title"=>"Q5 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?  (LAST TIME WORKED)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q6 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question? (WORK TIME)       "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.50", "title"=>"Q7 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question? (LONGEVITY 2)      "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.50", "title"=>"Q8 - Did the rep ask questions (main and follow up questions) before moving to the next question? (MEDICAL HISTORY)    "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.50", "title"=>"Q9 - Did the rep ask questions (main and follow up questions) before moving to the next question? (ALREADY REPRESENTED)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.50", "title"=>"Q10 - Did the rep ask questions (main and follow up questions) before moving to the next question?  (US CITIZEN)     "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.50", "title"=>"Q11- Did the rep ask questions  (main and follow up questions) before moving to the next question?  (SS TAXES) "],
                            ]
                        ], 

                        
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"MEDICARE"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No","NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"50.00", "title"=>"Did the rep ask the Medicare items completely before transferring to the LIA?"],
                            ]
                        ], 
                        

                        
                    ],


                    
            ],
            


            //
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"0.00", "groupmode"=>"default", "title"=>"NCE CHECK"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"NCE CHECK"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No", "NA"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Is this NCE?"],
                            ]
                        ], 

                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Valid", "Invalid", "NA"]', "score"=>'{"Valid":100,"Invalid":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"NCE Driver"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Valid", "Invalid", "NA"]', "score"=>'{"Valid":100,"Invalid":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Is this NCE?"],
                            ]
                        ], 

                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Age","Longevity","Longevity 2","Last time worked","Work Time","Work Credits","Social Security Benefits","Medical History","Post Hearing","Already Represented","Citizen","Income","SS Taxes","CD Client","NA"]', "score"=>'{"Age":0,"Longevity":0,"Longevity 2":0,"Last time worked":0,"Work Time":0,"Work Credits":0,"Social Security Benefits":0,"Medical History":0,"Post Hearing":0,"Already Represented":0,"Citizen":0,"Income":0,"SS Taxes":0,"CD Client":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"Eliminate Reason"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Age","Longevity","Longevity 2","Last time worked","Work Time","Work Credits","Social Security Benefits","Medical History","Post Hearing","Already Represented","Citizen","Income","SS Taxes","CD Client","NA"]', "score"=>'{"Age":0,"Longevity":0,"Longevity 2":0,"Last time worked":0,"Work Time":0,"Work Credits":0,"Social Security Benefits":0,"Medical History":0,"Post Hearing":0,"Already Represented":0,"Citizen":0,"Income":0,"SS Taxes":0,"CD Client":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"What is the eliminate reason?"],
                            ]
                        ], 

                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Lack of Probing","Force Transfer","Incorrect Probing","Incorrectly asking PQ Questions","Skipping PQ Question","Active Listening","Verbal Collision","NA"]', "score"=>'{"Lack of Probing":0,"Force Transfer":0,"Incorrect Probing":0,"Incorrectly asking PQ Questions":0,"Skipping PQ Question":0,"Active Listening":0,"Verbal Collision":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.0000", "title"=>"Drilldown Reason"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Lack of Probing","Force Transfer","Incorrect Probing","Incorrectly asking PQ Questions","Skipping PQ Question","Active Listening","Verbal Collision","NA"]', "score"=>'{"Lack of Probing":0,"Force Transfer":0,"Incorrect Probing":0,"Incorrectly asking PQ Questions":0,"Skipping PQ Question":0,"Active Listening":0,"Verbal Collision":0,"NA":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"What is the drilldown reason?"],
                            ]
                        ],

                        
                    ],



            ],

        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}