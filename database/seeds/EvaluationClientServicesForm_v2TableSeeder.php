<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationClientServicesForm_v2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"Client Services Quality Form v2", "description"=>"Client Services Quality Form v2", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"6", "usr_grp"=>"CG"]);
        $form_details = [
            //GAINING CLAIMANT'S INTEREST
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"100.00", "groupmode"=>"default", "title"=>"CALL HANDLING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"5.00", "title"=>"Opening"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the agent introduce himself and brand the company?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"20.00", "title"=>"Issue Identification"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the agent understand the CL's reason or calling?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the agent actively listen?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the agent probe appropriately?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"20.00", "title"=>"Issue Resolution"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the agent answer/address all questions/concerns?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Was the agent explanation easily understood by the claimant?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the agent follow the recommended retention technique? (acknowledge, uncover, tailor-value response)"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"30.00", "title"=>"Value Add"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the agent proactively address all relevant cases, notes from history, and docs in notes & attachments? "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the agent set proper expectation on the next steps?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the agent provide alternative resolution when needed?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"Did the agent sound confident and courteous on the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent acknowledge the concern and empathize as needed?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Did the agent follow proper phone etiquette?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"5.00", "title"=>"Closing"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"3.00", "title"=>"Did the agent ask if there was anything else they could assist with?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"2.00", "title"=>"Did the agent do a recap before ending the call?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"20.00", "title"=>"After Call Work"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the agent's notes match their call? "],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Did the agent complete all follow up work needed for the call?"],
                            ],
                        ],
                        
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CRITICAL ERROR"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CE - Opening"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to verify who they are speaking to and the last 4 digits of SSN?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to mention the call recording verbiage?"],
                            ]
                        ],
                        [
                        "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CE - Verification"],
                        "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to verify updated Claimant contact information (address, phone number, Authorized Contact)?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CE - Issue Resolution"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to build appropriate cases when a client wished to stop the process?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to notify anyone when a CL threatens to sue?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep attach appeal paperwork for the wrong CL?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to provide accurate information?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CE - Value Add"],
                            "questions"=>
                                [
                                    ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to follow proper phone etiquette?"],
                                ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"Unprofessional Behavior"],
                            "questions"=>
                                [
                                    ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Was the rep unprofessional during the call?"],
                                ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"DNC"],
                            "questions"=>
                                [
                                    ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep fail to follow the DNC Policy?"],
                                ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"Fraud"],
                            "questions"=>
                                [
                                    ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep commit any fraudulent act?"],
                                ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"Stat Manipulation"],
                            "questions"=>
                                [
                                    ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Did the rep do anything to boost the metrics?"],
                                ]
                        ],
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}