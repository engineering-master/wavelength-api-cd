<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'user_id'       => 1000,
                'dim_user_id'   => 'EC0001',
                'site'          => 1,
                'name'          => 'Super Admin',
                'email'         => 'portal@admin.com',
                'usr_grp'       => 'CG'
            ]
        );
    }

}