<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationApplicationMasterQAInvoicesFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluationform = EvaluationForm::create(['title' => 'Application QA Invoices Form','description' => 'Application QA Invoices Form','headers' => '[]','ratings' => '[]','version' => '1.0','workflow_id' => '1','active' => '1','created_by' => '1000','type' => '2','usr_grp' => 'CG']);

        $questiongroup1 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'COMPLIANCE','percentage' => '50.00','groupmode' => 'default']);
        $questiongroup2 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'CUSTOMER EXPERIENCE','percentage' => '50.00','groupmode' => 'default']);
        $questiongroup3 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'AUTO-FAILURE','percentage' => '-100.00','groupmode' => 'auto-failure']);
        //Question Group 1
        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'PURPOSE OF CALL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'Review any previous notes determine reason why you are calling?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        

        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'OPENING','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
                ','score' => '{"Yes":100,"No":0,"NA":0}
                ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'Correct use of call tree/options when offered','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'Use correct call opening greeting/verbiage  Obtain name of person speaking with','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'Confirm the Providers Name if not specified on the phone\'s answering recording','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);


        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'SET THE CALL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Confirm correct dept reached (ie med records or ask to be transferred)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Redo intro if you have been transferred",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);


        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'NEXT STEPS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "State clearly reason for call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Provide correct request type in opening (I'm calling about X request)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);


        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'Asked Follow Up Questions','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Give provider correct CL name/info - including AKA if needed",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Confirm with provider or copy service if invoice can be paid over the phone by credit card",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Confirm the invoice details (invoice #, amount) to confirm correct invoice is being paid.",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Provide correct Credit Card number to the provider.",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Obtain authorization code or verbally ensure with the provider that the payment was processed.",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Once payment is completed, did the Agent confirm how the records would be sent?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent confirm our contact info for the Provider's preferred method of sending records?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Confirm Date records will be sent",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        
        //Invoice Follow Up
        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'Invoice Follow Up','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent provide the payment or dispute details to the provider/copy service?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent follow up on where the records were previously sent?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "How were they sent (mail (paper, USB, CD), fax email)?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Confirm provider's mailing address, fax number, email address being used to send records from",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Date they were sent?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "If it has been over 2 weeks ask if the records can be resent.  If under 2 weeks advise provider we will look for them.",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);

        //If records have not been sent
        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'If records have not been sent','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "When will they?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "How will they be sent?  Mail, Fax, Email? (Email preferred method)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Confirm provider's mailing address, fax number, email address being used to send records from",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Confirm who you are speaking with and a good callback number if needed",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "If you need to leave a voicemail, all appropriate details are left for the provider.",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Thank the respondent for their time ",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Select correct disposition in the guide",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Make detailed notes summarizing call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);

        //When Processing Invoice - Procedural Application
        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'When Processing Invoice - Procedural Application','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent confirm the invoice is for the correct MRR?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent confirm the invoice is compliant with State Statue? If not, did the Agent escalate?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent process the invoice correctly per SOP (i.e. CIOX inv paid online, not by check)?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent input the correct dollar amount?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent correctly enter the address for check payment?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent record any confirmation #/authorization codes provided?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent use the correct formatting per SOP when completing the guide (i.e. include invoice #)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "If payment is made online, was the correct dollar amount and client info entered correctly?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '2.64','comment' => '1','commentmandatory' => '0']);


        //Question Group 2: CUSTOMER EXPERIENCE
        $questionlineitem = Question::create(['group_id' => $questiongroup2->id,'title' => 'CALL CONTROL','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Understand content of conversation and respond correctly",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Handle questions or objections appropriately, Responds appropriately",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Does the rep allow the provider to speak without talking over them?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Does the rep have the information needed in order to answer the provider's questions without delaying the call?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Does not repeat questions or ask for repeat answers excessively",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Listen attentively (ask for names to be spelled or numbers clarified. Repeat back information) Active Listening.  ",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Ask effective probing questions",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Respond clearly and concisely,  Offer to spell names (if needed)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Sound friendly & confident (Be aware of delay in voice transmission due to internet connection speeds)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Avoid excessive dead air ",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Empathize and/or apologize appropriately",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Escalate the call appropriately",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '7.70','comment' => '1','commentmandatory' => '0']);


        //Question Group 3: AUTO-FAILURE
        $questionlineitem = Question::create(['group_id' => $questiongroup3->id,'title' => 'CRITICAL ERRORS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup3->id,'title' => "Disconnect or avoid the call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup3->id,'title' => "Display unprofessional behavior in the call",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup3->id,'title' => "Disclosing HIPAA protected information to wrong provider",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup3->id,'title' => "Not being prepared for call - no review of previous notes",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup3->id,'title' => "Failure to update Fax, Phone, Provider or Copy Service fields if info is provided",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup3->id,'title' => "Misrepresent the company",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup3->id,'title' => "Falsify information in notes, about actions being taken, or client info",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
    }

}