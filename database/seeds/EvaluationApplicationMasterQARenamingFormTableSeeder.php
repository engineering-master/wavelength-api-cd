<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationApplicationMasterQARenamingFormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evaluationform = EvaluationForm::create(['title' => 'Application QA Renaming Form','description' => 'Application QA Renaming Form','headers' => '[]','ratings' => '[]','version' => '1.0','workflow_id' => '1','active' => '1','created_by' => '1000','type' => '5','usr_grp' => 'CG']);

        $questiongroup1 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'COMPLIANCE','percentage' => '100.00','groupmode' => 'default']);
        $questiongroup2 = QuestionGroup::create(['evaluation_form_id' => $evaluationform->id,'title' => 'AUTO-FAILURE','percentage' => '-100.00','groupmode' => 'auto-failure']);
        
        //Question Group 1
        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'Technical Application','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent use the correct guide?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $questionlineitem = Question::create(['group_id' => $questiongroup1->id,'title' => 'Procedural Application','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent Identify the correct client?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent Identify the correct MRR#?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent confirm the document type correctly?  (MISC/INV/Recs/RFC)?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Agent separated any combined documents appropriately (RFCs from Records)",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Are all the pages correctly ordered and nothing upside down?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent confirm completeness of the fax?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "If the document is not complete, did the Agent process per SOP and handle the incomplete document correctly?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "If the document is a RFC form, did the Agent format the MRR per SOP? (RFC type, Date signed, etc)?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "If a new MRR was created, did the Agent use the correct provider for the new MRR?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Did the Agent manually attach the file(s) if applicable?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => "Were Records sent in Lieu of completing the RFC?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'Did Agent make note that the records were sent "In Lieu of"?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'If escalated, did the agent correctly note how the file was processed and use the correct MRR?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'If RFC received, did the agent confirm it was formatted correctly?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
                ','score' => '{"Yes":100,"No":0,"NA":0}
                ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'For file size too large, did the agent attach the file correctly?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
                ','score' => '{"Yes":100,"No":0,"NA":0}
                ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'If MRR was created, during review did the agent confirm the correct DOS and address were used?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
                ','score' => '{"Yes":100,"No":0,"NA":0}
                ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'When combining additional records did the agent order pages correctly?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
                ','score' => '{"Yes":100,"No":0,"NA":0}
                ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'During the review did the agent confirm pages are oriented correctly?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
                ','score' => '{"Yes":100,"No":0,"NA":0}
                ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup1->id,'title' => 'During the review did the agent confirm the attached file is complete?','type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
                ','score' => '{"Yes":100,"No":0,"NA":0}
                ','actual_score' => '5.00','comment' => '1','commentmandatory' => '0']);


        //Question Group 2: AUTO-FAILURE  
        $questionlineitem = Question::create(['group_id' => $questiongroup2->id,'title' => 'CRITICAL ERRORS','type' => 'lineitems','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '0.0000','comment' => '0','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent rename the document to the wrong client?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent rename the document to the wrong MRR?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent fail to correct documents per the SOP?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent select the wrong document type?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent fail to check that all the records are for the correct client?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
        $question = Question::create(['parent_question_id' => $questionlineitem->id,'group_id' => $questiongroup2->id,'title' => "Did the Agent fail to manually attach any documents?",'type' => 'radio','mandatory' => '0','values' => '["Yes","No","NA"]
        ','score' => '{"Yes":100,"No":0,"NA":0}
        ','actual_score' => '-10.0000','comment' => '1','commentmandatory' => '0']);
    }

}