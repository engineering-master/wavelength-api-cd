<?php

use Illuminate\Database\Seeder;
use App\EvaluationDispositionDefinitions;

class EvaluationDispositionDefinitionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EvaluationDispositionDefinitions::unguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        EvaluationDispositionDefinitions::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        EvaluationDispositionDefinitions::create(['id' => '1','name' => 'Critical - Fraud','severity' => '1','description' => 'High']);
        EvaluationDispositionDefinitions::create(['id' => '2','name' => 'Critical - Campaign Escalation','severity' => '1','description' => 'High']);
        EvaluationDispositionDefinitions::create(['id' => '3','name' => 'Critical - Agent Escalation','severity' => '1','description' => 'High']);
        EvaluationDispositionDefinitions::create(['id' => '4','name' => 'Coaching - Auto Fail','severity' => '1','description' => 'High']);
        EvaluationDispositionDefinitions::create(['id' => '5','name' => 'Low Compliance score','severity' => '2','description' => 'Medium']);
        EvaluationDispositionDefinitions::create(['id' => '6','name' => 'Low Quality score','severity' => '2','description' => 'Medium']);
        EvaluationDispositionDefinitions::create(['id' => '7','name' => 'Default','severity' => '3','description' => 'Low']);
    }
}
