<?php

use Illuminate\Database\Seeder;
use App\EvaluationStatusMaster;

class EvaluationStatusMastersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EvaluationStatusMaster::create([
                'id' => 1,
                'status' => 'Assigned',
                'description' => 'Assigned' 
            ]
        );
        EvaluationStatusMaster::create([
                'id' => 2,
                'status' => 'In Progress',
                'description' => 'In Progress' 
            ]
        );
        EvaluationStatusMaster::create([
                'id' => 3,
                'status' => 'Yet to be acknowledged',
                'description' => 'Yet to be acknowledged' 
            ]
        );
        EvaluationStatusMaster::create([
                'id' => 4,
                'status' => 'Acknowledged',
                'description' => 'Acknowledged' 
            ]
        );
        EvaluationStatusMaster::create([
                'id' => 5,
                'status' => 'Disputed',
                'description' => 'Disputed' 
            ]
        );
        EvaluationStatusMaster::create([
                'id' => 6,
                'status' => 'Dispute Accepted',
                'description' => 'Dispute Accepted' 
            ]
        );
        EvaluationStatusMaster::create([
                'id' => 7,
                'status' => 'Dispute Rejected',
                'description' => 'Dispute Rejected' 
            ]
        );
        EvaluationStatusMaster::create([
                'id' => 8,
                'status' => 'In Reevaluation',
                'description' => 'In Reevaluation' 
            ]
        );
        EvaluationStatusMaster::create([
                'id' => 9,
                'status' => 'Reevaluated',
                'description' => 'Reevaluated' 
            ]
        );
        EvaluationStatusMaster::create([
                'id' => 10,
                'status' => 'Closed',
                'description' => 'Closed' 
            ]
        );
    }
}
