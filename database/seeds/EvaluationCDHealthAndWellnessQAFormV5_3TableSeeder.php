<?php

use Illuminate\Database\Seeder;
use App\EvaluationForm;
use App\QuestionGroup;
use App\Question;

class EvaluationCDHealthAndWellnessQAFormV5_3TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {
        //Form
        $evaluationform = EvaluationForm::create(["title"=>"CD Health and Wellness - QA Form - V5.3", "description"=>"CD Health and Wellness - QA Form - V5.3", "headers"=>"[]", "ratings"=>"[]", "version"=>"1.0", "workflow_id"=>"1", "active"=>"1", "created_by"=>"1000", "type"=>"6", "usr_grp"=>"CG"]);
        $form_details = [
            //GAINING CLAIMANT'S INTEREST
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"100.00", "groupmode"=>"default", "title"=>"CALL HANDLING"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"7.00", "title"=>"GAINING CLAIMANT'S INTEREST"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"7.00", "title"=>"Opening - Did the rep open the call promptly and completely (including call introduction)?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"83.00", "title"=>"QUALIFYING THE CLAIMANT"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q1 -  Did the rep ask the question (main and follow up questions) before moving to the next question? (TYPE OF BENEFITS RECEIVED)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"8.00", "title"=>"Q2 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?  (HEALTH INSURANCE)  
                                What do you have for health insurance?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"8.00", "title"=>"Q3 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?(HEALTH INSURANCE)                
                                Who do you currently have for your health insurance?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q4 - Did the rep ask questions and receive a clear and affirmative answer (main and follow up questions) before moving to the next question?  (CHANGE IN PLAN WITHIN THE LAST 3 MONTHS)
                                Have you changed your insurance plans in the last 3 months?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Q5 - Did the rep ask questions (main and follow up questions) before moving to the next question? (CHILDREN BELOW 18 )
                                Do you have any children under the age of 18 when you were awarded disability benefits?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Q6 - Did the rep ask questions (main and follow up questions) before moving to the next question? (SPOUSE )  
                                Did you have a spouse who passed away within 7 years of you being found disabled?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q7 - Did the rep ask questions (main and follow up questions) before moving to the next question? (MILITARY)
                                Were you ever in the military?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"4.00", "title"=>"Q8 - Did the rep ask questions (main and follow up questions) before moving to the next question? (PRESCRIPTION PAYMENT)
                                What do you pay for prescriptions"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Q9 - Did the rep ask questions (main and follow up questions) before moving to the next question?  (PRESCRIPTION COUNT)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"5.00", "title"=>"Q10 - Did the rep ask questions  (main and follow up questions) before moving to the next question?  (PRESCRIPTION FILL)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"15.00", "title"=>"Eligible Pitch/es - Did the rep offer elligble pitch/es?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"0.00", "title"=>"EFFECTIVE PRESENTATION"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Active Listening - Did the rep listen actively throughout the call?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Addressing Concern - Did the rep address the claimant's concern/question?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Confident Delivery - Was the agent confident in his/her delivery? (Delivering the Message clearly, appropriate tone, pace, volume)"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Empathy and Responding Appropriately - Did the rep respond appropriately and/or empathize when necessary?"],
                            ],
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"10.00", "title"=>"HANDLING OBJECTIONS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Acknowledged Reason for Objection - Did the rep acknowledge the reason for objection?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Uncovering underlying reason for Objection - Did the agent uncover the underlying reason for objection (when needed)?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 0,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Tailored Valued Rebuttal - Did the agent provide a tailored value rebuttal?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"0.00", "title"=>"Assumptive Transition - Was the agent assumptive in his transition to continue the conversation/sale?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes": 100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"10.00", "title"=>"Objection Handling Process - Did the rep attempt to address the claimant's objection? (Allowed rebuttal State)"],
                            ],
                        ],
                    ]
            ],

            //CRITICAL ERRORS
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"CRITICAL ERROR"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR - GAINING CLAIMANT'S INTEREST"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Opening - Did the rep fail to utilize the Opening spiel in verbatim?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Call Recording Verbiage -Did the rep provide the call recording spiel?"],
                            ]
                        ],
                        [
                        "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR - EFFECTIVE PRESENTATION"],
                        "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Misleading - Did the rep provide misleading statements that influenced the claimant's decision making?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR - HANDLING OBJECTIONS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Rebuttal Policy - Did the rep fail comply with the rebuttal regulation?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR - QUALIFYING QUESTIONS"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Skipping of Questions - Did the rep skip any of the 10 questions?"],
                            ]
                        ],
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"CRITICAL ERROR - CLOSE"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Invalid Sale - Did the rep transfer ineligible claimant?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Pitches and Handoff Spiel - Did the rep fail to use the prescribed spiel when providing the pitch,  fail to use the transfer spiel and fail  seek claimant's authorization to be transferred?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Improper Disposition - Did the rep dispose of the call inappropriately  (except for DNC)?"],
                            ]
                        ],
                    ]
            ],
            //ZTP
            [
                "group"=>["evaluation_form_id"=>"#evaluation_form_id#", "percentage"=>"-100.00", "groupmode"=>"auto-failure", "title"=>"ZTP"],
                "group_data"=>
                    [
                        [
                            "lineitem"=>["group_id"=>"#group_id#", "type"=>"lineitems", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0,"NA":0}', "comment"=>"0", "commentmandatory"=>"0", "actual_score"=>"-100.00", "title"=>"ZTP"],
                            "questions"=>
                            [
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Unprofessional Behavior - The rep  fail  to display professional behavior?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"DNC Policy - Did the rep fail to comply to DNC policy?"],
                                ["group_id"=>"#group_id#", "parent_question_id"=>"#parent_question_id#", "type"=>"radio", "mandatory"=>"0", "values"=>'["Yes", "No"]', "score"=>'{"Yes":100,"No":0}', "comment"=>"1", "commentmandatory"=>"1", "actual_score"=>"-10.0000", "title"=>"Fraud / Stat Manipulation - Did the rep take deliberate action to improve his scorecard?"],
                            ]
                        ],
                    ]
            ],
        ];

        //Do not change this
        foreach($form_details as $form_detail)
        {
            $form_detail["group"]["evaluation_form_id"] = $evaluationform->id;
            $group = QuestionGroup::create($form_detail["group"]);
            foreach($form_detail["group_data"] as $group_data) 
            {
                $group_data["lineitem"]["group_id"] = $group->id;
                $lineitem = Question::create($group_data["lineitem"]);
                foreach($group_data["questions"] as $question)
                {
                    $question["group_id"] = $group->id;
                    $question["parent_question_id"] = $lineitem->id;
                    Question::create($question);
                }
            }
        }

    }
}