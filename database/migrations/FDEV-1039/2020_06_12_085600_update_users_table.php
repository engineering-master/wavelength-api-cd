<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\Employee;
use App\NonEmployeeUser;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        $employees = Employee::Active()->where('supervisor_user_id','CDEAV11')->get();
        $managerid = NonEmployeeUser::where('username','CDEAV11')->first();

         foreach($employees as $user){
             $app_user = User::where('dim_user_id', $user->user_id)->first();
             if($app_user) {
                $app_user->suerpvisorid = $user->supervisor_user_id;
                $app_user->save();
             }             
             $nonEmployeeUser = NonEmployeeUser::where('username',$user->user_id)->first();     
             if($nonEmployeeUser) {
                $nonEmployeeUser->manager_id = $managerid->id; 
                $nonEmployeeUser->save();
             }             
         }  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
