<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\NonEmployeeUser;
use App\NonEmployeeUserSystemMapping;
use Carbon\Carbon;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $nonEmployeeUser = NonEmployeeUser::where( 'updated_at', '>', Carbon::now()->subDays(30))->get();
        foreach($nonEmployeeUser as $empuser){
            $user = User::where('user_id',$empuser->id)->first();
            if($user){
                $managers = NonEmployeeUserSystemMapping::select('system_user_id')->where('user_id', '=', $empuser->manager_id)->pluck('system_user_id');
                $user->suerpvisorid=isset($managers[0]) ? $managers[0] : "";
                $user->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
