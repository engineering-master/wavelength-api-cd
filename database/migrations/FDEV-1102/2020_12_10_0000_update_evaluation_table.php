<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Evaluation;

class UpdateEvaluationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
         //Evaluations
         $evaluations = Evaluation::where('assigned_to',1387)->get();
         foreach($evaluations as $eval){
             $evaluation = Evaluation::where('id', $eval->id)->first();
             $evaluation->assigned_to = 1378;
             $evaluation->save();
         }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
