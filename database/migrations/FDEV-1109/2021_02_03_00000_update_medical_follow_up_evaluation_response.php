<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Output\ConsoleOutput;

use App\Evaluation;
use App\EvaluationForm;
use App\Answer;
use App\Question;
use App\QuestionGroup;

class UpdateMedicalFollowUpEvaluationResponse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        try {
            DB::beginTransaction();
            EvaluationForm::where('id',11)->update(['type' => 2]);// Form type update

            $evaluations = Evaluation::where('evaluation_form_id', 11)->get();//Application Follow Up Quality Form
            foreach ($evaluations as $eval) {
                $score = [];
                //Compliance Score start
                $complianceData = Answer::whereIn('question_id', [517,519,520,521,523,524,526,527,529,530,531,532,534,535,536,537,538,540,541,542,543,545,546,547,548,550,551,552,553,554,555,556,558,559,560,561,563,564,565,566,567,568,569])->where('evaluation_id', $eval->id)->get();            
                $qustionsActualscore = Question::where('type','radio')->where('group_id',59)->get();
                foreach($qustionsActualscore as $qusScore){ 
                $actual_score=0;
                $qa_score = 0;                    
                    foreach($complianceData as $cmp){ 
                            if ($cmp->answer_text == 'NA') {
                                $qa_score += 0;
                                $actual_score += 0;
                            }
                            else if ($cmp->answer_text == 'No' || $cmp->answer_text == 'Yes') {
                                $actual_score += $qusScore->actual_score;
                                if ($cmp->answer_text == 'Yes') {                                    
                                    $qa_score += $qusScore->actual_score;
                               }
                               else {
                                    $qa_score += 0;
                               }
                        }
                    }
                }
                $complianceScore =0;
                if($qa_score > 0){
                    $complianceScore = ($qa_score/$actual_score)*100; 
                }//Compliance score End
                //Customer exp start
                $custExpData = Answer::whereIn('question_id', [571,572,573,574,575,576,577,578,579,580,581,582])->where('evaluation_id', $eval->id)->get();
                $custExpDataqustionsActualscore = Question::where('type','radio')->where('group_id',60)->get();
                foreach($custExpDataqustionsActualscore as $custExpqusScore){ 
                $cust_actual_score=0;
                $cust_qa_score = 0;                    
                    foreach($custExpData as $custExp){ 
                            if ($custExp->answer_text == 'NA') {
                                $cust_qa_score += 0;
                                $cust_actual_score += 0;
                            }
                            else if ($custExp->answer_text == 'No' || $custExp->answer_text == 'Yes') {
                                $cust_actual_score += $custExpqusScore->actual_score;
                                if ($custExp->answer_text == 'Yes') {                                    
                                    $cust_qa_score += $custExpqusScore->actual_score;
                            }
                            else {
                                    $cust_qa_score += 0;
                            }
                        }
                    }
                }
                $custExpScore = 0;
                if($cust_qa_score > 0){
                    $custExpScore =  ($cust_qa_score/$cust_actual_score)*100; 
                }  
                $final_score =  (($complianceScore * 0.5)  +  ($custExpScore * 0.5));
                //Customer exp end
                $autoFail = Answer::whereIn('question_id', [583,584,585,586,587,588,589,590])->where('evaluation_id', $eval->id)->where('answer_text', 'Yes')->count();

                if($autoFail>0){
                    $autoFailScore = $autoFail;                    
                    $final_score = 0;
                }else{
                    $autoFailScore = 0;
                }
                $score = array(
                    '0' => array('groupname' => 'COMPLIANCE', 'groupid' => 59, 'obscore' => $complianceScore),
                    '1' => array('groupname' => 'CUSTOMER EXPERIENCE', 'groupid' => 60, 'obscore' => $custExpScore),
                    '2' => array('groupname' => 'AUTO-FAILURE', 'groupid' => 61, 'obscore' => $autoFailScore),
                );                
                $evalData = Evaluation::find($eval->id);
                $groupScore = json_encode($score);
                $evalData->group_score = $groupScore;  
                $evalData->final_score = round($final_score,2);                   
                $evalData->save();
            }
            DB::commit(); 
        } catch (\Exception $e) {
            DB::rollback();
            dd($e->getMessage());
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
