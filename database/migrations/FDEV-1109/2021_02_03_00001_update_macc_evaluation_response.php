<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Output\ConsoleOutput;

use App\Evaluation;
use App\EvaluationForm;
use App\Answer;
use App\Question;
use App\QuestionGroup;

class UpdateMaccEvaluationResponse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        try {
            DB::beginTransaction();            
            EvaluationForm::where('id',12)->update(['type' => 2]);// Form type update
           
            $evaluations = Evaluation::where('evaluation_form_id', 12)->get();//MACC Quality Form
            foreach ($evaluations as $eval) {
                $score = [];
                //Compliance Score start
                $complianceData = Answer::whereIn('question_id', [592,593,595,597,598,599,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,625,626,627,629,630,631,632,633,634])->where('evaluation_id', $eval->id)->get();            
                $qustionsActualscore = Question::where('type','radio')->where('group_id',62)->get();
                foreach($qustionsActualscore as $qusScore){ 
                $actual_score=0;
                $qa_score = 0;                    
                    foreach($complianceData as $cmp){ 
                            if ($cmp->answer_text == 'NA') {
                                $qa_score += 0;
                                $actual_score += 0;
                            }
                            else if ($cmp->answer_text == 'No' || $cmp->answer_text == 'Yes') {
                                $actual_score += $qusScore->actual_score;
                                if ($cmp->answer_text == 'Yes') {                                    
                                     $qa_score += $qusScore->actual_score;
                                }
                                else {
                                     $qa_score += 0;
                                }
                        }
                    }
                }
                $complianceScore =0;
                if($qa_score > 0){
                    $complianceScore = ($qa_score/$actual_score)*100; 
                }//Compliance score End
                //Customer exp start
                $custExpData = Answer::whereIn('question_id', [636,637,638,639,640,641,642,643,644,645,646,647,648,649])->where('evaluation_id', $eval->id)->get();
                $custExpDataqustionsActualscore = Question::where('type','radio')->where('group_id',63)->get();
                foreach($custExpDataqustionsActualscore as $custExpqusScore){ 
                $cust_actual_score=0;
                $cust_qa_score = 0;                    
                    foreach($custExpData as $custExp){ 
                        if ($custExp->answer_text == 'NA') {
                            $cust_qa_score += 0;
                            $cust_actual_score += 0;
                        }
                        else if ($custExp->answer_text == 'No' || $custExp->answer_text == 'Yes') {
                            $cust_actual_score += $custExpqusScore->actual_score;;
                            if ($custExp->answer_text == 'Yes') {                                    
                                    $cust_qa_score += $custExpqusScore->actual_score;
                            }
                            else {
                                    $cust_qa_score += 0;
                            }
                        }
                    }
                }
                $custExpScore = 0;
                if($cust_qa_score > 0){
                    $custExpScore =  ($cust_qa_score/$cust_actual_score)*100; 
                }                
                $final_score =  (($complianceScore * 0.5)  +  ($custExpScore * 0.5));
                //Customer exp end
                $autoFail = Answer::whereIn('question_id', [651,652,653,654,655,656,657])->where('evaluation_id', $eval->id)->where('answer_text', 'Yes')->count();
                if($autoFail>0){
                    $autoFailScore = $autoFail;
                    $final_score = 0;
                }else{
                    $autoFailScore = 0;
                }
                $score = array(
                    '0' => array('groupname' => 'COMPLIANCE', 'groupid' => 62, 'obscore' => $complianceScore),
                    '1' => array('groupname' => 'CUSTOMER EXPERIENCE', 'groupid' => 63, 'obscore' => $custExpScore),
                    '2' => array('groupname' => 'AUTO-FAILURE', 'groupid' => 64, 'obscore' => $autoFailScore),
                );
                $evalData = Evaluation::find($eval->id);
                $groupScore = json_encode($score);
                $evalData->group_score = $groupScore;
                $evalData->final_score = round($final_score,2);          
                $evalData->save();
            }
            DB::commit(); 
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
