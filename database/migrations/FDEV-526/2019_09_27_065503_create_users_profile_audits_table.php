<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersProfileAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_profile_audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('prev_role_id')->nullable();
            $table->integer('change_role_id')->nullable();
            $table->string('prev_email')->nullable();
            $table->string('change_email')->nullable();
            $table->string('prev_unite')->nullable();
            $table->string('change_unite')->nullable();
            $table->string('status')->nullable(); //Accept,Reject
            $table->dateTime('updated_at')->nullable(); 
            $table->string('updated_by')->nullable(); 
            $table->string('created_by')->nullable();
            $table->dateTime('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_profile_audits');
    }
}
