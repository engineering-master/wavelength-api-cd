<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Log;
use App\SurveyResponse;
use App\Call;

class UpdateCallIdOnSurveyResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $emptyCalls = $moreCalls = [];
        $surveyResponses = SurveyResponse::all();
        foreach ($surveyResponses as $surveyResponse) {
            $call = Call::select('ucid')->where('unique_id', $surveyResponse->call_id);
            $callCount = $call->count();
            if ($callCount == 1) {
                $callDetail = $call->first();
                $surveyResponse->call_id = $callDetail->ucid;
                $surveyResponse->save();
            } else if ($callCount > 1) {
                $moreCalls[] = [
                    'id' => $surveyResponse->id,
                    'call_id' => $surveyResponse->call_id
                ];
            } else if (empty($callCount)) {
                $emptyCalls[] = [
                    'id' => $surveyResponse->id,
                    'call_id' => $surveyResponse->call_id
                ];
            }
        }
        Log::Info("************Empty Call************");
        Log::Info(print_r($emptyCalls, true));
        Log::Info("************More than one Call************");
        Log::Info(print_r($moreCalls, true));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
