<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_question_id')->unsigned()->nullable();
            $table->integer('parent_question_answer_expression')->unsigned()->nullable();
            $table->integer('group_id')->unsigned();
            $table->longText('title');
            $table->mediumText('description')->nullable();
            $table->string('type', 25)->nullable();
            $table->integer('order')->nullable();
            $table->boolean('mandatory')->default(false);
            $table->text('values')->nullable();
            $table->text('rawvalues')->nullable();
            $table->text('score')->signed()->nullable();
            $table->decimal('actual_score', 6,4)->nullable();
            $table->tinyinteger('comment')->nullable();
            $table->tinyinteger('commentmandatory')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('group_id')->references('id')->on('question_groups');
            $table->foreign('parent_question_id')->references('id')->on('questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
