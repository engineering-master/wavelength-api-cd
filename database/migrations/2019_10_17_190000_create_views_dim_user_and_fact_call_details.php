<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
#use DB;

class CreateViewsDimUserAndFactCallDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       //wavelength-cd: Create dim_user view
        $procedure_name = "dim_user";
        $procedure = "
        CREATE 
            ALGORITHM = UNDEFINED 
        VIEW $procedure_name AS
        SELECT 
            `dw`.`dim_user`.`id` AS `id`,
            `dw`.`dim_user`.`user_id` AS `user_id`,
            `dw`.`dim_user`.`vendor` AS `vendor`,
            `dw`.`dim_user`.`from_date` AS `from_date`,
            `dw`.`dim_user`.`to_date` AS `to_date`,
            `dw`.`dim_user`.`name` AS `name`,
            `dw`.`dim_user`.`email` AS `email`,
            `dw`.`dim_user`.`supervisor_user_id` AS `supervisor_user_id`,
            `dw`.`dim_user`.`status` AS `status`,
            `dw`.`dim_user`.`job_title` AS `job_title`,
            `dw`.`dim_user`.`department` AS `department`,
            `dw`.`dim_user`.`location` AS `location`,
            `dw`.`dim_user`.`hire_date` AS `hire_date`,
            `dw`.`dim_user`.`termination_date` AS `termination_date`,
            `dw`.`dim_user`.`country` AS `country`,
            `dw`.`dim_user`.`usr_grp` AS `usr_grp`
        FROM
            `dw`.`dim_user`;";
        //DB::unprepared("DROP procedure IF EXISTS $procedure_name;");
        DB::unprepared($procedure);

        //wavelength-cd: Create fact_call_details view
        $procedure_name = "fact_call_details";
        $procedure = "
        CREATE 
            ALGORITHM = UNDEFINED 
        VIEW $procedure_name AS
        SELECT 
            `dw`.`fact_call_details`.`source_pbx` AS `source_pbx`,
            `dw`.`fact_call_details`.`ucid` AS `ucid`,
            `dw`.`fact_call_details`.`unique_id` AS `unique_id`,
            `dw`.`fact_call_details`.`segment` AS `segment`,
            `dw`.`fact_call_details`.`call_date` AS `call_date`,
            `dw`.`fact_call_details`.`segstart` AS `segstart`,
            `dw`.`fact_call_details`.`segstop` AS `segstop`,
            `dw`.`fact_call_details`.`agent_id` AS `agent_id`,
            `dw`.`fact_call_details`.`extension` AS `extension`,
            `dw`.`fact_call_details`.`duration` AS `duration`,
            `dw`.`fact_call_details`.`queuetime` AS `queuetime`,
            `dw`.`fact_call_details`.`ringtime` AS `ringtime`,
            `dw`.`fact_call_details`.`talktime` AS `talktime`,
            `dw`.`fact_call_details`.`holdtime` AS `holdtime`,
            `dw`.`fact_call_details`.`acwtime` AS `acwtime`,
            `dw`.`fact_call_details`.`skill_campaign` AS `skill_campaign`,
            `dw`.`fact_call_details`.`call_disp` AS `call_disp`,
            `dw`.`fact_call_details`.`direction` AS `direction`,
            `dw`.`fact_call_details`.`calling_pty` AS `calling_pty`,
            `dw`.`fact_call_details`.`outbound_cid` AS `outbound_cid`,
            `dw`.`fact_call_details`.`dialed_num` AS `dialed_num`,
            `dw`.`fact_call_details`.`conference` AS `conference`,
            `dw`.`fact_call_details`.`consulttime` AS `consulttime`,
            `dw`.`fact_call_details`.`da_queued` AS `da_queued`,
            `dw`.`fact_call_details`.`held` AS `held`,
            `dw`.`fact_call_details`.`transferred` AS `transferred`,
            `dw`.`fact_call_details`.`holdabn` AS `holdabn`,
            `dw`.`fact_call_details`.`origlogin` AS `origlogin`,
            `dw`.`fact_call_details`.`agt_released` AS `agt_released`,
            `dw`.`fact_call_details`.`lead_id` AS `lead_id`,
            `dw`.`fact_call_details`.`list_id` AS `list_id`,
            `dw`.`fact_call_details`.`disposition` AS `disposition`,
            `dw`.`fact_call_details`.`called_count` AS `called_count`,
            `dw`.`fact_call_details`.`server_ip` AS `server_ip`,
            `dw`.`fact_call_details`.`dispivector` AS `dispivector`,
            `dw`.`fact_call_details`.`recordingfileurl` AS `recordingfileurl`,
            `dw`.`fact_call_details`.`screenfileurl` AS `screenfileurl`,
            `dw`.`fact_call_details`.`client` AS `client`,
            `dw`.`fact_call_details`.`external_id` AS `external_id`,
            `dw`.`fact_call_details`.`usr_grp` AS `usr_grp`,
            CASE
                WHEN `dw`.`fact_call_details`.`direction` = 'OUTBOUND' THEN `dw`.`fact_call_details`.`dialed_num`
                WHEN `dw`.`fact_call_details`.`direction` = 'MANUAL' THEN `dw`.`fact_call_details`.`dialed_num`
                WHEN `dw`.`fact_call_details`.`direction` = 'INBOUND' THEN `dw`.`fact_call_details`.`calling_pty`
            END AS `customer_num`
        FROM
            `dw`.`fact_call_details`;";
        //DB::unprepared("DROP procedure IF EXISTS $procedure_name;");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
