<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Evaluation;
use App\Call;
class  UpdateStatusIdStringToIdInEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $all_status = array
        (
            "Assigned"              => 1,
            "In Progress"           => 2,
            "Yet to be acknowledged"=> 3,
            "Acknowledged"          => 4,
            "Disputed"              => 5,
            "Dispute Accepted"      => 6,
            "In Reevaluation"        => 7,
            "Reevaluated"           => 8,
            "Dispute Rejected"      => 9,
            "Closed"                => 10,
        );
        foreach($all_status as $key=>$status_id)
        {
            $all_status[strtolower($key)] = $status_id;
            $key = preg_replace("/[^A-Za-z]+/", "", $key);
            $all_status[strtolower($key)] = $status_id;
        }
        $evaluationsResponses = Evaluation::all();
        foreach ($evaluationsResponses as $evaluationResponse) {
            $evaluationResponse->status_id = preg_replace("/[^A-Za-z]+/", "", $evaluationResponse->status_id);
            $evaluationResponse->status_id = strtolower($evaluationResponse->status_id);
            if (isset($all_status[$evaluationResponse->status_id])) {
                $evaluationResponse->status_id = $all_status[$evaluationResponse->status_id];
                $evaluationResponse->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
