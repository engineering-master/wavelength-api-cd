<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SurveyResponse;
use App\Call;
use App\Employee;

class AlterHeadersOnSurveyResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $surveyResponses = SurveyResponse::all();
        foreach ($surveyResponses as $surveyResponse) {
            $headers = json_decode($surveyResponse->headers);
            foreach($headers as $header) {
                if ($header->name == 'Call ID') {
                    $header->name = 'Phone Number';
                    $call = Call::select('customer_num')->where('unique_id', $surveyResponse->call_id)->first();
                    $header->value = '';
                    if ($call) {
                        $header->value = $call->customer_num;
                    }
                }
            }
            $headers = json_encode($headers);
            $surveyResponse->headers = $headers;
            $surveyResponse->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
