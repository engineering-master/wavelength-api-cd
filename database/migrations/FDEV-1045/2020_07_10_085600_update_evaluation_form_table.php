<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\EvaluationForm;

class UpdateEvaluationFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $evaluationForm = EvaluationForm::get();
        foreach($evaluationForm as $FORM){
            $eval_form = EvaluationForm::where('id',$FORM->id)->first();
            $eval_form->type =1;
            $eval_form->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
