<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\Employee;
use App\NonEmployeeUser;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
        $appusers = User::whereIn('dim_user_id',['EC0017','EC0028','EC5044','EC5049','EC5057','EC5073','EC5077','EC5090','EC6547','EC6563',
                                                'EC6566','EC6567','EC6636','EC6640','EC6652','EC6653','EC6656','EC6661','EC6667','EC6673',
                                                'EC6674','EC6678','EC6679','EC6680','EC6681','EC6683','EC6690'])->get();

         foreach($appusers as $user){
             $wa_app_user = User::where('dim_user_id',$user->dim_user_id)->first();
             if($wa_app_user) {
                $wa_app_user->suerpvisorid = 'ECTL7001';
                $wa_app_user->save();
             }
         }
         
        $NonEmployeeUser = NonEmployeeUser::whereIn('username',['EC0017','EC0028','EC5044','EC5049','EC5057','EC5073','EC5077','EC5090','EC6547','EC6563',
        'EC6566','EC6567','EC6636','EC6640','EC6652','EC6653','EC6656','EC6661','EC6667','EC6673','EC6674','EC6678','EC6679','EC6680','EC6681','EC6683','EC6690'])->get();

         foreach($NonEmployeeUser as $user){
            $nonEmployeeUser = NonEmployeeUser::where('username',$user->username)->first();     
            if($nonEmployeeUser) {
               $nonEmployeeUser->manager_id = 2168; 
               $nonEmployeeUser->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
