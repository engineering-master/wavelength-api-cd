<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CampaignTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'evaluation_form_campaign_mappings', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('form_id')->unsigned();
                $table->string('type', 10);
                $table->integer('type_id');
                $table->integer('created_by')->unsigned()->nullable();
                $table->integer('updated_by')->unsigned()->nullable();
                $table->timestamps();
                $table->foreign('form_id')->references('id')->on('evaluation_forms');
                $table->foreign('created_by')->references('user_id')->on('users');
                $table->foreign('updated_by')->references('user_id')->on('users');
                       
            }
        );

        Schema::create(
            'campaign_groups', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 50);
                $table->string('description', 100)->nullable();
                $table->integer('created_by')->unsigned()->nullable();
                $table->integer('updated_by')->unsigned()->nullable();
                $table->timestamps();
                $table->foreign('updated_by')->references('user_id')->on('users');
                $table->foreign('created_by')->references('user_id')->on('users');
            }
        );

        Schema::create(
            'campaign_skills', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 50);
                $table->string('description', 100)->nullable();
                $table->integer('group_id')->unsigned();
                $table->integer('created_by')->unsigned()->nullable();
                $table->integer('updated_by')->unsigned()->nullable();
                $table->timestamps();
                $table->foreign('group_id')->references('id')->on('campaign_groups');
                $table->foreign('created_by')->references('user_id')->on('users'); 
                $table->foreign('updated_by')->references('user_id')->on('users');
            }
        );

        Schema::disableForeignKeyConstraints();
        Schema::table(
            'evaluations', function (Blueprint $table) {
                $table->integer('skill_campaign_id')->unsigned()->nullable()->after('status_id');
                $table->foreign('skill_campaign_id')->references('id')->on('campaign_skills');
            }
        );
        Schema::enableForeignKeyConstraints();

        Schema::table(
            'dispositions', function (Blueprint $table) {
                $table->string('skill_campaign', 100)->after('disposition');
                $table->dropColumn(['usr_grp']);
            }
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('evaluation_form_campaign_mappings');
        Schema::disableForeignKeyConstraints();
        Schema::table(
            'evaluations', function (Blueprint $table) {
                $table->dropForeign(['skill_campaign_id']);
                $table->dropColumn(['skill_campaign_id']);
            }
        );
        Schema::enableForeignKeyConstraints();

        Schema::drop('campaign_skills');
        Schema::drop('campaign_groups');

        Schema::table(
            'dispositions', function (Blueprint $table) {
                $table->string('usr_grp', 100)->after('disposition');
                $table->dropColumn(['skill_campaign']);
            }
        );

    }
}
