<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SurveyResponse;
use App\Call;

class UpdateNameDispositionNumberOnSurveyResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_responses', function (Blueprint $table) {
            $table->string('agent_name')->nullable();
            $table->string('disposition')->nullable();
            $table->string('phone_number')->nullable();
        });
        $surveyResponses = SurveyResponse::all();
        foreach ($surveyResponses as $surveyResponse) {
            $headers = json_decode($surveyResponse->headers);
            foreach($headers as $header) {
                if ($header->name == 'Phone Number') {
                    $surveyResponse->phone_number = $header->value;
                }
                if ($header->name == 'Disposition') {
                    $surveyResponse->disposition = $header->value;
                }
                if ($header->name == 'Agent Name') {
                    $surveyResponse->agent_name = $header->value;
                }
            }
            $surveyResponse->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
