<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SurveyResponse;
use App\Call;
class  UpdateAcknowledgeStatusOnSurveyResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $surveyResponses = SurveyResponse::all();
        foreach ($surveyResponses as $surveyResponse) {
                if ($surveyResponse->acknowledge_status == "pending") {
                    $surveyResponse->acknowledge_status ="Yet to be acknowledged";
                }
            $surveyResponse->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
