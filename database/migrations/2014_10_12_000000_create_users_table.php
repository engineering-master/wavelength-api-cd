<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::defaultStringLength(191);
        Schema::create('users', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->index('user_id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->integer('eid')->nullable();
            $table->integer('suerpvisorid')->nullable();
            $table->string('unite')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('user_id')->on('users');
            $table->timestamps();
            $table->softDeletes();            
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
