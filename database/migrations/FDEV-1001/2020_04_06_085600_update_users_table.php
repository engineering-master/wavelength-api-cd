<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\UserSystemMapping;
use App\NonEmployeeUser;
use App\NonEmployeeUserSystemMapping;
use App\EvaluationStatusAudit;
use App\EvaluationDispositions;
use App\Evaluation;
use App\Comment;
use App\GroupUser;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
         //Evaluations
         $evaluations = Evaluation::where('created_by',1108)->get();
         foreach($evaluations as $eval){
             $evaluation = Evaluation::where('id', $eval->id)->first();
             $evaluation->created_by = 3168;
             $evaluation->save();
         }
        //Evaluation status audit
        $evaluationStatusAudit = EvaluationStatusAudit::where('created_by',1108)->get();
        foreach($evaluationStatusAudit as $evalstatus){
            $evaluationstatus = EvaluationStatusAudit::where('id', $evalstatus->id)->first();
            $evaluationstatus->created_by = 3168;
            $evaluationstatus->save();
        }
        //Evaluation Dispostion
        $evaluationdispositions = EvaluationDispositions::where('created_by',1108)->get();
        foreach($evaluationdispositions as $disposition){
            $disposition = EvaluationDispositions::where('id', $disposition->id)->first();
            $disposition->created_by = 3168;
            $disposition->save();
        }

        //Evaluation Comment
        $evaluationComment = Comment::where('created_by',1108)->get();
        foreach($evaluationComment as $comment){
            $comment = Comment::where('id', $comment->id)->first();
            $comment->created_by = 3168;
            $comment->save();
        }

        // Application
        UserSystemMapping::where('user_id',1108)->delete();
        User::where('user_id',1108)->delete();

        //Auth portal
        NonEmployeeUserSystemMapping::where('user_id',1108)->delete();
        GroupUser::where('user_id',1108)->delete();
        NonEmployeeUser::where('id',1108)->delete();     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
