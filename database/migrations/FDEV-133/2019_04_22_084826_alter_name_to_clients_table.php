<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Client;
use App\Employee;

class AlterNameToClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $employees = Employee::select('vendor')->groupBy('vendor')->pluck('vendor');
        foreach($employees as $employee) {
            $client = Client::where('client_name', $employee)->first();
            $data = [];
            if (!$client) {
                $client = new Client;
                $client->client_name = $employee;
                $client->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
