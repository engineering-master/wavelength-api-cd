<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SurveyResponse;
use App\Call;
use App\Employee;

class AlterHeaderToSurveyResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $surveyResponses = SurveyResponse::all();
        foreach ($surveyResponses as $surveyResponse) {
            $sites = $dispositions = "Not Found";
            $headers = json_decode($surveyResponse->headers);
            foreach($headers as $header) {
                if ($header->name == 'Team Manager') {
                    $header->name = 'Supervisor';
                }
                if ($header->name == 'Site') {
                    $sites = "Found";
                }
                if ($header->name == 'Disposition') {
                    $dispositions = "Found";
                }
            }
            if ($sites == "Not Found" || $dispositions = "Not Found") {
                $site = $disposition = [];
                $calldetails = Call::select('agent_id', 'disposition')->where('unique_id', $surveyResponse->call_id)->first();
                if ($calldetails) {
                    if ($sites == "Not Found") {
                        $employee = Employee::where('user_id', $calldetails->agent_id)->first();
                        if ($employee) {
                            $site[] = [
                                'name' => "Site",
                                'value' => $employee->vendor,
                                'type' => "text",
                                'disabled' => true
                            ];
                            $headers = array_merge($headers, $site);
                        }
                    }
                    if ($dispositions == "Not Found") {
                        $disposition[] = [
                            'name' => "Disposition",
                            'value' => $calldetails->disposition,
                            'type' => "text",
                            'disabled' => true
                        ];
                        $headers = array_merge($headers, $disposition);
                    }
                }
            }
            $headers = json_encode($headers);
            $surveyResponse->headers = $headers;
            $surveyResponse->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
