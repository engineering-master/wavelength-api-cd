<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Output\ConsoleOutput;

use App\SurveyResponse;

class AlterSurveyResponseGroupScore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $output = new ConsoleOutput();
        $surveys = DB::table('survey_responses')->where('survey_id', 1)->get();
        foreach ($surveys as $survey) {
            $score = [];
            $complianceData = DB::table('answers')->whereIn('question_id', [200,201,202,203,204,205,206,207,208,209,210,211,212,213])->where('survey_response_id', $survey->id)->pluck('value')->toArray();
            $custExpData = DB::table('answers')->whereIn('question_id', [214,215,216,217,218,219,220,221,222,223,224,225,226,227])->where('survey_response_id', $survey->id)->pluck('value')->toArray();
            $complianceScore = array_sum($complianceData);
            $custExpScore = array_sum($custExpData);
            $autoFail = DB::table('answers')->whereIn('question_id', [228,229,230,231,232,233])->where('survey_response_id', $survey->id)->where('answer_text', 'Yes')->count();
            if($autoFail>0){
                $autoFailScore = $autoFail;
            }else{
                $autoFailScore = 0;
            }
            $score = array(
                '0' => array('groupname' => 'COMPLIANCE', 'groupid' => 25, 'obscore' => $complianceScore),
                '1' => array('groupname' => 'CUSTOMER EXPERIENCE', 'groupid' => 26, 'obscore' => $custExpScore),
                '2' => array('groupname' => 'AUTO-FAILURE', 'groupid' => 27, 'obscore' => $autoFailScore),
            );
           $surveyData = SurveyResponse::find($survey->id);
           $groupScore = json_encode($score);
           $surveyData->group_score = $groupScore;
           
            $surveyData->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
