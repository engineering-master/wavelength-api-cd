<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Evaluation;

class  UpdateFinalScoreDefaultNullTo0InEvaluationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Update existing evaluations.final_score value from null to 0 
       $evaluationsResponses = Evaluation::whereNull('final_score')->get();
        foreach ($evaluationsResponses as $evaluationResponse) {
            $evaluationResponse->final_score = 0;
            $evaluationResponse->save();
        }

        //Alter evaluations.final_score schema's default value null to 0 
        Schema::table('evaluations', function (Blueprint $table) {
            $table->decimal('final_score', 5, 2)->default('0')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
