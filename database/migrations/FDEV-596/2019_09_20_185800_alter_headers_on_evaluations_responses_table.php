<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Evaluation;

class AlterHeadersOnEvaluationsResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ini_set('memory_limit', -1);
        $evaluationsResponses = Evaluation::all();
        foreach ($evaluationsResponses as $evaluationResponse) {
            $evaluation = Evaluation::select("users.user_id", "users.name")->leftJoin("users", "users.user_id", "=", "evaluations.assigned_to")->where("id", $evaluationResponse->id)->first();
            $headers = json_decode($evaluationResponse->headers, true);
            $headers[] = [
                "name"      => "Evaluated By",
                "value"     => $evaluation->name ? $evaluation->name : "",
                "type"      => "text",
                "disabled"  => true
            ];
            $evaluationResponse->headers = json_encode($headers);
            $evaluationResponse->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
