<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
use App\NonEmployeeUser;
use App\NonEmployeeUserSystemMapping;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    { 
       $NonEmployeeUser = NonEmployeeUser::whereIn('username',['DC80024','DC80027','DC80030','DC80031','DC80033','DC80035','DC80036','DC80037','DC80038','DC80039','DC80040','DC80041'])->get();

         foreach($NonEmployeeUser as $user){
            $supervisor = NonEmployeeUserSystemMapping::where('user_id',$user->manager_id)->first(); 
            if($supervisor) {
                $wa_app_user = User::where('dim_user_id',$user->username)->first();
                    if($wa_app_user) {
                        $wa_app_user->suerpvisorid = $supervisor->system_user_id;
                        $wa_app_user->save();
                    }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
    }
}
