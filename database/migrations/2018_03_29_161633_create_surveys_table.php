<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::defaultStringLength(191);
    Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('title')->nullable();
            $table->longText('description')->nullable();
            $table->longText('headers')->nullable();
            $table->longText('ratings')->nullable();
            $table->string('version')->nullable();
            $table->boolean('active')->default(false);
            $table->integer('created_by')->unsigned()->nullable();
            $table->string('type')->nullable();
            $table->foreign('created_by')->references('user_id')->on('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }

}
