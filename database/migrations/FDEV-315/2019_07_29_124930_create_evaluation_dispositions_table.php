<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationDispositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_dispositions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('evaluations_id')->unsigned();
            $table->integer('evaluation_disposition_definitions_id')->unsigned();
            $table->integer('severity_id');
            $table->integer('created_by')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });
        Schema::table('evaluation_dispositions', function($table) {
            $table->foreign('evaluations_id')->references('id')->on('survey_responses');
            $table->foreign('evaluation_disposition_definitions_id','evaluation_disposition_definitions_id_foreign')->references('id')->on('evaluation_disposition_definitions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_dispositions');
    }
}
