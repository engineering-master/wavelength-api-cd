<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
#use DB;

class AlterStructureToAddMissingFieldsUsersEvaluationFormsEvaluationsPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //wavelength-cd.users: Add missing fields & update datatypes
        Schema::table('users', function (Blueprint $table) {
            $table->string('suerpvisorid', 45)->change();
            $table->string('dim_user_id', 100)->after('user_id');
            $table->string('site', 45)->after('suerpvisorid');
            $table->string('usr_grp', 100)->after('deleted_at');
        });
        
        Schema::table('evaluation_forms', function (Blueprint $table) {
            $table->string('usr_grp', 100)->after('updated_at');
        });
        
        Schema::table('evaluations', function (Blueprint $table) {
            $table->string('agent_id', 100)->change();
            $table->text('headers')->change();
            $table->integer('tag_id')->nullable()->after('rawvalues');
        });
        
        Schema::table('permissions', function (Blueprint $table) {
            $table->string('page', 50)->after('name');
            $table->integer('mandatory')->after('page');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
