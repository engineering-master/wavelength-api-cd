<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_workflows', function (Blueprint $table) { 
            $table->increments('id');
            $table->string('description', 100);
            $table->integer('status');
            $table->date('start_date');
            $table->text('wf_details');
            $table->integer('created_by')->unsigned()->nullable();
            $table->foreign('created_by')->references('user_id')->on('users');
            $table->integer('updated_by')->unsigned()->nullable();
            $table->foreign('updated_by')->references('user_id')->on('users');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('evaluation_forms', function (Blueprint $table) {
            $table->integer('workflow_id')->after('version')->unsigned()->nullable();
        });
        Schema::table('evaluation_forms', function (Blueprint $table) {
            $table->foreign('workflow_id')->references('id')->on('evaluation_workflows');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_workflows');
    }
}
