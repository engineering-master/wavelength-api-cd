<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Evaluation;

class AlterEvaluationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('surveys', 'evaluation_forms');
        Schema::rename('survey_responses', 'evaluations');
        //Survey status updated
        $evaluations = Evaluation::all();
        foreach ($evaluations as $evaluation) {
            $status = $evaluation->status;
            if ($status == 'submitted') {
                $evaluation->status = $evaluation->acknowledge_status;
            }
            $evaluation->save();
        }
        
        Schema::table('evaluations', function (Blueprint $table) {
            $table->renameColumn('survey_id','evaluation_form_id');
            $table->renameColumn('employee_id','agent_id');
            $table->renameColumn('status','status_id');
        });  
        Schema::table('evaluations', function (Blueprint $table) {
            $table->string('assigned_to')->after('agent_id');
            $table->string('allow_dispute')->after('status_id')->default('True');
            $table->integer('predecessor_id')->after('evaluation_form_id')->nullable();
            $table->dropColumn(['acknowledged_by', 'acknowledge_status', 'comment', 'comment_created']);
        }); 
        Schema::table('question_groups', function (Blueprint $table) {
            $table->dropForeign(['survey_id']);
        });
        
        Schema::table('question_groups', function (Blueprint $table) {
            $table->renameColumn('survey_id','evaluation_form_id');
        });
        Schema::table('question_groups', function (Blueprint $table) {
            $table->foreign('evaluation_form_id')->references('id')->on('evaluation_forms');
        });
        Schema::table('answers', function (Blueprint $table) {
            $table->renameColumn('survey_response_id','evaluation_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    
    }
}
