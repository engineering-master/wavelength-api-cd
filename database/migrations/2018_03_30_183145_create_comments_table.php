<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function(Blueprint $table)
        {
          $table->increments('id');
          $table->text('body')->nullable();
          $table->boolean('keycommentindicator')->nullable();
          $table->boolean('clientvisable')->nullable();
          $table->string('commentable_id');
          $table->string('commentable_type');
          $table->integer('created_by')->unsigned()->nullable();
          $table->foreign('created_by')->references('user_id')->on('users');
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
