<?php
/*
 * This file is part of ENUM Types.
 *
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'spstopic' => [
      [
        'Billing Accuracy',
        'Brand Competitors',
        'Brand Loyalty'
      ]
    ],
    'callsearchkey' => [
      ['name' => 'is equal to', 'value' => 'equal'],
      ['name' => 'is not equal to', 'value' => 'notequal'],
      ['name' => 'contains', 'value' => 'like']
    ],
    'calldirections' => [
      'Inbound',
      'Inbound Direct',
      'Outbound',
      'Outbound Direct'
    ],
    'countsearchkey' => [
      ['name' => 'All Calls', 'value' => 'All'],
      ['name' => '% of Calls', 'value' => 'Percentage'],
      ['name' => 'Number of Calls', 'value' => 'Number']
    ],
    'allow_entity_to_process' => [
      'allow_score' => [
        ['Assigned', 'In Progress', 'In Reevaluation'],
      ],
      'allow_comment'=>[
        [
        'Yet to be Acknowledged',
        'Acknowledged','Disputed',
        'Dispute Accepted',
        'Dispute Rejected',
        'In Reevaluation',
        'Reevaluated',
        'Closed'
        ]
        ],
        'allow_evaluation_disposition' => [
          ['Yet to be acknowledged',
           'Reevaluated']
        ],
        'allow_revaluation_assignment' => [
          ['In Reevaluation']
        ],
    ],
    'comment_type'=>[
      'evaluation',
      // 'answer'
    ],
    'evaluation_status' => [
      ['id'=>'1', 'name'=>'Assigned'],
      ['id'=>'2', 'name'=>'In Progress'],
      ['id'=>'3', 'name'=>'Yet to be acknowledged'],
      ['id'=>'4', 'name'=>'Acknowledged'],
      ['id'=>'5', 'name'=>'Disputed'],
      ['id'=>'7', 'name'=>'Dispute Rejected'],
      ['id'=>'8', 'name'=>'In Reevaluation'],
      ['id'=>'9', 'name'=>'Reevaluated'],
      ['id'=>'10', 'name'=>'Closed'],
    ],
    'status_list' => [
      ['id'=>'1', 'name'=>'Assigned'],
      ['id'=>'2', 'name'=>'In Progress'],
      ['id'=>'3', 'name'=>'Yet to be acknowledged'],
      ['id'=>'4', 'name'=>'Acknowledged'],
      ['id'=>'5', 'name'=>'Disputed'],
      ['id'=>'6', 'name'=>'Dispute Accepted'],
      ['id'=>'7', 'name'=>'Dispute Rejected'],
      ['id'=>'8', 'name'=>'In Reevaluation'],
      ['id'=>'9', 'name'=>'Reevaluated'],
      ['id'=>'10', 'name'=>'Closed'],
    ],
    'trigger_notification' => [
      '3' => ["6", "2", "8"],//'QA Manager, Agent and Team manager',
      '4' => ["2", "6", "7"],//'Agent, QA Manager, QA',
      '5' => ["6", "7", "2"],//'QA Manager, QA', Agent',
      '8' => ["7", "8", "2"],//'QA, Team manager', Agent',
      '7' => ["7", "8", "2"],//'QA, Team manager, Agent',
      '10' => ["7", "6", "8"],//'QA QA Manager, Team manager',
      '9' => ["6", "8", "2"], //'QA manager, Team manager', Agent',
    ],
    'dependent_fields' => [
      [
        "field"=>"customer_num",
        "field_alias"=>"Phone Number",
        "campaign_types"=>[],
        "campaign_ids"=>[1,2,3,4,5,6,7,8,9,10,12,13,14,15]
      ],
      [
        "field"=>"lead_id",
        "field_alias"=>"Case Number",
        "campaign_types"=>[],
        "campaign_ids"=>[10,11,13]
      ],
      [
        "field"=>"unique_id",
        "field_alias"=>"MRR Number",
        "campaign_types"=>[],
        "campaign_ids"=>[10,11,13]
      ],
      [
        "field"=>"state",
        "field_alias"=>"State",
        "campaign_types"=>[],
        "campaign_ids"=>[12]
      ]
    ]

];
