<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get(
    '/v1.1/login',
    function () {
    }
)->middleware('auth.jwt');
Route::post('/v1.1/auth/token', 'AuthController@auth');
Route::post('/v1.1/request/token', 'AuthController@getToken');
Route::post('/v1.1/refresh/token', 'AuthController@refreshToken');
Route::post('/v1.1/sendRocketMessage', 'UserController@sendRocketMessage');
Route::get('/v1.1/permissions', 'RolePermissionController@getPermissions');
Route::get('/v1.1/roles/{role_id?}', 'RolePermissionController@getRoles');
Route::post('/v1.1/role/permissions', 'RolePermissionController@assignPermissions');
Route::post('/v1.1/user/role', 'RolePermissionController@assignRole');
Route::post('/v1.1/listusers', 'UserController@listUsers');
// All routes must go thorugh this middleware - For SSO
Route::group(
    ['prefix' => 'v1.1' ,'middleware' => ['auth.jwt']],
    function () {
       
        Route::get('me', 'UserController@userBasicInformation');
        Route::get('enums', 'EnumController@index');
        Route::post('calls', 'CallSearchControlller@getCallSearch');
        Route::get('/comments/{type}/{id}', 'CommentController@show');
        Route::post('/{type}/{id}/comment', 'CommentController@comment');

        Route::get('evaluationforms', 'EvaluationFormController@EvaluationFormsListGet');
        Route::post('evaluationform', 'EvaluationFormController@EvaluationFormCreate');
        Route::get('evaluationform/{form_id}', 'EvaluationFormController@EvaluationFormGet');

        Route::get('evaluationform/{id}/questions', 'EvaluationFormController@getEvaluationFormQuestions');
        Route::get('evaluationform', 'EvaluationFormController@getAllEvaluationForms');
        Route::post('evaluation/{eval_id}/transition/{id}', 'EvaluationController@evaluationTransition');
        Route::put('evaluation/{eval_id}/score', 'EvaluationController@evaluationScore');
        Route::post('evaluation/{eval_id}/assign', 'EvaluationController@assignQASpecialist');
        Route::post('call/{call_id}/evaluation', 'EvaluationController@acceptEvaluation');
        Route::put('evaluation/{eval_id}/allowdispute', 'EvaluationController@allowDispute');
        Route::post('evaluations/search', 'EvaluationSearchController@evaluationSearch');
        Route::get('evaluations/{eval_id}', 'EvaluationSearchController@getEvaluations');
        Route::get('call/{call_id}/evaluations', 'EvaluationSearchController@getCallsEvaluations');
        Route::resource('/evaluation/disposition', 'EvaluationDispositionController');
        Route::post('evaluation/{eval_id}/disposition', 'EvaluationController@evaluationDisposition');
        
        Route::get('jwtparser', 'UserController@jwtParser');
        
        Route::get('sites', 'SiteController@index');
        Route::post('sites/agent', 'SiteController@getAgent');

        Route::resource('workflow', 'WorkflowController');
        Route::get('users', 'UserController@getusers');
        Route::post('user/profile', 'ProfileSettingsController@profileSettings');
        Route::post('user/profile/{id}', 'ProfileSettingsController@updateProfile');
       
        Route::put('notification/{id}/markasread', 'NotificationController@markAsRead');
        Route::put('notifications/markallasread', 'NotificationController@markAllAsRead');
        Route::put('notifications/{ids}/markselectedasread', 'NotificationController@markSelectedAsRead');
        Route::get('notifications/{sort_type}', 'NotificationController@getNotifications');
        Route::get('notification/{id}', 'NotificationController@getNotification');
       
        Route::group(
            ['prefix' => 'campaign'], function () {
                Route::get('groups', 'CampaignGroupController@index');
                Route::post('group', 'CampaignGroupController@store');
                Route::put('group/{id}', 'CampaignGroupController@update');
                Route::get('skills', 'CampaignSkillController@index');
                Route::post('group/{group_id}/skill', 'CampaignSkillController@store');
                Route::put('skill/{id}', 'CampaignSkillController@update');
                Route::post('{type}/{type_id}/form/{id}', 'CampaignGroupController@mapGroupOrSkills');
                Route::get('group/{group_ids}/skills', 'CampaignGroupController@getCampaignSkill');
                Route::get('{type}/{type_ids}/forms', 'CampaignGroupController@getCampaignForm');
                Route::get('{type}/{type_ids}/disposition', 'CampaignSkillController@getSkillDisposition');
            }
        );

        /* Unused Endpoints */
        /*
        Route::post('surveyres', 'EvaluationController@surveyres');
        Route::get('clients', 'EnumController@getClients');
        Route::get('callcount', 'CallSearchControlller@callcount');
        Route::get('call/{id}', 'CallSearchControlller@getCall');
        Route::get('callform', 'CallSearchControlller@getCallform');
        Route::post('comment', 'CommentController@store');
        Route::get('evaluationforms', 'EvaluationFormController@getEvaluationForms');
        Route::get('evaluationform', 'EvaluationFormController@getAllEvaluationForms');
        Route::get('evaluationform/{id}', 'EvaluationFormController@getOneEvaluationForm');
        Route::post('evaluationform', 'EvaluationFormController@createEvaluationForm');
        Route::get('evaluation/agentslist', 'EvaluationController@agentslist');
        Route::post('evaluation/calls', 'EvaluationController@evaluationSearch');
        Route::get('evaluation/{eval_id}', 'EvaluationController@showEvaluation');
        Route::get('evaluationsearch', 'EvaluationController@evaluationSearch');
        Route::post('acknowledge', 'EvaluationController@updateAcknowledgement');
        */
    }
);
