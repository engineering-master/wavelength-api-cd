<?php
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});

/*Auth::routes();

Route::get('/home', 'CallSearchControlller@index')->name('home');
Route::get('/dashboard', 'CallSearchControlller@search');
Route::get('/dashboard/datatable', 'CallSearchControlller@datatable');
Route::get('/dashboard/result', 'CallSearchControlller@result');
Route::post('/dashboard/call', 'CallSearchControlller@view_calls');
Route::get('/dashboard/get_agents', 'CallSearchControlller@get_agents');
Route::get('/dashboard/customercall', 'CallSearchControlller@customercalldatatable');
Route::get('/dashboard/agentcall', 'CallSearchControlller@agentcalldatatable');
Route::get('surveys', 'SurveyController@show');
Route::post('surveys/create', 'SurveyController@store');
Route::resource('comments','CommentController');
Route::post('comments/updatecomment','CommentController@updatecomment');
*/
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
